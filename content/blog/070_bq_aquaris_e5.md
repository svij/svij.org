---
title: "Review bq Aquaris E5 - Ubuntu Edition"
categories: ["Ubuntu"]
date: "2015-07-11 15:45:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

<a href="/graphics/e5_e45_2.webp" data-lightbox="image" data-title="E5 and E4.5">
    <img src="/graphics/e5_e45_2.webp" width="250" style="float: right; margin:20px;">
</a>

The second Ubuntu Phone on the market is also the second phone from the spanish manufacturer bq. The Aquaris E5 is available to buy since a couple of weeks, it's the bigger brother of the E4.5, which was the first Ubuntu Phone.

### Software

The bq Aquaris E5 runs the same software as the E4.5 and the Meizu MX4. I've covered the software features on my review of the Aquaris E4.5. Like my review of the [Meizu MX4](/blog/2015/07/05/review-meizu-mx4-ubuntu-edition/), this review will not cover all the software features. Instead, I will only mention the software features which are associated with the hardware of the E5 itself in the upcoming sections.

### Hardware

<a href="/graphics/e5_ubuntuphone_geschwister.webp" data-lightbox="image" data-title="All three Ubuntu Phones">
    <img src="/graphics/e5_ubuntuphone_geschwister.webp" width="250" style="float: left; margin:20px;">
</a>

The Aquaris E5 is powered by a Quad Core MediaTek CPU with a Frequency of 1.3 Ghz. The GPU has a frequency of 500 Mhz. The size of the RAM is 1 GB.

It is exactly the same CPU, GPU and RAM which are also built in to the E4.5. You'll notice the overall speed is pretty much the same on both devices. The startup time of apps are still pretty slow, with roughly five seconds. You'll also notice that the system is sometimes laggy and stuttering, especially when you switch between scopes. Anyway, the speed inside of apps are good.

The device is 8,65mm thin and therefore thinner than a centimeter. It is also slightly thinner than the E4.5. The bigger display, with a size of 5", results in a length of 14,2cm and a width of 7,1cm. Like on the E4.5, the bezels are pretty huge, because of the non-existing Android-Buttons, which are present on the Android-Version of the E5. It has a similar size as the Meizu MX4, but it is a few centimeters slimmer.

The design is exactly the same as on the E4.5. You'll find a micro-USB Port on the bottom, next to the speakers. On the left side, you have both dual-SIM slots and on the right side there are the volume- and power-buttons. On the top, you can insert the microSD card, with a size of up to 32 GB.

#### The Screen

<a href="/graphics/e5_vorderseite.webp" data-lightbox="image" data-title="The front side">
    <img src="/graphics/e5_vorderseite.webp" width="250" style="float: right; margin:20px;">
</a>

Like the name already suggests, the E5 has a 5" screen with a resolution of 720 x 1280 pixels. This is a pixel density of 294 ppi. The E4.5 on the other side has only a 4.5" screen with a resolution of 540 x 960 and a pixel density of 240 ppi. You directly notice the difference between those screens. The higher resolution results into sharper images, icons and fonts. The screen isn't as good as on the Meizu MX4, but still not bad at all.

#### The Camera

<a href="/graphics/e5_kamera_reinoldikirche2.webp" data-lightbox="image" data-title="Church in Dortmund">
    <img src="/graphics/e5_kamera_reinoldikirche2.webp" width="250" style="float: left; margin:20px;">
</a>

<a href="/graphics/e5_dortmunderU.webp" data-lightbox="image" data-title="The Dortmunder U">
    <img src="/graphics/e5_dortmunderU.webp" width="250" style="float: right; margin:20px;">
</a>

This phone has two cameras, one on the front and one on the backside. The front camera takes photos with 5 Megapixels, the camera on the back with 13 megapixels.

The photos are slightly better than on the E4.5, but they're still washy and the reproduction isn't that much better. You can use is for a few simple photos anyway.

#### Battery life

The battery life is pretty good! On a „normal“ usage, you can easily use the phone for two days. This is a similar battery life as on the E4.5, which has become pretty good after a couple of software updates.

#### Haptics and Quality

The overall finish is good, especially for a device which goes for 200€. There are not many differences in the build quality compared to the E4.5. The backside is made of hard plastic, which is a bit slippery, but still feels better in your hand than most other smartphones with plastic backcovers. The frame around the display is also made of plastic, which may easily results in scratches.

### Conclusion

The bq Aquaris E5 is solid device. On the software side there aren't any big issues. Also the  hardware of the E4.5 and E5 is so similar that one is as good os the other when it comes to running the installed software.

Anyway the bq Aquaris E5 is still a device for Ubuntu enthisiasts. You'll a get a pretty good device for 200€. The price is only 30€ higher compared to the E4.5, but includes a bigger and better display.
