---
title: Git-Repositorys Hosten mit Gitea
categories: ["Git"]
date: "2017-04-08T21:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg01.met.vgwort.de/na/a1933654ec744241936cd07ad988ca74"
cover:
  image: "/graphics/gitea_repoview.webp"
---

Um Git-Repositorys zu Hosten gibt es heutzutage sehr viele verschiedene Möglichkeiten.
Die einfachste und wohl bekannteste Lösung für viele Open-Source-Projekte ist die Nutzung von GitHub.
GitHub bietet auch das Hosten von privaten, nicht öffentlichen, Repositorys an,
doch ist dies kostenpflichtig. GitHub ist zudem nicht Open Source und man kann
es nicht selbst Hosten. Eine Alternative ist GitLab, welches sowohl als Service
zum kostenfreien Nutzen verfügbar ist, als auch Open Source ist. Auf GitLab.com
kann man im Gegensatz zu GitHub seinen Code in privaten Repositorys schieben, ohne
bezahlen zu müssen. Da GitLab Open Source Software ist, lässt sich die Community Edition
auch auf eigener Hardware selbst hosten. Allerdings ist GitLab vergleichsweise
fett und braucht selbst mit wenigen Nutzern recht viel Arbeitsspeicher, hat dafür aber eine Vielzahl an
Funktionen. Für Firmen und anderen Projekte dürfte GitLab wohl eine gute Wahl sein,
doch ist es für das Hosten von privaten Repositorys für einzelne Personen eher fett, da man die meisten
Features wohl eh nicht braucht. Eine weitere Alternative ist Gogs, bzw. dessen neuer
Fork namens Gitea. Gitea ist ein recht neuer Fork von Gogs, was wiederum ein quasi-Klon
von GitHub ist. Wobei sich der Ausdruck „Klon“ mehr auf das Aussehen und die
grundlegenden Funktionen richtet und somit weniger auf den darunter liegenden
Code.

<a href="/graphics/gitea_repoview.webp" data-lightbox="image">
  <img src="/graphics/gitea_repoview.webp" style="margin:10px;">
</a>

Gitea ist in der Programmiersprache Go geschrieben und ist ein Git-Server der
auch eine Web-Oberfläche daherkommt. Die grundlegenden Features sind dabei ähnlich
zu GitHub und GitLab. So können über die Web-Oberfläche Nutzer angelegt werden,
die dann Git-Repositorys anlegen können. Dabei bietet es auch die Möglichkeit Organisationen
anzulegen, um Repositorys gruppenbezogen anzulegen und zu Verwalten. In diesen
Organisationen können dann noch Teams mit verschiedenen einfachen Berechtigungen
für verschiedene Aufgaben angelegt werden.

Neben dem reinen Hosten von Git-Repositorys gibt es für die Projekte einen Issue-Tracker
sowie die Möglichkeit Pull-Requests zu erstellen. Die Web-UI beinhaltet weiterhin
die gängigen Funktionen von GitHub-ähnlichen Portalen: Die Anzeige von Branches,
Commits und der Historie aller getätigten Änderungen. Ebenfalls ist eine Wiki-Komponente
enthalten.

Wie zuvor schon erwähnt ist Gitea ein Fork von Gogs. Letzteres gibt es schon länger,
hat aber über die Zeit ein wesentliches Problem: Es gibt nur einen Maintainer
und dieser ist in der Vergangenheit häufiger mal für ein paar Monate abwesend gewesen.
Andere Leute hatten leider keine Commit-Rechte, weshalb das Projekt einige Monate
inaktiv war, obwohl Contributor da waren, die mitentwickeln wollten. Auch Bug-Reports
und Bug-Fixes blieben dadurch teilweise Monate liegen. Die Contributions-Grafik
des [Maintainers](https://github.com/Unknwon) zeigt dieses Problem auch deutlich
dar. Da keine Änderung in Sicht ist und Maintainer auch kein Interesse hat andere
Co-Contributor in das Team aufzunehmen, wurde ein Fork gestartet, was unter dem
Namen Gitea läuft. Der Entwickler von Gogs äußert sich dazu aber positiv, da dies
die Stärke von Open Source Software darstellt und es die gewählte MIT-Lizenz auch
zulässt.

### Installation

Die Installation von Gitea ist recht simpel. Es gibt aktuell noch keine Pakete
für Linux-Distributionen, die es bei Gogs hingegen gibt. Die händische Installation
von Gitea ist allerdings recht simpel. Die folgende Anleitung geht davon aus,
das ein frisches System vorliegt, sei es in einer virtuellen Maschine oder in
einem System-Container.

Es bietet sich an einen `git` System-Nutzer anzulegen, über dem die Kommunikation
über SSH verläuft, wenn man mit Git hinterher arbeitet. Um Gitea auszuführen, muss
man lediglich eine einzelne Binärdatei herunterladen und ausführen. Unter [dl.gitea.org/gitea](https://dl.gitea.io/gitea/)
finden sich die aktuellen Ordner mit den Veröffentlichungen. Aktuell ist beim
Schreiben dieses Artikels die Version 1.1.0. Die Versionsnummern sind dort
etwas irritierend, deshalb lohnt sich ein Blick in die [Releases](https://github.com/go-gitea/gitea/releases),
um die aktuellste Versionsnummer zu ermitteln.

Nachdem Herunterladen muss die Datei noch ausführbar gemacht werden:

    $ wget -O gitea https://dl.gitea.io/gitea/1.1.0/gitea-1.1.0-linux-amd64
    $ chmod +x gitea

Anschließend kann die Datei ausgeführt werden, um die erste Konfiguration durchzuführen:

    $ ./gitea web

Im Web-Browser kann man nun die IP-Adresse des Systems aufrufen, um Gitea über
das Web zu konfigurieren. Die Web-Oberfläche läuft im Standard allerdings auf
Port 3000. Gitea braucht eine Datenbank, die bei der
Installation konfiguriert werden muss. Diese muss man sich zuvor dementsprechend
installieren und die Zugangsdaten bei der Installation angeben.

Damit man anschließend Gitea nicht über die Kommandozeile immer starten muss, sollte
man einen Service anlegen. Vorlagen dafür liegen im [`contrib`](https://github.com/go-gitea/gitea/tree/master/contrib)
Verzeichnis des Gitea-Repositorys. Ein passendes systemd Unit-File findet sich
unter [`contrib/systemd/gitea.service`](https://github.com/go-gitea/gitea/blob/master/contrib/systemd/gitea.service)
bei dem nur wenige Dinge geändert werden müssen. Bei einem Upgrade muss nur noch
das Binary ersetzt werden und der Dienst neugestartet werden.

Weitere Konfiguration kann über die Ini-Datei erfolgen, die unter `custom/conf/app.ini` liegt. Der Pfad ist dabei abhängig vom Installationsort. Wenn man die
Installation als `git` Systemnutzer durchgeführt hat und das Binary im Home-Directory liegt, dann liegen alle relevanten Daten demnach im Home-Verzeichnis des
Git-Systemnutzers.

### Systemstatus

Gitea ist sehr leicht und sparsam, was den Ressourcenverbrauch angeht. Folgende
Daten liefert der System Monitor Status, nachdem der Server in diesem Fall mit
nur einem Benutzer läuft.

<a href="/graphics/gitea-systemmonitor.webp" data-lightbox="image">
  <img src="/graphics/gitea-systemmonitor.webp" style="margin:10px;">
</a>

Dies ist einer der wesentlichen Gründe, weshalb sich der Einsatz von Gitea
vor allem bei kleinen Projekten lohnt. Im Gegensatz zu GitLab ist Gitea und auch Gogs klein
und leicht, lässt sich einfach warten und hat zudem eine einfache Web-Oberfläche
mit diversen Funktionen, die auch GitHub hat, um Repositorys einfach zu Hosten.
Zuvor nutzte ich [gitolite](http://gitolite.com/gitolite/index.html), was aber manchmal etwas anstrengender zu nutzen war,
da man für viele Kleinigkeiten ständig mit Konfigurationsdateien hantieren muss,
etwa dann, wenn man ein Repository anlegen will. Da ist die Konfiguration mittels
einer Web-Oberfläche deutlich komfortabler.

### Fazit

Gitea bietet viele Features die auch in anderen Git-Hosting-Servern verfügbar
sind. Durch den geringen Ressourcenverbrauch bietet es sich besonders für private
Repositorys und kleine Projekte an. Abstriche muss man hingegen trotzdem an den
ein oder anderen Stelle machen, was aber ziemlich normal ist. So sind die Berechtigungsstrukturen
für Organisationen eher einfach umgesetzt und auch an einigen anderen Stellen
müssen wohl Abstriche gemacht werden. Für diese gibt es aber weiterhin GitLab,
was sich größeren Projekten oder Firmen schon besser eignet.

Auf der Projektseite auf [docs.gitea.io](https://docs.gitea.io/en-us/) findet sich
die Dokumentation. Darunter auch mehr Informationen zum Funktionsumfang, dem Upgrade
von Gogs, und alternative Installationsmöglichkeiten.
