---
title: Persönlicher Erfahrungsbericht über das HTC Flyer
categories: ["Android"]
date: "2012-01-22T00:15:00+01:00"
slug: "personlicher-erfahrungsbericht-uber-das-htc-flyer"
toc: true
---

Seit nun einigen wenigen Wochen besitze ich das HTC Flyer, welches ich für knapp 266€ erworben habe.

Vor dem Kauf stellte ich mir die Frage, welche Größe mein Tablet haben sollte, ich schwankte zwischen allen verfügbaren Größen von 7" bis 10". Durch das gute Angebot wurde es letztendlich doch das 7" große HTC Flyer.

Das Flyer ist mit einer Single Core CPU ausgestattet, die mit 1,5GHz getaktet ist, zudem besitzt einen Gigabyte an Arbeitsspeicher. Weitere technischen Spezifikationen findet man auf der Herstellerseite von HTC. Ich besitze das Gerät ohne UMTS, also das reine WLAN-Gerät und 16GB Speicher.

Ausgeliefert wurde das Flyer mit Android in Version 2.3.4 mit einer speziell für das Flyer angepasste Sense UI. Als nettes Zusatz–„Addon” liegt ein batteriebetriebener Stift bei, mit denen sich Notizen anlegen lassen und Screenshots erstellen lassen, die man hinterher bemalen und beschriften konnte. Bei Android 2.3.4 war auch nur dieses möglich. Für weitere Dinge war der Stift nicht zu gebrauchen.

Vorletzte Woche kam dann ein Update raus, so dass endlich Andriod 3.2.1 Honeycomb ausgelieferrt wurde. Angekündigt war Honeycomb für das Tablet bereits Mitte letzten Jahres und es sollte nach Plan auch noch 2011 rauskommen, was jedoch knapp verpasst wurde. Durch die neue Version wurden die Hardwaretasten deaktiviert, die Tasten "Home", "Zurück" und den "Einstellungen" Button sind seitdem komplett funktionslos, da sie mit der Bar am unterem Rand des Bildschirmes ersetzt worden sind. Zu dem wurde auch die Funktionalität des Stiftes geändert. Dieser lässt sich nun netterweise systemweit nutzen, und somit für mehr Dinge gebrauchen, als nur für das Kritzeln und Schreiben.

![Homescreen](/graphics/homescreen.webp)

Selbst habe ich das Schreiben auf dem Tablet nur kurz und rudimentär ausprobiert. Das Problem an der ganzen Sache ist, dass das Tablet zum einen relativ dick und hart ist, verglichen zu Papier. Außerdem muss man sich schon dran gewöhnen nicht mehr auf Papier zu schreiben, sondern auf Glas. Für den Schreiber fühlt sich das völlig anders an und man hat gelegentlich das Gefühl, dass man „wegrutscht”. Obwohl man die dicke des Stiftes individuell anpassen kann ist der Platz auf einen 7" Display eher begrenzt. Es passen nur wenige Worte nebenbeinander hin, abhängig natürlich von der persönlichen Schriftgröße.
Auch die Sense Oberfläche wurde mit dem Update auf 3.2 runderneuert. Persönlich gefällt sie mir besser als die vorherige Sense Version für Tablets unter Android 2.3 Gingerbread.

Ich selbst nutze das Tablet hauptsächlich für das Lesen von Skripten aus den Vorlesungen der Universität. Durch die Größe von 7" ist es dadurch teilweise sogar ziemlich gut, da man so im Bus eine gute Größe hat, um anständig lesen zu können. Auch ist es gut zu gebrauchen um Fotos zu betrachten, besonders dann, wenn mehrere Leute in der Nähe sind, die gerne einen Blick drauf werfen wollen. Verglichen zu einem Laptop ist da zwar die Bildschirmgröße deutlich kleiner, aber es lässt sich viel einfacher und handlicher rumgeben als einen Laptop.

Die Foto und Videoqualität ist eher passabel. Bei guten Lichtverhältnissen ist es hingegen noch recht gut brauchbar. Zum Test habe ich unser Aquarium gefilmt und auf [YouTube](http://www.youtube.com/watch?feature=player_embedded&v=-APVOlnY02E) hochgeladen.

Im Vergleich zu anderen 7" Tablets die billigen Preissegment zwischen 100 und 200€ liegen, liegen die Vorteile beim Flyer besonders bei der Verarbeitung und der Auflösung des Displays. Ich hatte eher wenig Interesse ein Tablet zu kaufen, welches deutlich über 300€ kostet, da es mir für den Preis dann doch nicht sonderlich Wert war. Mit einer Auflösung von 1024x600 Pixeln lässt sich das Tablet recht gut verwenden und auch die Leistung ist gut. Einen Nachteil, dass das Flyer nur ein Single-Core CPU hat, spüre ich bisher noch nicht sonderlich. Bereut habe ich den Kauf bisher noch nicht sonderlich.

