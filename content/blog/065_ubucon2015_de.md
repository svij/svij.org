---
title: "Ubucon 2015 in Berlin"
categories: ["Ubuntu"]
date: "2015-05-24 23:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Vor wenigen Stunden haben wir die mittlerweile neunte deutsche Ubucon
[angekündigt](http://ubucon.de/2015/community-in-touch). Ich selbst bin seit
der Ubucon 2013 in Heidelberg im Organisationsteam. Dieses Jahr bin ich erstmals
Hauptorganisator. Im Idealfall verteile ich also erfolgreich die Aufgaben an die
anderen Leute im Organisationsteam, in der Realität sieht das ein wenig anders aus. ;-)

Die Ubucon findet dieses Jahr in Berlin an der [HWTK](http://www.hwtk.de/) vom
23. bis 25. Oktober 2015 statt. Am gleichen Ort wurde bereits die Ubucon im
Jahr 2012 veranstaltet. Auch der [„Call for Papers“](http://ubucon.de/2015/cfp)
wurde zeitgleich gestartet.
