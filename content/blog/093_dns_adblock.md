---
title: "Adblocking auf DNS-Ebene mit dnsmasq"
categories: ["Server"]
date: "2016-12-31 17:20:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Auf meinem [Homeserver](https://svij.org/blog/2016/04/24/homeserver-migration-auf-ubuntu-1604/#homeserver-migration-auf-ubuntu-1604) laufen einige Dienste die für das
Heimnetzwerk wichtig sind. Das relevante ist dabei der DHCP- und DNS-Server.
Um den Homeserver als DHCP- und DNS-Server zu verwenden, nutze ich [dnsmasq](https://wiki.ubuntuusers.de/Dnsmasq/).

Wie der Titel schon verrät, geht es hier um Adblocking auf DNS-Ebene. Die Anfragen
an bekannte Ad- und Tracking-Server sollen bereits über das DNS verhindert werden.
Konkret heißt das, dass wenn eine Domain eines bekannten Werbeanbieters aufgerufen
wird, diese auf `127.0.0.1` auflöst. Der wesentliche Vorteil ist, dass das Adblocking
somit nicht mehr in jedem Browser auf jedem Gerät im Netzwerk konfiguriert werden
muss. Während sich dies auf die normalen Rechner mit Desktop-Betriebssystemen
nicht sonderlich auswirkt, hat das einen großen Vorteil bei mobilen Geräten. So
bekommen alle Apps und Browser im Heimnetzwerk einen Adblocker. Nebenbei blocken
einige Adblocker-Blocker einen nicht mehr, wenn man so ein Setting fährt...

Auf der Seite [pgl.yoyo.org/adservers/serverlist.php](http://pgl.yoyo.org/adservers/serverlist.php) gibt es eine Adserver Liste die regelmäßig aktualisiert wird. Auf
der [Hauptseite](http://pgl.yoyo.org/adservers/) gibt es auch ein Formular, um
die Liste im passenden Format für den eingesetzten DNS-Server zu bekommen. Für
dnsmasq ist es etwa dann [folgender Link](http://pgl.yoyo.org/adservers/serverlist.php?hostformat=dnsmasq&showintro=0&mimetype=plaintext). Der Inhalt der Datei muss
anschließend nur noch nach `/etc/dnsmasq.d-available/adblock.conf` geschoben werden,
gefolgt von einem Symlink von `/etc/dnsmasq.d/adblock.conf` auf die entsprechende
Datei. Da die Liste regelmäßig aktualisiert wird, kann man zusätzlich noch ein
Cronjob oder systemd Timer anlegen, damit die Liste regelmäßig heruntergeladen
wird. Nach einem Neustart von dnsmasq ist dann der Filter auch schon aktiv.
Wer einen solchen Filter auch außerhalb von zuhause nutzen möchte, der kann sich
zusätzlich einen VPN installieren und konfigurieren, um den Traffic durch das Heimnetz
zu tunneln. Mit [pi-hole](https://pi-hole.net/) gibt es auch eine (fast) fertige
Lösung, um das DNS-Adblocking mit Hilfe eines Raspberry Pis umsetzen zu können. Getestet
habe ich das allerdings nicht.
