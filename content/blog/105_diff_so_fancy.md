---
title: Schönere Git Diffs im Terminal mit diff-so-fancy
categories: ["Git", "Kurztipps"]
date: "2018-01-12T19:45:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
slug: "schonere-git-diffs-im-terminal-mit-diff-so-fancy"
vgwort: "https://vg09.met.vgwort.de/na/18b3538948c948549401528a8de3ff6d"
---

Es sind manchmal die kleinen Dinge, die einem im Terminal noch glücklicher machen,
für Probleme, die man vorher gar nicht hatte. Ein Beispiel ist etwa das Anzeigen von
Unterschieden zwischen zwei Versionen mit einer Datei mit `diff` bzw. in diesem
Fall ein einfaches `git diff`.

Mit `diff-so-fancy` lässt sich das Git Diff spürbar aufhübschen, sodass es besser
für den Menschen lesbar ist. Wie so häufig sagen Bilder mehr als viele Worte,
weshalb hier ein Screenshot folgt, wo das Standard Diff verwendet wird:

![Diff not so fancy](/graphics/diff-not-so-fancy.webp)

Und hier eins mit `diff-so-fancy`:

![Diff so fancy](/graphics/diff-so-fancy.webp)

Es sind zwar nur viele Kleinigkeiten die dadurch schöner und besser ersichtlich sind,
doch das reicht schon, um einen besseren Überblick über seine Änderungen zu
bekommen. Ich selbst bearbeite Dateien fast nur auf der Kommandozeile und schaue
auch die Diffs dort an – seien es die gerade bearbeiteten Dateien oder auch das
Diff aus der Git Historie.

Das Git-Repository des Projektes findet sich auf [GitHub/so-fancy/diff-so-fance](https://github.com/so-fancy/diff-so-fancy).
Die Installation kann entweder über NPM, Nix oder brew erfolgen. Schönerweise
gibt es für ArchLinux ein Paket, weshalb dort die Installation besonders einfach ist.
Aber auch sonst ist es lediglich ein einfaches Script, was auf dem
Dateisystem ohne Paketverwaltung problemlos irgendwo abgelegt werden kann.

Ich nutze nahezu die Standard-Einstellungen, dessen Einrichtung in der
[README#Usage](https://github.com/so-fancy/diff-so-fancy#usage) zu finden sind.
Einzig `markEmptyLines` habe ich explizit abgeschaltet. Die Einstellungen landen
lediglich in der `~/.gitconfig`.

Danke an [chris34](https://c3vo.de/) für den Tipp.
