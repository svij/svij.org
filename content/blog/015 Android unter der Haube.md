---
title: "Android unter der Haube"
categories: ["Betriebssystem"]
tags: ["Web", "Mobile"]
date: "2013-06-15T9:30:00+02:00"
toc: true
---

Mittlerweile gibt es eine größere Anzahl von Betriebssystemen und Plattformen
für mobile Endgeräte wie Smartphones und Tablets. Viele davon setzen davon auf
einen Linux-Kernel. Android ist dabei die weltweit verbreitetste Plattform.
Dieser Artikel geht grundsätzlich auf das Betriebssystem ein und somit weniger
auf dessen Bedienung.

Android gehört mit über [900 Millionen](http://www.heise.de/mobil/meldung/900-Millionen-Android-Geraete-aktiviert-1863837.html)
aktivierten Geräten, zu dem am weitest
verbreiteten Betriebssystem auf mobilen Endgeräten. Offiziell wird Android von 
der Open Handset Alliance entwickelt und voran getrieben.

## Geschichtliches

Die Firma Android wurde ursprünglich von Andy Rubin im Jahr 2003 gegründet. Im
Jahr 2005 erfolgte die Übernahme durch Google. 2007 fand die Gründung
der [„Open Handset Alliance“](http://www.openhandsetalliance.com/) statt, welche 
offiziell die Entwickler von Android ist. Die Open Handset Alliance ist ein
Zusammenschluss einer Vielzahl von Firmen wie Netzbetreibern, Software-Firmen,
Marketing-Unternehmen, Firmen aus der Halbleiterindustrie sowie diversen Geräte-Herstellern.
Dieses Konsortium wird von Google geleitet, welches die Entwicklung von Android
maßgeblich steuert und vorantreibt.

Die Android Version 1.0 wurde im September 2008 veröffentlicht. Die
aktuellste Version ist 4.2.2 „Jelly Bean“, welche im November 2012 veröffentlicht
wurde.

### Android Open Source Project

Der offizielle Name des Open-Source-Projekts von Android lautet 
[„Android Open Source Project“](http://source.android.com/).
Hierbei handelt es sich um „nacktes“ Android, welches von der Open Handset 
Alliance bzw. von Google entwickelt wird. Geräte-Hersteller sowie Interessierte
finden dort die Quellen von Android sowie Anleitungen,
wie der Code kompiliert und portiert werden kann.

## Systemarchitektur

Android nutzt nicht einen gewöhnlichen Linux-Kernel sondern eine relativ
stark modifizierte Version. Google hat dabei den Kernel soweit
angepasst, dass dieser besser auf eingebetteten Geräten lauffähig ist, da der
Standard-Linux-Kernel zu einer zu hohen Last führen würde.

Ursprünglich setzte man beim Kernel auf einen Version 2.6.
Mittlerweile werden neuere angepasste Linux-Kernel verwendet, die bei
vielen Geräten unterschiedlich sind. Beim Nexus 4 sowie beim Nexus 10 ist es 
beispielsweise der Linux-Kernel 3.4. Unter den Anpassungen an dem
Kernel fallen Faktoren wie ein optimiertes Energie-Management
oder die Ersetzung und Veränderung von verwendeten Treibern und Bibliotheken.

Der unter Android verwendete Linux-Kernel enthält dabei auch nur
die Treiber, die für das entsprechende Gerät notwendig sind. Darunter fallen
Treiber für Kamera, WLAN, Audio, Flashspeicher und für weitere vorhandene
Hardware-Schnittstellen sowie einem speziellen Binder IPC. 

Auf dem Linux-Kernel aufsetzend finden sich diverse Bibliotheken, darunter unter
anderem SSL, SQLite, WebKit und einige weitere. Der Android-Kernel
setzt im Gegensatz zum Linux-Kernel dabei nicht auf die C-Bibliothek [Glibc](https://de.wikipedia.org/wiki/Glibc)
sondern auf Googles Eigenentwicklung [Bionic libc](https://en.wikipedia.org/wiki/Bionic_%28software%29).
Bionic libc wird speziell für Android von Google entwickelt. Es ist deutlich
kleiner als Glibc, enthält allerdings deswegen auch deutlich weniger Funktionen.
Bionic hingegen ist speziell an mobile Endgeräte sowie ARM-Prozessoren angepasst.

### Android-Laufzeitumgebung

In der Android-Laufzeitumgebung befinden sich zum einen die Java Core Bibliotheken
sowie die Dalvik Virtual Machine. Die Dalvik VM ist eine virtuelle
Maschine, in der die Android Apps ausgeführt werden. Android Apps werden in der
Regel in Java geschrieben. Andere Sprachen sind auch möglich, allerdings ist
Java Androids Programmiersprache der Wahl, womit auch das Android
Framework umgesetzt ist. Der programmierte Java-Code wird zuerst
in Java Bytecode, also .class-Dateien kompiliert. Diese sind in diesem
Schritt noch kompatibel mit der Java Virtual Machine
([JVM](https://de.wikipedia.org/wiki/Java_Virtual_Machine)).
Im Anschluss folgt dann die Konvertierung von den .class-Dateien zu einer
einzelnen .dex-Datei. Diese ist nicht nur komprimiert, sondern auch nur
noch kompatibel zur Dalvik VM.

![Android Systemarchitektur](/graphics/android-systemarchicture.webp)

[Quelle: by Smieh](https://commons.wikimedia.org/wiki/File:Android-System-Architecture.svg) mit [CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/deed.en)

Die [Dalvik VM](http://sites.google.com/site/io/dalvik-vm-internals/2008-05-29-Presentation-Of-Dalvik-VM-Internals.pdf)
wurde speziell für langsame Prozessoren angepasst. Besonders zum
Entstehungszeitraum von Android waren die eingesetzten ARM-Prozessoren noch
durchaus leistungsschwach, was heute vor allem
im Hinblick auf die aktuellen High-End-Geräte nicht mehr ganz zutrifft.
Das gleiche gilt für den Arbeitsspeicher, sodass Dalvik VM
ursprünglich speziell an Geräten mit wenig Arbeitsspeicher ausgerichtet war.
Der dritte und letzte Punkt ist, dass es auf Betriebssystemen laufen soll, die
über keinen [Swap-Speicher](https://de.wikipedia.org/wiki/Swapping) verfügen.

### Application Framework

Androids Application Framework lässt sich in viele Teile gliedern. Darunter
befindet sich unter anderem der Activity-, Window-, Package- und Notification-Manager.

Jede installierte Android App läuft im System als eigenständiger Nutzer und bekommt
vom System eine eindeutige ID zugewiesen, die jedoch nicht von einer App abfragbar
ist. Das System legt für jede App dabei die Zugriffsrechte im Dateisystem fest,
sodass keine anderen Apps auf die Daten von anderen Apps zugreifen können. Eine
App läuft stets in einem einzelnen Prozess, die in einer eigenen Dalvik VM läuft.
Eine App kann dadurch nur auf seine eigenen Dateien zugreifen. Dies dient zur
Sicherheit des gesamten Systems. Mit dazu gehören auch Zugriffe auf System-Komponenten,
die standardmäßig nicht zugänglich sind. Gelockert wird dies durch Berechtigungen,
die eine App „anmelden“ muss. Diese Berechtigungen müssen dabei explizit vom 
Benutzer, der die App installiert,
abgenickt werden. Durch diesen Mechanismus, gewährt das System einer App
Zugriffe auf diverse Inhalte vom System, etwa Zugriff auf das Internet oder auf
Kontaktdaten aus dem Telefonbuch.

Eine einzelne App kann aus verschiedenen Unterkomponenten bestehen.
Es gibt Activities, Services, Content Providers sowie Broadcast Receivers.
Eine Activity ist simpel ausgedrückt ein „Bildschirm“, der graphische Bedienelemente
beinhaltet. Eine App besteht häufig aus mehreren Activities. Bei der Kontakte-App
wäre die Ansicht der Kontakte beispielsweise eine Activity, ebenso wie das Anzeigen
von Informationen zu einem einzelnen Kontakt.

Eine weitere Komponente einer App ist ein Service. Ein Service läuft stets im
Hintergrund und hat keine graphische Oberfläche. Es ist dafür gedacht, Tätigkeiten
auszuführen, die nicht zwingend an eine Activity gebunden sind, etwa das Abrufen
von E-Mails im Hintergrund oder das Abspielen von Musik.

Ein Content Provider ist ein verteilter Speicher für verschiedene Anwendungen.
Eine App kann dabei einen Content Provider zur Verfügung stellen, an dem dann
mehrere Apps ihre Dateien sowohl abrufen als auch modifizieren können. 

Ein Broadcast Receiver lauscht auf Broadcast-Nachrichten vom System. Eine
Broadcast-Nachricht ist eine Nachricht, die vom System ausgelöst wird und allen
Apps auf dem Android-Gerät zur Verfügung stehen, auf denen eine App reagieren kann.
Wenn die Kamera-App ein Foto geschossen hat, dann wird beispielsweise eine
Broadcast-Nachricht gesendet. Alternativ gibt es auch eine Broadcast-Nachricht,
wenn das System vollständig hochgefahren ist. 

Android besitzt ein interessantes Feature, um Komponenten von anderen Apps zu
starten oder auch Daten von einer App zur nächsten App zu senden. Dies nennt
sich „Intent“. Ein Intent ist eine „Absicht“, um eine bestimmte Aktion auszuführen.
Intents werden sowohl innerhalb von Apps genutzt als auch als Kommunikationsmittel
zu anderen Apps. Dadurch ergeben sich sehr gute und einfache Möglichkeiten Daten
in einer App bzw. zwischen Apps zu transportieren. Beispielsweise kann eine App
einen Button zum Hinzufügen von Kalender-Einträgen implementieren. Dieser startet
durch einen Intent die Activity der Kalendar-App. Intern erzeugt dabei das System
einen neuen Prozess für die zu startende App, sofern sie sich nicht schon im
Speicher befindet. 

## Lizenzen

Das Android Open Source Project (kurz AOSP) nutzt maßgeblich die [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).
Davon ausgenommen sind unter anderem Kernel-Patches, diese stehen unter der 
[GPLv2](http://www.gnu.org/licenses/gpl-2.0.html).
Jede Person, die an der Entwicklung von Android teilnehmen möchte, muss zuvor,
dem „Contributor License Agreement“ zustimmen.

Die Änderungen am Linux-Kernel erfolgt öffentlich, sodass man die Entwicklung
des AOSP-Kernels stets auf dem Blick haben kann. Anders sieht es wiederum mit
dem restlichen Sourcecode aus, welchen Google unter der Apache License 2.0
stellt. Dieser wird stets hinter Googles verschlossenen Türen entwickelt und
erst mit Freigabe einer neuen Version veröffentlicht. Das Verfahren wurde beim
Release von Android 3.0 „Honeycomb” allerdings eine Weile ausgesetzt, sodass
dessen Sourcecode-Veröffentlichung erst mit der Veröffentlichung von Android 4.0 erfolgte.
Google erntete aus Seiten der Open-Source-Community durch dieses Verhalten
enorme Kritik, da dies nicht für die Freiheit von Android sprach. Zu den
Kritikern gehörte unter anderem [Richard Stallman](http://www.guardian.co.uk/technology/2011/sep/19/android-free-software-stallman).

Nicht alles, was auf einem gängigen Android-Gerät standardmäßig installiert ist,
ist Open Source. Vor allem die Google Apps stehen nicht unter einer 
Open-Source-Lizenz und sind definitiv Closed Source. Darunter fallen unter anderem GMail,
Google Maps oder auch der Google Play Store samt Google Play Services. 

## Android-Derivate

Es gibt diverse Android-Derivate, die hauptsächlich als ROMs bezeichnet werden.
Der wohl bekannteste Vertreter ist dabei wohl [Cyanogenmod](http://www.cyanogenmod.org/).

Cyanogenmod ist ein Android-Derivat, welches maßgeblich von Steve Kondik, auch als
„Cyanogen“ bekannt, initiiert wurde. Mittlerweile gehört Cyanogenmod zu den am
meist genutzten Android Derivaten. Laut Aussagen des Projekts, ist es auf über
5 Millionen Geräten installiert. Die [Palette an Geräten](http://wiki.cyanogenmod.org/index.php?title=Devices),
auf denen Cyanogenmod lauffähig ist, ist riesig. Die Entwickler ergänzen nach
eigenen Ermessen diverse Funktionen und machen die Nutzung dieser Features verfügbar.
Insbesondere bietet Cyanogenmod häufig auch aktuellere Android-Versionen an, als
es standardmäßig für ein Gerät gibt. Für die Nutzer bietet Cyanogenmod und andere
ROMs die einzige Alternative, wenn Geräte von Hardware-Herstellern nicht mehr
mit neueren Android-Versionen versorgt werden.

<div class="thumbnail span3 pull-right">
<img src="/graphics/android-lockscreen_cyanogenmod.webp" />
<div class="caption">
Der Lock-Screen von Cyanogenmod.
</div>
</div>

[„Replicant“](http://replicant.us) ist ein weiteres Android-Derivat, welches
im Vergleich zu Cyanogenmod allerdings den vollen Fokus auf den Open-Source-Gedanken
setzt. Da ein Android-Gerät sehr viele verschiedene Hardware-Sensoren
und Schnittstellen besitzt, braucht jeder/jede davon ihre eigenen Treiber. Viele
Hardware-Hersteller liefern dabei die Treiber lediglich im binären Format aus,
sodass sie nicht unter einer Open-Source-Lizenz stehen. Das Projekt Replicant
ist hingegen vollständig Open Source, was ebenfalls das Ziel des Projektes ist.

Neben den oben genannten Android-Derivaten gibt es noch zahlreiche weitere. Die
Anzahl an Derivaten lässt sich kaum überblicken, da viele davon lediglich auf
einzelnen Geräten lauffähig ist.

## Google Play

Unter dem Namen „Google Play“ fasst Google seit 2012 viele seiner Dienste zusammen.
Darunter den „Google Play Store“, der bis März 2012 noch „Android Market“ hieß,
in dem der Vertrieb von Apps lief. Seitdem sind noch einige Dienste hinzugefügt
worden, etwa Google Music, Google Movies und Google Books. 

Etwas, was dem Endanwender verborgen bleibt, ist der Dienst „Google Play Services“.
Hier bündelt Google diverse Programmierschnittstellen zu Google Maps, Google+
und weiteren Google-Diensten. Entwickler können mit den Google
Play Services rund um die Google-Dienste Apps bilden, etwa die Verwendung
von Google Maps innerhalb eigener Applikationen. Die Programmierschnittstellen
aus den Google Play Services laufen auf einer breiten Masse an Android-Versionen, 
somit sind Features nicht an eine spezielle Android-Version gebunden.

Alle Dienste unter dem Dach von Google Play stehen ebenfalls nicht unter einer
Open-Source-Lizenz. Es gibt einige Apps, welche die Schnittstellen von Google
Play Services nutzen. Nachteilig ist hierbei, dass die Google Play Services
eben nur auf Geräten funktionieren, die auch die Google Play Services installiert haben.
Dies sind in der Regel alle auf dem Markt erhältlichen Android-Geräte. Eine
Ausnahme ist das Amazon Kindle Fire. Das Tablet von Amazon nutzt zwar selbst
Android, allerdings hat Amazon das System stark modifiziert und kommt ohne
den Google Play Store und somit auch ohne die Google Play Services aus.
Auch andere Android-Derivate, die
nicht die Google-Dienste installiert haben, können dann die Apps nicht nutzen,
welche die Programmierschnittstellen der Google Play Services nutzen.

Nutzer, die Apps installieren wollen, sind dabei stets auf den Google Play Store
angewiesen, wofür eine explizite Registrierung bei Google notwendig ist. Anwender,
die zwar Android nutzen möchten, allerdings freie Apps nutzen wollen, können sich
als Alternative Apps über [F-Droid](http://f-droid.org/) installieren. Dort
finden sich lediglich Apps, die unter einer Open-Source-Lizenz stehen.

## Aktualisierungen

Ein wichtiges Thema bei Android sind die Aktualisierungen der Geräte. Das
Problem hierbei ist, dass Google häufig ein oder zwei neue Android-Versionen
pro Jahr veröffentlicht, diese erreichen allerdings immer sehr spät die breite
Masse an Benutzern. Googles Nexus-Geräte sind dabei die einzigen Geräte, die
von Google direkt mit Aktualisierungen versorgt werden, und das meist schon
einige Wochen nach der Vorstellung einer neuen Version. Die Nexus-Geräte sind
dabei Googles Referenzgeräte bezüglich „nacktem“ Android, da sie ohne jegliche
Modifikationen erscheinen. Anders sieht es bei Geräten von den gängigen
Hardware-Herstellern aus. Diese statten ihre Geräte in der Regel mit einer
eigenen Oberfläche aus. Samsungs Oberfläche heißt Touchwiz, die von HTC nennt
sich Sense. Die Oberflächen greifen dabei häufig sehr tief in das Gerät ein,
sodass eine Aktualisierung auf eine neue Android-Version durchaus länger dauert.
Zudem kommen dort noch einige weitere betriebswirtschaftliche Faktoren hinzu,
weshalb eine Aktualisierung sechs Monate oder gar länger dauern kann.

Monat um Monat veröffentlicht Google eine [Statistik](http://developer.android.com/about/dashboards/index.html)
für App-Entwickler, die aussagt, welche Version wie häufig genutzt wird. Jeder
Zugriff eines Nutzers auf den Google Play Store wird von Google protokolliert,
um diese Statistik zu ermitteln. Im Juni 2013 verwendeten so lediglich 4% aller
Android-Geräte die neueste Version 4.2. Obwohl die Version bereits im
November 2012 erschien, haben viele Geräte-Hersteller noch keine Aktualisierung
ausgeliefert. Für einen Großteil der aktuell am Markt befindlichen Geräte
wird es vermutlich auch gar keine Aktualisierung auf 4.2 geben. Den größten
Anteil einer einzelnen Version macht Version 2.3 mit immer noch 36% aus.

## Kritik

Aus Open-Source-Sicht ist Android sowohl Fluch als auch Segen. Android basiert
zwar auf Open-Source-Code und steht eben auch unter einer Open-Source-Lizenz,
allerdings sind viele Dienste um Android herum weder Open Source, noch verfolgen diese
einen Open-Source-Gedanken. Zudem wird Android hinter verschlossenen Türen
entwickelt und maßgeblich von Google geführt.

Google hat es wiederum geschafft ein Open-Source-Projekt auf Millionen von 
Geräten zu bringen,
die mit einem angepassten Linux-Kernel laufen. Die Änderungen 
fließen dabei teilweise auch wieder zurück in den Kernel. Android ist für Endanwender
in der Regel nicht völlig frei, doch gibt es Möglichkeiten
ein Android-Gerät zu nutzen, welche mit sehr viel Freier Software arbeitet.
