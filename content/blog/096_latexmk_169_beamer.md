---
title: "Kurztipp: Latexmk und 16:9 LaTeX Beamer Slides"
categories: ["Kurztipps"]
date: "2017-03-18T9:40:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Manchmal sind es die kleinen Dinge, die das Arbeiten mit LaTex und in meinem
speziellen Fall LaTeX Beamer vereinfachen. Ein hilfreiches Tool ist etwa
[Latexmk](https://mg.readthedocs.io/latexmk.html). Wer häufiger an LaTeX-Dokumenten
arbeitet, kennt wohl das Problem, dass man den Code mehrfach kompilieren muss,
damit alle Cross-Referenzen aktualisiert werden. Etwa dann, wenn neue Kapitel
hinzugefügt werden, die dann im Inhaltsverzeichnis gelistet werden sollen.
Latexmk macht das ganze etwas schlauer und kompiliert das Dokument gleich
genau so oft durch, wie es nötig ist.

Das nächste Problem was Latexmk löst ist, dass man bei faktisch jeder Änderung
ein aktualisiertes Dokument haben will. Latexmk besitzt auch einen "watch" Modus
der bei jeder Änderung, und somit immer dann, wenn man eine Datei speichert, direkt
neu kompiliert.

Das ganze sieht dann so aus:

    $ latexmk -pvc main.tex

Wenn man, wie ich, XeLaTeX nutzt, dann kann man auch einfach `-xelatex` als weiteren
Parameter angeben. So muss man anschließend nur noch das Quelldokument verändern
und kann sich die Änderung ein paar Sekunden später mit dem PDF-Viewer seiner Wahl
anschauen.

### 16:9 LaTeX Beamer Folien

Heutzutage haben die meisten neueren Beamer und vor allem Fernseher eine 16:9
Auflösung. Das Standard-Format von LaTeX-Beamer Folien ist nicht 16:9, sondern 4:3.
Das Format lässt sich aber ganz einfach anpassen.

    \documentclass[aspectratio=169]{beamer}

Mit `aspectratio` kann man das Format angeben, unterstützt werden 1610, 149, 54, 43
und 32. Der Default ist 43. Man sollte bloß beachten, dass die Grafiken „richtig“
eingebunden werden sollten, sodass beim Wechsel zwischen den Formaten die Texte und Grafiken
weiter gut aussehen und nicht über den Rand schießen.
