---
title: "FirefoxOS – ein weiteres System für mobile Geräte"
categories: ["Betriebssystem"]
tags: ["Web", "Mobile"]
date: "2013-06-15T9:00:00+02:00"
slug: "firefoxos-ein-weiteres-system-fur-mobile-gerate"
toc: true
---

Ein neues offenes System auf dem Markt für Smartphones ist Firefox OS. Das System setzt dabei größtenteils auf die neuesten Web-Technologien. Im April 2013 erschienen mit dem Geeksphone Keon und Peak die ersten Entwickler-Geräte mit Firefox OS.

## Geschichtliches ##

![Der App-Launcher von FirefoxOS](/graphics/firefoxOS_applauncher.webp)

Im Juli 2011 gab es erstmal die [Ankündigung](https://groups.google.com/forum/?fromgroups=#!topic/mozilla.dev.platform/dmip1GpD5II%5B1-25-false%5D)
von Firefox OS durch Andreas Gal von der Mozilla Foundation. Damals erfolgte
die Ankündigung sowie die Entwicklung unter dem Projektnamen „Boot to Gecko“
(B2G). Das wichtigste Merkmal ist, dass es fast ausschließlich auf die neuesten
Web-Technologien aufbauen soll. Die Entwicklung soll dabei von vornherein
komplett offen stattfinden, sodass interessierte Personen stets die Entwicklung
beobachten und auch etwas dazu beisteuern können. Dies ist etwas, was 
beispielsweise bei Android
nicht möglich ist, da dies hinter verschlossenen Türen entwickelt und erst 
veröffentlicht wird, wenn eine neue Version fertig ist.

Die Apps füre Firefox OS soll dabei ebenfalls mit Web-Technologien aufgebaut sein,
sodass App-Entwickler mit HTML5, CSS3 und JavaScript Apps programmieren
und damit auf Hardware-Elemente des Gerätes zugreifen können. Es wurde zudem
betont, dass die Apps keinerlei Nachteile zu konventionellen Apps wie unter iOS
oder Android haben sollen.

## Unter der Haube ##

Unter der Haube von Firefox OS befinden sich insgesamt drei wichtige Komponenten.
Die Namen lauten Gonk, Gecko und Gaia.

### Gonk ###

Gonk ist simpel ausgedrückt die Linux-Distribution von Firefox OS, welches
nur die wesentlichen Dienste enthält. Gonk besteht aus dem Linux-Kernel und einem
Hardware Abstraction Layer ([HAL](https://de.wikipedia.org/wiki/Hardwareabstraktionsschicht)).
Der Kernel sowie einige Bibliotheken sind dabei
gängige Open-Source-Projekte, wie libusb und bluez. Einige Teile des Hardware
Abstraction Layers stammen dabei aus dem Android-Projekt, darunter unter anderem
GPS und die Kamera. Gonk ist darauf abgestimmt, dass Gecko darauf läuft. Gecko
hat dabei direkten Zugang zum vollen Telefon-Stack sowie zum Framebuffer.

![Auch das Wechseln zwischen Apps ist möglich, wenn man die Home-Taste gedrückt hält.](/graphics/firefoxOS_wechseln_zwischen_apps.webp)

Gonk nutzt den Kernel des [„Android Open Source Project“](http://source.android.com/).
Auf der Wiki-Seite von [Mozilla](https://wiki.mozilla.org/B2G/Architecture) 
schreiben sie selbst,
dass der Kernel im Vergleich zum Upstream-Linux-Kernel nur leicht modifiziert ist.

### Gecko ###

Die Laufzeitumgebung von Apps ist „Gecko“. Gecko implementiert die offenen
Web-Standards für HTML, CSS und JavaScript. Es beinhaltet zudem einen Netzwerk-
und Grafik-Stack sowie eine Layout-Engine und eine virtuelle Maschine zum
Ausführen von JavaScript-Code.

### Gaia ###

Gaia ist der Name der Benutzer-Oberfläche von Firefox OS. Alles was auf dem
Bildschirm eines Gerätes dargestellt wird, ist Bestandteil von Gaia. Hierzu zählen 
der Lock-Screen, der Home-Screen sowie die vorinstallierten Anwendungen, darunter
die Telefon-, Kamera- und Kalender-App. Gaia ist komplett in HTML, CSS und
JavaScript geschrieben und läuft unter Gecko.

Die Kommunikation mit dem
Betriebssystem ist vollständig über offene Web-Programmierschnittstellen
umgesetzt, welche auch auf anderen Betriebssystemem und Web-Browsern laufen
könnten. Alle Apps von Drittanbietern laufen dabei neben Gaia.

## Bedienung und Look & Feel ##

Beim Start eines Gerätes mit Firefox OS erscheint nach einem Bootsplash mit einem
Mozilla-Logo der App-Launcher. Der App-Launcher hat größere Ähnlichkeiten zu den
Standard-App-Launchern von Android und iOS.

![Eine Web-Suche lässt sich durchführen oder man kann mobile Webseiten besuchen](/graphics/firefoxOS_suche_und_apps.webp)

Insgesamt besteht das System standardmäßig aus vier Home-Screens. Nach dem Start erscheint
einer der Home-Screens, welche lediglich die Uhrzeit sowie das aktuelle Datum
anzeigt. Am unteren Rand befinden sich vier kreisrunde App-Icons. Konkret sind
dies die Telefon-, SMS-, Kontakte- und Browser-App. Die untere App-Leiste ist
dabei permanent auf dreien der vier Home-Screens aktiv. Der Start-Home-Screen
befindet sich auf der zweiten Position. Die ungefähre Position erkennt man zudem
am oberen Rand, wo ein schmaler orangefarbener Balken eingeblendet ist. Auf
den weiteren zwei Home-Screens befinden sich lediglich App-Launcher, welche zum
Starten von Apps dienen. Eine Kamera-App ist genauso vorinstalliert wie eine
E-Mail-, Musik- und Galerie-App.

Wenn man nach ganz links wischt, erscheint der letzte Home-Screen. Dort befindet
sich neben einer Suche auch noch diverse Kategorien, über denen man Apps
starten kann, die keineswegs installiert werden müssen. In der Regel sind
die dort enthaltenen Apps mobile Webseiten, beispielsweise von YouTube oder Twitter,
die man auch über einen gängigen mobilen Browser auch auf anderen Plattformen
aufrufen kann.

Die graphische Oberfläche von Firefox OS besitzt zudem noch einen Bereich für
Benachrichtigungen. Wie auch unter Android, erreicht man diese durch das
Herunterziehen der Benachrichtigungsleiste.
Der Benachrichtigungsbereich ist halbtransparent und zeigt die Benachrichtungen
untereinander an. Die Benachrichtigungen verschwinden in der Regel nach dem
Klick auf diese oder durch die Benutzung des Buttons "Clear all", welche alle
vorhandenen Benachrichtigungen verbirgt.

Im Benachrichtigungsbereich sitzen nicht nur die Benachrichtigungen, sondern
auch eine Leiste mit Buttons, um diverse Hardware-Komponenten zu aktivieren oder
deaktivieren, wie etwa dem WLAN, Mobilfunk, GPS, Bluetooth oder dem Flugzeugmodus.
Zusätzlich gibt es dort noch einen Button, um in die Einstellungen zu gelangen.

### Lock-Screen ###

Der Lock-Screen ist schlicht gehalten und sieht standardmäßig genauso aus wie
der Home-Screen. Vom Lock-Screen aus kann man entweder direkt die Kamera-App
öffnen oder alternativ das Gerät entsperren. In den Einstellungen kann man
zusätzlich festlegen, ob Benachrichtigungen auf dem Lock-Screen dargestellt
werden sollen.

![Der Lock-Screen](/graphics/firefoxOS_lockscreen.webp)

### Einstellungen ###

In den Einstellungen lassen sich viele gängige Dinge einstellen. Besonderheiten,
die nicht von anderen mobilen Plattformen bekannt sind, findet man dort auch.
Die wohl herausragendste Eigenschaft von Firefox OS ist wohl, dass man einzelnen Apps
die Rechte zu bestimmten Hardware-Schnittstellen entziehen kann. Dies ist
beispielsweise unter Android und iOS gar nicht möglich. Entweder installiert man
sich auf diesen Plattformen die Apps mit den geforderten Rechten oder man
installiert sie sich nicht. Bei Firefox OS gibt es daher eine größere und bessere
Möglichkeit Apps ihre Rechte zu entziehen. Diverse Apps fragen beispielsweise
nach der aktuellen geographischen Position. Standardmäßig fragt Firefox OS den Nutzer,
ob man der App diese Rechte gibt. Dies kann man akzeptieren oder eben ablehnen.
Je nach App kann man folglich nicht alle Funktionalitäten nutzen, wenn man spezielle
Rechte wegnimmt. Eine Navigations-App macht ohne Zugriff auf die Geolokalisierung
schließlich wenig Sinn.

## Apps ##

Unter Firefox OS gibt es zwei verschiedene Arten an Apps.
Jeder Nutzer kann sich ziemlich einfach eine mobile Webseite als „App“ installieren.
Letztendlich ist es ein einfache Verknüpfung auf eine mobile Webseite. Dazu reicht
es, eine Webseite zu besuchen, etwa die mobile Seite von Twitter, und dann dort
den Sternchen-Button zu drücken. Entweder kann man dann einen Bookmark im Browser
hinzufügen oder man kann einen Launcher auf den Home-Screen hinzufügen. Wenn man
nun den App-Launcher auf dem Home-Screen betätigt, startet der Firefox-Browser
und zeigt die mobile Webseite an. Es fühlt sich dabei wie eine echte App an.
Diese Art von Apps nennen sich „Hosted Apps“.

![Der Benachrichtigungsbereich](/graphics/firefoxOS_notifications.webp)

Die zweite Art ist die Möglichkeit „echte“ Apps zu installieren. Dazu gibt es
den „Marketplace“, aus dem man sich Apps installieren kann. Aktuell ist das Angebot
von Apps übersichtlich. Da es sich um eine neue Plattform handelt, ist dies
nachvollziehbar. Nennenswerte Apps sind Twitter, Wikipedia, Facebook
oder auch Nokias Maps-App „HERE Maps“. Letztgenannte ist sogar vorinstalliert. Der
Unterschied hierbei ist, dass die Apps über dem Marketplace „Packaged Apps“
sind. [„Packaged Apps“](https://developer.mozilla.org/en-US/docs/Web/Apps/Packaged_apps)
sind Pakete, in denen die HTML-, CSS- und JavaScript-Dateien enthalten sind.
Aktualisierungen der Apps werden über den Marketplace angeboten und verteilt.

Packaged Apps werden vor der Veröffentlichung im Marketplace zuvor geprüft. Im
Gegensatz zu den sogenannten Hosted Apps, die oben bereits beschrieben wurden,
bieten Packaged Apps eine höhere Sicherheit.
Zudem lassen sich hierbei die Berechtigungen der App konfigurieren, was bei Hosted
Apps nicht möglich ist. Die Funktionalität einer Hosted App sowie einer
Packaged App ist dabei nahezu gleich. Beim Look und Feel von den Twitter-Apps
merkt man an sich keinen Unterschied. Das gleiche gilt auch für die enthaltenen
Funktionen.

## Firefox OS ausprobieren ##

Um Firefox OS auszuprobieren, gibt es verschiedene Möglichkeiten. Zuerst
sei genannt, dass man sich eines der Entwickler-Geräte vom spanischen
Hersteller [Geeksphone](http://www.geeksphone.com/) kaufen kann. Das günstigere
und schwächere [Geeksphone Keon](http://shop.geeksphone.com/en/phones/1-keon.html)
kostet 91€ während das größere und leistungsfähigere
Geeksphone Peak 149€ kostet. Das Keon besitzt einen 1 Ghz starken Qualcomm 
Snapdragon-Prozessor mit 512 MB RAM sowie einem 4 GB großen ROM. Das Display ist 
3,5 Zoll groß.

![Eine Kalender-App ist vorinstalliert](/graphics/firefoxOS_kalender.webp)

Das [Peak](http://shop.geeksphone.com/en/phones/5-peak.html) ist mit einem
1,2 Ghz starken Dual-Core Qualcomm Snapdragon-S4-Prozessor
ausgestattet und kommt ebenfalls mit 512 MB RAM sowie einem 4 GB großen ROM. Das
Display ist bei diesem Modell 4,3 Zoll groß.

Das Problem an diesen beiden Geräten ist allerdings, dass es kurz nach Verkaufsstart
bereits ausverkauft war. Zum Zeitpunkt des Artikels (Anfang Juni 2013) waren
beide Geräte weiterhin ausverkauft.

Andere Hersteller planen ebenfalls die Herstellung von Firefox OS-Smartphones,
darunter [Alcatel und ZTE](http://www.netzwelt.de/news/92870-alcatel-zte-erste-smartphones-mozilla-os-planung.html).
Auf der Computex gab Foxconn [bekannt](http://www.heise.de/open/meldung/Foxconn-und-Mozilla-bauen-Geraete-mit-Firefox-OS-1875291.html),
dass diese ebenfalls Geräte mit Firefox OS auf den Markt bringen wollen, darunter
auch ein Tablet.

Wer allerdings nicht warten will oder kein Geld ausgeben möchte, um Firefox OS
auszuprobieren, der kann auch eine ROM auf einem Android-Smartphone installieren.
Einige ROMs findet man unter anderem im Forum von [xda-developers.com](http://forum.xda-developers.com/).
Ich selbst habe dabei Firefox OS auf meinem Samsung Nexus S installiert und
ausprobiert. Dafür gab es in einem [Foren-Thread](http://forum.xda-developers.com/showthread.php?t=1814302)
eine funktionsfähige Anleitung.

Die letzte Methode, Firefox OS zu testen, ist wohl die einfachste. Zunächst muss
ein installierter Firefox-Browser zur Verfügung stehen. Im Anschluss kann man sich den
[Firefox OS Simulator](https://addons.mozilla.org/de/firefox/addon/firefox-os-simulator/)
als Plug-in installieren. Das Plug-in ist etwa 70 MB groß. Der Firefox OS Simulator
ist schnell installiert und man bekommt einen guten Überblick über das System.
Der Simulator läuft soweit sehr flüssig und lässt sich gut bedienen. Ein 
Nachteil ist, dass es zu einen relativ hohen Prozessor-Auslastung auf
dem Rechner führt. Zudem ist die Kamera-App funktionslos, da ja keine Kamera
vorhanden ist.

## Fazit ##

Firefox OS ist ein weiteres mobiles System auf Linux-Basis, welches gut auf
schwächeren Geräten läuft und zudem frei ist. Im Vergleich zu Android
ist es deutlich freier, da man durch das Zurückziehen von Berechtigungen
standardmäßig viel mehr Macht hat. Durch den Einsatz von Web-Technologien ist es für
App-Entwickler einfach möglich Apps zu programmieren oder zu portieren.

Der Marktstart für Endanwender ist für Ende 2013 geplant. Hierbei erscheinen
die Geräte zunächst in den Ländern [Brasilien, Polen, Spanien und 
Venezuela](http://www.zdnet.de/88151357/firefox-os-startet-im-sommer-in-funf-landern/).
Ab 2014 dürften dann auch Geräte mit Firefox OS auf dem deutschen Markt
verfügbar sein.

Die Mozilla Foundation steht bereits jetzt mit vielen [großen Partner-Firmen](http://www.mozilla.org/de/firefox/partners/) in Kontakt,
welche die Entwicklung von Firefox OS unterstützen. Darunter die großen
Telekommunikationsunternehmen wie Telefonica, die Deutsche Telekom oder Sprint.

Das System macht schon jetzt einen guten Eindruck. Von der Bedienung her werden
keine neuen Akzente gesetzt. Die möglichen Nachteile des 
Systems werden sich wohl nach und nach zeigen.

