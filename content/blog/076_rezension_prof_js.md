---
title: "Rezension: Professionell entwickeln mit JavaScript"
categories: ["Rezension"]
date: "2015-10-11 19:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Das Buch „Professionell entwickeln mit JavaScript“ des Autors Philip Ackermann
behandelt grundsätzlich das Entwickeln von Anwendungen mit der Programmiersprache JavaScript.

### Was steht drin?

Das Buch unterteilt sich in acht Kapiteln, die insgesamt 450 Seiten lang sind. Zu Beginn wird eine Einführung in JavaScript gegeben, in dem die gängigen Begriffe erläutert, die vorhandenen Laufzeitumgebungen genannt sowie ein Überblick über eine Auswahl von existierenden Entwicklungsumgebungen und Debugging-Tools geliefert werden. Das Kapitel schließt mit der ersten Einführung in die Sprache selbst ab.
Das zweite Kapitel befasst sich mit den Funktionen und den funktionalen Aspekten von JavaScript. Es wird unter anderem eine Einführung in die funktionale Programmierung gegeben und die Unterschiede zur imperativen Programmierung aufgezeigt. Das dritte Kapitel umfasst das Thema der objektorientierten Programmierung. Dort wird erläutert, wie man objektorientiert in JavaScript programmiert, also wie man Klassen programmiert und wie man mit Prototypen, Vererbungen und Datenkapselungen arbeitet. Weiterhin wird die Emulation von diversen weiteren Prinzipien der objekt-orientierten Programmierung erklärt.

Im vierten Kapitel geht es um den neuen ECMAScript Standard in Version 6, der erst kürzlich, im Juni 2015, veröffentlicht wurde. Das Kapitel umfasst alle Neuerungen und Änderungen und zeigt, wie man sie im Alltag verwenden kann. Im darauffolgenden fünften Kapitel dreht es sich um den Entwicklungsprozess. Es werden sehr viele JavaScript-Bibliotheken und Node.js-Pakete genannt, die für den Entwicklungsprozess hilfreich sind, um etwa die Code-Qualität zu verbessern oder den Code für das Deployment zu minifizieren.
Das sechste Kapitel dreht sich rund um das Test-Drive-Development von JavaScript-Anwendungen, es begrenzt sich nicht nur auf das Testen von JavaScript-Anwendungen auf Webseiten sondern es wird auch erläutert, wie man mit diversen Test-Frameworks Node.js-Anwendungen testet. Im anschließenden vorletzten Kapitel dreht es sich um die Entwurfsmuster der „Gang of Four“. So werden viele verschiedene Entwurfsmuster aus der Softwaretechnik genannt, erläutert und gezeigt, wie man diese in JavaScript umsetzt. Zum Schluss folgen im letzten Kapitel die verschiedenen Architekturmuster von modernen JavaScript-Frameworks, wie sie in AngularJS, Backbone.js und Co. eingesetzt werden.

### Wie liest es sich?

Der Autor beschreibt in dem Buch sehr viele Aspekte von JavaScript, die teilweise kompliziert und umfangreich sind. Einige Passagen muss man gegebenenfalls mehrfach lesen um es vollständig zu verstehen. Die Erklärungung sind in der Regel kurz und knackig, sodass nicht sehr viel um den heißen Brei herumgeredet wird.
Der Autor bringt immer wieder Erfahrungen und Tipps ein, die für das reale Arbeiten hilfreich sind, etwa wann man diverse Dinge tun sollte und wann man es vermeiden sollte.


### Kritik

Das Buch richtet sich primär an Leute, die schon Erfahrungen mit einer Programmiersprache haben und sich JavaScript aneigenen wollen. Diesen Umstand merkt man auch im Buch, da das gängige Programmierverständnis vorausgesetzt wird. Nichtsdestotrotz ist das Buch auch für Programmierer geeignet, die bisher nur wenig Programmiererfahrung besitzen.

Der Titel des Buches impliziert, dass man durch dieses Buch mit JavaScript professionell programmieren lernen kann. Dem wird das Buch auch vollkommen gerecht, da eine große Breite an Themen, die sich rund um das Thema JavaScript drehen, behandelt werden. Positiv ist zudem hervorzuheben, dass nicht im Detail auf jede mögliche Syntax eingegangen wird, also wie man etwa Schleifen programmiert oder if-Abfragen tätigt. Dadurch ist viel Platz für die wirklich notwendigen Dinge vorhanden, die sonst zu kurz kämen. Ebenfalls positiv hervorzuheben ist, dass sich der Autor nicht auf spezielle JavaScript-Bibliotheken festlegt, von denen es ja mittlerweile sehr viele gibt. So können auch Leser dem Buch was abgewinnen, die JavaScript nicht im Browser nutzen.

Weiterhin werden häufig bei der Nennung von JavaScript-Bibliotheken Alternativen aufgezeigt und Tipps gegeben, welche Vor- und Nachteile die jeweilige Bibliothek hat, etwa ob man grunt oder gulp einsetzen sollte. Viel von den JavaScript-Funktionen selbst lernt man wiederum nicht, also z. B. wie man mit Datums-Objekten arbeitet. Dieser Fakt schadet aber dem Buch keineswegs, da man solche Dinge sowieso schneller im Internet nachschlägt. Für knapp 35€ erhält man ein gutes Buch, um einen vollständigen Einblick in das Arbeiten mit JavaScript zu gewinnen.


### Buchinfo:

**Titel:** [Professionell entwickeln mit JavaScript](https://www.rheinwerk-verlag.de/professionell-entwickeln-mit-javascript_3365/)

**Autor:**  Philip Ackermann

**Verlag:**  Rheinwerk

**Umfang:**  450 Seiten

**ISBN:**  978-3-8362-2379-9

**Preis:** 34,90 Euro (broschiert), 29,90 Euro (E-Book)

Da es schade wäre, wenn das Buch bei mir im Regal verstaubt, wird es verlost. Dies
ist „drüben“ bei [freiesMagazin](http://www.freiesmagazin.de/20151011-oktoberausgabe-erschienen)
in den Kommentaren möglich.
