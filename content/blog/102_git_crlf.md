---
title: "CRLF in LF automatisch umwandeln in Git-Repositorys"
date: "2017-07-07T21:30:00+02:00"
Categories: ["Git"]
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/bf7dd3229b57465ab7062a34d2dc718e"
---

Es sind manchmal viele Kleinigkeiten in einem Projekt in einer Gruppe, die
Mitarbeiter ziemlich nerven können. Neben offensichtlichen Dingen wie Clean Code und
einheitliche Bennung von Dateien, Klassen und Methoden, ist eine Sache die häufiger
im Zusammenspiel zwischen Windows- und Linux-Nutzern stört: Der Zeilenumbruch.

Im Windows-Universum besitzen Zeilenumbrüche ein CR LF und unter Linux LF. Näheres
zu Zeilenumbrüchen findet sich im entsprechenden [Wikipedia-Artikel zu Zeilenumbrüchen](https://de.wikipedia.org/wiki/Zeilenumbruch).

Git Repositorys sollten immer nur LF als Zeilenendung enthalten, da gemischte
Zeilenenden sich auf den Betriebssystemen unterschiedlich auswirken und einfach
unschön in der Handhabung sind. Für Windows-Nutzer
kann beim Auschecken automatisch LF durch CR LF ersetzt werden. Ebenso
sollten immer die CR LF in LF umgewandelt werden, wenn man etwas in das Repository
eincheckt. In der Git für Windows Standard-Installation wird nachgefragt, wie
Zeilenumbrüche gehandhabt werden sollen. Im Standard wird die zuvor beschriebene
automatische Umwandlung vorgeschlagen, die man akzeptieren sollte.

Nichtsdestotrotz schaffen es einige, solche Optionen abzuwählen und es landen
am Ende Dateien mit gemischten Zeilenenden im Repository was ziemlich unschön ist.
Am „schönsten“ ist es etwa dann, wenn Projekt-Mitarbeiter eine Zeile in einer
Datei ändern, durch die Änderung aber jede Zeile der Datei anpassen und das ohne
weitere Prüfung in das Repository schieben.

Das ganze lässt sich auf mehreren Weisen verhindern. Prinzipiell hilft es die
Personen natürlich aufzuklären, warum man einheitliche Zeilenumbrüche verwenden
sollte und wie man es konfiguriert. Die Konfiguration für jede Person ist
prinzipiell recht simpel. Dazu muss lediglich der Konfigurationsschalter
`core.autocrlf` auf `input` gesetzt werden, wenn man unter Linux oder macOS
arbeitet. Unter Windows sollte jedenfalls `core.autocrlf` auf `true` gesetzt
werden. In dem Fall wird dann die zuvor angesprochene automatische Umwandlung
aktiviert. Die Konfiguration kann entweder in `~/.gitconfig` händisch erfolgen
oder man nutzt diesen Befehl:

    $ git config --global core.autocrlf true

Dies führt allerdings noch nicht vollständig dazu, dass bestehende Zeilenumbrüche die in
Dateien vorhanden sind, korrigiert werden. Dazu helfen Tools wie `dos2unix`, aber
auch Git hilft dabei, wenn man geänderte Dateien neu eincheckt.

Damit nicht nur die lokale Konfiguration jedes Projekt-Mitarbeiters korrekt
konfiguriert ist, kann man zusätzlich auf das Repository die Einstellungen
durchsetzen. Dazu muss die Datei `.gitattributes` im Repository hinterlegt werden,
in dem die Zeile `* text=auto` enthalten ist. Nachdem die Datei committet wurde
und/oder die Änderung in der Config steht, meldet Git beim Commiten von Dateien
mit CR LF folgende Meldung:

    $ git add chapter02/
    warning: CRLF wird in Projektdokumentation/chapter02/cryptdb/02_problem.tex durch LF ersetzt.
    Die Datei wird ihre ursprünglichen Zeilenenden im Arbeitsverzeichnis behalten.

Anschließend lebt es sich angenehmer, wenn direkt alle Projekt-Mitarbeiter die
korrekte Einstellung nutzen.
