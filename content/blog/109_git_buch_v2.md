---
title: "Zweite Auflage meines Git-Buches erschienen!"
date: "2019-05-10T17:30:00+02:00"
categories: ["Git"]
toc: true
vgwort: "https://vg02.met.vgwort.de/na/60f0daba717a4cdda57781ac18befe52"
---

Vor schon fast Jahren erschien die [erste Auflage meines Git-Buches](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/),
welches in Zusammenarbeit mit dem [mitp-Verlag](https://mitp.de) erschien.

Jetzt bin ich froh die zweite Auflage ankündigen zu können, die ab jetzt erhältlich
ist!

![Cover der zweiten Auflage des Git-Buches](/graphics/git-buch-v2-cover.webp)

## Was steht drin?

Das Buch hat ein wenig zugenommen, so sind zwei kleinere Kapitel dazugekommen
und das Buch umfasst somit insgesamt 12 Kapitel auf insgesamt 288 Seiten.

 * **Einführung:**
    Im ersten Kapitel geht es zunächst um den Einstieg in das Thema von
    Versionsverwaltung. Konkret also was eine Versionsverwaltung ist, wofür sie
    gut ist und welche Arten es gibt.

 * **Die Grundlagen:**
    Das zweite Kapitel befasst sich mit den Grundlagen, also wie man ein
    Repository anlegt, wie Commits man tätigt und wie Git intern grob arbeitet.

 * **Arbeiten mit Branches:**
    Im Anschluss folgt auch schon das Kapitel zum Branching mit Git. Erklärt
    wird, wie man Branches erstellt, wofür sie gut sind und wie sie
    zusammengeführt werden können. Dabei werden sowohl normale Merges mit und
    ohne Merge-Konflikten behandelt als auch das Rebasen von Branches.

 * **Verteilte Repositories:**
    Verteilte Repositories kamen in den ersten drei Kapitel noch nicht vor. In
    diesem Kapitel wird erläutert wie man mit Remote-Repositories arbeitet und
    wie ein kleiner Workflow mit drei Repositories aussieht.

 * **Git Hosting:**
    Das fünfte Kapitel befasst sich mit dem Einsatz von Git auf dem Server. Im
    Wesentlichen wird der Workflow GitHub und GitLab erläutert, sodass am Ende
    bekannt ist, wie man ein kleines Repositorys maintaint und wie man Änderungen
    mittels Pull-Requests bei anderen Projekten beiträgt und was dabei zu beachten
    ist. Kurz thematisiert werden auch externe Tools wie Jenkins oder Travis-CI,
    die zur kontinuierlichen Integration nützlich sind.

 * **Workflows:**
    Wichtig beim täglichen Einsatz von Git in einem Projekt ist die Auswahl
    eines passendes Workflows. Hierbei werden verschiedene Workflows
    beispielhaft genannt, die in einem Projekt eingesetzt werden können, etwa
    Git Flow.

 * **Hooks:**
    Mit Hooks lassen sich einige Dinge automatisieren, wenn etwa Commits
    getätigt werden. Zweites Nebenthema sind Git-Attribute.

 * **Umstieg von Subversion:**
    Wenn der Leser nicht gerade völliger Neuling ist, dann ist der Umstieg von
    Subversion wahrscheinlich. Das Kapitel behandelt die wesentlichen Unterschiede
    in den Konzepten und in den Kommandos und bringt auch eine Anleitung wie man
    ein Subversion-Repository in ein Git-Repository überführt.

 * **Tipps und Tricks:**
    Das Kapitel befasst sich mit einer Sammlung von Tipps und Tricks die
    man mehr und weniger regelmäßig gebrauchen kann. Thematisiert werden unter
    anderem die Fehlersuche mit Bisect, das Arbeiten mit Patches und das
    Neuschreiben der Historie mit Filter-Branch.

 * **Grafische Clients:**
    Während die übrigen Kapitel kaum auf graphische Clients eingeht, ist dies bei
    diesem Kapitel anders. Hier erfährt man einen groben Überblick über einige der zur
    Verfügung stehenden grafischen Clients von Git.

 * **Nachvollziehbare Git-Historien:**
    Das erste der beiden neuen Kapiteln befasst sich mit meinem aktuellen Lieblingsthem:
    Wie muss man vorgehen, um eine nachvollziehbare Git-Historie zu erstellen.
    Das Kapitel umfasst sowohl Tipps und Hinweise, wie die Commit-Messages
    auszusehen haben sollten, als auch wie groß bzw. klein die Commits sein sollten.

 * **Frequently Asked Questions:**
    Dieses Kapitel stellt einige Problemstellungen vor, die bei Git-Nutzern
    regelmäßig zu Irritationen führen und häufiger nachgefragt werden. Für
    viele Probleme gibt es mehrere Lösungen und teilweise wurden diese Lösungen
    schon diverse Male in den vorherigen Kapiteln behandelt. Trotzdem soll es
    hier einen kurzen Überblick über allgemeine Probleme zum Nachschlagen
    geben, der auf die entsprechenden Stellen im Buch für nähere Erläuterungen
    verweist.

## Was ist neu?

Mich fragten schon vor Erscheinen des Buches einige Leute: „Lohnt es sich für mich das
Buch zu kaufen, wenn ich schon die erste Auflage habe.“ Und die Antwort ist
dafür recht klar: „Höchstwahrscheinlich nicht.“.

Die beiden Kapitel 11 und 12 sind neu. Da gehts, wie oben erwähnt, um
nachvollziehbare Git-Historien sowie um Frequently Asked Questions. Neben diesen
beiden Kapiteln habe ich viele Rechtschreib- und Tippfehler korrigiert. Zudem
habe ich (fast) alle Grafiken neu gemacht. Diese sehen jetzt (hoffentlich) ein
kleines wenig schöner und moderner aus. Hier und da habe ich auch noch diverse
Formulierungen und Aktualisierungen reingebracht. Im Wesentlichen allerdings nichts
großartig neues im Vergleich zur Vorausgabe, eben von den beiden neuen Kapiteln
abgesehen.

## Leseprobe und Kauf

Eine Leseprobe gibt es natürlich auch. Diese findet sich [hier](https://www.mitp.de/out/media/9783747500422_Leseprobe.pdf).

Bücher lassen sich bekanntlicherweise an vielen Stellen kaufen zu einem festen
Preis. Es kostet auch in der zweiten Auflage in der Print-Ausgabe 29,99€ und 25,99€
in der digitalen Ausgabe. Wer ein Bundle aus beiden haben möchte, der zahlt 34,99€.
Die Bundle-Veriante gibt es allerdings nur auf Verlagswebseite.

Wer sich das Buch kaufen möchte, dem kann ich ein Kauf direkt auf der [mitp-Verlagsshop](https://www.mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git-oxid.html)
nahe legen, da dies für den Verlag und dem Autor (also mich) am besten ist.

Auf [Amazon findet man das Buch natürlich auch](https://amzn.to/2VwxRWT).
Es ist sowohl dort als auch bei anderen Händlern ab dem 15. Mai 2019 verfügbar.
