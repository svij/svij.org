---
title: "Wärmepumpe nach einem Jahr: Lohnt sich"
categories: ["Sonstiges"]
date: 2025-01-04T16:25:00+01:00
cover:
  image: "/graphics/waermepumpe.webp"
---

Im August 2023 habe ich eine Wärmepumpe in meinem Haus einbauen lassen. In diesem Blogpost ziehe ich mal ein erstes Zwischenfazit, nachdem die Wärmepumpe schon ein volles Kalenderjahr hier läuft und fleißig das Haus heizt, sowie für warmes Wasser sorgt.

**tl;dr:**
- 27.450 € Investition (nach Abzug der Förderung)
- Plattenheizkörper! Keine Fußbodenheizung
- 1551 kWh Stromverbrauch
- 6,9 COP
- mindestens 850 € Ersparnis an laufenden Kosten im Jahr 2024

Ich werde hier ein paar Zahlen, Daten und Fakten nennen, damit man idealerweise ein gutes Bild bekommt, wie die Realität für mich ausschaut. Es gibt allerdings viele verschiedene Faktoren, die starken Einfluss auf die Effizienz und die laufenden Kosten haben. Wenn jemand anderes in genau diesem Haus leben würde, könnten die Zahlen natürlich vollkommen anders sein.

### Vorgeschichte

Wichtig für meinen Fall ist, dass die Heizung auf jeden Fall ausgetauscht werden musste. Das Haus wurde im Jahr **2000** gebaut und aus diesem Jahr stammt auch die Gasheizung, die ersetzt werden musste, da sie immer wieder ausfiel und es kaum noch Ersatzteile für diese Gasheizung gab. Im Jahr Sommer 2021, also noch lange vor dem russischen Angriffskrieg auf die Ukraine und als die Gaspreise mehr oder minder „bezahlbar“ waren, war mir jedenfalls klar: Es muss eine Wärmepumpe her, schließlich wird auch der CO₂-Preis weiter steigen.

Das Haus ist ein Reihenmittelhaus mit drei Etagen + (beheizbaren) Keller. Heiz-technisch gesehen hat es einen Vorteil: Ich habe nur nach vorn und hinten wesentliche Wärmeverluste und nicht nach links und rechts. Daher lag der Gasverbrauch in den Jahren zuvor bei 8.000 kWh bis 12.000 kWh, je nachdem wie kalt der Winter war. Viel „optimiert“ wurde da allerdings nicht. Es war und ist auch weiterhin keine Fußbodenheizung vorhanden, sondern reguläre Plattenheizkörper.

### Eine Odyssee des Wärmepumpen-Umbaus

Die Wärmepumpe ins Haus zu bekommen, war leider richtig anstrengend. Mittlerweile sollte es allerdings weniger ein Problem sein. Das hatte zwei Gründe: Das Finden von gescheiten Handwerkern dauerte Zeit, zudem gab es generelle Lieferprobleme, die sich auf die Wartezeit meiner Wärmepumpe ausgewirkt haben.

Wie zuvor geschrieben: Ich fing im Jahr Sommer 2021 an, versuchte einige Angebote von Handwerkern zu bekommen, aber leider ist Ghosting und keine offene Kommunikation und komplette Ineffizienz bei Handwerkern vollkommen üblich.

Einige waren auch ganz irritierend: Ein Heizungsbauer meinte aus dem Bauchgefühl heraus, dass sich das für mich nicht lohnt und hat mir stattdessen eine Hybrid-Heizung (Gas + Pellet) empfohlen. Nein, danke!

Ein anderer war ganz irritiert, dass die Gaspreise explodiert sind, was man als Gas-Heizungsbauer ja eigentlich wissen sollte. Vor allem ging zu dem Zeitpunkt schon der Krieg in der Ukraine los. Ein weiterer wollte einfach nur eine neue Gastherme einbauen, weil das ja „einfacher und günstiger“ ist. Alle diese Handwerker bauen übrigens Wärmepumpen ein … allerdings eben nicht ausschließlich.

Schlussendlich ist es dann die [Waterkotte Basic Line Air Bloc](https://www.waterkotte.de/waermepumpen/basic-line-air-bloc) von der Firma Waterkotte (die [seit 2020](https://www.nibe.com/de/investoren/pm-news-acquisitions-other/2020---news-acquisitions-other/2020-03-10-nibe-ubernimmt-deutschen-warmepumpenhersteller-waterkotte-gmbh) zu [NIBE](https://www.nibe.com/) gehört) geworden. Die Firma war bisher in Herne ansässig, wo diese auch produziert werden. Von meiner Haustür sind sie gerade mal fünf Kilometer entfernt.

Eingebaut wurde sie dann, nach zahlreichen Lieferschwierigkeiten, die es damals gab, im September 2023. Ich war zudem der Erste der dieses neue Modell eingebaut bekommen hat.

Die Gastherme mit Wassertank befand sich im 2. Obergeschoss, die Wärmepumpe mit zwei Tanks kam wiederum in den Keller, was zu ein paar Änderungen der Wasserleitungen bedurfte. Das Außengerät steht wiederum knapp 6 m von der Hausaußenwand entfernt, um weder uns noch die Nachbarn mit dem potenziellen Lärm zu belästigen.

Der Aufwand, den Garten umzugraben, war übrigens auch nicht unerheblich. Hier sieht man das Bild während der Arbeiten, wo über mehrere Tage der Garten auf Kniehöhe aufgebuddelt wurde, bevor die Leitungen zum Außengerät gelegt werden konnten. Dafür musste dann ein Kirschbaum geopfert werden.

![Gartenumbau](/graphics/waermepumpe_gartenumbau.webp)

### Zahlen, Daten, Fakten

Kommen wir zu den Zahlen, Daten und Fakten. Und hier kann es große Abweichungen geben, da diese dann doch sehr davon abhängig sind, wie das Haus gebaut und gedämmt ist, sowie wie der Wasserverbrauch im Haus zeitlich geregelt ist.

In der Regel läuft die Heizung mit einer **Vorlauftemperatur von 40 °C**, das Brauchwasser hat wiederum eine **Temperatur von 46 °C**. Je nach Bedarf habe ich allerdings die Temperatur vom Brauchwasser angepasst. Im Sommer über reichen 42 °C etwa locker aus.

Die Heizkörper habe ich **nur** im Wohnzimmer im Erdgeschoss tauschen lassen. Dort sind nun dickere Plattenheizkörper verbaut. Das sind im Wesentlichen auch die einzigen Heizkörper, die dauerhaft an sind. Auf den anderen Etagen wird effektiv wenig bis gar nicht geheizt. Das war auch schon vor der Wärmepumpe effektiv so. Wichtiger Punkt ist, finde ich, dass die Dämmung des Hauses so weit auch gut. Allerdings sind hier auch keine Kinder im Haushalt, sondern drei Erwachsene.

Im folgenden Bild sieht man etwa den Temperaturverlauf für die Monate Oktober 2024 bis Dezember 2024 in verschiedenen Räumen:

- „Livingroom“ ist das Wohnzimmer im Erdgeschoss
- „Floor“ ist der Flur im 1. OG, wo weder Heizkörper noch Fenster gibt
- „Attic“ ist ein Wohn- und Schlafzimmer im 2. OG, was wenig genutzt wird

![Temperaturverlauf im Haus](/graphics/waermepumpe_temperaturverlauf.webp)

Im Kalenderjahr 2024 lief die Heizung bis Ende März sowie erneut ab Anfang November. Also (fast) fünf Monate. Warmes Brauchwasser wurde natürlich ganzjährig benötigt. Wie man anhand des Temperaturverlaufs in den drei Räumen sehen kann, ist vorwiegend das Dachzimmer recht kalt. An den Tagen rund um Weihnachten und Neujahr wurde das Zimmer häufiger genutzt und auch geheizt, was sich den Peaks im Temperaturverlauf erkennen lässt. Würde der Raum dauerhaft genutzt, hätte das schon spürbaren Einfluss auf den Heizbedarf und Stromverbrauch der Wärmepumpe.

#### Optimierungen

Im Laufe der Zeit habe ich natürlich stellenweise ein paar Optimierungen durchgeführt.

Das Brauchwasser wurde nur zwischen 10 und 18 Uhr erwärmt, um vor allem den Strom von der PV-Anlage direkt selbst zu verbrauchen. Von April bis September ist der Betrieb also fast komplett aus dem selbst produzierten Strom gedeckt. Manchmal, vor allem wenn über mehrere Tage/Wochen Besuch da ist, habe ich es allerdings abschalten müssen. Für drei Personen, die über den Tag verteilt duschen, reichte es in der Regel, wenn die Wärmepumpe einmal für etwa 45 Minuten läuft.

In den Nachtstunden von 22 Uhr abends bis 8 Uhr morgens liegt die Vorlauftemperatur 10 °C unter dem eingestellten Wert am Tage. Dadurch läuft die Wärmepumpe deutlich seltener in der Nacht und spart Energie, ohne, dass einem kalt wird.

Weitere Optimierungen hätte ich gerne noch „auf Abruf“ gemacht, geht aber leider nicht, da ich die Wärmepumpe so neu ist, dass sie zwar einen Netzwerkanschluss besitzt, aber diese noch funktionslos ist. Die Idee wäre, dass ich durch die Nutzung meines variablen Stromtarifs bei günstigem Strom die Vorlauftemperatur hochdrehen könnte, um bei teurem Strom nicht laufen zu müssen. Ob überhaupt eine entsprechende API nutzbar vorhanden ist, weiß ich allerdings auch nicht.

![Energieverbauch über den Tag verteilt](/graphics/waermepumpe_stromverbrauch.webp)

Am obigen Bild ist der Stromverbrauch (und Ertrag) an einem dunklen Dezembertag ersichtlich. Erkennbar sind jeweils die Leistungsaufnahme der Wärmepumpe für warmes Brauchwasser, sowie der Heizung. Andere Elektronik, wie etwa die Küche zur Mittagszeit, sind ebenfalls erkennbar. Die Daten stammen vom Wechselrichter der PV-Anlage.

#### Anschaffungskosten

Kommen wir zu den Anschaffungskosten, jeweils mit Mehrwertsteuer und ein wenig gerundet:

- Handwerkerleistungen inkl. Wärmepumpe: 37.950 €
- Elektrikerarbeiten: 4.300 €
- BAFA-Förderung: -14.800 €
- **Gesamt: 27.450 €**

Die Anschaffung der Wärmepumpe hat mich, abzüglich der BAFA-Förderung von 35%, also knapp 27.450 € gekostet. Interessant finde ich noch folgende Kosten ein wenig hervorzuheben:

- 2400 € für die Ausschachtung im Garten inklusive Legen der Zuleitungen und Fundament für das Außengerät
- 1300 € für *zwei* neue Heizkörper im Wohnzimmer
- 3900 € für den hydraulischen Abgleich (inkludiert Kosten für den Energieberater)

Diese Kosten sind allerdings **vor** Abzug der BAFA-Förderung. Größtes Einsparpotenzial wären die Gartenarbeiten gewesen. Diese wollte ich zunächst selbst machen, habe ich dann aber zum Glück nicht, da im Boden einige große alte Ziegel und Mauerwerke gefunden wurden, die entfernt werden mussten, und diese hatte ich schon erahnt. Wenn man die Wärmepumpe direkt an die Hausmauer stellen kann, fallen solche Kosten natürlich weg.

#### Verbrauchszahlen

Und jetzt wird es spannend: Der vorherige Gasverbrauch lag bei 8.000 kWh bis 12.000 kWh pro Jahr. Das hing natürlich stark davon ab, wie warm oder kalt der Winter war.

Meine Verbrauchszahlen sehen für das gesamte Kalenderjahr 2024 wie folgt aus:

- Stromverbrauch Wärmepumpe 2024: 1.551 kWh
- Wärmemengenzähler 2024: 10.722 kWh
- COP: 6,9

Der Wärmemengenzähler lag in etwa so hoch, wie auch die Jahre zuvor geheizt wurde, da nur eben mit Gas. Der doch sehr hohe COP hat mich positiv überrascht, denn 1551 kWh an Strom ist nicht viel.

Dadurch, dass von Frühling bis Spätherbst der selbsterzeugte PV-Strom verwendet wird, ist die tatsächliche Strombelastung aus dem Netz nochmals geringer. Was allerdings überwiegend für das warme Brauchwasser relevant ist. Natürlich läuft die Wärmepumpe primär im Winter viel, wo kaum Sonne da ist.

Wartungskosten gibt es natürlich auch, diese sind für mich in diesem Kalenderjahr bisher nicht angefallen. Diese gibt es allerdings für Gasheizungen auch, wo dann alle zwei Jahre Schornsteinfegerkosten dazukommen.

Wenn ich heute einen neuen Gasanbieter aussuche, für 2025, dann werden mir bei den üblichen Vergleichsportalen für einen Verbrauchswert von 10.700 kWh mindestens ~1300 € an Kosten für das gesamte Jahr angezeigt. Darin sind Neukundenboni mit einberechnet. Einen ähnlichen Vergleich habe ich Anfang des Jahres allerdings nicht gemacht.

Bei meinem Strom setze ich auf den [dynamischen Stromtarif von Tibber, über den ich hier schrieb](https://svij.org/blog/2024/03/03/erfahrungsbericht-ein-jahr-mit-dem-dynamischen-stromtarif-von-tibber/). Durch die Nutzung des eigenen PV-Stroms, dem nicht eingespeisten selbst-produzierten Strom, sowie des dynamischen Stromtarifs ist eine genaue Berechnung der Kosten schwierig. Wenn ich hier von 0,30 € pro kWh ausgehe, auch wenn mein tatsächlicher Strompreis wahrscheinlich niedriger war, dann komme ich auf **450 €** Stromkosten für die Wärmepumpe. Insgesamt habe ich im Jahr 2024 3.792 kWh aus dem Netz bezogen, zu 1344 €. Dazu kommen natürlich noch Einnahmen für die Erträge aus dem eingespeisten Strom.

Gespart habe ich vermutlich knapp 850 € im ersten vollen Jahr. Wie zuvor erwähnt, ist es schwierig die tatsächliche Ersparnis zu berechnen. Zudem sind natürlich auch die Gaspreise schwankend. Wartungskosten für Gas- wie auch Wärmepumpe habe ich hier mal außen vor gelassen.

### Fazit

Einige mögen jetzt vielleicht denken, dass das ja ganz schön lange dauert, bis sich das amortisiert: Stimmt!

Allerdings darf man nicht vergessen, dass als Alternative eine neue Gasheizung auch Geld kostet. Von Nachbarn hörte ich Summen in Höhe von 10.000 bis 14.000 € für nahezu baugleiche Häuser.

Fakt ist, dass der CO₂-Preis weiterhin steigen wird. Beim Strompreis gehe ich schlicht davon aus, dass dieser nicht (wieder) so stark steigen wird. Mit immer geringerem Gasverbrauch wird auch die Nutzung des Gasnetzes teurer und langfristig [zurückgebaut, wie in Mannheim etwa](https://www.fr.de/wirtschaft/erste-deutsche-stadt-stellt-gasnetz-ein-gasheizungen-sind-nicht-nachhaltig-und-zukunftsorientiert-zr-93403078.html). Ich gehe also stark davon aus, dass meine jährliche Ersparnis in den nächsten Jahren deutlich steigen wird.

Eine Fußbodenheizung brauche ich hier nicht, die Zusatzkosten muss man erst einmal „verheizen“. Einen zusätzliche Stromzähler für günstigeren Wärmepumpenstrom habe ich zum Glück auch nicht eingebaut, schließlich kostet so ein Umbau mit neuem Zählerschrank auch noch einige tausend Euro. Mein Elektriker sprach hier von zusätzlichen Kosten von 5000 €.

Ach ja und wenn jemand Aussagen zur Lautstärke des Außengeräts vermisst: Stört effektiv nicht. Im Sommer läuft es meist nur 45 Minuten pro Tag, im Winter zwar häufiger, aber da sind meist ja sowieso die Fenster zu. Man bekommt also nichts mit. Wobei der Abstand zum Haus hier natürlich hilft.

Ansonsten kann ich nach einem Jahr sagen: Ich würde alles genauso noch einmal machen.
