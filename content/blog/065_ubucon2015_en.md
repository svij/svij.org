---
title: "9th Ubucon 2015 in Germany"
categories: ["Ubuntu"]
date: "2015-05-24 23:45:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

This year we're going to organize the 9th Ubucon in Germany. This time it will
be held in the capitol Berlin starting on the 23th and ending on the 25th October 2015.
I was a member of the organization team for the since 2013, but this year I am
the head of the organisation team.

We also started the [„Call for Papers“](http://ubucon.de/2015/cfp). The main language
is German, but we might also allow talks in English. I'm really looking forward
to the event. And I'm also hoping that more people will visit the Ubucon. In the
last years we had roughly 100-150 attendees. This years slogan is „Community in Touch“,
which includes the opportunity of getting in touch with the community and „Ubuntu Touch“.
