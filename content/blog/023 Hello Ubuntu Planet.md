---
title: "Hello Ubuntu-Planet!"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2014-02-03 16:00:00+02:00"
toc: true
---

Hello Ubuntu Planet!

I am the first Ubuntu Member in 2014, and I thought it might be a good thing
to say hello. My membership application got approved on the 2nd January 2014.
You can find my application [in the wiki](https://wiki.ubuntu.com/svij).

Anyway, I am introducing me here again. My Name is Sujeevan Vijayakumaran, mostly
online active under the name „svij“. On the contrary of my name, I am German and
I live in the Ruhrgebiet, working as a full time Software Developer and still studying
IT at a small private university in Dortmund.

The most of the people here should not know me. My contributions to Ubuntu are mostly
LoCo-Activities in Germany. The biggest German Ubuntu website is [ubuntuusers.de](http://ubuntuusers.de).
It is a really big website with a great wiki, with forums, a planet and our
own news site „Ikhaya“. Back in 2010 I started contributing to the German
version of the Ubuntu Weekly Newsletter. After a few months I joined the Ikhayateam
and started writing articles about Ubuntu, Linux and Open Source Software.
Last year I was elected to one of leader of ubuntuusers.de. This year, I got
reelected, so I am doing this „job“ again.

Beside my contribution to ubuntuusers.de I am also active in representing Ubuntu
at several conferences in Germany. I have attended the [OpenRheinRuhr](http://openrheinruhr.de)
three times, the CeBIT 2012 (just one day), the LinuxTag in Berlin (also just one day),
the Software Freedom Day in Hamburg, and two German Ubucons, which took place
in Berlin (2012) and Heidelberg (2013). At last years Ubucon I was also a member
of the organization team. I will also be co-organising this years Ubucon
in Katlenburg.

Besides my Ubuntu-related contribution, I was also an editor of [„freiesMagazin“](http://freiesmagazin.de)
for about a year. freiesMagazin is a free magazine, with articles about Open
Source Software. The magazine is comparable with FullCircleMagazine. I left
the team a few months back, but I am still contributing as an article writer
to that project.

Both two projects does not include coding. But I am also starting my first small
software project, which you can find on my [github-Account](http://github.com/svijee).
I am currently writing an Android App for [Taskwarrior](http://taskwarrior.org),
which includes support for Taskwarrior 2.3 main feature: syncing. That project
did not get much love in the last few months, but I am really trying to finish
and publish my first version in the near future.

In the future, I will sometimes post articles on the Ubuntu planet. My blog
was German-only until today, but this might change. I will probably post only a few
articles to the ubuntu planet. But we will see, maybe I will post more ;-).

My blog is static and is made with [pelican](http://getpelican.com). Therefore
I do not have a comment section here. If you want to contact me, you can send
a mail to svij at ubuntu dot com ;-).

PS: You can also find me on [Twitter](http://twitter.com/svijee) and [Google+](https://plus.google.com/u/0/+SujeevanVijayakumaran)


