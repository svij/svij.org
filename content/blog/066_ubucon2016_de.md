---
title: "Ubucon Europe 2016 in Essen"
categories: ["Ubuntu"]
date: "2015-06-05 22:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Vor einigen Tagen haben wir die [Ubucon 2015 angekündigt](/blog/2015/05/24/ubucon-2015-in-berlin/),
welche dieses Jahr in Berlin stattfinden wird. Durch die Planungen für dieses Jahr
haben wir glücklicherweise auch schon einen Ort für das nächste Jahr so gut wie sicher.

Nächstes Jahr würde die zehnte Ubucon in Deutschland stattfinden und wir dachten,
dass es doch eine gute Gelegenheit wäre, die Ubucon deutlich zu erweitern. Bereits vor
einigen Monaten hatte [Costales](https://plus.google.com/u/0/+MarcosCostales/posts/jLzUVbFmHuv)
auf Google+ an diverse „Ubuntu Phone Insider“ gefragt, ob eine europäische Ubucon
nicht toll wäre, was von allen bejaht wurde. Bislang passierte da aber nach knapp
drei Monaten nicht viel… bis jetzt! :)

### Ubucon Europe!

Wir - das Organisationsteam der deutschen Ubucon - erweitern damit die Ubucon
nächstes Jahr zur *Ubucon Europe 2016*! Wir sind aktuell noch in einer sehr
frühen Phase der Planung und wir brauchen daher auch Feedback. Die Ubucon Europe
wird in Essen stattfinden, an einem Wochenende im September oder Oktober. Der genaue
Ort ist noch nicht fix, wir haben allerdings noch zwei Optionen innerhalb von Essen
offen, wo die Ubucon Europe unterkommen kann. Dadurch, dass Essen im Ruhrgebiet ist,
lässt es sich gut über den Zug-Fernverkehr erreichen und auch der Düsseldorfer Flughafen
ist für internationale Gäste nicht fern.

### Feedback!

Wir haben erst kürzlich die Domain [ubucon.eu](http://ubucon.eu) gesichert, wo
zukünftige Ankündigungen erfolgen werden. Zu Beginn brauchen wir allerdings Feedback
für die weiteren Planungen. In den letzten Jahren hatten wir meistens um die 150
Teilnehmer bei einer deutschen Ubucon, wie die Teilnehmerzahl bei einer Ubucon
Europe aussehen würde, können wir zur Zeit nicht erahnen.

Dieser Blogpost läuft auch in Englisch über [planet.ubuntu.com](http://planet.ubuntu.com),
um die Resonanz zu erfragen. Auch hier stelle ich an die deutsche Community die
Frage, wer zu einer europäischen Ubucon nach Essen kommen würde und ob Interesse
daran besteht, einen Vortrag oder Workshop in englischer Sprache zu halten. Ob oder
in welcher Form wir deutsche Vorträge zulassen, ist noch nicht diskutiert worden.
Nebenbei suchen wir natürlich weiterhin Leute, die bei der Ubucon mithelfen wollen.
Dies betrifft nicht nur die Ubucon Europe im nächsten Jahr sondern
auch die diesjährige Ubucon in Berlin!
