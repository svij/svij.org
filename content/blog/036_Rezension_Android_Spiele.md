---
title: "Rezension: Spieleprogrammierung mit Android Studio"
categories: ["Rezension"]
date: "2014-10-05 10:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Das Buch „Spieleprogrammierung mit Android Studio“ vom Autor Uwe Post erschien
2014 in der ersten Auflage. Auf fast 400 Seiten erlernt man sowohl den Umgang
mit Android Studio als auch die Spieleprogrammierung unter Android.

### Was steht drin?

Das Buch ist in elf Kapiteln unterteilt und umfasst 381 Seiten. Der Autor
Uwe Post ist Chefentwickler einer Smartphone-Spielerfirma. Veröffentlicht
wurde es vom Galileo Computing Verlag.

Das erste Kapitel ist die „Einführung“ in der ein kurzer Überblick über
Spiele-Klassiker gegeben wird. Im darauffolgenden zweiten Kapitel erfolgt
die erste Einführung in Android Studio, in dem nicht nur die Installation
kurz erläutert wird, sondern auch einige Tipps zur Arbeitserleichterung gegeben
werden. Danach folgt auch schon das erste kleine Spiel, welches im dritten
Kapitel erstellt und erklärt wird. Das aufgeführte Spiel wird dabei
Schritt für Schritt aufgebaut. Der Fokus liegt in diesem Kapitel auf die
Grundlagen eines Android-Projekts unter Android Studio.

Im vierten Kapitel folgt erstmals der Einsatz von Bildbearbeitungsprogrammen.
In diesem Buch wird dabei grundsätzlich Gimp genutzt. Weiterhin werden Animationen
behandelt, so dass man sinnvolle Animationen in Spielen einbauen kann.
Smartphones besitzen grundsätzlich sehr viele Sensoren, die man sehr gut
für Spiele nutzen kann. Das fünfte Kapitel behandelt daher die Nutzung vom
Lagesensor. Im sechsten Kapitel folgt dann die Nutzung von Audio und Musik-Stücken.

Eine neue relativ neue Art von Spielen sind location-based Spiele. Im siebten Kapitel
wird ein kleines Spiel programmiert, in dem man sich frei herumbewegen muss.
Die Geolokation erfolgt mittels GPS.
Die letzten Kapitel im Buch vertiefen die Art und Weise der Spiele
sowie auch die Komplexität. Im achten Kapitel wird die Bibliothek libgdx verwendet um ein
schnelles 2D-Spiel zu erzeugen, während es im neuten Kapitel an die Königsdisziplin
geht: 3D-Spiele. Auch für 3D-Spiele wird die Open Source Bibliothek libgdx verwendet.
Die Beschreibungen und Erläuterungen gehen relativ weit, so dass man nicht nur
ein Szenenbild aufbaut, sondern auch ein kleines 3D-Labyrinth erstellt.

Die letzten beiden Kapitel beziehen sich nicht mehr direkt auf die
Spiele-Programmierung. Das zehnte Kapitel beschäftigt sich mit Social-Gaming,
also wie man den Spielstand seines Spiels in Google Play Games Services
einbindet oder auch wie man einen Facebook-Login realisiert. Das letzte
Kapitel behandelt eher den wirtschaftlichen Aspekt von Spielen, nämlich
die Monetarisierung. Der Autor gibt einige Tipps, welche Arten von Monetariserung
es gibt, und wie man sie sinnvoll einsetzen kann, um im Idealfall gutes Geld
zu verdienen.

### Wie liest es sich?

Das Buch liest sich durchwegs sehr gut. Der Autor entwickelt in jedem Kapitel
ein eigenes Spiel, welches zum Thema passt. Die Erläuterungen sind meistens
nicht zu kurz, aber auch nicht sonderlich lang. Nach dem ein oder anderen
Code-Schnipsel folgt meist eine prägnante Erklärung was genau getan wird.

Für dieses Buch sind nicht nur gute Java-Kenntnisse Pflicht, sondern auch
grundsätzliches Verständnis der Programmierung von Android-Apps sind hier
sehr sinnvoll. Installationsanleitungen sind kurz gehalten, die erste Installation
von Android Studio wird nicht im Detail erklärt, da dies die Leser des Buches
sowieso können sollten. Praktisch ist vorallem, dass alle Anwendungen im Buch Open Source
Anwendungen sind. Neben Gimp, wird auch Inkscape kurz thematisiert.
Der Autor selbst nutzt nach eigenen Angaben Ubuntu, das Buch ist allerdings
nicht zwangsläufig auf Ubuntu ausgerichtet, sondern behandelt alle drei
Betriebssysteme zu gleichen Anteilen.

### Kritik

Das Buch bietet einen guten Einstieg in die Programmierung von Spielen für Android.
Die Erklärungen sind durchgängig gut zu verstehen und vieles lässt sich einfach
nachvollziehen. Man merkt, dass der Autor sehr gute Erfahrungen in der
Spiele-Programmierung besitzt. Es kommen durchaus viele Hinweise, wenn einige
Tools oder Verfahren umständlich oder kompliziert sind. Die Schreibweise
von Uwe Post ist zudem sehr angenehm zu lesen.

Für knapp 35€ bekommt man daher ein gutes Buch über die Android-Spieleprogrammierung.
Der Leser gewinnt dabei ein sehr guten Einstieg in die Welt der Spiele, worauf
man dann schon fast sein ersten Spiel programmieren und veröffentlichen kann.
Das Buch lässt sich insbesondere für die jenigen empfehlen, welche gute
Erfahrung in der Android-Entwicklung besitzen und in die Spiele-Programmierung
einsteigen wollen.


#### Buchinfo:

**Titel:** [Spieleprogrammierung mit Android Studio](https://www.galileo-press.de/spieleprogrammierung-mit-android-studio_3537/)

**Autor:** Uwe Post

**Verlag:** Galileo Computing, 2014

**Umfang:** 381 Seiten

**ISBN:** 978-3-8362-2760-5

**Preis:** 34,90 (gebunden), 29,90 (E-Book)

Da es schade wäre, wenn das Buch bei mir lediglich im Regal
verstaubt, wird es verlost. Dies ist „drüben“ bei [freiesMagazin](http://www.freiesmagazin.de/20141005-oktoberausgabe-erschienen)
in den Kommentaren möglich.
