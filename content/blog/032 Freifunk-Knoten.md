---
title: "Mein erster Freifunk-Knoten"
categories: ["Open Source"]
date: "2014-08-10 10:00:00+02:00"
toc: true
---

Die WLAN-Abdeckung in unserem Wohnzimmer war schon seit längeren eher schlecht
als recht. Der einzige WLAN-Router stand auf einer anderen Etage und daher
war ein weiterer WLAN Access-Point im Wohnzimmer nicht schlecht, um auch dort
mit dem Laptop, Smartphone und Tablet eine anständige Konnektivität zu haben.

Eher zufällig bin ich über einen Podcast auf die Idee gekommen auch einen
Freifunk-Knoten aufzusetzen. Ich bestellte mir daraufhin ein [TP-Link WDR4300](http://www.tp-link.com.de/products/details/?model=TL-WDR4300#over)
für knapp 45€. Die einzelnen Freifunk-Communities bieten sowohl eigene
flashbare Firmwares als auch Anleitungen. Die [Anleitung](http://freifunk-ruhrgebiet.de/anleitung/)
von Freifunk-Ruhrgebiet war dabei gut beschrieben und nach wenigen Minuten
stand mein erster eigener Freifunk-Knoten.

Es gab dabei nur ein Problem für mich: Ein zweites privates WLAN-Netz ließ sich
über das Web-Interface der Freifunk Firmware nicht setzen. Nachdem ich etwas
gesucht hab, stieß ich auf ein [GitHub-Issue](https://github.com/freifunk-gluon/gluon/issues/122)
mit genau meiner Anforderung, welche erst vor wenigen Tagen umgesetzt wurde.

Um dies umzusetzen muss man zum jetzigen Zeitpunkt zunächst das Git-Repository
[freifunk-gluon/packages](https://github.com/freifunk-gluon/packages) klonen.
Mit folgenden Befehl kann man dann ganz einfach die benötigten Dateien auf den
Router schieben:

    scp -r gluon/gluon-luci-private-wifi/files/* root@192.168.1.1:/

Nach einem Reload des Web-Interfaces kann man nun auch ein privates WLAN
Access-Point aufsetzen. Damit konnte ich nicht nur mein Hauptziel umsetzen,
sondern zum Glück auch den zweiten Freifunk-Knoten Castrop-Rauxels online
setzen.
