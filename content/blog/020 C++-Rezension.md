---
title: "Rezension: Grundkurs C++ (Galileo Computing)"
categories: ["Rezension"]
date: "2013-10-22 19:45:00+02:00"
toc: true
---

Das Buch „Grundkurs C++“ vom Autor Jürgen Wolf bietet einen guten Einstieg
in die Programmiersprache C++. Trotz einem günstigen Preises ist es ausführlich
genug um einen umfassenden Einblick in die Sprache C++ zu liefern.

### Was steht drin?

Das Buch enthält insgesamt 12 Kapitel und umfasst dabei 447 Seiten. 
Die Kapitel bauen inhaltlich aufeinander auf, sodass es sich empfiehlt das Buch
von vorne bis hinten durchzuarbeiten. Die ersten fünf Kapitel decken dabei die
Grundlagen von C++ ab.

Das Buch beginnt im ersten Kapitel mit
einem kleinen „Einstieg in die Welt von C++“, welches nur wenige Seiten umfasst.
Dabei wird auf die Historie, die Eigenschaften und Merkmale von C++ eingegangen.

Im anschließenden zweiten Kapitel wird ausführlich auf die Verwendung von
den Basisdatentypen von C++ eingegangen. Zu Beginn erfolgt auch die erste
„Hallo Welt“-Anwendung. In den folgenden Abschnitten wird dann Schritt für
Schritt zunächst die Standardein- und ausgabe erklärt, und im Anschluss
erfolt die Erläuterungen mit Beispielen zu den Basisdatentypen.

Das dritte Kapitel behandelt das Arbeiten mit Funktionen und Kontrollstrukturen.
Die Funktionen von Kontrollabfragen werden mit Symbolen und vorallem kleinen Grafiken für
Programmieranfänger sowohl gut erläutert, als auch gut grafisch dargestellt.
Die Definition und Deklaration von Funktionen wird genauso thematisiert, wie auch
die Präprozessor-Direktiven.

Zu den letzten Grundlagenkapitel gehört das vierte Kapitel „Höhere und fortgeschrittene
Datentypen“. In diesem Kapitel wird vorallem auf Zeigerarithmethik, Referenzen
und das Arbeiten mit Zeichenketten eingegangen. Komplettiert wird das Kapitel 
mit Arrays, der Nutzung von höheren Datentypen bei Funktionen sowie dynamische
Speicherobjekte.

Das fünfte Kapitel gehört nicht mehr zu den Grundlagenkapitel. Es ist ein
relativ kurzes Kapitel und behandelt die Modularisierung von C++-Anwendungen
mit Namespaces. Es folgt darauf aufbauend ein ausführliches Kapitel zu
Klassen in dem auf die wichtigen Merkmale wie Konstruktoren, Destruktoren
und Elementfunktionen eingangen wird. Im siebten Kapitel wird anschließend
auf Objekte und Klassenelemente ausführlich eingangen.

Innerhalb der nächsten Kapitel wird außerdem auf das Überladen von Operatoren
sowie die Vererbung von Klassen besprochen. Ein etwas fortgeschrittenes
Thema sind Templates. Der Autor läutert in dem Kapitel nicht nur die Funktionsweise
und den Einsatz von Templates, sondern gibt auch Tipps, wie man effektiv
mit Templates arbeitet.

Zum Schluss des Buches gibt es noch ein Kapitel über die Fehler-Behandlung
in C++ sowie ein weiteres Kapitel in dem ausführlicher auf die Neuerungen
in C++11 eingegangen wird.

### Wie liest es sich?

Der Autor hat einen angenehmen Schreibstil und erklärt die angesprochenen Themen
gut und sicher. Programmiereinsteiger, die bisher noch keinen Kontakt zu
Programmiersprachen hatten, können aus diesem Buch einiges lernen. Alle
wichtigen Themen die man für das Erlernen von C++ braucht, werden ausführlich
angesprochen. An vielen Stellen werden häufig Hinweise darauf gegeben, wie
etwaige Dinge unter dem neuen C++11-Standard zu erledigen sind. Am Ende von jedem
Kapitel befinden sich Übungsaufgaben zur Lernkontrolle, welche in drei Leveln
unterteilt sind. Level 1 beinhaltet die einfachen Fragen, während Level 2 und
Level 3 jeweils schwierige Fragen enthält. Die Antworten zu den Fragen befinden
sich dabei auf den letzten Seiten am Ende des Buches. Das praktische an den Übungen ist,
dass besonders die Aufgaben aus Level 3 zum Programmieren anregen und dabei
auch für Anfänger nicht zu schwierig ist. Jeder Leser kann dabei seinen Lernfortschritt
selbst erfassen, da man dann Teile eines Kapitels erneut durchnehmen muss, um
auf die Lösung zu den Aufgaben zu kommen.

### Kritik

An diesem Buch gibt es fast nichts negatives auszusetzen. Mit über 400 Seiten
ist das Taschenbuch prall gefüllt mit allen nötigen Dingen die man 
als C++-Programmierer wissen muss,
um mit der Programmierung in C++ starten zu können. Das Buch heißt nicht
umsonst „Grundkurs C++“, da es wirklich nur eine Einführung in C++ geben soll.

Das Buch kostet nur 9,90€ und siedelt sich damit im untersten Preissegment an. Für
diesen niedrigen Preis bekommt der Leser sowohl eine gute als auch preisgünstige
Leistung geboten. Besonders für Programmiereinsteiger in C++ bietet sich das Buch
gut an, da sie nur die grundlegenden Themen behandelt. Programmierer mit C++
Erfahrung können erwartungsgemäß diesem Buch nicht sehr viel abgewinnen, da dies
schließlich nicht die Zielgruppe darstellt.

#### Buchinfo

**Titel:** [Grundkurs C++](http://www.galileocomputing.de/katalog/buecher/titel/gp/titelID-2313)

**Autor:** Jürgen Wolf

**Verlag:** Galileo Computing

**Seiten:** 447

**ISBN:** 978-3-8362-2294-5

**Preis:** 9,90€
