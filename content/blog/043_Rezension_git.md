---
title: "Rezension: Git - kurz & gut"
categories: ["Rezension"]
date: "2014-11-02 20:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Das Buch „Git - kurz & gut“ vom Autor Sven Riedel ist ein kompaktes Taschenbuch
welches in die Nutzung von Git einleitet. Es richtet sich vorallem an
Umsteiger von Nutzern anderer Versionsverwaltungsprogrammen.

### Was steht drin?

Das Buch umfasst neun Kapitel, abgedruckt auf 192 Seiten. Daneben enthält das
Buch ebenfalls noch fünf Anhänge.

Das erste Kapitel ist die Einleitung. In diesem Teil wird zunächst geklärt,
was Bestandteil des Buches ist und wie die Konventionen sind. Im zweiten Kapitel
folgt anschließend der direkte Einstieg in Git. Dort werden in wenigen Seiten
Konzepte von Git erklärt, etwa den Arbeitsbaum, den Index und was Commits,
Branches, Refs und Tags sind. Weiterhin wird kurz das Thema der Remote-Repositories
angeschnitten. Das dritte Kapitel beinhaltet die Installation von Git. Dabei wird nicht nur
die Installation von Binär-Paketen beschrieben, sondern auch die Kompilierung
aus dem Source-Code. 

Während die ersten drei Kapitel eher einleitende Kapitel waren, geht es erst
in Kapitel 4 richtig los. In diesem Kapitel wird ein erstes Repository angelegt, dieses wird
anschließend konfiguriert und einige Funktionen von Git erläutert. Darunter
fallen zunächst einmal die Versionierungsbefehle, also konkret handelt es
sich dabei um das Hinzufügen von Dateien in den Index, das Erstellen das Rebasen
von den ersten Commits. Weiterhin wird auch noch der Stash, das Mergen und
die Nutzung von Remote-Repositories beschrieben.

Das fünfte Kapitel behandelt Tipps und Tricks. Dies sind tiefergreifende
Funktionen von Git, die man im tag-täglichen Leben stellenweise eher seltener braucht.
Darunter fallen etwa das Rückgängigmachen von Merges, das Rückgängigmachen
von gelöschten Branches oder auch die Nutzung von eigenen Git Aliases.
Im nachfolgenden sechsten Kapitel wird recht kurz ein Einblick in ein mögliches
Branching-Modell und wie man es mit Git umsetzt.

Die letzten drei Kapitel sind eher ergänzende Kapitel. Zunächst geht es um
die Nutzung von Github, welches ganz bewusst die oberflächlichen Funktionen
von Github erläutert, da sich dort regelmäßig etwas ändert. GUI-Clients
von Git wurden bis zu Kapitel 8 noch nicht behandelt, was an dieser Stelle
dann getan wird. Das Buch schließt mit dem 9. Kapitel ab, in dem die
Installation und Konfiguration eines Git-Clients mit einem Subversion-Server
vermittelt wird.

Der fünf-geteilte Anhang enthält weitere Informationen, etwa Vergleiche
zwischen Git und Subversion-Befehlen, Git-Log-Formatierungen, Formate von Commit-Namen,
sowie einen Ausblick auf Git 2.0.

### Wie liest es sich?

Das Buch ist nicht nur ein Taschenbuch sondern auch eine Referenz. Diesen Fakt
merkt man dem Buch auch an, da die Erkläuterungen meistens recht kurz gehalten
sind und diese sind daher meistens auch nicht sonderlich detailreich. Es sind
prinzipiell zwar keine Kenntnisse in der Nutzung von anderen 
Versionsverwaltungsprogrammen notwendig, doch könnten Kenntnisse für dieses Buch
schon von Vorteil sein. Es werden häufiger Vergleiche zu Subversion gezogen und
im selben Schritt erläutert, welche ähnliche lautende Befehle jeweils andere
Auswirkungen in Git haben.

Screenshots, Bilder oder Diagramme sind recht spärlich vorhanden. Manchmal
wäre das ein oder andere zusätzliche Diagramm nützlich, um die Arbeit von
und mit Git besser verbildlichen zu können. Dies wäre vorteilhaft, da der Leser
dann im idealfall schneller versteht. Es sind zwar Abbildungen vorhanden,
die Anzahl ist allerdings relativ überschaubar.

### Kritik

Dieses Buch richtet sich grundsätzlich eher an Subversion-Umsteiger statt
an komplette Neulinge. Diese Information steht zwar in der Einleitung, allerdings
steht dies nicht auf der Zusammenfassung auf der Rückseite des Buches.
Leser, die noch keinerlei Erfahrung mit Versionsverwaltungsprogrammen haben,
könnten Schwierigkeiten mit diesen Buch haben. Die übrigen Leser mit Erfahrung
sollten hingegen keinerlei Probleme haben. Dieses Taschenbuch ist ziemlich
kompakt gehalten, was sich stellenweise am Inhalt auswirkt, da die Erläuterungen
somit ebenfalls ziemlich kurz sind. Grundsätzlich ist das Buch
daher wirklich nur für Umsteiger aus anderen Versionsverwaltungsprogrammen
oder für Sachkundige zu empfehlen, welche ein kleines, günstiges Buch zum
Nachschlagen brauchen.

### Buchinfo:

**Titel:** [Git - kurz & gut](http://www.oreilly.de/catalog/gittb2ger/)

**Autor:** Sven Riedel

**Verlag:** O'Reilly

**Umfang:** 192 Seiten

**ISBN:** 978-3-95561-734-9

**Preis:** 12,90€ (broschiert), 9,90€ (E-Book)

Da es schade wäre, wenn das Buch bei mir im Regal verstaubt,
wird es verlost. Dies ist „drüben“ bei [freiesMagazin](http://www.freiesmagazin.de/20141102-novemberausgabe-erschienen)
in den Kommentaren möglich.
