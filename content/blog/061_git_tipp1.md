---
title: "Git Tip #1: Sub-Directorys, Single-Branches und Repositorys ohne Historie ausschecken"
categories: ["Git"]
tags: ["ubuntuusers-planet"]
date: "2015-03-31 16:15:00+02:00"
slug: "git-tip-1-sub-directorys-single-branches-und-repositorys-ohne-historie-ausschecken"
toc: true
---

Bevor meine Git-Tutorial Reihe weiter geht, fange ich mal eine kleine neue
Reihe an. In dieser „Git Tip“-Reihe werde ich hin und wieder mal, das ein
oder andere kleine Git-Feature oder Problem beleuchten und wie man es lösen kann. Meistens wohl dann, wenn ich selbst auf ein Problem stoße.

Vor allem in der Qualitätssicherung kommt es häufiger vor, dass man ein Repository
mit nur einen Branch klonen will, ein Repository gänzlich ohne
Historie klonen will oder nur ein Sub-Directory klonen will.

Der erste Punkt ist recht einfach zu lösen. Der folgende Befehl klont genau einen Branch:

	# git clone <url> --branch <branchname> --single-branch
	git clone git@github.com:svijee/taskwarrior-androidapp.git --branch master --single-branch

Wie man sieht, bringt diese Funktion Git selbst mit. Es ist also kein wirkliches Problem.

Auch das Klonen eines Repositorys _ohne_ Versionshistorie lässt sich mit den Boardmitteln lösen:

	# git clone --depth 1 --branch <branch> <url>
	git clone --depth 1 --branch master git@github.com:svijee/taskwarrior-androidapp.git

In diesem Fall wird nur der aktuelle Stand des Repositorys aus dem Branch „master“ geklont. Je nachdem wie viele Revisionen man aus der Versionshistorie möchte, kann man den Parameter "depth" entsprechend anpassen. Man kann die Historie auch ganz einfach prüfen, in dem man ein nachfolgendes "git log"
ausführt, welches nur einen Commit anzeigen sollte:

	$ git log
	commit 098c0fd18317ad3abe4ecad3a007ff6a6dcd501b
	Author: Sujeevan Vijayakumaran <mail@svij.org>
	Date:   Fri Sep 19 21:19:40 2014 +0200

	    Upgraded compileSdkVersion from 19 to 20

Anders sieht es allerdings aus, wenn man nur einen Unterordner klonen will. Dies ist nicht direkt mit Git selbst möglich, hier muss man sich ein wenig „tricksen“.

	# git archive --remote=<url> <branch> <sub-dir> | tar xvf -
	git archive --remote=git@github.com:svijee/taskwarrior-androidapp.git master taskwarrior | tar xvf -

Hier wird der Befehl „git archive“ „missbraucht“, um einen Unterorder zu klonen. Der Befehl „git archive“ ist dafür da, aus dem Git-Repository ein Tar-Archiv zu erzeugen. Dieser kann allerdings auch Tarball aus einem Unterverzeichnis erstellen. Mit einer Pipe kann man sich den Ordner dann auch direkt und ohne Umwege auf die Festplatte schreiben lassen.
