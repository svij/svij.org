---
title: "Statische Webseiten mit Pelican erstellen"
categories: ["Web"]
tags: ["Web"]
date: "2014-07-02 21:25:00+02:00"
toc: true
---

Heutzutage werden viele Webseiten im Internet dynamisch generiert, seien
es Nachrichten-Seiten, Social Networks oder auch Blogs. Statische Webseiten
gehören eigentlich schon fast der Vergangenheit an. Ein Vertreter um statische
Blogs zu generieren ist Pelican.

## Statisch?

Webseiten werden entweder dynamisch oder statisch erzeugt. Dynamische Webseiten
findet man quasi an jeder Ecke des Internets, seien es Blogs die
mit Content-Management-Systemen wie Wordpress oder Foren die mit phpBB laufen.
Die einzelnen Seiten
werden bei dynamischen Webseiten in der Regel beim Aufruf der Webseite generiert.
Als bloßer Besucher einer Webseite merkt man in der Regel keinen großen Unterschied,
im Hintergrund passieren dann allerdings doch ein paar Dinge. In Blogs oder
Foren werden bei einem Seitenaufruf unter anderem Datenbank-Abfragen getätigt,
welches Artikel, oder Kommentare enthält. Aus den Daten werden anschließend
auf dem Server der HTML-Code für diese spezifische Seite generiert und dem
Besucher zurückgeliefert. Häufig werden dabei die Seiten gecached, um bei einer
gleichen Abfrage die Seite schneller zurückliefern zu können.

Bei statischen Webseiten läuft das ganze anders ab. Die HTML-Seiten werden hierbei
nämlich nicht bei jedem Abruf dynamisch erzeugt, sondern liegen als einzelne
HTML-Seiten schon vorab auf dem Server. Wenn man also einen statischen Blog
betreiben will, dann schreibt man den Blogpost auf dem heimischen Rechner
und rendert dort die fertigen HTML-Seiten, welche dann auf dem Webserver
hochgeladen werden. Pelican kommt genau hier ins Spiel. Mit Pelican ist es möglich
solche statischen Blogs zu erstellen. Statische Blogs haben diverse Vor- und
Nachteile. Der wohl größte Vorteil ist, das statische Blogs sehr schnell sind
und nicht von anderen Diensten im Hintergrund, etwa von Datenbanken, ausgebremst werden.
Daraus resultiert auch eine geringere Serverlast, da weniger Dienste laufen müssen,
da nur fertige HTML-Dateien zurückgeliefert werden. Ein Webserver müsste daher
beispielsweise ohne PHP und MySQL-Modul laufen. Nachteillig bei einem statischen
Blog ist, dass man fast keine Interaktion mit Lesern führen kann. Kommentare sind
von Haus aus dadurch nicht möglich. Umgehen kann man dies allerdings mit externen
Diensten.

## Allgemeines & Features

Pelican ist in Python geschrieben und ermöglicht das generieren von statischen
Webseiten. Um den Inhalt einer Webseite zu generieren, müssen die einzelnen
Seite in einer Auszeichnungssprache vorliegen. Pelican unterstützt drei
verschiedene Auszeichnungssprachen: [reStructuredText](http://docutils.sourceforge.net/rst.html),
[Markdownn](http://daringfireball.net/projects/markdown/) und [AsciiDoc](http://www.methods.co.nz/asciidoc/).

### Installation

Die Installation von Pelican ist über zwei Wege möglich. Entweder man nutzt
die Python-Tools „easy_install“ oder „pip“ um sich die aktuelle Version zu installieren.
Durch das ausführen von

    $ easy_install pelican

oder 

    $ pip install pelican

wird die aktuellste Version 3.3 von Pelican auf dem System installiert. Alternativ
kann man auch Pelican über die Paketverwaltung der Distribution installieren.
Unter Ubuntu heißt das Paket beispielsweise „python-pelican“. Der Nachteil ist
hierbei, dass man unter Umständen nicht die aktuellste Version besitzt. In
Ubuntu 14.04 ist hingegen die aktuelle Pelican-Version 3.3 verfügbar.

### Erste Webseite anlegen

Nach der Installation kann man anschließend recht schnell eine einfache Seite
mit Pelican erzeugen, dazu gibt es den Befehl __pelican-quickstart__, der ohne
zusätzliche Parameter aufgerufen werden kann. Beim Ausführen des Befehls
werden viele Fragen gestellt, die beantwortet werden müssen um das ersten Blog
zu erstellen. Als Beispiele lege ich hier mal einen Blog namens „freiesBlog“ an.

    $ pelican-quickstart 
    Welcome to pelican-quickstart v3.3.0.

    This script will help you create a new Pelican-based website.

    Please answer the following questions so this script can generate the files
    needed by Pelican.

    > Where do you want to create your new web site? [.] freiesBlog
    > What will be the title of this web site? Ein freies Blog
    > Who will be the author of this web site? svij
    > What will be the default language of this web site? [en] de
    > Do you want to specify a URL prefix? e.g., http://example.com   (Y/n)  
    > What is your URL prefix? (see above example; no trailing slash) http://example.com
    > Do you want to enable article pagination? (Y/n) Y
    > How many articles per page do you want? [10] 10
    > Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) Y
    > Do you want an auto-reload & simpleHTTP script to assist with theme and site development? (Y/n) Y
    > Do you want to upload your website using FTP? (y/N) N
    > Do you want to upload your website using SSH? (y/N) Y
    > What is the hostname of your SSH server? [localhost] server
    > What is the port of your SSH server? [22] 22
    > What is your username on that server? [root] svij
    > Where do you want to put your web site on that server? [/var/www] /var/www
    > Do you want to upload your website using Dropbox? (y/N) N
    > Do you want to upload your website using S3? (y/N) N
    > Do you want to upload your website using Rackspace Cloud Files? (y/N) N
    Done. Your new project is available at /home/sujee/Repositories/freiesBlog

Während der Erstellung werden also durchaus viele Dinge abgefragt die direkt
beim Beginn relevant sind. Alles lässt sich allerdings auch nachträglich ändern.
Die wichtigsten Fragen sind hier der Pfad für die Dateien, der Title der Webseite,
der Autorenname sowie die Domain. Praktisch ist allerdings, dass man direkt
angeben kann auf welchen Server die Webseite hochgeladen wird. Egal ob FTP
oder SSH, dies kann ziemlich einfach angegeben werden. In diesem Beispiel wird
SSH verwendet, statt FTP.

Pelican hat also den Ordner „freiesBlog“ erzeugt, in dem alle nötigen Dateien
der Webseite liegen. Dies wären zum einen die Unterordner „content“ und zum
anderen der Ordner „output“.
In „content“ liegen die Roh-Inhalte der Webseite und unter „output“ liegen
anschließend die fertig generierten HTML-Seiten der Webseite, die auf einen
Server hochgeladen werden können. Weiterhin sind zwei Konfigurationsdateien
vorhanden: __pelicanconf.py__ und __publishconf.py__. In der Datei pelicanconf.py werden
eher grundsätzliche Eigenschaften der Webseite konfiguriert, während unter publishconf.py
Konfigurationen zum Veröffentlichen der Webseite liegen. Näheres dazu folgt
im Verlaufe des Artikels. Übrig bleiben noch drei Dateien: fabfile.py, Makefile
sowie develop_server.sh. Mittels dem Python-Tool Fabric ist es möglich die
fabfile.py-Datei zu nutzen. Äquivalent dazu gibt es das Makefile, welches mit
dem Make-Kommando genutzt werden kann. Mit make als auch mit fabric lassen
sich ziemlich ähnliche Dinge erledigen. Aus diesem Grund werde ich grundsätzlich
nur auf make eingehen:

    $ make html

Der obige Befehl werden die HTML-Dateien aus den angelegten Seiten generiert.
Im Hintergrund wird allerdings „pelican“ aufgerufen. Sowohl das Makefile als auch
fabfile.py rufen im Hintergrund den Befehl „pelican“ mit diversen Parametern auf.
Beide Tools dienen ausschließlich dazu, ein einfaches Interface zur Nutzung zu
bieten. Mit __make html__ wird ein einziges Mal die HTML-Dateien der Webseite
erzeugt, wenn man allerdings häufiger etwas ändert und die Änderungen begutachten
möchte, dann bietet es sich an __make regenerate__ aufzurufen. In dem Falle werden
Änderungen an den Quelldateien automatisch erkannt und die Webseite wird neu
generiert. Noch praktischer ist es, wenn die Webseite mittels eines einfachen
HTTP-Webservers über dem Browser erreichbar ist. Dafür gibt es den Befehl
__make serve__. Dieser startet einen Server auf Port 8000, sodass die Webseite
unter der URL __http://localhost:8000__ erreichbar ist. Möchte man nun __make regenerate__
und __make serve__ kombinieren, gibt es den Befehl __make devserver__, bei dem
sowohl die HTML-Dateien generiert werden, als auch ein Server gestartet wird.
Der Server lässt sich anschließend mit __./develop_server.sh stop__ wieder stoppen.

Mit diesen einfachen Befehlen kann man mit Pelican arbeiten. Wenn man beim
Einrichten der Webseite die SSH-Zugänge konfiguriert hat, kann man die Webseite
mit __make rsync_upload__ ganz einfach auf den Server hochladen. Die Webseite
ist dann sofort online.

### Der Inhalt

In Pelican werden zwischen zwei verschiedenen Inhaltstypen unterschieden. Dies
sind zum einen Seiten („pages“) und zum anderen Artikel.

Wie zu Beginn dieses Artikels schon erwähnt, lassen sich Artikel in reStructuredText,
Markdown oder AsciiDoc schreiben. Exemplarisch als Erläuterung folgt nun
ein Artikel in Markdown.

    title: Pelican-Beispielartikel in Markdown
    categories: Sonstiges
    date: 2014-27-04 13:37

    # Lorem Ipsum

    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
    tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At 
    vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
    no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit 
    amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut 
    labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
    et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata 
    sanctus est Lorem ipsum dolor sit amet.

Das obige Beispiel ist ein kleines Beispiel, wie ein Artikel in Markdown
aussehen kann. Vor dem eigentlichen Artikel folgt der Header in dem einige
Informationen angegeben werden können.
__Title__ gibt selbstredend den Titel des Blog-Artikels an.
Mit Einträgen in __Category__ können Kategorien eingetragen werden, in welche
der Artikel erscheinen soll. Die Kategorien enthalten dann beim Erzeugen
der HTML-Dateien den entsprechenden Artikel. Jede Kategorie besitzt dann
zusätzlich einen RSS-Feed. Der dritte Punkte ist hier das Datum, welches das
Veröffentlichungsdatum des Blog-Artikels darstellt. Die drei aufgeführten
Punkte sind nicht die einzigen, es gibt auch noch einige weitere Daten, die man
angeben kann, wie etwa einen speziellen [Slug](https://de.wikipedia.org/wiki/Clean_URL),
einen Autor oder auch eine Zusammenfassung des Artikels.

Der eigentliche Text enthält in diesem Beispiel lediglich eine Markdown-Syntax
nämlich das __#__. Dies ist das Markdown-Zeichen welches eine Überschrift der
ersten Ebene darstellt. Es kann in Artikeln auf die volle [Markdown-Sprache](http://markdown.de/)
zurückgegriffen werden.

Neben Artikeln können auch normale Seiten angelegt werden, wie etwa eine
Impressumsseite. Dazu reicht es, wenn man einen __pages__ Ordner innerhalb des
__content__ Ordners erstellt und dort die relevanten Seiten anlegt.

### Einstellungen

Die Einstellungsmöglichkeiten von Pelican sind sehr vielfältig. Die Basiskonfigurationsdatei
ist standardmäßig __pelicanconf.py__. Dort sind unter anderem auch die
Optionen enthalten, die man beim Anlegen des Blogs spezifiziert
hat. 

Wenn der Blog bloß von einer einzigen Person befüllt wird, bietet es sich an
einen Default-Autor zu setzen. Dies geht mit der Option „AUTHOR“. Weiterhin
kann man in „SITEURL“ den Seitennamen anpassen, sofern man den ursprünglichen
Namen beim Erzeugen des Blogs ändern möchte.

Wenn man von einer anderen Blog-Software wechselt oder besondere Wünsche
für die URLs der einzelnen Seiten und Blog-Artikeln hat, bietet es sich an
die URLs entsprechend zu konfigurieren.

In meinem Fall sieht die URL-Konfiguration so aus:

    ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
    ARTICLE_SAVE_AS = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

Alle Blog-Artikel liegen unter dem Sub-Directory __blog__. Dahinter folgen
die Zahlen für das Jahr, Monat, Tag und schlußendlich der Slug.
Das Resultat sieht dann so aus: __/blog/2014/04/27/suchen-und-finden-mit-ack-statt-grep/__.
Der Blog-Artikel wird allerdings in einer einzelnen HTML-Datei speichert, sodass
Pelican für jeden einzelnen Blog-Artikel die Ordner-Struktur erzeugt und dort
eine __index.html__ Datei anlegt.

Die vollständigen Einstellungsmöglichkeiten von Pelican findet man in
der offiziellen [Dokumentation](http://docs.getpelican.com/en/3.3.0/settings.html).

## Plugins

Die Basisfunktionalität von Pelican lässt sich mit Hilfe von Plugins erweitern.
Hierfür gibt es ein offizielles GitHub-Repository, welches von den Pelican-Entwicklern
gepflegt wird, aber hauptsächlich von externen Helfern eingereicht werden.
In dem [Repository](https://github.com/getpelican/pelican-plugins) kann man stöbern
und diverse Plugins ausprobieren. So gibt es beispielsweise Plugins zum
Erzeugen von [PDF-Dateien](https://github.com/getpelican/pelican-plugins/tree/master/pdf)
aus den Artikeln, oder ein Plugin zum einfachen Einbinden von [YouTube-Videos](https://github.com/kura/pelican_youtube).

## Themes

Ebenfalls in einem [GitHub-Repository](https://github.com/getpelican/pelican-themes)
finden sich eine größere Anzahl an Themes, die man für seinen Pelican-Blog 
einsetzen kann. Nachdem Herunterladen des Repositories, reicht es, wenn man
ein Theme in der Konfigurationsdatei spezifiziert.

Falls man nicht eines der vorgegebenen Themes nutzen will, kann man auch ein
eigenes Theme für Pelican entwickeln. Wie das geht, kann man ebenfalls
in der [Dokumentation](http://docs.getpelican.com/en/3.3.0/themes.html) nachlesen.

## Fazit

Mit Pelican lässt sich schnell und einfach ein statisches Blog aufsetzen
und betreiben. Es kommt mit allen Vor- und Nachteilen von statischen Blogs
daher. Wer eine sinnvolle Kommentarfunktion vermisst, der muss sich mit
externen Diensten aushelfen, wie etwa [Disqus](http://disqus.com/). Für
bereits existierende Blogs gibt es auch einen [Importer](http://docs.getpelican.com/en/3.3.0/importer.html#import),
welches Artikel in das Pelican-Format überträgt. Mit den zahlreichen Konfigurationsmöglichkeiten
ist es zudem möglich seinen Blog bis in das Detail so anzupassen, wie man es
möchte.
