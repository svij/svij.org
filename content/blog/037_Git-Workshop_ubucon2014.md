---
title: "Fertig! Git-Workshop auf der Ubucon 2014"
categories: ["Events"]
date: "2014-10-19 22:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

An diesem Wochenende fand die Ubucon 2014 in der Gemeinde [Katlenburg-Lindau](https://de.wikipedia.org/wiki/Katlenburg-Lindau)
statt. Einen Rückblick auf die diesjährige Ubucon folgt an dieser Stelle
allerdings noch nicht. Dies folgt im Laufe der Woche.

Im August [hatte ich bereits angekündigt](/blog/2014/08/11/git-workshop-auf-der-ubucon-2014/),
dass ich dieses Jahr einen Git-Workshop auf der Ubucon halten werde. Wie geplant,
tat ich dies dann auch. Für mich war der Workshop generell mal was neues, da ich bislang
lediglich Vorträge auf der Ubucon gehalten habe. Aus demselben Grund war auch 
die Zeitplanung schwierig. Meine Schätzung von zwei bis drei Stunden hat ungefähr
hingehauen, sodass wir nach knapp 2,5h fertig waren.

Insgesamt haben knapp 17 Leute teilgenommen. Die Anzahl war zum Glück nicht zu
groß, sodass man einen guten Überblick halten konnte. In dem Workshop behandelte
ich die grundsätzlichen Git-Features. Dies umfasst das einfache Committen
von Änderungen, das Arbeiten mit Branches, sowie die Nutzung von Remote-Repositories.

Die Folien habe ich dieses Mal erstmals auf Speakerdeck.com hochgeladen. Diese
lassen sich [hier](https://speakerdeck.com/svij/git-workshop-fur-einsteiger)
ansehen. Man sollte jedoch beachten, dass hier wie üblich meine gesprochenen
Worte fehlen und sich die Folien daher grundsätzlich größtenteils an die
Teilnehmer des Workshops richtet. Der Quell-Dateien finden sich separat
in einem [Repository auf Github](http://github.com/svijee/git-workshop).

Bald sollte dann auch endlich der erste Teil des Git-Tutorials fertig sein. Den
veröffentliche ich dann, wie gewohnt, hier und in eine der kommenden Ausgaben
von [freiesMagazin.de](http://www.freiesmagazin.de/).

<script async class="speakerdeck-embed" data-id="29bba9a039f2013278c35eb6dd3f65c6" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
