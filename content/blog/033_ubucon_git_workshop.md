---
title: "Git-Workshop auf der Ubucon 2014"
categories: ["Entwicklung"]
date: "2014-08-11 19:25:00+02:00"
toc: true
---

Dieses Jahr findet erneut eine Ubucon statt, diesmal in der Gemeinde
Katlenburg-Lindau in der Nähe von Göttingen. Wie auch letztes Jahr in Heidelberg
beteilige ich mich auch etwas an der Organisation der Veranstaltung.

Der [„Call for Papers“](http://ubucon.de/2014/cfp) startete schon vor längerer
Zeit. Diesmal bin ich zwar nicht mit einem Vortrag vertreten, führe aber erstmals
einen Workshop durch. Wie der Titel schon verrät, handelt es sich um einen
[„Git für Einsteiger“-Workshop](http://ubucon.de/2014/programm#git)

Den Workshop habe ich zwar noch nicht direkt vorbereitet, allerdings kann ich
hier schon einmal verraten, dass eine Artikel-Reihe zu dem Thema in Arbeit ist,
worauf der Workshop basieren wird.
Den ersten Teil habe ich bereits fertiggestellt. Geplant habe ich zur Zeit
etwa vier Teile, die ich nach und nach bei [freiesMagazin](http://www.freiesmagazin.de/)
veröffentlichen werde. Je nach Lust, Laune, Zeit und Feedback könnte ich mir
auch sehr gut weitere Teile vorstellen, aber da warte ich erst einmal ab. ;-)

By the way: Das Ende des „Call for Papers“ ist schon diesen Freitag. Ich kann
nur jeden ermutigen einen Vortrag einzureichen. Aktuell ist das [Programm](http://ubucon.de/2014/programm)
eher dünn. Die [Anmeldung](http://ubucon.de/2014/anmeldung) ist ebenfalls schon
seit einigen Wochen offen.

Wir sehen uns dann (hoffentlich) auf der diesjährigen Ubucon! :-)
