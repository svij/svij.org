---
title: "Firmware vom CalDigit TS3 Plus unter Linux updaten"
categories: ["Sonstiges"]
date: 2021-01-03T11:00:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: "https://vg05.met.vgwort.de/na/860279715b09460090f7d449578406d2"
vgwortt: blablabla
---

Seit dem letzten Jobwechsel besitze ich am heimischen Arbeitsplatz ein Dell XPS
13 und nutze dazu die Thunderbolt-Dockingstation [„CalDigit TS3
Plus“](https://www.caldigit.com/ts3-plus/). Leider funktionierte sie bisher
nicht so gut wie ich erwarten würde, da diese unter Last häufiger mal
abstürzt und neu startet was vor allem dann passiert, wenn ich in Videocalls
bin.

Praktisch wäre es ja, wenn es ein Firmware-Update geben würde, die mögliche Fehler
korrigiert. Wenn man nun unter [downloads.caldigit.com](https://downloads.caldigit.com/)
schaut, sieht man aber nur Möglichkeiten die Firmware zu aktualisieren für
Windows und macOS. Hab ich leider beides nicht.

Auch wenn CalDigit keinen offiziellen Update-Mechanismus unter Linux anbietet,
ist dies trotzdem möglich. Denn es lässt sich durch den [fwupdmgr](https://fwupd.org/)
aktualisieren.

`fwupdmgr` ist ein Tool, um die Firmware von einzelnen Hardware-Komponenten des
Rechners unter Linux zu aktualisieren. Bei meinen privaten und dienstlichen
Laptops waren unabhängig von der Dockingstation einige
Firmware-Aktualisierungen verfügbar.  Bei mir sieht das aktuell wie folgt aus:

    $ sudo fwupdmgr upgrade
    Devices with no available firmware updates: 
        • TS3 Plus
        • Integrated Webcam HD
        • SSDPEMKF512G8 NVMe INTEL 512GB
        • Touchpad
        • UEFI dbx
    Devices with the latest available firmware version:
        • System Firmware

Wie man sieht, wird mein Dock „TS3 Plus“ zwar aufgelistet, aber es findet automatisiert
keine Updates.

Aufgelistet werden können die Devices mit `fwupdtool`:

    $ sudo fwupdtool get-devices
    Laden …                  [***************************************]
    XPS 13 9300
    │
    ├─TS3 Plus:
    │     Device ID:          8c35acb4a72f912a0d0ccf6c27bb9eb2b866b02c
    │     Current version:    35.00
    │     Vendor:             CalDigit, Inc. (TBT:0x003D)
    │     GUIDs:              22a019cc-64d4-592b-a5ea-259176b330cd ← THUNDERBOLT\VEN_003D&DEV_0011&REV_00
    │                         7c778112-7403-5f54-aef8-2f4f4f50ce2f ← THUNDERBOLT\VEN_003D&DEV_0011
    │                         4b455155-82b1-5d27-8093-70163de758c2 ← TBT-003d0011
    │     Device Flags:       • Updatable
    │                         • System requires external power source
    │                         • Device stages updates
    │   
    […]

Die Ausgabe erfolgt hier verkürzt. Die Dockingstation hat zurzeit die Firmware-Version
35 installiert. CalDigit bietet aber Version 44 an.

Im konkreten Fall kann unter
[downloads.caldigit.com](https://downloads.caldigit.com/) der Windows Firmware
Updater heruntergeladen werden. In dem ZIP-File befinden sich im `TS3-Plus`
Ordner die einzelnen vorhandenen Firmware-Versionen. Hier wird
`CalDigit_TS3_Plus_44.01.bin` benötigt.

Das Update lässt sich nun über `fwupdmgr` installieren:

    $ sudo fwupdtool install-blob <Firmware Blob> <TS3 Plus Device ID>

In meinem konkreten Fall also mit der Device-ID, die man über `fwupdtool
get-devices` abrufen kann:

    $ fwupdtool install-blob CalDigit_TS3_Plus_44.01.bin 8c35acb4a72f912a0d0ccf6c27bb9eb2b866b02c

Und das war es dann auch schon. Mit einem erneuten Prüfen mit `fwupdtool
get-devices` wird nun die aktuelle Version angezeigt. Ob das nun meine Probleme
gefixt hat, kann ich hingegen noch nicht sagen. Dafür ist das Update erst
seit paar Minuten installiert.
