---
title: "Das DevOps-Buch ist fertig!"
categories: ["Books"]
date: 2024-02-24T09:40:00+01:00
cover:
  image: "/graphics/devops-buch-bild.webp"
---

Nachdem in den letzten Jahren mein [Git-Buch](https://www.mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git.html) und der [Schnelleinstieg: Git](https://www.mitp.de/IT-WEB/Software-Entwicklung/Git-Schnelleinstieg.html) erschienen sind, ist jetzt auch mein neuestes Buch fertig: [DevOps](https://www.rheinwerk-verlag.de/devops/)!

Ein wenig zur Entstehungsgeschichte habe ich in einem separaten Blogpost geschrieben: [Warum man keine Fachbücher schreiben sollte …](https://svij.org/blog/2024/02/11/warum-man-keine-fachb%C3%BCcher-schreiben-sollte/)

## Was steht drin?

Das Verständnis was DevOps ist und was es auch nicht ist, ist häufig sehr umstritten. Wobei „umstritten“ hier auch schon fast das falsche Wort ist. Es herrschen viel mehr sehr viele Missverständnisse, was die Bedeutung angeht.

DevOps bedeutet nicht, dass Entwickler und Admins nun die gleichen Jobs erledigen. DevOps bedeutet auch nicht, dass man beim Programmieren tägliche neue Tools einsetzen muss, es keine geplanten Deployments mehr gibt und Software nur noch in Containern läuft.

_DevOps_ ist viel größer: Es verspricht eine neue Kultur der Zusammenarbeit sowie bessere Prozesse und Workflows. So liefern Sie Änderungen schneller aus und sorgen für kürzere Feedback-Schleifen zwischen Development und Operations.  

In zahlreichen Projekten habe ich gelernt, was in der Entwicklung und im Betrieb moderner Software gut funktioniert und was nicht gut funktioniert. Im Buch sind viele Beispiele und Praxistipps enthalten, wie  eine moderne und zeitgemäße Arbeitsumgebung für IT-Projekte geschafft werden können und wie die DevOps-Transformation in Teams gelingt.

## Leseprobe und Kauf

Eine Leseprobe gibt es natürlich auch. Diese findet sich [hier](https://s3-eu-west-1.amazonaws.com/gxmedia.galileo-press.de/leseproben/5562/leseprobe_rheinwerkverlag_devops_%E2%80%93_wie_it-projekte_mit_einem_modernen_toolset_und_der_richtigen_kultur_gelingen.pdf).

Das Buch gibt es überall da, wo es Bücher zu kaufen gibt. Das Buch als Print und als E-Book kostet jeweils 39,90 € oder 44,90 € im Bundle. Idealerweise kauft man Bücher (und zwar gerade Fachbücher) [direkt beim Verlag](https://www.rheinwerk-verlag.de/devops/). Davon hat sowohl der Verlag als auch ich, als Autor, am meisten.

Auf [Amazon findet man das Buch natürlich auch](https://amzn.to/3uRLKBN) (Affiliate-Link). Es ist sowohl dort als auch bei anderen Händlern spätestens ab dem 5. März 2024 verfügbar.
