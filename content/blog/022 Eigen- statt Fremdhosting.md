---
title: Eigen- statt Fremdhosting!
categories: ["Web"]
date: "2013-10-28T21:20:00+02:00"
toc: true
---

Bis vor kurzem nutzte ich sehr viele externe Dienste. Viele meiner Daten lagen
dabei häufig bei großen amerikanischen Firmen. Das dürfte auch bei vielen
anderen der Fall sein. Besonders auf Grund der NSA-Debatte war es mir nun um
so wichtiger, die nötigsten Dienste selbst zu managen.

### Früher

Bis vor kurzem lagen meine privaten E-Mails mit meinen zwei Domains bei Google.
Ich nutzte dafür [Google Apps](http://www.google.de/intx/de/enterprise/apps/business/),
welches damals noch kostenlos für Privatpersonen war. Es gab einige Einschränkungen
in Hinsicht der Anzahl der Benutzerkonten, dies war für mich allerdings nicht
relevant. Später gab es nur noch die Möglichkeit kostenpflichtige Google Apps
Konten zu führen. Ich als „Altkunde“ konnte es allerdings weiterhin kostenfrei
nutzen. Dort liefen also einige meiner E-Mail-Adressen. Funktionierte einwandfrei
und ohne Probleme. War im Allgemeinen ziemlich zufrieden mit dem Service –
ist ja schließlich Google. Ich nutzte von den vielen Funktionen allerdings *nur*
den E-Mail-Dienst und sonst keine weiteren Dienste von Google Apps. Alle
weiteren Dienste von Google, etwa Kalender, Kontakte oder Google+ liefen
über eine normale Google Mail Adresse.

Weiterhin nutzte ich hin und wieder Dropbox. Wirklich viele Daten lagen dort
nicht. Es diente hauptsächlich zur einfachen Synchronisation von Uni-Arbeiten
zwischen Laptop und PC. Auf meinem Android-Smartphone nutzte ich zudem die
App mit aktivierten Sofort-Upload bei WLAN-Verbindung, um meine geschossenen
Fotos möglichst zeitnah und ohne großen Aufwand zu sichern.

Zusätzlich pflegte ich einige private Git-Repositorys bei [Bitbucket](https://bitbucket.org/).
Diese Repositorys dienten hauptsächlich dafür, einige Artikel und Texte,
die ich sowohl für [freiesMagazin](http://freiesMagazin.de) als auch für
[ubuntuusers.de](http://ubuntuusers.de) schreibe, versioniert abzuspeichern.
In einem weiteren Repository liegen die Daten und Artikel dieses Blogs.
Für öffentliche Repositorys nutze ich übrigens ausschließlich [Github](https://github.com/svijee).

### Jetzt

Im Wesentlichen wollte ich unbedingt erreichen, dass meine E-Mails nicht
mehr bei Google liegen. Im Idealfall wäre natürlich ein selbst aufgesetzter
E-Mail-Server. Das wäre mir aber *viel* zu aufwändig auf Dauer, daher war
dies eher nicht eine Option, obwohl ein vServer zur Verfügung steht.

[Dirk Deimeke](http://dirk.deimeke.net) empfahl mit stattdessen [uberspace.de](http://uberspace.de).
Da es dort einen kostenlosen Probe-Monat gibt, habe ich das mal schnell
ausprobiert und war schon sehr erstaunt, was da so alles möglich ist.
Es handelt sich bei uberspace um einen Shared-Hoster, wo man nicht nur
einen simplen Webserver laufen hat, sondern auch SSH-Zugriff bekommt.
Auch kann man dort seine E-Mails lagern. Als Web-Mailer wird [roundcube](http://roundcube.net)
verwendet.

Meine E-Mails von einer der beiden Domains hab ich dann recht fix auf
uberspace übertragen. Einige Tage später folgten dann auch noch die Mails
meiner anderen Domain. Das funktionierte relativ einwandfrei und ich hatte
damit keine großen Probleme. Ich musste lediglich [einige Filter definieren](https://uberspace.de/dokuwiki/mail:maildrop),
damit entsprechende Mails in die richtigen Unterordner landen.

Damit wäre schon einmal der wichtigste Punkt abgehakt.

Zusätzlich zu den E-Mails ist auch dieser Blog mit auf die uberspace-Konto
umgezogen. Da diese Webseite ausschließlich aus HTML-Dateien besteht,
war das ebenfalls einfach möglich. Die Piwik-Installation wanderte
ebenfalls rüber. Beides lag zuvor bei meinem vServer. Ein Umzug wäre streng genommen
nicht unbedingt notwendig gewesen.

Im zweiten Schritt ersetzte ich Dropbox durch ownCloud. Die ownCloud
Android App besitzt zwar nicht sehr viele Funktionen, meine geliebte „Sofot Upload“
Funktion von Fotos ist dort wenigstens verfügbar und das für 0,79€.
Die wenigen Daten wanderten somit von Dropbox zu meiner ownCloud-Instanz.
Meine gespeicherten Kontakte wanderten von Google ebenfalls zur ownCloud. Zur
Synchronisation mit Android kommt hierfür die App [CardDAV-Sync free](https://play.google.com/store/apps/details?id=org.dmfs.carddav.sync)
zum Einsatz.

Damit wäre auch Punkt 2 abgehakt.

Uberspace bietet auch eine einfache Möglichkeit eigene Git-Repositorys
zu hosten. Eine gute Anleitung dafür ist in ihrem [Wiki](https://uberspace.de/dokuwiki/cool:gitolite)
enthalten. Meine Repositorys wanderten somit von Bitbucket zu uberspace.

Punkt 3: Check!

### In Zukunft

Ein paar Dienste werde ich wohl ebenfalls noch umziehen. Dies wäre zum einen
den Kalender von Google. Meine RSS-Feeds lagen ebenfalls lange bei Google,
später dann bei Feedly und nun bei [Dirks](http://dirk.deimeke.net) TinyTinyRSS-Instanz.
(Danke dafür!)

### Fazit

Der Wechsel vom Fremd- zum Eigenhosting war mir wichtig. Es ist zwar teurer, aber
dafür hat man selbst die Kontrolle über die eigenen Daten. Da man bei uberspace
den „Pay what you want“-Ansatz verfolgt, darf ich auch so viel zahlen, wie ich möchte.
Auch mal ganz nett.

Einen vServer habe ich zusätzlich auch noch. Dort läuft nun nur noch ein OpenVPN-Daemon.


