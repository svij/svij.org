---
title: Schneller suchen mit Schlüsselwörtern in Firefox
categories: ["Kurztipps"]
tags: ["Web"]
date: "2011-01-10T18:00:00+02:00"
toc: true
---

Firefox bietet ein nettes Feature um schneller und auch angenehmer Suchen durchzuführen. Standardmäßig gibt es rechts die Suchbox, wo Google als Suchmaschine voreingestellt ist. Durch ein Klick auf das Drop-Down-Menü kann man andere Suchmaschinen oder auch Wörterbücher wie [Leo](http://leo.org) hinzufügen. Einfacher gestaltet sich die Suche, wenn man Schlüsselwörter hinzufügt. Mit den Schlüsselwörtern kann man dann direkt die Suche in der Adresszeile des Browser durchführen.

So muss man zunächst das Drop-Down-Menü der Suchbox anklicken und danach weiter auf "Suchmaschinen verwalten..."

![Liste der Suchmaschinen verwalten](/graphics/Liste_der_Suchmaschinen_verwalten_009.webp)

Als Schlüsselwort kann man jedes beliebige Wort oder Buchstabe wählen. Am Sinnvollsten ist es, wenn man etwas kurzes nimmt, also einen Buchstaben zum  Beispiel. Ich habe hier Schlüsselwörter u.a. für [Google](http://google.de), [Wikipedia](http://wikipedia.org/) und dem Wörterbuch [Leo](http://leo.org) gesetzt. Wenn ich nun ein Wort nachschlagen will, muss man lediglich ein **g** für die Google-Suche, ein **w** für die Wikipedia-Suche vorne dranhängen. 
<code>w Google</code> leitet den Nutzer dann direkt zum Wikipedia-Artikel weiter.
