---
title: "All-remote workspace at home"
categories: ["Sonstiges"]
date: 2021-12-05T16:10:00+01:00
tags: ["ubuntu-planet"]
vgwort: "https://vg05.met.vgwort.de/na/f15278bd1acc46fab87d7306116cedc6"
cover:
  image: "/graphics/remote-work-setup.webp"
---

It's been a little over 1,5 years since I joined GitLab as my first all remote
company. About half a year ago, I wrote about what [I learned in one year at GitLab](/blog/2021/04/12/one-year-at-gitlab-7-things-which-i-didnt-expect-to-learn/).
In this blog post I will describe my setup how I work because I got several
questions about it over the last time. I can also blame [dnsmichi](twitter.com/dnsmichi/)
who published a similar post [about his setup](https://dnsmichi.at/all-remote-workspace/) ;-).

I can certainly recommend the page about ["Considerations for a Productive Home Office
or Remote Workspace“](https://about.gitlab.com/company/culture/all-remote/workspace/)
in the GitLab Handbook about [All-Remote](https://about.gitlab.com/company/culture/all-remote/).

# The Desk

Even long before I worked from home full time I bought a standing desk. I own
a IKEA Bekant which only has a "up" and "down" button which is a bit annoying
since I always have to hold the button when I want to move the desk up or down.
Back in 2017 I've written a blog post about [my experiences with it in German](/blog/2017/04/08/hohenverstellbarer-schreibtisch-nach-zwei-jahren-im-einsatz/).

It's also always good to have some sort of cable management hidden underneath the
desktop. Otherwise, your legs will always touch the cables, and it will look ugly.

As part of the desk, I do have three (or should I say four?) arms mounted. One
for my 32" 4K Samsung Screen, one for my Notebook-Stand, one for my microphone
and an additional cheap „magic arm“ for my Canon EOS 700D which I use as a
Webcam.

# The Screen

As mentioned above, I'm using a 32" Samsung 4K display. A lot of folks I know
are using ultra-widescreen monitors, which I personally do not like that much
because they most likely have a smaller resolution. I rather prefer to use a 4K
screen without scaling so that I have more space available for my windows.

I used to have a 28" 4K screen, which was a bit small when you want to use it
without (much) scaling. Moving forward I hope I will not need to scale it though.
For now, my glasses are a good "bugfix" for my eyes, so I can work without
scaling on the system side.

# The Laptop and The Dockingstation

I used to have Thinkpads in the past, but I recently switched to Dell XPS. I
have two Dell XPS 13. One for work (in white) and one private (in black).

While I personally prefer to run ArchLinux (btw I use Arch!) I'm running the
latest Ubuntu LTS on my work laptop.

The laptop is connected to a [CalDigit
TS3-Plus](https://www.caldigit.com/ts3-plus/) which is my docking station.
This was one of the few docking station which supported 4K@60Hz back when I bought
this. I would prefer a docking station with more USB-ports. Right now I have another
USB-Hub (hidden under the desktop) because the ports provided by most of the
docking stations out there are not really enough for me.

# Audio and Video

As already mentioned above, I use my Canon EOS 700D as my webcam. I'll mostly
use an 18-55mm lens. The camera is connected to an [elgato Cam Link 4k](https://www.elgato.com/en/cam-link-4k).
The biggest downside of this setup currently is that the format is not perfect,
as the HDMI-output has two black bars on the left and right side of the video.

I try to keep my background as boring as possible, that's why all of you will
mostly only see my yellow wall behind me.

As a microphone, I use an [M-Audio Uber-Mic](https://m-audio.de/uber-mic). It's
being used in meetings and also for my (German) podcast [TILpod](tilpod.net)
which I record remotely with [Dirk Deimeke](https://dirk.deimeke.ruhr/). For
listening in meetings and also for videos/music, I'm using my Sony WH-1000X3.

# Lighting

I do not have a perfect lighting solution yet. I do have a small desk light
which is okay for the desk itself but not for video calls. The main light
in my office is an [IKEA Floalt](https://www.ikea.com/de/de/p/floalt-led-lichtpaneel-dimmbar-weissspektrum-20436317/)
which can be dimmed.

# What else is on the desk?

 - Stream Deck, mostly for changing audio settings
 - a mechanical keyboard (HyperX with brown Cherry MX switches)
 - a boring Logitech mouse
 - a big mouse pad
 - a wrist rest pad
 - some old Logitech speaker
 - a Brother printer/scanner
 - a YubiKey 5 NFC
 - a few (water) bottles

# What do I use for on the go?

I didn't really travel that much for obvious reasons. However, I do have these
items as well:

 - a no-name 8-in-1 Hub with multiple USB-Ports, HDMI, microSD and SD and USB-C
 Power Delivery.
 - a cheap no-name clip-on micrphone because the Laptop-internal microphone are mostly crappy anyway
 - a LAN cable just in case
 - a USB-C cable
 - my Sony WH-1000X3
