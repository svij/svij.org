---
title: "Ein Buch mit Open und Closed Source Software schreiben"
categories: ["Sonstiges"]
date: "2016-08-03 15:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Wie ich in meinem vorherigen Blog-Post schrieb, habe ich ein [Buch über Git](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/)
geschrieben, dass seit kurzem erhältlich ist. Mehr Informationen zum Buch selbst,
findet sich im verlinkten [Blog-Artikel](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/).

In diesem Blog-Post soll es um die Tools und Programme gehen, die ich genutzt
habe, um das Buch zu schreiben. Dabei beziehe ich allerdings ausdrücklich nur auf
die Programme, die ich bei mir genutzt habe und nicht die Programme die beim Verlag
im Einsatz waren. Ich habe zwar versucht nur Open Source Software zu nutzen, um
mein Buch zu schreiben, allerdings war das nicht so möglich, wie ich es mir gehofft habe.
Dazu später aber mehr!

## Das Schreiben

Insgesamt umfasst das Buch mit allem drum und dran 272 Seiten. Zu Beginn der
Arbeit an dem Buch war vor allem eins wichtig: Das Schreiben möglichst
vieler Wörter und Sätze. Um die Formatierung habe ich mich zu Beginn noch gar nicht
gekümmert, da zunächst eh erstmal der Inhalt grob stehen sollte. Da mein Buch
auf mein [bisher vierteiliges Git-Tutorial](/blog/category/git/) basiert, war
das Grundkonzept schon gegeben.

Zu Beginn brauchte ich ein Textverarbeitungsprogramm das möglichst
wenig ablenkt, darunter fallen auch so Sachen wie Wortvervollständigungen. 
Es sollte außerdem die Generierung von PDF-Dateien ermöglichen, damit ich sehen kann, wie viel Inhalt
in etwa schon geschrieben wurde. Außerdem wollte die Quelldateien einfach versionieren,
bei einem Buch über Git ganz überraschend mit Git! ;-)

Als Textverarbeitungsprogramm wollte ich daher ein simplen Text-Editor nutzen, es
wurde letztlich im wesentlichen [vim](www.vim.org). Den Text habe ich dazu
in Markdown geschrieben, was ich anschließend mit [Pandoc](http://pandoc.org)
in PDF-Dateien konvertiert habe. Die Größe und Positionierung von Grafiken habe
ich zu dem Zeitpunkt sowieso noch nicht weiter beachtet. Das Verfahren hat soweit
auch gut funktioniert: Ich konnte mich um den Inhalt kümmern, wurde nicht von
(überladenen) Textverarbeitungsprogrammen genervt und konnte es einfach versionieren.
Meine Zeilen habe ich soweit meistens auf 80 bis 90 Zeichen pro Zeile begrenzt.

Soweit ich alleine gearbeitet hatte, war der Einsatz von vim mit Markdown und Pandoc
keine Einschränkung für mich. In Zusammenarbeit mit dem Verlag musste der Inhalt
allerdings in eine Microsoft Office oder LibreOffice Writer Dokumentenvorlage überführt werden.
Ich wählte hier LibreOffice Writer, die Vorlage wurde vom Verlag geliefert und
beinhaltete im wesentlichen Formatierungen, damit der Satz vom Verlag durchgeführt
werden konnte. Das Zusammenspiel von LibreOffice in Austausch mit Microsoft Office (auf Seiten des Verlages)
funktioniert erstaunlicherweise gut mit nur ganz wenigen Komplikationen. Das war
ein Punkt, den ich so nicht erwartet hatte. Dabei wurde im wesentlichen die Korrektur-
und Stichwortverzeichnis-Funktion verwendet.

## Die Grafiken

Kein Fachbuch kommt ohne Grafiken aus. In meinem [Git-Tutorial im Blog](/blog/category/git/)
hatte ich nur wenige Grafiken. Zu Beginn erstmal gar nichts und später zu meist
Screenshots von Git-Programmen, die eine Historie darstellen. Das sah dann etwa so aus:

<a href="/graphics/git2-branch-master.webp" data-lightbox="image" data-title="Die aktuellen Commits auf dem Master-Branch.">
	<img src="/graphics/git2-branch-master.webp" style="margin:20px;">
</a>

Für ein Blog war das zwar noch okay, für ein Buch wollte ich das allerdings nicht
haben, so fehlten etwa zusätzliche Informationen, es war wenig flexibel und 
klein war die Darstellung auch noch.

Ein Ersatz musste also her. Ich brauchte sowohl Grafiken für die Darstellung
der Git-Historie, als auch etwa zur Veranschaulichung von Remote-Repositories. Es
gibt zwar Tools die ein Git-Repository grafisch darstellen können, doch ist es
meist wenig flexibel, insbesondere wenn man etwa Unterschiede zwischen Fast-Forward-Merges
von anderen Merge-Strategien darstellen wollte.

Zunächst schien [Graphviz](http://www.graphviz.org/) das passende Tool für mich
zu sein. Es ist Open Source und man schreibt den Code in Textdateien, aus denen
dann die Grafiken erzeugt werden können. In der Theorie las sich das zwar
praktisch, für meinen Einsatzzweck war es allerdings noch nicht so praktisch,
wie ich nach einigen Stunden herausfinden durfte. Wesentlicher Grund war, dass
Graphviz bei die Blasen mit Text frei bewegt, so wie es passt. So war eine
geradlinige Git-Historie recht einfach darstellbar, sobald man mehrere Branches
mit gleichen Git-Objekten untereinander platzieren wollte. Das ist vermutlich
irgendwie möglich, doch war es nach vielen Stunden herausprobieren mir immer noch
nicht gelungen, weshalb ich das wieder verworfen habe.

Die Wahl fiel letztendlich auf [yEd Graph Editor](https://www.yworks.com/products/yed),
welches leider nicht Open Source ist, aber es ist wenigstens kostenfrei. Da es ein
Java-Tool ist, lässt es sich immerhin ohne Probleme auch unter Linux ausführen.
Damit konnte ich dann nicht nur die Git-Historie schön und mit wenig Aufwand
erstellen, sondern auch die Darstellung von Workflows mit mehreren Clients und
Servern war angenehm möglich.

Die oben dargestellte Grafik mit den drei Commits aus Gitg, sah mit yEd erzeugt
dann so aus:

<a href="/graphics/gitbuch-yed.webp" data-lightbox="image" data-title="Die aktuellen Commits auf dem Master-Branch.">
	<img src="/graphics/gitbuch-yed.webp" style="margin:20px;">
</a>

## Statistiken

Durch die Nutzung von Git lassen sich einige Statistiken aus dem Repository
herauslesen. Da ich weder ausschließlich mit reinen Textdateien gearbeitet habe,
noch mit mehreren Personen dran gearbeitet habe, fallen einige mögliche Statistiken
weg. Viele Commits sind vor allem recht groß, vor allem am Anfang, da dort erstmal nur
viel Text geschrieben wurde und zu späteren Zeitpunkten gab es erst viele kleine
Commits, wo jedes Mal Fehler ausgebügelt wurden.

Den ersten Commit tätigte ich am 12. September 2015 und den letzten finalen Commit
am 15. Juli 2016. Das sind insgesamt 308 Tage, also 10 Monate und 4 Tage. Insgesamt
erzeugte ich 287 Commits.

Mit dem recht alten, altbackenen Tool [gitstats](http://gitstats.sourceforge.net/)
lassen sich weitere Statistiken generieren, etwa die Anzahl der Commits pro Wochentag
pro Stunde:

<a href="/graphics/gitbuch-stats1.webp" data-lightbox="image" data-title="Anzahl der Commits pro Stunde">
	<img src="/graphics/gitbuch-stats1.webp" style="margin:20px;">
</a>

Oder auch die Anzahl der Commits pro Wochentag, ganz ohne die Aufteilung in die
Stunden:

<a href="/graphics/gitbuch-stats2.webp" data-lightbox="image" data-title="Anzahl der Commits pro Wochentag">
	<img src="/graphics/gitbuch-stats2.webp" style="margin:20px;">
</a>

Es gibt noch einige weitere Statistiken und auch verschiedene Tools, die aus Git-Repositories
Statistiken erzeugen. Für mein Buch (und für diesen Blog-Post).

Die jenigen die  selbst liebäugelt ein Fachbuch zu schreiben, denen empfehle ich
ein Blick auf die Vortragsfolien vom Vortrag [„Von der Idee zum (Fach-)Buch“](https://www.deimeke.net/dirk/blog/index.php?/archives/3461-Von-der-Idee-zum-Fach-Buch-....html) von Dirk Deimeke.

