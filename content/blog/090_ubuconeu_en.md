---
title: "UbuCon Europe in the retrospective"
categories: ["Events"]
date: "2016-11-27 12:30:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Last weekend the very first UbuCon Europe took place in Essen, Germany. It was
the second UbuCon where I was the head of the organisation team. But this one
was the first international UbuCon, which had a few more challenges compared to
a national UbuCon. ;)

This blog posts focuses on both: the event itself and some information about
the organisation.

### Thursday

The first unofficial day of the UbuCon was Thursday, where some people already
arrived from different countries. We were already ten people from five different
countries and we visited the Christmas market in Essen, which opened on that day.
Gladly we had Nathan Haines with us, so he could translate all the alcoholic
drinks from German to English, because I don't know anything about that. ;)

### Friday

<a href="/graphics/ubucon2016_zollverein.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_zollverein.webp" style="margin:10px;">
</a>

The first official day started in the afternoon with a guided tour through [Zeche Zollverein](http://www.zollverein.de/).
We were 18 people, this time from eight different
countries. The tour showed us the history of the local area with the coal
mines which were active in the past. They showed us the whole production line
from the coal mining to the processing. The tour took two hours and after that
we went to the Unperfekthaus, where the first social event of the weekend took
place. There, we were roughly fifty persons mostly drinking, eating and talking.

It was also the first chance to see familiar and new faces again!

<a href="/graphics/ubucon2016_desk.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_desk.webp" style="margin:10px;">
</a>

### Saturday

Saturday started with my quick introduction to the event. After that Canonical CEO
Jane Silber hold the first keynote where she talked mostly about the IoT and the
Cloud. I was glad that she followed my invitation, even though she had to leave
after lunch. The day was packed with different talks and workshops.

<a href="/graphics/ubucon2016_jane_keynote.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_jane_keynote.webp" style="margin:10px;width:300px">
</a>

I sadly
couldn't join every talk but the talks from Microsoft about "Bash on Ubuntu on Windows"
was quite interesting. Laura Czajkowskis talk about "Supporting Inclusion & Involvement
in a Remote Distributed Team" was short but also interesting. The day ended with the
raffle and the UbuCon Quiz. Everyone could buy an unlimited amount of raffle ticket for 1€
so there were a few people with more than ten tickets. We mostly had different Ubuntu
USB-Sticks, three Ubuntu Books, Microsoft T-Shirts, a Nextcloud Box and the bq Aquaris M10 Tablet
which were pretty popular. Funnily some people won more than one prize. The UbuCon
Quiz afterwards was funny too. The ultimate answer to every question seemed to
be "Midnight Commander" :). After the quiz the second social event started
and was joined by about 80 persons.

<a href="/graphics/ubucon2016_audience.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_audience.webp" style="margin:10px;">
</a>

### Sunday

After the long Saturday the started again at around 10 o'clock in the morning.
There were different talks and workshops again. Daniel Holbach did a workshop
on how to create snaps, Costales did a talk about his navigation app uNav. Later
Alan Pope talked about how to bring an app as a snap to the store. Elizabeth K.
Joseph was talking on how to build a career with Ubuntu and FOSS and Olivier
Paroz talked about Nextcloud and the upcoming features.

<a href="/graphics/ubucon2016_im_vortrag.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_im_vortrag.webp" style="margin:10px;">
</a>

The day and also the conference ended on 5pm. At that time many people were
already on their way back home.

### Conclusion

We've welcomed 130 persons from
17 different countries and three continents. Originally I didn't expect that
many people from other countries. In the end there were 55 % attendees from
Germany. In the last year we had a similar amount of people who attended the
German UbuCon. Personally I'm pretty happy that the event took place without
big issues or problems. The biggest problem was just the payment which was rather
complicated for most of the people. It was a good decision to use the Unperfekthaus
as a venue for our event. We didn't have to organise food and drinks, because that
was already included. The projectors were already setted up and even the WiFi
worked without problems. The mix of the talks were good too: We had different
levels of talks, for beginners and for advanced users and developers.

<a href="/graphics/ubucon2016_besucher.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_besucher.webp" style="margin:10px;">
</a>

At this place I want to thank to a lot of people. First of all to Canonicals
Community-Team including David Planella, Michael Hall, Daniel Holbach and Alan
Pope who helped us with the overall organisation and where always ready when
we needed help. Also, thanks to Marius Quabeck and Ilonka O. who joined the weekly
hangouts with the Community-Team and helped in a lot of smaller and bigger organisation
stuff, too. Jonathan Liebers and Jens Holschbach actually brought the UbuCon
to Essen, even though the Unperfekthaus wasn't the first choice. Ilonka and
Veit Jahns also helped with the handling of all the submitted talks and workshops.
Sarah, Peter and Philipp were on the wrong place at the wrong time and got recruited
to handle the registration desk: Thanks and Sorry ;)! Last but not the least Torsten Franz
and Thoralf Schilde from the ubuntu Deutschland e.V. who were our legal entity
to host the UbuCon and handle all the bills.

Also: Never forget the [Sponsors](http://ubucon.org/en/events/ubucon-europe/sponsors/):
[Microsoft](https://microsoft.com), [otris software AG](http://otris.de), [Nextcloud](https://nextcloud.com/),
[bytemine](https://www.bytemine.net/de/index.html), [b1 systems](https://www.b1-systems.de/en/),
ubuntu-fr and [Ubuntu User](http://www.ubuntu-user.com/).

Besides the help in the organisation I also want to thank every speaker and
visitor who actually formed the content of the conference. I'm really glad that
so many people said that they liked it and I'm really looking forward for next
years UbuCon Europe which will take place in Paris, France!

See you there!
