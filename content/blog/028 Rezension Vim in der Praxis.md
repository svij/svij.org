---
title: "Rezension: Vim in der Praxis"
categories: ["Rezension"]
date: "2014-05-04 11:00:00+02:00"
toc: true
---

Das Buch „Vim in der Praxis“ vom Autor Drew Neil erschien Ende Dezember in der
ersten Auflage und bündelt auf über 300 Seiten eine große Anzahl an Tipps und 
Tricks für die Nutzung von Vim.

### Was steht drin?

Das Buch umfasst insgesamt 121 Vim-Tipps auf 21 Kapitel, abgedruckt auf 353 
Seiten. Es wurde vom Autor Drew Neil ursprünglich auf Englisch für den Verlag
„Pragmatic Bookshelf“ verfasst. Die deutsche Übersetzung erfolgte durch 
Lars Schulten für den O'Reilly Verlag.

Zusätzlich zu den 21 Kapiteln ist das Buch zudem in sechs einzelnen Teilen
unterteilt. Diese Teile unterteilen das Buch sinnvoll in verschiedene
Unterpunkte, die für die Nutzung von Vim nützlich ist. Es wird unterteilt
in „Modi“, „Dateien“, „Rascher vorankommen“, „Register“, „Makros“, „Muster“
und „Werkzeuge“.

Das Buch beginnt mit einem „Lies mich“ und „Das vergessene Handbuch“, welches 
man definitiv lesen sollte, sofern man sinnvoll mit dem Buch arbeiten möchte.
Im „Lies mich“ wird der Aufbau des Buches erläutert, während „das vergessene 
Handbuch“ auf die Notationen und Formen des Buches eingeht, damit man sämtliche 
Tastendrücke, die im Buch erwähnt werden, nachvollziehen kann.

Das Buch ist nicht als klassisches Lehrbuch aufgebaut, welches man von vorne
nach hinten durcharbeiten kann bzw. soll. Stattdessen wird bereits zu Beginn
ausdrücklich empfohlen das Buch quer durchzublättern und nach und nach einige
der vielen Tipps auszuprobieren und nachzuvollziehen.

Im ersten Teil beschreibt der Autor die Funktionsweise der einzelnen Modi von
Vim. Er nennt und erläutert die folgende fünf Modi an Hand von zahlreichen Beispielen:
„Normaler Modus, „Einfüge Modus“, „Visueller Modus“ sowie „Kommandozeilenmodus“.

Im zweiten Teil geht es grundsätzlich um die Handhabung von Dateien, etwa wie
man Dateien öffnet, speichert oder generell mehrere Dateien gleichzeitig
verwaltet. Im Anschluss folgt der dritte Teil der die Bewegungen innerhalb von
Dateien erläutert. Besonders dieser Teil sollte von Einsteigern zuerst gelesen
und verstanden werden, da die Bewegungen innerhalb von Vim eines der essentiellen
Dinge sind um Vim nutzen zu können.

Der vierte Teil dreht sich um Register, also wie man Texte kopiert, einfügt und
wie man mit Makros arbeitet. Im Anschluss folgt der nächste Teil über Muster.
Dort wird nicht nur erklärt, wie man schnell und effizient sucht, findet und
ersetzt. Der letzte und sechste Teil bespricht vorhandene Werkzeuge, die Vim
sinnvoll ergänzt. So wird unter anderem erklärt, wie man Code kompilieren kann
mit Make, eine automatische Vervollständigung aktivieren kann oder wie man
die Rechtschreibprüfung von Vim aktiviert und nutzt.

### Wie liest es sich?

Das Buch setzt grundsätzlich keinerlei Kenntnisse in Vim voraus.
Nichtsdestotrotz dürften sich Vim-Anfänger anfänglich mit dem Buch schwertun,
da vor allem die Bedienung von Vim zu Beginn ungewohnt ist. Leser, die vorher
mindestens den "vimtutor" durchgearbeitet haben, werden hiermit allerdings
weniger Probleme haben.

Die Tipps in dem Buch lesen sich grundsätzlich ziemlich gut und sind so gut
wie immer sehr gut verständlich. Die angeführten Beispiele lassen sich leicht
nachmachen und verstehen. Häufig muss man allerdings die Notation der Beispiele
im „Lies mich“ nachblättern.

Die Tipps behandeln nicht nur die grundsätzlichen Vim-Features, sondern gehen
auch sehr tief in einige Funktionen ein. Nur wenige Funktionen, die angesprochen
werden, werden oberflächlich angekratzt, denn meistens wird ausführlich
erläutert, was mit welchen Kommando möglich ist und auch wie man es möglicherweise
optimieren kann.

Nachdem man das Buch einmal durchgearbeitet hat, vergisst man leider sehr schnell
viele der Tipps. Das Buch eignet sich zudem besonders als Handbuch, welches man
dann zu Rate ziehen kann, wenn man es gerade braucht, denn merken kann man sich
alle Tipps nach dem ersten Ausprobieren leider nicht.

### Kritik

Vim-Anfänger werden mit diesem Buch Vim lieben lernen. Es macht viel Spaß die
einzelnen Tipps durch zuarbeiten und dabei einiges auszuprobieren. Sobald man
erst einmal angefangen hat, bleibt man häufig gut dabei und macht weitere
Tipps und Tricks. Für den Leser wird dabei auch deutlich, wieviele Möglichkeiten
Vim so bietet und wie man sie auch in der Praxis sinnvoll einsetzen kann.

Der einzige echte Kritikpunkt meinerseits ist lediglich der Preis. Mit 35€ ist es
wahrlich kein Schnäppchen für ein Buch welches ohne farbige Seiten daherkommt.
So gibt es zwar viele Fachbücher die zwar ausführlicher sind, allerdings kann
man diesem Buch wenigstens auf fast jeder Seite was neues abgewinnen, sofern
man noch kein Vim-Profi ist. Wer sich also sinnvoll mit Vim auseinanderzusetzen
will, der findet mit diesem Buch eine sehr gute Grundlage um Vim zu erlernen.

#### Buchinfo
**Titel:** [Vim in der Praxis](http://www.oreilly.de/catalog/practicalvimger/)

**Autor:** Drew Neil, Übersetzung von Lars Schulten

**Verlag:** O'Reilly, 2013

**Umfang**: 353 Seiten

**ISBN**: 978-3-95561-578-9

**Preis**: 34,90€ (broschiert), 28,00€ (eBook)
