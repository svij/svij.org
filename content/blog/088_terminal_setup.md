---
title: "Persönliches Terminal-Setup"
categories: ["Shell"]
date: "2016-11-01 10:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Manchmal fragen mich Leute, was ich denn genau für ein Terminal-Setup habe. So
unter anderem auch ein Azubi auf der Arbeit der mich schlicht fragte: "Dein
Terminal sieht so toll aus, was nutzt du da (alles)?". So toll sieht es zwar
nicht aus, aber hier mal mein Setup, damit ich das in Zukunft nur noch verlinken
muss.

<a href="/graphics/terminal-setup.webp" data-lightbox="image">
  <img src="/graphics/terminal-setup.webp" style="margin:10px;">
</a>

**Terminal-Emulator**: Als Terminal-Emulator verwende ich hauptsächlich zwei
KDE-Anwendungen: [Konsole](https://wiki.ubuntuusers.de/Konsole/) und [Yakuake](https://wiki.ubuntuusers.de/Yakuake/). Yakuake kommt dort zum Einsatz, wenn ich auf kleinen
Displays bin – wie am Laptop – und Konsole auf großen Displays.

**Terminal Fenstermanager**: Früher hatte ich eine Zeit lang [screen](https://wiki.ubuntuusers.de/Screen/) verwendet mit einer eigenen Konfiguration, später dann [tmux](https://wiki.ubuntuusers.de/tmux/). Letzteres ebenfalls mit eigener Konfiguration. Nach einer
Weile hat mich das ständige Frickeln an meiner eigenen Konfiguration genervt,
sodass ich auf [byobu](https://wiki.ubuntuusers.de/byobu/) umgestiegen bin. Byobu
ist letztendlich nur ein Wrapper um screen oder tmux und hat im Standard schon
eine Leiste unten, wo nicht nur die geöffneten Terminal-Fenster aufgelistet werden,
sondern auch CPU-Auslastung, Uptime, Temperatur, Akku-Prozentanzeige, Uhrzeit
und vieles mehr dargestellt werden. Durch das Drücken der F9-Taste lassen sich
viele weitere Status-Notifications aktivieren und deaktivieren. Praktisch ist
auch, dass zumindest ab Ubuntu 16.04 byobu vorinstalliert ist. In anderen
Distributionen ist es allerdings teilweise gar nicht erst in den offiziellen
Paket-Quellen enthalten.

**Shell**: Als Shell nutze ich [zsh](https://wiki.ubuntuusers.de/Zsh/) allerdings
ohne große eigene Konfiguration.

**Prompt**: Viel wichtiger als der Einsatz von zsh oder Bash ist für mich jedoch
[liquid prompt](https://github.com/nojhan/liquidprompt). Dazu hatte ich vor
drei Jahren einen eigenständigen [Blog-Artikel](https://svij.org/blog/2013/09/07/liquid-prompt/) verfasst. Mittlerweile wurde das auch weiter entwickelt, sodass ich es jetzt
vor allem schätze, dass bei jedem Befehl auch `time` ausgeführt wird, sodass man
schnell sieht, wie lange eine Aktion gedauert hat. Im wesentlichen ist mir da
allerdings die Integration von Git sehr hilfreich, wenn ich täglich damit arbeite.
