---
title: "Interview with Ronnie Tucker about Full Circle Magazine"
categories: ["Ubuntu"]
date: "2015-09-10 21:30:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

This is an interview with Ronnie Tucker, the creator and founder of „Full Circle Magazine“. You can find the translated German article in [this Ikhaya Article](https://ubuntuusers.de/ikhaya/7053/).

----
__Hey Ronnie, thanks for joining this interview. First of all, the
most of our readers might not know you, so can you say a few words
about yourself, who you are?__

Ronnie: Thanks for having me. I'm the editor, and founder, of Full
Circle magazine which I started (on a whim) about eight years ago.

__I suppose, that's not your day job, what do you do in "real life"
except on working on FCM?__

Real life consists of stock control and stock management in the
licensed trade. In other words: I try to make sure people don't steal
booze.

__You've have released the 100th issue of FCM a few days ago, what is
your main motivation to keep working on this magazine?__

It's my way of giving something back to the community. And since so
many people seem to like Full Circle, I never get tired of doing it.

__So, what was the starting point of FCM? I mean how did this project
start?__

I saw a post on [Ubuntu Forums](http://ubuntuforums.org) where someone was looking for help in
making an Ubuntu magazine. I replied, but got nothing. The post was
old. At that point I thought 'why not' and decided to start an Ubuntu
magazine of my own with help (initially) from the Ubuntu Forum members.

__That's actually a cool way to start a exciting project. Which topics
do you actually cover, is it only Ubuntu?__

The forum was a good starting point as it was through there that the
name originated, and the first regular writers (and helpers) emerged.

The major cover is Ubuntu, but I'd publish anything that's related to
\*buntu or any derivative of Ubuntu (eg: Mint, etc.)

__Can anyone (like me) submit their own articles for FCM or how do you
handle them?__

Absolutely anyone can submit an article. I end up sounding like a
broken record at times when I keep saying 'no articles = no magazine'.
And that would be sad!

__Do you have (or had) any problems with a low number of submitted
articles?__

All the time! Hence, my constant harrassing of readers trying to get
submissions. We get thousands of downloads each month yet very few
submissions. Most people think they don't know anything, but they do!
Everyone has something they can write about. Even if it's just some
simple advice, or reviewing software/hardware they have/use.

__That actually remembers me to our German (kind of) equaivalent
magazin (called [freiesMagazin.de](http://freiesmagazin.de)) which also - sadly - have the same
problem. Except the authors - how many people are working on an issue
each month?__

I gather up the articles and put them in a Google Docs folder. There
are about 10 proof-readers who go through them correcting any mistakes,
and after that there'll be several (independent) teams translating.

__And after that, which programs do you use to produce the
magazine?__

Articles are stored on Google Docs, so any text alterations are done
there. Any image resizing is done in GIMP, and all the layout work (and
    creation of the PDF) is done in Scribus.

__Hm, wasn't that LibreOffice in the past?__

There are some edits done in LibreOffice prior to uploading to Google
Docs, but not so much these days as Google Docs is pretty good at
converting ODT to Docs format.

__You mentioned earlier, that the magazin has "thousands of
downloads". Do you have an actual number?__

There's no definitive number of downloads as there are multiple places
to get each issue (the main site, Google Play, Issuu, Magzter, etc.)
but for the main site for FCM#100 there were almost 2,500 downloads on
launch day followed by another 1,500 downloads, then about 1,000 per
day thereafter. After about a week it'll sit at about 500 downloads per
day. Roughly.

__Are you satisfied with number of downloads and the feedback of the
readers?__

Never! I always want MORE! :)

__Haha, then you can explain me, why a reader should download the FCM?
I mean, you could also publish single articles in a blog, isn't FCM a
bit old-fashioned?__

True, but the PDF only weighs in at 10-15MB and a LOT of our readers
don't have fast broadband (me!) or fiber optic, so a lot of readers
like downloading the PDF for offline reading. Same idea with the EPUB
for mobile devices.

__Anyway, over those 100 issues, what was the most interesting or
funny moment, which happened? Or which you can't forget?__

Oh definitely the first December issue way back. I decided to go silly
with it and put a Santa Tux on the cover and use tinsel and baubles in
the issue. A good dozen or so readers complained it was too Christian.
I'm not religious. I just did it for a laugh. Sometimes I just can't win...

__Oh wow. So, I'm nearly done. Any question or topic which I forgot or
might be important for you?__

Just to encourage readers to send in articles. Even if your not a
native English writer, the proof-readers will tweak your article, so
don't worry about any language barrier. And I do try to publish
everything I get. Unless you write negatively about 'Windoze' for
several pages. At that point I'll bin it as I'm not about to get into
throwing mud at Windows, or Microsoft.

__Alright, thanks for the interview!__

My pleasure!
