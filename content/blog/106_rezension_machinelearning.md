---
title: "Rezension: Machine Learning with Python"
categories: ["Rezension"]
date: "2018-02-07T20:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/3ade2ffd00e24296a06255e60dea27ef"
---

Nach längerer Zeit folgt an dieser Stelle mal wieder eine Buchrezension. In
diesem Artikel wird das Buch „Machine Learning mit Python“ mit dem Untertitel
„Das Praxis-Handbuch für Data Science, Predictive Analytics und Deep Learning“
vom Autor Sebastian Raschka besprochen. Erschienen ist das Buch beim mitp Verlag.
Zu dem handelt es sich bei diesem Buch um eine Übersetzung aus dem Englischen.

**Hinweis**: Der mitp Verlag stellte mir für die Rezension ein Rezensionsexemplar
frei zur Verfügung. Das Buch erschien in der deutschen Übersetzung bereits
am 30. November 2016, am 22. Dezember 2017 folgt dann schon die [zweite Auflage](http://www.mitp.de/733),
in dem der Fokus um TensorFlow erweitert wurde. Diese Rezension richtet sich
explizit auf die **erste** Auflage des Buches. Genannte Kritik-Punkte können
durchaus korrigiert bzw. verbessert worden sein.

### Was steht drin?

Das Buch unterteilt sich in 13 Kapiteln, die insgesamt 424 Seiten umfassen. Das
Buch beginnt zunächst mit einem in das maschinelle Lernen, dabei wird auf die
drei verschiedenen Arten eingegangen und beispielhaft erläutert, sowie in die
Notation und Terminologie in dem Fachbereich eingegangen. Im zweiten Kapitel
folgt direkt schon die erste Übung, in der ein Lernalgorithmus programmiert
und erläutert wird, der für die Klassifizierung trainiert wird. Weiter geht
es im dritten Kapitel über die Verwendung von scikit-learn, welches die
Bibliothek ist, die in diesem Buch verwendet wird, um Machine Learning
Algorithmen zu implementieren. Die ersten Kapitel setzten noch auf vorgefertigte
Datensätze, die man nicht selbst ermitteln musste. In Kapitel 4 und 5 geht es
daher um die Datenvorverarbeitung und die Datenkomprimierung, damit man das in
den selbst geschriebenen Klassifizierern verwenden kann.

Je weiter man in dem Buch liest, desto tiefer gehen auch die Themen, so werden
in den darauf folgenden Kapiteln Hyperparameter-Abstimmung und die Kombination
verschiedener Modelle thematisiert. Das achte Kapitel führt wiederum in das
Thema des Natural Language Processings ein, wo erklärt wird, wie man
Stimmungsanalysen ableiten kann.

Das Buch führt aber auch in die Praxis ein, denn die bisherigen Kapitel
erläuterten nur an Hand von Skripten, die bei jedem Lauf von vorne aus den
Daten gelernt haben. Im neunten Kapitel wird exemplarisch gezeigt, wie man eine
Web-Anwendung implementieren kann, welches die zuvor implementierten
Algorithmen nutzt und während der Nutzung auch weiter lernt und anwendet. Die
übrigen Kapitel umfassen weiterhin eine Regressionsanalyse, Clusteranalyse, die
Verwendung neuronale Netze zur Bildererkennung, sowie neuronale Netze mit der
Bibliothek Theano.

### Kritik

Das Buch ist sehr umfangreich und bietet sehr viele Informationen zu dem doch
großen Bereich des Machine Learnings. Der größte Kritik-Punkt meinerseits ist,
dass es nicht wirklich einsteigerfreundlich ist. Für viele Themen sind zwar
Beschreibungen und Erklärungen da, allerdings sind diese zumeist relativ knapp
und nicht immer ganz nachvollziehbar. Das Buch ist zwar schon relativ
umfangreich, doch könnte dies einen Tick umfangreicher sein, um vor allem
Einsteiger abzuholen. In dem Fall müssen für einige Themenbereiche nämlich noch
zusätzliche Quellen bezogen werden, um auch die Gedankengänge der Umsetzung
besser nachvollziehen zu können. Auch Erfahrung im Umgang mit Python ist sehr
vorteilhaft. Wer keine Erfahrung in beiden Gebieten mitbringt, wird mit dem
Buch eher nicht glücklich.

Hier sollte man allerdings noch beachten, dass die zweite Auflage erschienen
ist, die nicht nur scikit-learn verwendet, sondern auch tiefer in die populäre
Bibliothek TensorFlow einführt. Das Buch ist in der zweiten Auflage somit auch
noch umfassender geworden.

Unabhängig davon gilt es hervorzuheben, dass das Buch für die Zielgruppe von
Personen mit Erfahrung mit Python und den Grundzügen von Machine Learning sehr
viele Informationen und praxisrelevante Informationen vermittelt. Dass es eine
Übersetzung aus dem Englischen ist, merkt man dem Buch nicht an. Man sieht auch
sehr deutlich, dass der Autor viele Kenntnisse und Erfahrung in dem Bereich hat,
da er an vielen Stellen auf mögliche Probleme eingeht und passende Lösungen
parat hat.

### Buchinfo:

**Titel:** [Machine Learning mit Python](https://www.amazon.de/dp/B01MXOGW9T)

**Autor:** Sebastian Raschka

**Verlag:** mitp

**Umfang:** 424 Seiten

**ISBN:** 3958454224

**Preis:** 49,99€

***[Hier gehts zur zweiten Auflage!](http://www.mitp.de/733)***
