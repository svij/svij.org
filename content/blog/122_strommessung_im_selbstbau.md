---
title: "Strommessung mit Open Source Tools"
date: "2021-05-02T09:45:00+02:00"
categories: ["Sonstiges"]
vgwort: "https://vg04.met.vgwort.de/na/99a3262c1a6845d494249e6874a5fb04"
tags: ["ubuntuusers-planet"]
cover:
  image: "/graphics/strom_steckdosen.webp"
---

Seit August 2018 besitze ich eine Photovoltaik-Anlage auf dem Dach. Dazu hatte
ich nach einem Jahr [einen Blogpost](/blog/2019/08/07/photovoltaik-anlage-auf-dem-dach/)
geschrieben, wo es unter anderem um das Messen vom Strom des ganzen Haushalts
geht.

Nun wollte ich noch ein wenig mehr Strom im Haushalt messen, allerdings dachte
ich da mehr an einzelne Geräte bzw. an einzelne Steckdosen. Wichtig war für mich
auch, dass ich die Daten kontinuierlich speichern kann und so den Stromverbrauch
über einen gewissen Zeitraum erfassen kann. Außerdem wollte ich selbst Herr über
die Daten sein und hatte auch kein Problem etwas zu frickeln. Da ich vor einem
Jahr etwa dann auf den [Blogpost von Kristian
Köhntopp](https://blog.koehntopp.info/2020/05/20/gosund-and-tasmota.html)
gestoßen bin, habe ich das einfach auch ausprobiert, allerdings mit einer
anderen Datenhaltung. In diesem Blogpost gehe ich nicht auf die genauen einzelnen
Schritte ein. Möchte viel mehr die Möglichkeiten auflisten, falls jemand ähnliches
geplant hat.

Hardware in meinem Fall:

 * Gosund SP111
 * SparkFun Serial Basic Breakout - CH340G DEV-14050 
 * eine Packung diverser Jumper Kabel

Aber nun der Reihe nach. Als Hardware kommen bei mir „Gosund SP111“ als „smarte“
Steckdosen zum Einsatz. Die bekommt man im 4er-Pack für 40€ bei einem großen
bekannten Händler. Mit
[tuya-convert](https://github.com/ct-Open-Source/tuya-convert) bekommt man dann
kabellos [Tasmota](https://tasmota.github.io/docs/) auf die Geräte, womit man
die Open Source Software auf die Geräte spielt und von der Herstellersoftware
befreit. Leider sieht es aktuell so aus, dass sich die aktuelle Geräte sich nicht mehr
mit tuya-convert flashen lassen. Stattdessen muss man die Steckdosen Aufschrauben und
per FTDI Kabel an den Rechner anschließen, um dann Tasmota flashen zu können. Ich
benutzte dazu den oben genannten Serial Basic Breakout. Das musste ich für
zwei der vier Steckdosen auch erledigen, da tuya-convert fehlgeschlagen war. Bei
den aktuell erhältlichen Gosund SP111 v2 Steckdosen lässt sich das allerdings wohl nicht mehr
so einfach öffnen, ohne die Steckdose zu zerstören.

Generell war das Aufschrauben auch eher ätzend und sehr frickelig, da man an
die Kontakte nicht so einfach drankommt. Eine Anleitung wie die Verkabelung
aussehen kann, findet sich [in diesem
Blogpost](https://www.fh19.de/smart-home/tasmota/gosund-sp111-mit-tasmota-flashen/).

Nach dem Flashen von Tasmota kann über die Web-Oberfläche das Gerät mit dem WLAN
verbunden werden. Anschließend kann das Gerät, ebenfalls über die Web-Oberfläche,
konfiguriert werden. Die genauen Konfigurationsdetails finden sich ebenfalls
im [Blogpost von Kristian Köhntopp](https://blog.koehntopp.info/2020/05/20/gosund-and-tasmota.html).

Bei mir sieht allerdings das Software-Setup für die Datenhaltung allerdings etwas anders aus.

Software-Setup:

 * MQTT Server ([mosquitto](https://mosquitto.org/))
 * [Prometheus](https://prometheus.io) Server für das Monitoring mit Datenhaltung
 * [mqtt2prometheus](https://github.com/hikhvar/mqtt2prometheus) um die MQTT Daten für Prometheus verfügbar zu machen
 * [Grafana](https://grafana.com) zur Visualisierung

Wie in meinem [anderen
Blogpost](https://svij.org/blog/2020/06/12/homeserver-setup-in-2020.../)
geschrieben, läuft bei mir alles in einem Kubernetes mit k3s auf meinem
Homserver.  Dort läuft entsprechend auch die oben genannte Software. Wie zuvor
erwähnt, geh ich nicht auf die einzelnen Konfigurationsoptionen ran. Dazu am
besten die Dokumentationen der einzelnen Tools zur Rate ziehen. Letztendlich
sieht es so aus, dass die Steckdosen die Daten per MQTT an den MQTT Server
schicken und ich mit mqtt2prometheus die Daten entgegennehme und es für
Prometheus bereitstelle. Der Prometheus Server scrapet sich dann die Daten von
dem Dienst. Theoretisch kann ich die Steckdosen auch über Home-Assistant steuern,
um diese etwa zeitgesteuert an- oder auszuschalten. Da ich das allerdings
nicht brauche, habe ich das auch nicht konfiguriert.

Das aktuelle Setup fahre ich erst wenige Wochen. Zuvor hatte ich eigenes
Scripting im Einsatz gehabt, wo ich die Daten per HTTP von den Steckdosen abgegriffen
habe und in eine InfluxDB gespeichert habe. Davon bin ich abgerückt, weil ich
die Handhabung von verhältnismäßig temporären Daten über Prometheus einfacher
zu verwalten finde und das über MQTT eh besser lösbar ist.

Die Steckdosen kommen bei mir an vier Stellen zum Einsatz:

 * an meinem Arbeitsplatz wo der Laptop, Bildschirm und sonstige Hardware hängt
 * an meiner Netzwerkecke, wo mein Server, Switch, WLAN AP, Telefon, Router, Raspberry Pi und sonstige Hardware hängt
 * in der Küche, wo im Wesentlichen ein Fernseher und ein paar kleinere Haushaltsgeräte hängen
 * im Wohnzimmer, wo der Fernseher mit Receivern, WLAN AP und sonstige TV-Hardware hängt

Das ganze Gefrickel habe ich ja aus zwei Gründen gemacht: Ich wollte einmal
ein wenig mit Hardware frickeln und mich mit mehr Tools und Protokollen auseinandersetzen.
Dinge wie MQTT, Tasmota und auch ein wenig mehr mit Prometheus spielen war jedenfalls
auch mal interessant. Der zweite Grund war auch den Stromverbrauch überwachen
zu können.

Dabei fielen mir genau zwei interessante Dinge auf, die ich hier eingehen werde.
Die ist zum einen der Stromverbrauch am Fernseher und zum anderen (vermutlich) Amoklaufende
Prozesse am Server.

Zunächst zum Fernseher. Der Graph der letzten 24 Stunden sieht so aus:

![Stromverbrauch im Wohnzimmer](/graphics/strom_livingroom.webp)

Was fällt auf: Ja, der Fernseher läuft hier ständig, aber da bin ich nicht Schuld und das ist hier auch nicht das Thema.
Spannender ist es eher zu sehen, dass der Fernseher beim Ausschalten zunächst nicht
direkt in das Standby geht, sondern noch mit knapp 20W relativ viel Strom zieht,
bevor es dann wirklich in den Standby geht. Die 80W in Betrieb sind übrigens
im Stromsparmodus der für die meisten Fälle bei uns völlig in Ordnung ist. Vor meinen
Experimenten lief es nicht im Stromsparmodus, das führte zu locker 50% höheren
Stromverbrauch, die ich vorher gar nicht betrachtet hatte.

Spannend war es allerdings diese Woche den Stromverbrauch an meiner Netzwerkecke
zu betrachten, wo der Hauptstromabnehmer mein Homeserver ist. Hier ist der
Screenshot von den letzten 7 Tagen:

![Stromverbrauch in der Netzwerkecke](/graphics/strom_floor.webp)

Der Homeserver zieht natürlich mehr Strom, wenn es unter höherer Last läuft. Warum
es da allerdings alle paar Sekunden bzw. Minuten von normal 75-80W auf 120W
springt, konnte ich mir nicht erklären. Das Debuggen war auch eher nicht zufriedenstellend.
Das System-Monitoring mit Prometheus zeigte keine Amoklaufende Prozesse. Die
Last sah soweit normal aus. Auch ein einfaches `htop` zeigte keine Auffälligkeiten
die für einen solchen Stromverbrauch verantwortlich sein könnten.

Einfache Lösung war dann: Reboot und schauen was passiert. Es hat geholfen.
Warum? Keine Ahnung.

Wie dem auch sei: Das Rumfrickeln hatte so neben dem Experimentierfaktor so auch
einen kleinen Vorteil beim Stromverbrauch, der allerdings bei mir sowieso im Wesentlichen
von der Photovoltaik-Anlage gedeckt wird.

Ich gehe übrigens davon aus, dass die Strommessung nicht wirklich genau ist. Wenn
ich den Stromverbrauch aller vier Steckdosen messe und es mit dem Stromverbrauch
des ganzen Hauses mit den Daten des Wechselrichters vergleiche, dann liegt häufig
der Stromverbrauch der vier Steckdosen über denen des ganzen Hauses, was keinen
Sinn ergibt. Aber als grobe Richtlinie ist das für mich in Ordnung.

