---
title: "Thoughts about Ubuntu Planet"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2014-02-06T07:00:00+02:00"
toc: true
---

It was really interesting for me, to read the discussion about the future
of the Ubuntu Planet. The first thing is that I am a new Ubuntu Member
with the ability to post to the planet. I do not blog regular, so I did not
know if I will use the ability to post to this planet. As you see, this is
at least my second blog post on the Ubuntu Planet.

Personally I did not read the Ubuntu Planet at all before I finished my application for
the Ubuntu membership. After that I added the planet to my feed reader and started
reading.

Except this planet, I am reading the planet at [planet.ubuntuusers.de](planet.ubuntuusers.de)
in German. As the
member of the „Ikhayateam“ of ubuntuusers.de, which writes articles and controls
the planet, I regularly check the planet. Over there, we (the team) had many
discussions which blog posts we want to see in the planet. We also focused what
our users want to read. Consequently we defined some rules. You can find the full rules
[in our Wiki](http://wiki.ubuntuusers.de/ubuntuusers/Ikhayateam/Planetenregeln).

The rules are divided into three parts: „acceptance criteria“, „code of conduct“
and „violation of the rules“. As we do not have anything similar to the
Ubuntu membership program over there, the blogs must meet the acceptance criteria.
Currently we have nine criteria. Each blogger has to apply to get the chance
to publish their blog posts to the ubuntuusers planet. We only accept
Open-Source, Ubuntu- and Linux-related blog posts. Private or off-topic blog
posts are not allowed. Additionally we want a specific tag or feed for the planet.
This could be something like an ubuntuusers-tag or simply an ubuntu-tag. The
article must contain the full article, and it is not allowed to include any
ads or links to commercial products. As we are a German website, we only allow
German blog posts. Also we do not want to have any nonactive members, the blog
must exist for at least two months. We regularly check if the blogs are active.
Nonactive blogs get a message and may be removed, if the stop blogging.

The above text contains the main rules of the planet. While some readers like
this rules, some readers does not like it. This does also apply to the bloggers.
The most of the times the bloggers follow our rules. There are not many blog
posts which we need to remove. In the last six months there were not more than
five invalid posts, which we hide afterwards.

Some bloggers did not like our rules, so there was one guy who started his
own (kind of) planet. [OSBN.de](http://osbn.de/) (Open Source Blog Network)
is the name of that planet. It also has a few rules, but it is less strict than
the rules of the ubuntuusers planet.

I personally do not think this rules should be applied in a way comparable
to the Ubuntu Planet. Personally I prefer Ubuntu-, Linux- and Open-Source-related
blog articles. I generally agree with [Jono](http://www.jonobacon.org/2014/01/29/on-planet-ubuntu/)
and [Randall](http://randall.executiv.es/pu-more-awesome-conclusion).

I am a new Ubuntu member with the possibility to post articles to this planet.
Funnily I do not have the possibility to publish my blog posts to the
ubuntuusers.de-Planet, because I do not blog regular enough, to get accepted.
In my case I will mainly blog about my LoCo Activities in Germany if this is
interesting for the Ubuntu planet.

With this article I mostly wanted to show you, how we at ubuntuusers.de are
handling the planet and which content we want to see. Maybe this should
give some of you some ideas what you should blog and what not.
