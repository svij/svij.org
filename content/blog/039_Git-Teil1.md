---
title: "Git für Einsteiger - Teil 1"
categories: ["Git"]
date: "2014-10-25T12:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
slug: "git-fur-einsteiger-teil-1"
vgwort: "https://vg02.met.vgwort.de/na/4eb4cc91ff024b959772ea7429927281"
---

Zur Entwicklung von Software wird in der Regel ein Versionsverwaltungsprogramm
genutzt. Es gibt zahlreiche Anwendungen um dies zu erledigen, zu den bekannteren Programmen
gehören Subversion, CVS, Mercurial, Bazaar und eben Git. Dieses mehrteilige
Tutorium soll den Einstieg in die Nutzung von Git erläutern.

**Dieses Tutorium besteht aus vier Teilen. Wem das Tutorium gefällt und mehr über
Git lernen möchte, der kann sich das Buch [„Versionsverwaltung mit Git“](https://amzn.to/3q0A5Iu)
für 29,99€ bestellen. In diesem Buch, was ich geschrieben habe, werden die Themen rund um Git noch
deutlich ausführlicher behandelt.**

### Was ist eine Versionsverwaltung?

Für die Leser, die noch keine Erfahrung oder Ahnung von 
Versionsverwaltungsprogrammen haben, ist es zunächst wichtig zu wissen, was denn
genau ein solches Programm macht. Die Bedeutung einer Versionsverwaltung lässt 
sich schon fast aus dem Wort ableiten: Es handelt sich um das Verwalten von 
Versionen. Konkret heißt dies, dass man von Dateien Versionen erzeugen kann, die
dann sinnvoll verwaltet werden können. Einzelne Versionen beinhalten keineswegs
nur große Änderungen, sondern sehr wohl auch kleinere Änderungen.

Viele kennen es: Man geht einer Tätigkeit nach – sei es an einem
Text, einem Bild oder an einem Video – und man möchte den aktuellen Stand immer mal 
wieder zwischenspeichern, damit man zum einen eine Sicherung der Datei hat und zum
anderen, damit man wieder auf eine ältere Version zurückspringen kann, etwa wenn etwas
falsch gelaufen ist. Jede Person 
hat dabei verschiedene Ansätze die einzelnen Versionen irgendwo abzulegen. Die 
eine Person fügt jeweils eine Versionsnummer in den Dateinamen ein, eine andere Person 
macht sich wiederum einzelne Ordner für jede Version mit dem aktuellen Datum, in
dem die einzelnen Stände gesichert werden.

Wirklich praktikabel und effizient sind keine der beiden genannten 
Varianten, da sich sehr schnell und sehr einfach Fehler einschleichen, etwa
wenn man alte Revisionen löscht, die man gegebenenfalls hinterher doch wieder
braucht.

Genau hier kommen Versionsverwaltungssysteme ins Spiel. Mit einer 
Versionsverwaltung werden zu den reinen Veränderungen noch weitere Informationen
zu einer Version gespeichert. Darunter fallen
Informationen zum Autor, die Uhrzeit und eine Änderungsnotiz. Diese werden bei jedem
Versionswechsel gespeichert. Durch die gesammelten Dateien lässt sich
so schnell und einfach eine Änderungshistorie ansehen und verwalten. Falls
zwischendurch Fehler in den versionierten Dateien aufgetreten sind, kann man dann
wieder zurück zu einer Version springen, um von dort aus erneut weiter zu machen.
Dabei ist es ganz egal, um was für eine Art von Dateien es sich handelt. Am
häufigsten werden Versionsverwaltungsprogramme zur Software-Entwicklung 
eingesetzt.

Aber nicht nur für Programmierer ist eine Versionsverwaltung 
sinnvoll, sondern auch, wie bereits geschrieben, kann der Einsatz für Grafiker oder Autoren durchaus 
nützlich sein. Ein Grafiker könnte sich so Versionen von bearbeiteten Bildern 
speichern, um bei misslungenen Änderungen wieder zurück springen zu können. Bei
Autoren geht es um Text, der ähnlich zum Quellcode von Software-Projekten,
gut verwaltet werden kann.

Es gibt drei verschiedene Konzepte zur Versionsverwaltung: die lokale, zentrale
und die verteilte Versionsverwaltung.

#### Lokale Versionsverwaltung

Die lokale Versionsverwaltung ist wohl selten in produktiven Umgebungen
zu finden, da sie lediglich lokal arbeiten und häufig nur einzelne Dateien
versionieren. Die oben bereits erwähnte manuelle 
Erzeugen von Versionen von Dateien wäre zum Beispiel eine lokale 
Versionsverwaltung mit einer einzelnen Datei. Es ist zwar ziemlich einfach zu
nutzen, allerdings resultiert daraus eine hohe Fehleranfälligkeit und sie ist
zudem wenig flexibel. Echte Versionsverwaltungssoftware gibt es mit [SCCS](https://de.wikipedia.org/wiki/SCCS)
und [RCS](https://de.wikipedia.org/wiki/Revision_Control_System) auch.
Der wohl größte Minuspunkt von lokaler Versionsverwaltung ist, dass man
nicht im Standard mit mehreren Personen an Dateien arbeiten kann. Zudem besteht
keinerlei Datensicherheit, da die Daten nicht zusätzlich auf einem entfernten
Server liegen, sofern nicht zusätzliche Backups durchgeführt werden.

#### Zentrale Versionsverwaltung

Eine zentrale Versionsverwaltung ist hingegen häufig in produktiven Umgebungen
zu finden. [Subversion](https://de.wikipedia.org/wiki/Subversion)
und [CVS](https://de.wikipedia.org/wiki/Concurrent_Versions_System)
sind beispielsweise 
Vertreter der zentralen Versionsverwaltung. Hauptmerkmal ist, dass das
Repository lediglich auf einem zentralen Server liegt. „Repository“ ist ein
englisches Wort für „Lager“, „Depot“ oder „Quelle“. Ein Repository ist somit
ein Lager, in dem die Daten liegen. Autorisierte Nutzer eines
Repositorys arbeiten dabei lokal mit einer Arbeitskopie der im Repository
vorhandenen Dateien. Die Logik der Versionsverwaltung liegt dabei auf dem
zentralen Server. Wenn man also auf eine andere Revision wechseln möchte oder
sich die Revisionsänderungen anschauen möchte, werden stets die Daten vom Server
heruntergeladen.

#### Verteilte Versionsverwaltung

Zu den verteilten Versionsverwaltungssystemen gehört nicht nur [Git](http://git-scm.com/),
sondern unter anderem auch [Bazaar](http://bazaar.canonical.com/en/) oder
[Mercurial](http://mercurial.selenic.com/). Im Gegensatz zur zentralen
Versionsverwaltung besitzt jeder Nutzer des Repositorys nicht nur eine
Arbeitskopie, sondern das komplette Repository. Wenn also zwischen verschiedenen
Revisionen gewechselt wird oder man sich die letzten Änderungen anschauen möchte,
muss nur einmal das Repository „geklont“ werden. Danach stehen alle
Funktionalitäten der Versionsverwaltung offline zur Verfügung. Dadurch wird
nicht nur unnötiger Netzwerktraffic vermieden, sondern auch die Geschwindigkeit
wird, durch den fehlenden Netzwerk-Overhead, deutlich erhöht.
Zusätzlich besitzen verteilte Versionswaltungssysteme eine höhere Datensicherheit,
da die Daten des Repositorys in der Regel auf vielen Rechnern verteilt liegen.

### Geschichtliches

Lange Zeit nutzten die Entwickler vom Linux-Kernel das proprietäre
Versionsverwaltungssystem [BitKeeper](http://www.bitkeeper.com/). Nach einer
Lizenzänderung seitens der Herstellerfirma von BitKeeper, konnte das Team
um den Linux-Kernel, allen vorran Linus Torvalds, BitKeeper nicht mehr
kostenfrei verwenden, weswegen Linus Torvalds mit der Entwicklung von Git begann.

Da die Entwicklung im Jahr 2005 begann, gehört Git zu den jüngsten
Versionsverwaltungssystemen. Für Linus Torvalds war es wichtig, dass das
künftige Versionsverwaltungssystem drei spezielle Eigenschaften besitzt. Dazu
gehörten zum einen Arbeitsabläufe, die an BitKeeper angelehnt sind, Sicherheit
gegen böswillige und unbeabsichtigte Verfälschung des Repositorys, sowie eine
hohe Effizienz. Das Projekt [„Monotone“](http://www.monotone.ca/), ebenfalls
ein Versionsverwaltungssystem, wäre fast perfekt gewesen. Es fehlte
lediglich die Effizienz. Mit Git erschuf Linus Torvalds dann doch eine
eigene Versionsverwaltung, die nicht auf den Quellen von Monotone oder BitKeeper
beruht.

Interessant ist auch die Namensnennung von Git. Git ist das englische Wort
für „Blödmann“. Linus Torvalds selbst sagte spaßeshalber: „I'm an egoistical
bastard, and I name all my projects after myself. First 'Linux', now 'Git'.“
(Deutsch: „Ich bin ein egoistischer Bastard, und ich nenne alle meine Projekte
nach mir selbst, Erst 'Linux' und nun 'Git'.“). Natürlich gibt es auch richtige
Gründe, das Projekt „git“ zu nennen. Zum einen enthält das Wort lediglich drei
Buchstaben, was das Tippen auf der Tastatur erleichtert, zum anderen gab es kein
genutztes UNIX-Kommando, womit es kollidieren würde. 

### Git-Repository starten

Git bietet einige interessante Funktionen, die nach und nach in diesem Tutorium
vorgestellt werden. Zunächst muss man Git installieren. Die gängigen
Linux-Distributionen stellen Git in ihrer Paketverwaltung unter dem Paketnamen "git" bereit.
Für andere Plattformen bietet die Git-Projekthomepage einer [Download-Seite](http://git-scm.com/downloads).

Um die Nutzung von Git sinnvoll zu erlernen, bietet es sich an die im Tutorium
angeführten Befehle ebenfalls auszuführen, um die Arbeitsweise vollständig
nachvollziehen kann. Damit dieses Tutorium einen sinnvollen Praxis-Bezug
hat, wird im Laufe der Zeit eine kleine statische Webseite mit dem HTML-Framework
[„Bootstrap“](http://getbootstrap.com/) gebaut.

Zu Beginn muss man zunächst ein leeres Projektverzeichnis anlegen, in dem
im Anschluss die Projekt-Dateien gespeichert werden. Dazu legt man zuerst den
Ordner "Webseite-mit-Git" an und wechselt dort hinein.

    $ mkdir Webseite-mit-Git
    $ cd Webseite-mit-Git

Jetzt kann man mit dem folgendem Befehl ein Git-Repository anlegen:

    $ git init
    Initialisierte leeres Git-Repository in /home/sujee/Webseite-mit-Git/.git/ 

Mit diesem Befehl erzeugt Git also ein leeres Repository im Projektordner.
Dazu legt Git, wie es die Ausgabe bereits mitteilt, ein Unterverzeichnis ".git"
im Projektverzeichnis an.

In diesem Ordner werden noch einige weitere Unterverzeichnisse angelegt, die
man mit dem Befehl "ls -l .git" einsehen kann.

    $ ls -l .git 
    insgesamt 32
    drwxr-xr-x 2 sujee sujee 4096 20. Jul 16:41 branches
    -rw-r--r-- 1 sujee sujee   92 20. Jul 16:41 config
    -rw-r--r-- 1 sujee sujee   73 20. Jul 16:41 description
    -rw-r--r-- 1 sujee sujee   23 20. Jul 16:41 HEAD
    drwxr-xr-x 2 sujee sujee 4096 20. Jul 16:41 hooks
    drwxr-xr-x 2 sujee sujee 4096 20. Jul 16:41 info
    drwxr-xr-x 4 sujee sujee 4096 20. Jul 16:41 objects
    drwxr-xr-x 4 sujee sujee 4096 20. Jul 16:41 refs

Wie man sieht, erzeugt Git einige Verzeichnisse und dort werden auch
Daten speichert. Das ".git"-Verzeichnis ist das einzige Verzeichnis in dem Git
die Informationen des Repositorys speichert. Daher sollte man dieses Verzeichnis keinesfalls
löschen, da man sonst alle Daten des Repositorys verliert, insbesondere alle 
Revisionen. Da zu diesem Zeitpunkt in dem Beispielrepository keinerlei 
Operationen mit Git durchgeführt worden sind, wäre das in diesem Beispiel 
natürlich nicht sonderlich tragisch. Man sollte allerdings die Dateien
nur anfassen, wenn man weiß was man tut.

#### Git Konfiguration

Da bereits ein leeres Repository angelegt worden ist, kann man ein Commit
hinzufügen. Was genau ein Commit ist, und wie man einen Commit tätigt, wird später
erläutert. Denn zunächst muss man noch seine Git-Installation konfigurieren.

Vorerst werden allerdings nur zwei Dinge konfiguriert: Der eigene Entwicklername
und dessen E-Mail-Adresse.

Mit dem folgenden Befehlen setzt man den eigenen Namen sowie die eigene 
E-Mail-Adresse:

    $ git config --global user.name "Sujeevan Vijayakumaran"
    $ git config --global user.email mail@svij.org

Mit diesen beiden Befehlen wird die Datei "~/.gitconfig" angelegt. Wenn man
in die Datei reinschaut steht folgendes darin:

    [user]
	    name = Sujeevan Vijayakumaran
	    email = mail@svij.org

Mit "git config -l" lässt sich über die Kommandozeile die Konfiguration ebenfalls ansehen.

Es muss beachtet werden, dass bei den oben genannten Befehlen die Git-Identität
global für den Benutzer gesetzt wird. Wenn man für einzelne Git-Repositorys
spezifische Einstellungen setzen will, dann reicht es, wenn man den Aufruf-Parameter
"--global" weg lässt. Dies ist häufig dann sinnvoll, wenn man verschiedene
E-Mail-Adressen für verschiedene Projekte nutzt. Die angegebenen Informationen
zu einem Entwickler sind nämlich für alle Personen einsehbar, welche mindestens
Lese-Rechte im Repository besitzen, sofern der Entwickler mindestens ein
Commit getätigt hat.

#### Der erste Commit

Jetzt beginnt das echte Arbeiten mit dem Repository. Zu Beginn ist das Repository
leer, da keine Dateien vorhanden sind. Es müssen also zunächst einige Ordner
und/oder Dateien angelegt werden. 

Der Befehl "git status" zeigt den aktuellen Status des Repositorys an. Es gibt
noch keine Commits, weshalb Git meldet, dass es sich um die "Initiale Version" handelt.


    $ git status
    Auf Branch master

    Initialer Commit

    nichts zu committen (Erstellen/Kopieren Sie Dateien und benutzen Sie "git add" zum Beobachten)

Da keine Dateien in dem Projekt-Verzeichnis vorhanden sind, meldet Git, dass
nichts zu committen ist. Für das Beispiel-Projekt „Webseite mit Git“ muss
zuerst das HTML-Framework „Bootstrap“ heruntergeladen und anschließend entpackt
werden. Zum Schluss kann das ZIP-Paket wieder entfernt werden.

    $ wget https://github.com/twbs/bootstrap/releases/download/v3.2.0/bootstrap-3.2.0-dist.zip
    $ unzip bootstrap-3.2.0-dist.zip
    $ mv bootstrap-3.2.0-dist/* .
    $ rmdir bootstrap-3.2.0-dist && rm bootstrap-3.2.0-dist.zip

Einige der oben aufgeführten Befehle geben Text auf der Standard-Ausgabe aus,
welcher hier aus Gründen der Übersichtlichkeit weggelassen worden ist.

Nachdem die Dateien im Projekt-Verzeichnis gelandet sind, bietet es sich an
noch einmal "git status" auszuführen.

    $ git status
    Auf Branch master

    Initialer Commit

    Unbeobachtete Dateien:
      (benutzen Sie "git add <Datei>..." um die Änderungen zum Commit vorzumerken)

	  css/
	  fonts/
	  js/

    nichts zum Commit vorgemerkt, aber es gibt unbeobachtete Dateien (benutzen Sie "git add" zum Beobachten)


Wie man sieht, zeigt Git an, dass unbeobachtete Dateien vorhanden sind.
Unbeobachtete Dateien sind Dateien, die nicht von Git verwaltet werden und für
Git somit unbekannt sind. Mit dem Befehl "git add" kann man Dateien und Ordner
zu dem Staging Bereich hinzufügen. Der Staging Bereich ist der Bereich, in dem
die Dateien hinzugefügt werden, um diese für einen Commit vorzumerken.

Zunächst wird nur der Ordner "css" hinzugefügt.

    $ git add css/

Eine Ausgabe erfolgt bei erfolreicher Ausführung nicht. Ein erneutes Ausführen
von "git status" gibt folgendes aus:

    $ git status
    Auf Branch master

    Initialer Commit

    zum Commit vorgemerkte Änderungen:
      (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)

	  neue Datei:     css/bootstrap-theme.css
	  neue Datei:     css/bootstrap-theme.css.map
	  neue Datei:     css/bootstrap-theme.min.css
	  neue Datei:     css/bootstrap.css
	  neue Datei:     css/bootstrap.css.map
	  neue Datei:     css/bootstrap.min.css
						      
    Unbeobachtete Dateien:
      (benutzen Sie "git add <Datei>..." um die Änderungen zum Commit vorzumerken)

	  fonts/
	  js/

Durch das Hinzufügen des Ordners "css", werden die einzelnen Dateien des Ordners
für den nächsten Commit vorgemerkt. Wenn man nicht den ganzen Ordner, sondern nur einzelne
Dateien hinzufügen möchte, geht das natürlich auch:

    $ git add fonts/glyphicons-halflings-regular.eot

Es bietet sich anschließend noch einmal an "git status" auszuführen:

    $ git status
    Auf Branch master

    Initialer Commit

    zum Commit vorgemerkte Änderungen:
      (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)

	    neue Datei:     css/bootstrap-theme.css
	    neue Datei:     css/bootstrap-theme.css.map
	    neue Datei:     css/bootstrap-theme.min.css
	    neue Datei:     css/bootstrap.css
	    neue Datei:     css/bootstrap.css.map
	    neue Datei:     css/bootstrap.min.css
	    neue Datei:     fonts/glyphicons-halflings-regular.eot

    Unbeobachtete Dateien:
      (benutzen Sie "git add <Datei>..." um die Änderungen zum Commit vorzumerken)

	    fonts/glyphicons-halflings-regular.svg
	    fonts/glyphicons-halflings-regular.ttf
	    fonts/glyphicons-halflings-regular.woff
	    js/


Vorher wurde das komplette "css"-Verzeichnis hinzugefügt. Mit dem Hinzufügen
einer einzelnen Datei, wird nicht mehr der Ordner allgemein von "git status"
gelistet, sondern es werden dann explizit alle einzelnen Dateien
aufgelistet.

Diesmal werden alle restlichen Dateien, die noch nicht beobachtet werden hinzufügt:

    $ git add fonts/ js/

Alternativ kann man auch den Befehl "git add -A" ausführen, um generell alle
unbeobachtete Dateien hinzuzufügen. Eine Ausgabe erscheint bei erfolgreicher
Ausführung nicht. Aber Achtung: Dies sollte man nur tun, wenn man sicher ist,
dass sonst keine weiteren temporären Dateien vorhanden sind, die nicht in dem
Commit landen sollen.

Falls doch Dateien ausversehen zum Staging Bereich hinzugefügt worden sind,
kann man sie ganz leicht mit "git rm --cached <Datei>" wieder entfernen.

Nach einem erneuten ausführen von "git status" werden alle hinzugefügte Dateien aus
den drei Unterordnern aufgelistet. Es bietet es sich nicht nur für Anfänger an jedes Mal vor
einem Commit die hinzugefügten Dateien mittels "git status" zu überprüfen, um
zu vermeiden, dass nicht ungewollte Dateien in das Repository eingetragen werden.

    $ git status
    Auf Branch master

    Initialer Commit

    zum Commit vorgemerkte Änderungen:
      (benutzen Sie "git rm --cached <Datei>..." zum Entfernen aus der Staging-Area)

	  neue Datei:     css/bootstrap-theme.css
	  neue Datei:     css/bootstrap-theme.css.map
	  neue Datei:     css/bootstrap-theme.min.css
	  neue Datei:     css/bootstrap.css
	  neue Datei:     css/bootstrap.css.map
	  neue Datei:     css/bootstrap.min.css
	  neue Datei:     fonts/glyphicons-halflings-regular.eot
	  neue Datei:     fonts/glyphicons-halflings-regular.svg
	  neue Datei:     fonts/glyphicons-halflings-regular.ttf
	  neue Datei:     fonts/glyphicons-halflings-regular.woff
	  neue Datei:     js/bootstrap.js
	  neue Datei:     js/bootstrap.min.js

Wenn alle Dateien korrekt mit "git add" eingetragen worden sind, kann man 
den ersten Commit tätigen. Der Commit enthält dabei dann eben genau die Dateien,
die man mit "git add" zum Staging Bereich hinzugefügt hat. Dateien, die man
eventuell ausgelassen hat, bleiben unangetastet.

Mit dem folgenden Befehl wird der erste Commit erzeugt.

    $ git commit -m "Bootstrap hinzugefügt."
    [master (Basis-Commit) 7f1c942] Bootstrap hinzugefügt.
    12 files changed, 9006 insertions(+)
    create mode 100644 css/bootstrap-theme.css
    create mode 100644 css/bootstrap-theme.css.map
    create mode 100644 css/bootstrap-theme.min.css
    create mode 100644 css/bootstrap.css
    create mode 100644 css/bootstrap.css.map
    create mode 100644 css/bootstrap.min.css
    create mode 100644 fonts/glyphicons-halflings-regular.eot
    create mode 100644 fonts/glyphicons-halflings-regular.svg
    create mode 100644 fonts/glyphicons-halflings-regular.ttf
    create mode 100644 fonts/glyphicons-halflings-regular.woff
    create mode 100644 js/bootstrap.js
    create mode 100644 js/bootstrap.min.js

Der Befehl speichert erst in diesem Punkt den aktuellen Staging Bereich in einen
Commit. Mit dem Parameter "-m" kann eine Commit-Nachricht direkt übergeben werden,
diese fasst in der Regel die aktuellen Änderungen zusammen, sodass andere 
Mitarbeiter in dem Repository die Änderungen in dem Commit schnell und einfach
nachvollziehen können. Man kann auch einen Commit erzeugen ohne den Parameter
"-m" anzugeben. Stattdessen öffnet sich der Standard-Editor des Systems
in dem man die Commit-Nachricht eintippen kann.

Ein erneutes "git status" zeigt nach dem Commit erstmal keine Änderungen an.

    $ git status
    # Auf Zweig master
    nichts einzutragen, Arbeitsverzeichnis sauber

Der erste Commit ist also getätigt. Dieser wird häufig initialer Commit
oder Basis-Commit genannt, weil er der erste Commit des Repositorys ist,
auf welchem die anderen Commits aufbauen.

Mit "git log" kann man die Historie des Repositorys anschauen. Bei lediglich einem
Commit ist es in diesem Fall natürlich sehr kurz.

    $ git log
    commit 7f1c942a8275fdeab84ebee61e6fe43a6d48e888
    Author: Sujeevan Vijayakumaran <mail@svij.org>
    Date:   Sun Jul 20 17:24:13 2014 +0200

	Bootstrap hinzugefügt.

Wichtig ist vor allem, dass jeder getätigte Commit eine eindeutige ID besitzt,
auf welche man sich beziehen kann, wenn dies benötigt wird. Dies kann unter
anderem dafür genutzt werden, um das Git Log zwischen zwei verschiedenen Revisionen
anzusehen oder auch um Änderungen eines speziellen Commits rückgängig zu machen.
Die ID ist eigentlich eine SHA-1 Checksumme, die aus den Änderungen erzeugt wird.
Weiterhin werden auch noch das Datum und der Autor im Commit vermerkt.

Nachdem der erste Commit erledigt ist, kann die Arbeit also beginnen. Die Webseite,
die mit diesem Tutorium angelegt wird, braucht zunächst eine "index.html"-Datei
mit folgendem Inhalt:

    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
      </head>
      <body>
        <h1>Hello, world!</h1>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
      </body>
    </html>

Die Datei dient als Basis-Template für das HTML-Framework Bootstrap. Sie wird
zunächst nicht von Git beobachtet, da die Datei neu ist. Auch diese Datei muss
in das Repository committet werden:

    $ git add index.html
    $ git commit -m "index.html hinzugefügt."
    [master 4cc7ce4] index.html hinzugefügt.
    1 file changed, 21 insertions(+)
    create mode 100644 index.html

Hiermit wurde also der zweite Commit erzeugt. Der Befehl "git log" listet
beide auf:

    $ git log

    commit 4cc7ce45fb1a73d10325b465062d1ffa3435702f
    Author: Sujeevan Vijayakumaran <mail@svij.org>
    Date:   Sun Jul 20 17:37:51 2014 +0200

	index.html hinzugefügt.

    commit 7f1c942a8275fdeab84ebee61e6fe43a6d48e888
    Author: Sujeevan Vijayakumaran <mail@svij.org>
    Date:   Sun Jul 20 17:24:13 2014 +0200

	Bootstrap hinzugefügt.


Da das Grundgerüst der Webseite steht, kann man die Startseite "index.html"
nach seinem Belieben anpassen. Die Webseite braucht jetzt einen Titel im HTML-Head
und einen kleinen Inhalt im Body.

Um den Titel zu verändern, reicht es Zeile 8 mit folgenden Inhalt zu überschreiben:

    <title>Webseite mit Git</title>

Statt der Überschrift "Hallo Welt", wird in diesem Beispiel git gegrüßt:

    <h1>Hallo Git!</h1>

Beide Dateien können jeweils mit einem Editor bearbeitet werden.
Nachdem man diese Änderungen durchgeführt hat,
kann man sich alle Änderungen mit "git diff" anschauen. Doch zunächst lohnt
sich mal wieder ein Blick auf die Ausgabe von "git status".

    $ git status
    Auf Branch master
    Änderungen, die nicht zum Commit vorgemerkt sind:
      (benutzen Sie "git add <Datei>..." um die Änderungen zum Commit vorzumerken)
      (benutzen Sie "git checkout -- <Datei>..." um die Änderungen im Arbeitsverzeichnis zu verwerfen)

	  geändert:       index.html

	  keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")

Git bemerkt automatisch, dass sich der Inhalt von "index.html" verändert hat.
Die Veränderung betrifft alle Änderungen seit dem letzten Commit.

Die Ausgabe von "git diff" zeigt letztendlich die Änderungen an der Datei an:

    $ git diff

    diff --git a/index.html b/index.html
    index 7a050c7..ea3b0af 100644
    --- a/index.html
    +++ b/index.html
    @@ -5,13 +5,13 @@
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    -    <title>Bootstrap 101 Template</title>
    +    <title>Webseite mit Git</title>
    
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
      </head>
      <body>
    -    <h1>Hello, world!</h1>
    +    <h1>Hallo Git!</h1>
    
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


Git zeigt die Unterschiede zwischen der aktuellen Datei und der Datei an, die
zuletzt committet wurde. Entfernte Zeilen werden mit einem Minus zu Beginn der
Zeile angezeigt, neu hinzugefügte Zeilen wiederum mit einem Plus. Gleichen Effekt
hat es bei geänderten Zeilen.

*Tipp:* Zur Besseren Übersicht über die Ausgaben von Git im Terminal bietet es sich
an, die Farbausgabe zu aktivieren. Dies kann global in der Konfiguration mit
folgenden Befehl gesetzt werden:

    $ git config --global color.ui true

Gelöschte Zeilen werden hiermit rot dargestellt und neu hinzugefügte Zeilen grün.
Dieser Konfigurationsschalter wirkt sich allerdings nicht nur hier aus, sondern
auch an weiteren Stellen.

Die Änderungen können wie gehabt wieder in den Staging Bereich gebracht werden
und anschließend committet werden:

    $ git add index.html
    $ git commit -m "Titel und Überschrift angepasst."
    [master 24e65af] Titel und Überschrift angepasst.
    1 file changed, 2 insertions(+), 2 deletions(-)

Wenn man alle Befehle wie angegeben ausgeführt hat, besitzt das Repository
drei Commits.

### Fazit

In diesem Teil wurden die grundlegendsten Git Kommandos erläutert. Man weiß nun
unter anderem, wie man Dateien zu einem Repository hinzufügt, ein Commit tätigt
und wie man sich das Repository-Log anschauen kann.

Der nächste Teil behandelt das Branching Modell von Git.

 * [Hier geht es zu Teil 2](/blog/2014/11/01/git-fur-einsteiger-teil-2/)</li>
 * [Hier geht es zu Teil 3](/blog/2015/01/12/git-fur-einsteiger-teil-3/)</li>
