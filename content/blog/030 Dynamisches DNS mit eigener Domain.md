---
title: "Dynamisches DNS mit eigener Domain"
categories: ["Kurztipps"]
date: "2014-07-05 12:45:00+02:00"
toc: true
---

Auch ich nutzte bislang den dynamischen DNS Dienst von noip.com. 
[Microsoft übernahm einige NoIP-Domains](http://www.heise.de/newsticker/meldung/Malware-Microsoft-erzwingt-Umleitung-von-Domains-des-DynDNS-Diensts-NoIP-2243605.html)
und davon war auch ich betroffen. Das ganze war für mich ungünstig, da ich dann
ein paar Tage nicht mehr auf meinen Homeserver von außerhalb zugreifen konnte.

Ich musste mir also entweder einen anderen Dienst suchen oder eben was eigenes
machen. Ein Arbeitskollege wies mich glücklicherweise darauf hin, dass diverse
Domain-Registrare die Möglichkeit anbieten, dynamische (Sub)domains zu konfigurieren.
Und siehe da: mein Domain-Registrar namecheap.com [bietet diese Möglichkeit](https://www.namecheap.com/support/knowledgebase/category.aspx/11/dynamic-dns) an.

Die Konfiguration war denkbar einfach. Zuerst habe ich die DynDNS-Funktion
auf meiner Domain freigeschaltet. Anschließend musste noch eine Anwendung her, welches
die IP in regelmäßigen Abständen aktualisiert. Dafür installierte ich 
das Paket `ddclient`, da es kompatibel mit Namecheap ist.

In der Konfigurationsdatei unter `/etc/ddclient/ddclient.conf` musste ich 
anschließend nur noch folgendes hinzufügen:

    use=web
    ##
    ## NameCheap (namecheap.com)
    ##
    protocol=namecheap,                             \
    server=dynamicdns.park-your-domain.com,         \
    login=svij.org,                                 \
    password=geheimesPasswortvonNamecheap           \
    dyndns

In der Basis-Konfigurationsdatei ist die Rohfassung für die Konfiguration
mit Namecheap bereits enthalten. Das notwendige Passwort findet man als Namecheap-Domain-Inhaber
in der Web-Konfiguration der eigenen Domain. Ich legte mir also zunächst
die Subdomain `dyndns` an, konfigurierte ddclient wie oben aufgeführt und startete
anschließend den Dienst von ddclient. Der Dienst prüft im 5-Minuten-Takt
ob sich die IP-Adresse geändert hat und aktualisiert dann gegebenfalls die IP bei Namecheap.

Interessant ist übrigens, dass der Schalter `server` wirklich die Domain
`dynamicdns.park-your-domain.com` verlangt und eben kein Platzhalter ist.

## Fazit

Durch den Einsatz der eigenen Domain für dynamisches DNS, spare ich mir wieder
einen externen Dienst. Stattdessen nutze ich einfach den Dienst, den ich sowieso
schon besitze. Den Account bei No-IP könnte ich nun löschen.
