---
title: "Neuer Podcast: TILpod"
categories: ["Sonstiges"]
date: 2020-12-04T10:00:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: "https://vg02.met.vgwort.de/na/546faafd721e40fca58cf8da4cc2f1f0"
---

Wir sind mittlerweile im Jahr 2020 angekommen. Podcasts haben sich mehr oder weniger
durchgesetzt und sehr viele Leute hören nicht nur Podcasts, sondern produzieren
auch selbst welche. Vor allem im Tech-Bereich gibts es gefühlt schon fast mehr
als genug...

Nichtsdestotrotz – oder gerade deswegen? – dachten sich [Dirk Deimeke](https://dirk.deimeke.ruhr/)
und ich: Es wird Zeit, dass wir auch mal einen Podcast starten. Den Gedanken
und die Pläne dazu hatten wir eigentlich schon vor zwei Jahren. Aber aus diversen
Gründen wurde daraus erstmal nichts und wir haben es aufgeschoben. Jetzt ist
aber endlich so weit.

Mittlerweile haben wir auch schon die nullte und die erste Folge
veröffentlicht. In der nullten Episode erzählen wir von unseren Plänen für den
Podcast. Darunter was das Konzept des Podcasts ist und welche Themen wir behandeln
wollen. Außerdem gibt es einen Rückblick wie und wann Dirk und ich uns kennen gelernt haben.

*Spoiler*: In diesem Podcast geht es nicht (ausschließlich) um Open Source, sondern vor allem um Tech, aber
auch um Produktivitäts- und IT-Karriere-Themen. Es wird also hoffentlich ein gesunder
Mix aus den verschiedenen Themen sein.

Die erste reguläre Episode behandelt das Thema „GitOps“ und „Keine Zeit haben“.
Wie ihr aus dem Titel dieses Blogposts erkennen könnt, lautet der Name des
Podcasts „TILpod“ und dieser ist unter [TILpod.net](https://tilpod.net/)
erreichbar. „TIL“ steht dabei für „Today I learned“, es geht also um Themen die
neu für uns beide waren.

Bevorzugt lässt sich der Podcast natürlich überall da hören, wo es Podcasts
gibt. Die nächste, und somit zweite Episode, erscheint am 1. Januar 2021. Wir
peilen also mindestens ein monatliches Release an. Je nach Lust und Laune kann
da durchaus noch eine zusätzliche Folge pro Monat entstehen.

Hört gerne rein! Und sehr cool wäre es, wenn ihr uns fleißig Feedback da lässt.
Für den Beginn müssen wir uns wohl erstmal ein wenig einspielen, bis es dann
hoffentlich über die Zeit besser wird – sowohl inhaltlich als auch von der
Technik.

### Links

 * [TIL000 – Wir stellen uns vor!](https://tilpod.net/episode/wir-stellen-uns-vor)
 * [TIL001 – Keine Zeit für GitOps](https://tilpod.net/episode/til001-keine-zeit-fuer-gitops)
