---
title: "My DevOps Book is now available in English!"
categories: ["Books"]
date: 2024-12-30T14:00:00+01:00
cover:
  image: "/graphics/devops-book-en.webp"
---

Next to my regular job I regularly work on my books. Previously I wrote a Git book in multiple editions over the last eight years. This year my DevOps book came out. All these books were in German. But now I'm happy that the DevOps book is also available in English.

## What's in it?

The understanding of what DevOps is and what it is not is often very controversial. Although, I'm exaggerating by using the word “controversial”. Clearly, there are a lot more misunderstandings about what “DevOps” really means.

DevOps does not mean that developers and admins now do the same jobs. DevOps also doesn't mean that you have to use new tools every day when programming, that there are no more planned deployments and that software only runs in containers.

DevOps is much bigger: It promises a new culture of collaboration, as well as better processes and workflows. This allows you to deliver changes faster and ensure shorter feedback loops between development and operations.

In numerous projects, I have learned what works well and what doesn't work well in the development and operation of modern software. The book contains many examples and practical tips on how to create a modern and up-to-date working environment for IT projects and how DevOps transformation in teams can succeed.

## Reading Sample and Purchasing

Of course there is also a sample. You can find it [here](https://s3-eu-west-1.amazonaws.com/gxmedia.galileo-press.de/leseproben/6030/2670_reading_sample.pdf).

The book is available wherever you can buy books. These are the listprices:
- Print: $49.95
- E-book: $44.95
- Bundle: $59.95

Ideally, you should buy books (especially technical books) [directly from the publisher](https://www.sap-press.com/devops_6030/). Both the publisher and I, as the author, benefit the most from this.

Of course, you can also find the book on [Amazon](https://www.amazon.com/DevOps-Frameworks-Techniques-Development-Strategies/dp/1493226703). Depending on your country, it will be available over the coming weeks and months. 
