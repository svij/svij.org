---
title: Bilder verkleinern mit Imagemagick
categories: ["Shell"]
date: "2011-06-08T23:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Eine Methode um schnell und relativ einfach Bilder mittels eines Scripts zu verkleinern, ist die Möglichtkeit ImageMagick zu nutzen. Dabei muss man zunächst ImageMagick aus den Paketquellen installieren.

Um nun die Bilder zu verkleinern, nutze ich das folgende Script, welches in dem Ordner mit den Bildern liegt.

    mkdir small;
    for i in *.JPG;
    do
    j=${i//\.JPG/};
    convert -resize 50% -quality 65 $i small/${j}_klein.JPG;
    done

Nach dem Erstellen des Scriptes muss man die Datei ausführbar machen. Dies geschieht am einfachsten im Terminal mittels:

    chmod +x dateiname

Nützlich zu wissen ist auch, wie das Script genau arbeitet. Zunächst wird in dem Ordner ein Ordner mit dem Namen small erstellt. In der darauffolgenden Schleife wird der convert Befehl von ImageMagick ausgeführt. In diesem Fall werden dabei zwei Parameter übergeben. Das ist zum einen -resize 50% und des Weiteren -quality 65. Bei dem Parameter resize kann man sowohl einen Prozentsatz als auch eine Auflösung zum Verkleinern bzw auch zum Vergrößern des Bildes angeben. Mit quality lässt sich die Qualität des Bildes beeinflussen. Je geringer die Auflösung und die Qualität ist, desto kleiner wird die Dateigröße, als auch die Qualität des Bildes. Für den Webgebrauch sind Bilder mit einer kleinen Dateigröße wichtig, da damit neben des verschnellerten Uploads auch die Anzeige beim Webseiten-Besucher beschleunigt wird. Meiner Meinung nach erkennt man bei der halbierung der Auflösung und Reduzierung der Qualität nur geringfügige Qualitätsverluste, die man für den Webgebrauch in Galerien einstecken kann. Aus diesem 
Grunde verkleinere ich alle meine Bilder die ich in meine Galerie hochlade mit diesen Einstellungen.
