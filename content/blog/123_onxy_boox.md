---
title: "E-Book Reader: Onyx BOOX Note3 im Einsatz"
date: "2021-05-08T19:15:00+02:00"
categories: ["Sonstiges"]
vgwort: "https://vg02.met.vgwort.de/na/c3e454ea239b46faac0dec79a1daabe4"
tags: ["ubuntuusers-planet"]
cover:
  image: "/graphics/onyx-boox.webp"
---

Lesen ist etwas, was ich generell viel zu wenig mache. Auf gedruckten Papier habe ich 
seit Ewigkeiten nicht mehr gelesen. Schon seit einigen Jahre besitze ich zwar
einen Kindle Paperwhite von Amazon, aber die Nutzung variierte stark. Mal mehr,
mal gar nicht. 

Ein paar Gründe führten zu dem Umstand, dass ich das Kindle weniger genutzt habe:

 * Es ist von Amazon und ich versuche aus verschiedenen Gründen so wenig bei Amazon wie möglich zu kaufen. Vor allem keine Bücher, da dann am wenigstens beim Autor hängen bleibt. (Kauft am besten beim Verlag!)
 * Keine Unterstützung für `.epub` und somit Zwang auf Tools wie Calibre zu setzen.
 * Keine (gute) Unterstützung für PDF-Dateien.
 * Keine (gute) Möglichkeit Annotationen an Dokumenten zu erstellen.

Die letzten beiden Use-Cases sind speziellere Funktionen die ich nicht so
häufig brauche. Als Autor und somit bei der Arbeit an der [dritten Auflage meines
Git Buches](https://svij.org/blog/2021/03/03/dritte-auflage-meines-git-buches-erschienen/)
muss ich mehrfach vor der Drucklegung die Fahne Korrekturlesen. Ich weiß, dass
beim Verlag sehr viel gedruckt wird, weil es sich einfach besser liest als auf
einem Bildschirm und dann mit Stift Anmerkungen gemacht werden. Ich hatte das
eine Weile lang auf einem Android Tablet gemacht, das war aber wegen des Displays
eher auf der Kategorie „nicht so geil zum Lesen“. Notizen für Korrekturen waren auch eher umständlich
und zudem besitze ich gar kein Tablet mehr.

Ich wollte also mehr Lesen und wollte auch die Möglichkeit haben, Notizen machen
zu können, idealerweise mit Stift.

Zur Wahl standen letztendlich drei Geräte:

 * [reMarkable 2](https://remarkable.com/store/remarkable-2) (458€)
 * [Onyx BOOX Note Air](https://www.boox.com/noteair) (479,99€)
 * [Onyx BOOX Note 3](https://www.boox.com/note3) (549,99€)

Alle drei Geräte sind E-Book-Reader mit guter Funktionalität um Notizen machen
zu können. Das reine Lesen von Büchern ist ja eher der langweilige Part. Alle
Geräte besitzen ein 10,3" großes Display. Die Größe war wichtig, damit das Machen von Notizen
auf einem nicht ganz so kleinem Gerät erfolgen muss. Ich hatte zwar Angst, dass
es zu groß sein könnte, aber das war dann doch passend.

Gekauft habe ich letztendlich das Onyx BOOX Note 3. Und hab mich im Wesentlichen
gegen ein reMarkable entschieden, was die eher bekanntere Marke ist. Meine Gründe
sind folgende:

 * Das reMarkable ist an die Hersteller-Cloud gebunden (Minus-Punkt)
 * Das Onyx BOOX läuft mit Android und ohne Zwang zur Bindung an Google (Plus-Punkt)
 * Das Onyx BOOX kann ohne Hersteller-Cloud und somit relativ „frei“ betrieben werden
 * Kollege Dirk hatte mal ein reMarkable 1 und kaufte sich später ein Onyx BOOX Max 3, [darüber schreibt er auch in seinem Blog](https://www.deimeke.net/dirk/blog/index.php?/archives/4008-Onyx-Boox-Max-3.html), und ist sehr zufrieden (Plus-Punkt)

Der Preis war natürlich nicht ganz so klein, aber man gönnt sich ja sonst nichts.

Grundsätzlich hat sich der Kauf gelohnt, es tut genau das, was es soll und es kommen
auch Updates mit weiteren Funktionen. Die Notiz-Funktion habe ich nicht nur für
die Korrekturen an meinem Buch genutzt, sondern auch für generelle Notizen, um
ablenkungsfrei Nachdenken zu können. Die App dafür hat sehr viele Funktionen,
wovon ich für meinen Zweck nicht so viel brauche.

Diejenigen die sich für richtig tiefe Reviews für solche Geräte interessiert, denen
kann ich den YouTube-Kanal [My Deep Guide](https://www.youtube.com/c/MyDeepGuide)
empfehlen, wo der Herr die verschiedensten E-Book-Reader miteinander vergleicht.
Es gibt einen sehr guten Eindruck was die Geräte können und in welchen Aspekten
die Geräte besser als die anderen sind.

Die Anzahl an Android Apps hab ich gering gehalten. Über den F-Droid Store habe
ich lediglich einen anderen Browser und den Nextcloud-Client für Datentransfers
installiert und das klappt wunderbar.

Ein Use-Case den ich beim Kauf noch nicht im Blick hatte, ist, dass ich über meinen
Arbeitgeber Zugang zur [O'Reilly Learning Platform](https://www.oreilly.com/)
bekommen habe. Da gibt es nicht nur Bücher zu lesen, sondern auch Video-Kurse
sind dort enthalten. Und das sind nicht nur Bücher von O'Reilly, sondern auch
deutsche Verlage wie dpunkt oder mitp sind vertreten, sodass ich da auch mein
Buch entdeckt habe. Die Inhalte lassen sich entweder im Browser lesen oder über
die eigene App, ansonsten bekommt man die Daten da nicht so einfach raus, da es
quasi eher dem Netflix-Modell entspricht. Die App konnte ich mir also auf mein
E-Book-Reader geladen und konnte so schon einige Bücher lesen, die ich auf jeden
Fall nicht im Browser am Bildschirm gelesen hätte. Vertraut mir, ich habs versucht...

***tl;dr***: Onyx BOOX Note 3 für relativ viel Geld gekauft. Ich lese jetzt deutlich
mehr. Es deckt meine Use-Cases gut ab. Es nervt nicht. Notizen machen per Stift
fühlt sich erstaunlich „echt“ an. Gerne wieder.
