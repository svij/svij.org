---
title: "Attending UbuCon Summit US in 01/2016"
categories: ["Ubuntu"]
date: "2015-12-19 18:50:00+02:00"
tags: ["ubuntu-planet"]
slug: "attending-ubucon-summit-us-in-012016"
toc: true
---

2016 will be my favourite „UbuCon-year“. The first UbuCon Summit in Pasadena
will take place and at the end of the year the first [UbuCon Europe](http://ubucon.eu) will take
place in Essen, Germany from 18th to 20th November. In the latter I'm the head
of the organisation team.

The UbuCon Summit is just around the corner and I'm really looking forward to attending
the event. It's the first time that I requested money from the Ubuntu Community
Donations Fund which got thankfully accepted. [The schedule](http://ubucon.org/en/events/ubucon-summit-us/schedule/)
is complete since a few days and there are many interesting talks, including the
opening keynote by Mark Shuttleworth. I'm also going to give a talk about the
[Labdoo Project](http://labdoo.org/), which is a humanitarian social network
to bring education around the globe. This will be also my first talk at a conference
in English ;-).

If you live in South California and didn't hear about UbuCon Summit yet, you should
definitely consider visiting this event. It'll be cohosted by the [South California
Linux Expo](https://www.socallinuxexpo.org/scale/14x/schedule/) which alos have many
interesting talks.

I'm looking forward to meeting all my old and new friends. Especially those which I
didn't meet yet, like Richard Gaskin and Nathan Haines, who organises the UbuCon
Summit. See you there!
