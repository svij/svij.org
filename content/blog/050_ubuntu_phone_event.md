---
title: "Ubuntu Phone Insider Event in London am 6. Februar"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-02-05 12:25:00+02:00"
slug: "ubuntu-phone-insider-event-in-london-am-6-februar"
toc: true
---

Für [Ikhaya](http://ikhaya.ubuntuusers.de) habe ich bereits im Dezember über
[den Launch vom ersten Ubuntu Phone](http://ubuntuusers.de/ikhaya/6443/) geschrieben.
Am morgigen 6. Februar findet dazu in London das sogenannte „Ubuntu Phone Insider
Event“ statt. Dort wird dann das erste Ubuntu Phone, welches von der spanischen
Firma bq gefertig wird, vorgestellt. Mit dabei sind nicht nur Mitarbeiter von
Canonical, sondern es sind auch viele Leute aus der Community eingeladen. Darunter
bin glücklichweise auch ich, sodass ich als wohl einziger Deutscher aus der
deutschen Community dort anwesend sein werde. Schön ist auch, dass
jedes eingeladene Community-Mitglied das Phone noch vor offiziellen Marktstart
geschenkt bekommt.

<a href="/graphics/ubuntuphone_event.webp" data-lightbox="image" data-title="Event-Einladung">
  <img src="/graphics/ubuntuphone_event.webp" style="margin:20px;">
</a>

Vor Ort versuche ich dann morgen noch ein paar nette Interviews zu führen, solange
es die Zeit und die Möglichkeit dazu gibt mit den passenden Leuten zu reden.
Immerhin hat [Jono Bacon über Twitter](https://twitter.com/jonobacon/status/562670491547090944)
einem Interview zugestimmt ;-). Falls jemand Fragen über Ubuntu Phone hat, die
nicht ganz offensichtlich im Internet stehen, dann kann man sie mir gerne mailen
(an svij@ubuntu.com). Ich versuche dann möglichst viel abzudecken.

Testbericht vom Phone, Geschehnisse vom Launch Event und weiteres über Ubuntu
Phone folgen dann sowohl hier als auch auf [ikhaya.ubuntuusers.de](http://ikhaya.ubuntuusers.de).
Zwischendurch werde ich morgen wohl auch über mein [Google+-Profil](https://plus.google.com/u/0/+SujeevanVijayakumaran)
und über [Twitter](https://twitter.com/svijee) vom Event berichten.
