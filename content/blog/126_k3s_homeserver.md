---
title: "Homeserver Setup 2021: k3s mit Flux und Helm"
categories: ["Server"]
date: 2021-11-28T18:30:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: "https://vg05.met.vgwort.de/na/f15278bd1acc46fab87d7306116cedc6"
---

Im Blogpost [„Homeserver in 2020…“](https://svij.org/blog/2020/06/12/homeserver-setup-in-2020.../)
beschrieb ich im Juni 2020, wie mein Homeserver-Setup damals aussah. In diesem
Blogpost gebe ich ein kleines Update, wie es heute, im Jahr 2021, aussieht.
Extremst viel hat sich nicht verändert, doch sind ein paar Funktionen dazu
gekommen, die die Wartung des Servers vereinfacht.

Damals wie heute gilt: Ich will möglichst viel lernen und dabei Dienste einsetzen
und betreiben, die für mich einen gewissen Mehrwert haben. Das ist sowohl der
Funktionalität des Dienstes selbst, als auch die Installationsweise, die mich
in welcher Weise auch immer weiter bringt.

# k3s

In der [November-Ausgabe TIL013](https://tilpod.net/episode/til013-patriotische-sysadmins-gegen-die-kubernetisierung-der-serverlandschaft) des Podcasts [TILpod](https://tilpod.net),
den ich mit [Dirk Deimeke](https://dirk.deimeke.ruhr/) veröffentliche, erzählte
ich bereits in Tonform warum, wieso und weshalb ich auf [k3s](https://k3s.io/)
als Kubernetes-Distribution für meinen Homeserver betreibe. Wer die
Podcast-Folge gehört hat, wird hier eher wenig Neues lesen.

Grundsätzlich betreibe ich, wie zuvor geschrieben, meinen Homeserver mit k3s.
Die Kubernetes-Distribution ist relativ „langweilig“: Es handelt sich um eine
leichtgewichtige Kubernetes-Distribution, die in einer einzelnen Binärdatei
ausgeliefert und installiert werden kann. Das macht die ganze Wartung auf
Dauer simpel. So kann ich ein Upgrade und Downgrade sehr simpel ausführen, in
dem ich die Binärdatei auf meinem Homeserver austausche. Das klappte grundsätzlich
auch immer. Upgrade-Probleme gibt es hingegen trotzdem, was allerdings mehr mit
Kubernetes als mit k3s selbst zu tun hatte.

So gab es mal Probleme mit der Unterstützung von btrfs als Dateisystem in
Kubernetes generell, weshalb ein Upgrade fehlschlug. Zudem ändern sich häufiger
mal `apiVersion` von Manifests, sodass man da auch mal nach einem Update
nacharbeiten muss. Aus diesem Grund schaue ich, dass ich möglichst häufig und
regelmäßig ein Upgrade von k3s ausführe, um mögliche Probleme frühzeitig zu
entdecken. Ansonsten ist man mit zu vielen Problemen gleichzeitig beschäftigt.

Das klingt zwar jetzt grundsätzlich abschreckend, kam in den letzten ~2 Jahren
allerdings auch nur zwei Mal vor, dass mal größere Dinge nicht funktioniert haben,
ansonsten gab es da weitaus weniger Probleme.

# Flux und Helm

Um meine Deployments auf den Cluster zu automatisieren, nutze ich [FluxCD](https://fluxcd.io/).
Bis September 2021 setzte ich noch auf das „alte“ Flux v1. Danach führte ich
ein Upgrade auf Flux v2 durch, was durchaus eine nennenswerte Weiterentwicklung
ist, die nicht abwärtskompatibel zu v1 ist.

Aber was tut es genau? Grundsätzlich bildet Flux den deklarativen Stand des
Repositories im Kubernetes-Cluster ab, neuerdings GitOps genannt. Das heißt im
konkreten, ich definiere im Repository – in einer gewissen Struktur – meine
Dienste. Ich verwende größtenteils wo es geht Helm Charts, da ich fast nur
Dienste hoste, die ich nicht selbst geschrieben habe.

Als „Suchmaschine“ für Helm Charts verwende ich [ArtifactHUB](https://artifacthub.io/).
Viele Helm-Charts sind allerdings ordentlich veraltet oder nicht gepflegt, von
daher prüfe ich immer, wie aktuell die Helm-Charts sind und von wem sie angeboten
wird. Es gibt zum Beispiel die Community [k8s-at-home](https://k8s-at-home.com/)
welche diverse Helm Charts zur Verfügung stellt, die ich nutze. Ansonsten sind
es häufig auch die Projekte selbst, die eigene Helm Charts anbieten, die ich
bevorzugt nutze. Dazu gehören zum Beispiel die Helm Charts von
[Grafana](https://github.com/grafana/helm-charts) und
[Prometheus](https://github.com/prometheus-community/helm-charts).

Mein Lieblingsfeature von Flux 2 ist einfache und komfortable Möglichkeit, um
automatische Updates auszuführen. Früher hatte ich die Updates händisch
vorgenommen. Das heißt, ich habe die YAML-Dateien aufgemacht und händisch
jeweils geprüft, ob es eine neuere Version des Helm-Charts gibt. Bei so zehn
Anwendungen, die ich so betrieben hatte, war das etwas nervig, weil das viel
unnötige Arbeit war.

Mit Flux 2 gebe ich nur noch an, in welcher Versionen ich die Helm-Charts installieren
möchte. Für Grafana sieht meine Zeile aktuell etwa so aus:

    version: ">=6.16.6 <7.0.0"

Es wird also mindestens die Helm Chart Version von Grafana 6.16.6 installiert
und wird kein Upgrade auf die nächste Major Version 7.0.0 durchgeführt.
Dieses Vorgehen mache ich für alle meine Helm-Charts, sodass ich keinen Aufwand
für Upgrades habe. Ausgenommen sind Major-Releases, weil sich da häufiger mal das
Format der `values.yaml` ändert. Manchmal wird es allerdings auch schon vorher
geändert, was zwar auch ein wenig umständlich ist, aber das passiert zum Glück
eher nicht so häufig.

Das automatische Upgrade von Versionen hab ich nun seit drei Monaten im Einsatz.
Um Upgrades zusätzlich mitzubekommen, habe ich die genutzten Helm-Charts über
[ArtifactHUB](https://artifacthub.io) abonniert. Wenn also neue Updates veröffentlicht
werden, erhalte ich eine E-Mail. So behalte ich Überblick über neue Major-Releases
von mir genutzten Helm-Charts, um manuell das Upgrade durchzuführen.

# Anwendungen

Bei den Anwendungen hat sich in den letzten 1,5 Jahren nicht so viel geändert:

 * [Nextcloud](https://nextcloud.com/) für Kalender, Kontakte und Dateisynchronisation
 * [Vaultwarden](https://github.com/dani-garcia/vaultwarden) als Passwortmanager (Reimplementation von Bitwarden)
 * [Prometheus](https://prometheus.io/) für das Monitoring
 * [Grafana](https://grafana.com) für die Visualisierung der Daten aus Prometheus, Influx und PostgreSQL
 * [GitLab](https://about.gitlab.com) für Git-Repositorys
 * [Unifi](https://ui.com) Controller für meine Unifi APs
 * [Syncthing](https://syncthing.net) für das Synchronisieren von Backups zu einer Offsite-Stelle
 * [rclone](https://rclone.org) als Cronjob für das Hochladen von Backups „in die Cloud“
 * [Dataexporter der Photovoltaik-Anlage](https://gitlab.com/svij/kostal-dataexporter), der die Daten in eine Influx und PostgreSQL Datenbank schreibt
 * [Influxdb](https://www.influxdata.com/) für die Daten der PV-Anlage, sowie Stromverbrauch und Temperatur-Sensoren
 * [PostgreSQL](https://www.postgresql.org/) für Nextcloud, Kostal Datenhaltung und GitLab
 * [Jellyfin](https://jellyfin.org/) für Multimedia-Daten, ersetzte Plex
 * [minio](https://min.io/) als S3-kompatibles Object-Storage-Backend für GitLab
 * [traefik](https://traefik.io) als Reverse Proxy
 * [Home-Assistant](https://www.home-assistant.io/) für Smart-Home
 * [mosquitto](https://mosquitto.org/) als MQTT broker
 * [paperless-ng](https://github.com/jonaswinkler/paperless-ng) für Dokumenten-Management
 * [fritz_exporter](https://github.com/pdreker/fritz_exporter) um Fritz!Box Daten für Prometheus zu exportieren
 * [starboard](https://aquasecurity.github.io/starboard/v0.13.0/) um die Container-Images im Cluster zu scannen

Außerhalb von Kubernetes läuft sonst weiterhin
[borgbackup](/blog/2016/11/12/verteiltes-backup-konzept-mit-borg/) und sonst
eigentlich nichts mehr.

In Zukunft werd ich noch einiges mehr ausprobieren müssen. Zum Beispiel
[ArgoCD](https://argo-cd.readthedocs.io/en/stable/) statt Flux. Ansonsten sind
noch etliche Projekte in der [CNCF Landscape Trail Map](https://github.com/cncf/landscape/blob/master/README.md#trail-map)
die man sich anschauen kann.
