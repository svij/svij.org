---
title: "Schreibtisch-Review: Stehen und Sitzen am IKEA Bekant"
categories: ["Sonstiges"]
date: "2015-04-25 18:30:00+02:00"
toc: true
---

Meistens rezensiere ich ja eher Bücher und andere technische Geräte, wie etwa
das ein oder andere Smartphone. Diesmal was ganz anderes, nämlich meinen Schreibtisch.

Seit etwa einem Monat besitze ich den [„BEKANT“](http://www.ikea.com/de/de/catalog/products/S69022523/)
getauften Schreibtisch von IKEA. Hierbei handelt es sich um einen elektrisch Höhenverstellbaren
Schreibtisch, den man für einen Preis von 519€ erwerben kann.

Das besondere – und somit der Kaufgrund für mich – war die Tatsache, dass dieser
Tisch zu den wenigen Tischen gehört, die höhenverstellbar und preislich erschwinglich
sind. Viele Tische kosten sonst über 1000€ was schon ein sehr hoher Preis ist.

<a href="/graphics/schreibtisch.webp" data-lightbox="image" data-title="Der Schreibtisch.">
  <img src="/graphics/schreibtisch.webp" style="margin:2px;">
</a>

Warum aber Stehen? Ich möchte hier nicht eine medizinische Studie starten, doch
gibt es einige ersichtliche Gründe für das Arbeiten im Stehen. Vor allem bei
Leuten die schon den ganzen Tag im Büro vor dem Rechner sitzen (wie ich) und
zuhause ebenfalls vor dem Rechner sitzen (wie ich), bewegen sich in der Regel
zu wenig. Der Rücken kann nach einer Weile weh tun, ebenfalls die Beine. Beim
Arbeiten im Stehen ist das anders – man entlastet mehr den Rücken … aber die Beine
tun „anders“ weh. ;-)

Ich selbst wechsele durch den Kauf des Tisches häufiger zwischen dem Sitzen und
dem Stehen. Anfangs muss man sich allerdings stark dran gewöhnen, jedenfalls
tun einem die Fersen recht schnell weh, ebenso schmerzt zu Beginn der
Rücken, wenn man jahrelang kaum gestanden hat. Dies legt sich allerdinga nach einer
gewissen Zeit. Nach knapp einem Monat kann ich
nur sagen, dass sich die Bewegung am Schreibtisch zwangsläufig deutlich erhöht hat.
Die Hemmung bei "oh, ich sollte jetzt mal Aufstehen" ist gering – schließlich steht
man schon und meistens bewegt man sich dadurch überhaupt, was man beim Sitzen
nicht sagen kann.

### Zum Tisch …

Der Tisch selbst ist eher unspektakulär. Bei der Tischplatte kann man zwischen
verschiedenen Formen wählen, ich selbst besitze ein simples Rechteckiges mit abgerundeten
Ecken. Um die Höhe des Tisches zu verstellen, ist ein kleines Bedienelement vorhanden,
dass man an jeder Kante des Tisches anbringen kann. Dieses Bedienelement besitzt
einfache "Hoch" und "Runter"-Knöpfe, um die Höhe zu verstellen. Das Wechseln vom Sitz- in den
Steh-Zustand dauert knapp 10 Sekunden. Umgekehrt sieht es ähnlich aus, natürlich
abhängig von seiner eigenen Körpergröße.

<a href="/graphics/schreibtisch-bedienelement.webp" data-lightbox="image" data-title="Das Bedienelement.">
  <img src="/graphics/schreibtisch-bedienelement.webp" style="margin:2px;">
</a>

Die Bedienung könnte ein wenig toller sein, da man schon relativ fest den Knopf drücken
muss, um den Tisch zu verstellen. Auch gibt es leider keine Möglichkeit seine beiden
Positionen zu speichern. Dies wäre nämlich durchaus praktisch, da man so jedes Mal
prüfen muss, ob die richtige Höhe erreicht wurde.

### Fazit

Der Tisch ist zwar nicht gerade erschwinglich im Vergleich nur normalen Tischen, doch lohnt es sich für mich jedenfalls
schon. Bis auf die Bedienelemente habe ich nichts anzumeckern und mit diesen kleinen
Makel kann man ganz gut leben. Praktisch ist auch: Wenn man an seinem Rechner, der
unter dem Tisch steht, ran möchte, dann kommt man deutlich leichter dran, wenn der
Tisch hoch steht. ;-)
