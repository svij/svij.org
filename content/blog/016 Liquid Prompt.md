---
title: "Liquid Prompt"
categories: ["Shell"]
date: "2013-09-07 21:00:00+02:00"
toc: true
---

Als Linux-Anwender greift man häufig zur Shell, um mit dieser zu arbeiten, 
beispielsweise zur Aktualisierung oder Installation von Paketen.
[Liquid prompt](https://github.com/nojhan/liquidprompt)
erweitert automatisch die Kommandozeile um einige nützliche Funktionen
und ist besonders für die Nutzer geeignet, die viel mit der Shell arbeiten.
Dabei wird sowohl die [Bash](http://tiswww.case.edu/php/chet/bash/bashtop.html) als auch die [zsh](http://zsh.sourceforge.net/) unterstützt. Welche Features es hat
und in welchen Fällen es ziemlich nützlich ist, wird im Artikel
erklärt.

## Installation

Für die Installation von Liquid prompt gibt es für die gängigen
Distributionen kein Paket, stattdessen kann man sich recht einfach den aktuellen Stand
aus dem Git-Repository herunterladen und dann nutzen. Dazu muss `git` installiert
sein. Zunächst holt man den aktuellen Stand vom GitHub-Repository:

    $ git clone https://github.com/nojhan/liquidprompt.git

Im Anschluss kann man Liquid prompt mit folgendem Befehl aktivieren:

    $ source liquidprompt/liquidprompt

Hiermit wird Liquid prompt allerdings nur ein einziges Mal aktiviert. Um es
dauerhaft zu aktivieren, muss der obere Befehl in die `.bashrc` bzw. in die
`.zshrc` kopiert werden, je nachdem welche Shell man nutzt. Dabei muss 
beachtet werden, dass der absolute Pfad eingetragen wird.

Im letzten Schritt kann Liquid prompt noch sehr gut konfiguriert werden.
Die Konfigurationsdatei liegt unter `liquidprompt/liquidpromtrc-dist`.
Diese Datei sollte dann entweder nach `~/.config/liquidpromptrc` oder
nach `~/.liquidpromptrc` kopiert werden.

Die Installation von Liquid prompt ist hiermit abgeschlossen. Bei Erscheinen
einer neueren Version reicht es, ein `git pull` im Verzeichnis `liquidprompt`
auszuführen.

## Was es kann

Bis jetzt wurde noch nicht wirklich erklärt, was Liquid prompt wirklich kann.
Die Aktivität von Liquid prompt merkt man im schlechtesten Falle gar nicht. Dies
hängt meist davon ab, in welchem Verzeichnis man sich gerade befindet. Der
Anwendungsfall von Liquid prompt ist nämlich ziemlich variabel, nicht zuletzt
deswegen heißt es „liquid“ (auf deutsch: „flüssig“).

Bei einer reinen Bash, d.h. ohne eine modifizierte `.bashrc`, sieht die Prompt
in etwa so aus:

    [sujee@thinkpadx ~]$ 

Den meisten dürfte bekannt sein, dass hierbei vier Dinge angezeigt werden: Der
Benutzername (hier: `sujee`), der Rechnername (hier: `thinkpadx`), das aktuelle
Verzeichnis (hier: `~`) sowie das Dollar-Zeichen, die eigentliche Shell-Prompt.

Wie oben beschrieben, aktiviert man mit

    $ source ~/liquidprompt/liquidprompt

Liquid prompt. Bei einer angepassten Konfiguration von Liquid prompt
erscheint das Home-Verzeichnis beispielsweise mit weiteren Informationen:

    11:24:51 ⌁58% [sujee:~] $ 

Dies wäre zum einen die aktuelle Uhrzeit und zum anderen der aktuelle Ladestatus
des Laptop-Akkus. Die einzelnen Elemente der Prompt werden dabei farbig dargestellt.
Für die einzelnen Funktionen gibt es in der Konfigurationsdatei Schalter zum
Aktivieren bzw. Deaktivieren der Funktion. Die Uhrzeit in der Prompt ist im
Standard ausgeschaltet. Zum aktivieren reicht es, 

    LP_ENABLE_TIME=1

zu setzen. 

Die Konfigurationsdatei ist generell ziemlich gut kommentiert, sodass man
recht gut sieht, welcher Schalter welche Funktionen auslöst.

### Für Entwickler

Für Entwickler ist besonders die Funktion für die verschiedenen Versionsverwaltungssysteme
interessant und ziemlich nützlich. Unterstützt werden Git, Subversion, Mercurial,
Fossil und Bazaar. Liquid prompt erkennt automatisch, wenn man sich in einem
Verzeichnis befindet, welches ein  
[Repository](https://de.wikipedia.org/wiki/Repository) der oben genannten Versionsverwaltungssysteme
ist.

Beim Betreten eines Git-Repositorys wird angezeigt, in welchem Zweig man sich
befindet und ob lokale Änderungen durchgeführt worden sind, die noch nicht
in das Repository übertragen wurden. Wenn keine lokalen Änderungen vorhanden sind,
wird der Zweigname in Grün angezeigt, wenn doch, wird er in Rot dargestellt
und in Klammern die Anzahl von hinzugefügten und gelöschten Zeilen
angezeigt. Entwicklern kann dies zum Teil Arbeit ersparen, da so ein 
Prüfen des Repositorys mit `git status` nicht notwendig ist. Ähnliche Funktionen
sind dabei auch für andere unterstützte Versionsverwaltungssysteme enthalten.

![Liquid Prompt](/graphics/liquidprompt.webp)

### Weitere Anzeigen

Neben diesen Features können noch einige System-Informationen dargestellt werden.
Im Standard werden jedoch nur wenige Informationen dargestellt. Diese erscheinen
nur dann, wenn sie wirklich notwendig sind. So wird unter anderem die Systemlast nur
dann angezeigt, wenn diese sehr hoch ist. Das gleiche gilt dabei auch für die
Prozessor-Temperatur. Diese Werte lassen sich aber ebenfalls sehr gut und
individuell anpassen.

Liquid prompt besitzt noch einige weitere Funktionen, die hier nicht weiter
aufgeführt werden. Wer die komplette Funktionsvielfalt nachlesen möchte,
findet eine Feature-Liste in der README des [GitHub-Repositorys](https://github.com/nojhan/liquidprompt#features).
Darin wird auch erklärt, wie man beispielsweise die Reihenfolge der Elemente der Prompt
vertauschen kann oder auch die Nutzung verschiedener Themes. Auch die Anpassung
von einzelnen Farben eines Themes ist möglich. 

## Fazit

Der Nutzer kann dabei Liquid prompt
vollkommen nach seinem eigenen Gefallen anpassen. Besonders für Shell-Liebhaber
bietet Liquid prompt sehr viele nützliche Features, die die Funktionen der Prompt
deutlich erweitern und somit auch die eigene Arbeit erleichtern. Durch die
hohe Anpassungsfähigkeit kann jeder Benutzer selbst seine Prompt
designen.

[via Dirks Logbuch](http://www.deimeke.net/dirk/blog/index.php?/archives/3241-guid.html|Blog-Artikel)
