---
title: "Visiting FrOSCon …"
categories: ["Events"]
date: "2015-08-25 20:50:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Last weekend, on the 22nd and 23rd August, the [FrOSCon](https://froscon.de)
took place in St. Augustin (next to Bonn) in Germany. It is one of the bigger
Open Source Conferences and my first visit. Short summary: It was great! There
were many talks, too bad there were many on talks on the same time, but
luckily all talks were [recorded](https://media.ccc.de/browse/conferences/froscon/2015/index.html).

Personally I gave to talks: about Snappy Ubuntu Core and about Ubuntu Phone. You
can watch both talks [here](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1503-ubuntu_phone.html)
and [here](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1523-snappy_ubuntu_core.html#video), both
talks are in German. Both talks had many attendees. [(Here is a small photo!)](https://plus.google.com/u/0/+SujeevanVijayakumaran/posts/3WiLopqpqKU) 

On Saturday I didn't visit more talks - on the evening, after the talks, there
was a free barbecue for everybody! Also, the entrance to the conference was completely
free in this year, which I strongly support.

On Sunday I went to the Talk of Benjamin Mako Hill about [„Access Without Empowerment“](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1645-access_without_empowerment.html), which was the only
English talk which I visited. I also visited a few more talks, if you are interested
to watch the other talks, you can have a look [here](https://media.ccc.de/browse/conferences/froscon/2015/).

The rest of the time was I mostly talking to people at the Ubuntu Booth, mostly showing
and presenting my Ubuntu Phones. Besides that we had a small Taskwarrior-Meetup with [Dirk Deimeke, Wim Schürmann and Lynoure Braakman](http://www.deimeke.net/dirk/blog/index.php?/archives/3607-Mini-Taskwarrior-Meetup-....html) which was quite funny and interesting ;).

I really like to visit different Open Source Conferences, mostly to learn new stuff
and talk to old and new friends. This time I've met many „old“ friends and also
met new guys. Surprisingly, I had the chance to meet and talk to Niklas Wenzel from the Ubuntu-Community, who is
involved in the development of different apps and features of Ubuntu Phone (and he's way younger than I would have expected)
and also Christian Dywan from Canonical.

I'm really looking forward to the next conferences, which will be [Ubucon](http://ubucon.de) in Berlin and [OpenRheinRuhr](http://openrheinruhr.de) in Oberhausen later this year!
