---
title: "Announcing Ubucon Europe 2016"
categories: ["Ubuntu"]
date: "2015-06-05 22:00:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

A few days back we announced the [9th Ubucon 2015 in Germany](/blog/2015/05/24/9th-ubucon-2015-in-germany/)
which takes place in Berlin. Next year would be the 10th Ubucon in Germany and we
think, we can make something special!

Back in March [Costales](https://plus.google.com/u/0/+MarcosCostales/posts/jLzUVbFmHuv)
asked on Google+ a few fellow „Ubuntu Phone Insiders“ (including me), what would
we think about an Ubucon Europe. While most of the people said that it
would be great, nothing much happened … until now! :)

### Ubucon Europe!

We - the organization team of the Ubucon Germany - are extending next years Ubucon
Germany to the *Ubucon Europe 2016*! We're currently in the early days of planning
and we need feedback from you all. The Ubucon Europe will take place in [Essen, Germany](https://en.wikipedia.org/wiki/Essen)
on a weekend in September or October 2016. The exact date and place is not fixed yet, we're currently settling that, thus we have two options in Essen.

Essen one of the many cities in the Ruhr area, which is located on the River „Ruhr“.
There are a couple of airports nearby most importantly the international [Airport in Düsseldorf](http://www.dus.com/).
You can travel from Düsseldorf Airport to Essen in roughly 30 minutes by train.

### Feedback needed!

We've just registered the domain [ubucon.eu](http://ubucon.eu) which will be the
place where future announcements will go. But firstly we need feedback from you
for further planning. Usually we have roughly 150 attendees each year at
Ubucon Germany. We actually don't have any idea how many people are interested in
a European Ubucon and how many people are willing travel to Essen to attend the event.

Therefore, we would be happy, if you can send me an email (to svij@ubuntu.com)
and tell me, if you are interested in attending the Ubucon Europe next year. Additionally,
it would be great to hear from where you are coming and if you are interested in
giving a talk or workshop. Any other comment is welcome too! We additionally do need
more people in the organization team, so if you are willing to help organize
the Ubucon Europe, please let me now, we have plenty of work to do! :-)
