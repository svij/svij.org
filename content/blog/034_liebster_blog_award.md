---
title: "Liebster Blog Award"
categories: ["Sonstiges"]
date: "2014-08-30 11:40:00+02:00"
toc: true
---

Der Dirk hat mich für den „Liebster Blog Award“ [nominiert](http://www.deimeke.net/dirk/blog/index.php?/archives/3428-Liebster-Blog-Award-....html). Dann will ich da nun auch mal mitmachen ;-).

<img src="/graphics/LiebsterAward.gif" style="float:left; margin-right:20px;">

Zunächst einmal die Regeln:

**1. Schreibe einen Post mit diesem Award, füge das Award-Bild ein und verlinke die Person die dir diesen Award verliehen hat.**

Done!

**2. Beantworte die elf Fragen.**

Das ginge ja auch.

**3. Denke dir elf (neue) Fragen aus.**

Zählt es auch, wenn es dieselben Fragen sind?

**4. Nominiere elf neue Blogs, die unter zweihundert Leser haben.**

Da habe ich wohl ähnliche Zweifel wie Dirk. Ich hab daher mal die „unter zweihundert
Leser“ gekonnt ignoriert.

**5. Sage den Bloggern die du nominiert hast, dass sie einen Award bekommen haben und somit getaggt wurden.**

Sollte schaffbar sein.

Und somit es geht los mit den Fragen von Dirk:

**Warum hast Du genau Dein Thema für das Blog gewählt und nichts anderes?**

Das ist genau das Thema was mich interessiert und womit ich mich jeden Tag beschäftige.

**Bloggst Du nur über ein Thema oder über mehrere Themen? Welche Themen sind das?**

Eigentlich blogge ich nur über Open Source Themen. Kaum mehr oder weniger.

**Bist Du ein Experte auf den Gebieten, über die Du bloggst?**

Nein, in der Regel nicht. Ich veröffentliche ja meistens meine Beiträge
auch auf anderen Seiten, wie etwa [freiesMagazin](http://freiesMagazin.de), [Pro-Linux](http://pro-linux.de)
und natürlich [Ikhaya auf ubuntuusers.de](http://ikhaya.ubuntuusers.de). Häufig kenne
ich mich der Materie, mit denen ich mich in den längeren Artikeln befasse, wenig
aus. Die Kenntnisse entwickeln sich meist während dem Schreiben
des entsprechenden Artikels. Ich werde quasi vom Einsteiger in der Thematik
zum „Experten“. Dies mache ich, weil ich möglichst viel Lernen will und ich mit
dem Schreiben von Artikeln auch am effektivsten lerne bzw. mich am effektivsten
mich mit diversen Themen auseinandersetze.

**Wie viel privates gibst Du in Deinem Blog von Dir preis? Wo liegt Deine persönliche Grenze?**

Nicht viel. Bis lang waren es größtenteils fachspezifische Blog-Posts, die sowieso
wenig bzw. keine privaten Informationen enthalten.

Meine persönliche Grenze? Gute Frage … über meinen Urlaub, Studium oder Arbeit
bloggte ich bisher generell nicht. Nicht unbedingt weil ich es privat halten
möchte, sondern eher, weil ich nicht das Gefühl habe, es würde jemanden interessieren. ;-)

**Welche Open-Source-Software nutzt Du bewusst und aus welchem Grund?**

Puh, soll ich da nun alles aufzählen? Das wäre aber lang. ;-)

Daher hier mal die Liste von der Software, die für mich am wichtigsten sind:
Linux, KDE SC, Taskwarrior. Warum? Weil es sehr gut funktioniert!

**Welche ehrenamtliche oder auch andere unbezahlte Arbeit leistest Du?**

Aktuell bin ich Ikhayateam-Mitglied sowie Teil der Projektleitung bei [ubuntuusers.de](http://ubuntuusers.de).
Nebenbei schreibe ich hin und wieder mal für [freiesMagazin](http://freiesMagazin.de),
wie oben schon erwähnt. Bei letzterem war ich auch mal eine Zeit lang im Redaktionsteam.

**Aus welchem Grund würdest Du Dein Blog gerne selber hosten, wenn Du könntest (oder auch warum nicht)?**

Tu ich ja quasi schon. Wobei man das Hosten bei [uberspace](http://uberspace.de)
nur bedingt als „selbst hosten“ definieren kann. Am liebsten würde ich den
Blog zuhause hosten, dafür wäre aber eine statische IP und eine höhere
Bandbreite von Vorteil.

**Welches Feature in Deiner Blogsoftware / dem Blogdienst magst Du am liebsten?**

Pelican ist statisch! So kann ich einiges an meinem Blog direkt an den Template-Dateien
ändern und hochladen. Außerdem hat Pelican keinen großen Overhead und ist somit
ziemlich leichtgewichtig. Die ganze Homepage kann ich auch ganz einfach mit
Git versionieren, was auch von Vorteil ist.

**Welches Feature in Deiner Blogsoftware / dem Blogdienst fehlt Dir am meisten?**

Eigentlich nur eine gescheite Kommentar-Funktion, was bei statischen Blogs ja
eh nur über externe Dienste geht, was ich allerdings vermeiden will. Ich könnte
zwar rein theoretisch Disqus nutzen, da ich es aber aus Tracking-Gründen sowieso
überall blocke, fällt das natürlich auch bei mir flach.

**Warum würdest Du auch bloggen, wenn Du keine oder nur sehr wenige Leser hättest? (Oder warum nicht).**

Ja. So viel blogge ich ja auch. Pro Tag verirren sich selten mehr als fünf
Besucher auf meinem Blog. Einen Peak gibt es nur, wenn ich einen Blog Post
veröffentliche, den ich dann auf Twitter und Google+ verlinke. Den größten
Peak gibt es allerdings genau dann, wenn ein Artikel bei Pro-Linux online geht. ;-)

**Wie finanzierst Du Dein Blog?**

Aus dem eigenen Geldbeutel. Einnahmen über den Blog erziele ich gar nicht. Ein
Flattr-Button ist zwar drin, gedrückt haben diese allerdings bislang auch nur
zwei Leute. Die Kosten halten sich sowieso in Grenzen.


Fertig! Hier nun meine Liste an nominierten Blogs:

**[Imaging-Life](http://www.imaging-life.org/blog/)** - von Bernhard. Ein schöner Blog über Fotografie. Von ihm muss ich noch viel lernen.

**[Torstens Blog](http://torstenfranz.wordpress.com/)** - Torsten berichtet hin und wieder von und über seinen Einsatz in der Ubuntu-Community.

**[C3VO](http://c3vo.de/)** - Christophs Blog behandelt größtenteils Open Source Themen.

**[markusholtermann.eu](https://markusholtermann.eu/)** - Markus bloggt „noch technischer“, meistens auf Englisch.

**[noisefloor](http://noisefloor-net.blogspot.de/)** - Jochen bloggt mal ausnahmsweise nicht ausschließlich über Linux-Themen.

**[Romconstruct](http://romconstruct.net/)** - Lars ist wohl der einzige in der Liste, der nicht mit mir im ubuntuusers-Team ist (bzw. war), sondern zwei Büros weiter sitzt. ;-). Diesmal auch mal ein nicht so technischer Blog!

Ich schaffe leider nur sechs, statt der gewollten elf Blogs. Ich wollte eigentlich
auch noch den Reiseblog von [die Ausreißerin](http://ausreisserin.de/) nominieren. Aber [sie hat schon](http://ausreisserin.de/rund-ums-reisen/liebster-award/). Da ich bei den Fragen relativ einfallslos bin, reiche
ich die Fragen an meine Nominierten einfach mal weiter. Viel Spaß!
