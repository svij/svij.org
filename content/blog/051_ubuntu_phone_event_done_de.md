---
title: "Impressionen vom Ubuntu Phone Insider Event"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-02-09 20:05:00+02:00"
toc: true
---

Wie auf meinem Blog [angekündigt](blog/2015/02/05/ubuntu-phone-insider-event-in-london-am-6-februar/)
fand am 6. Februar fand das „Ubuntu Phone Launch Insider Event“ in London statt.
Im [Ikhaya auf ubuntuusers.de](http://ubuntuusers.de/ikhaya/6568/) habe ich bereits
vorgestern einen kleinen Bericht veröffentlicht. Da ich dort keine persönlichen
Bemerkungen untergebracht habe, folgt hier nun ein etwas ausführlicherer Bericht.

### Die Einladung

Die ursprüngliche Einladung zu dem Event erhielt ich von Daniel Holbach im August
2014. Immerhin schon ein halbes Jahr her. Damals war zunächst nur von einer
kostenlosen Zurverfügungstellung die Rede. Weshalb die ursprüngliche Einladung
von [Toddy](http://ubuntuusers.de/user/toddy/) an mich überging. Danke hierfür toddy!

Später kam also dann die echte Einladung zum Event, allerdings noch ohne konkreten
Termin. Obwohl zunächst von Anfang Oktober die Rede war, verschob sich es immer
weiter, bis letztendlich im Dezember verkündet wurde, dass es am 6. Februar
stattfinden wird.

### Die Anreise

Die Anreise und Unterkunft in London für das Event wurde komplett von Canonical bezahlt.
Dies umfasste für Europäer zwei Nächte in London und bis zu drei Nächte für
Teilnehmer aus den USA.

Ich selbst reiste am Tag des Events, also am 6. Februar früh morgens an. Über
Google+ bin ich glücklicherweise am Vortag auf den Franzosen Vincent Jobard
aus der französischen Ubuntu-Community gestoßen, welcher zu ähnlicher Zeit
am Airport London-Heathrow landete wie ich, sodass wir uns gemeinsam auf dem Weg gemacht haben in die
Innenstadt von London.

Wir hatten zwar keine Einladung zur Besichtigung der Canonical-Büros, doch haben
wir es auf gut Glück versucht. Wie erwartet, durften wir in der Tat die Büros
besichtigen. Dort trafen wir dann zufällig auf drei Mitgliedern der italienischen
Community, die ebenfalls zum Event eingeladen waren. Die Büros waren eher unspannend,
da es kaum mehr als zwei Großraumbüros sind in dem viele Leute sitzen. Interessant
und verwunderlich war für mich allerdings, dass dort verhältnismäßig viele Macs
standen, die mit Mac OS X liefen. Eine Vielzahl davon stand allerdings bei den
Designern. Man sollte allerdings nicht vergessen, dass die meisten Canonical-Mitarbeiter,
die an Ubuntu arbeiten, von zuhause arbeiten und nicht in den Londoner Büros.
Netterweise führte uns Alan Pope durch die Büros und erzählte ein wenig vom
Arbeiten bei Canonical.

Anschließend ging es in das Hotel. Untergebracht waren alle Ubuntu-Community
Mitglieder im Hotel [„Le Méridien“](http://www.lemeridienpiccadilly.co.uk/)
direkt am Picadilly Circus. Laut Webseite kostet dort eine Nacht schlappe 300€.
Am Hotel versammelten sich so langsam fast alle eingeladenen Community-Mitglieder.
Insgesamt dürften dies zwischen 20 und 30 gewesen sein. Dabei waren Teilnehmer
aus Frankreich, Spanien, Italien, Finnland, USA und möglicherweise aus noch einigen
weiteren Ländern. Im Eingangsbereich fand dann auch eine kleine inoffizielle
Vorstellungsrunde statt mit den Kernfragen "Wie heißt du, wo kommst du her und
was machst du in der Ubuntu-Community?".

### Das Event

Um 13 Uhr Ortszeit fing das Treffen der Community an. Der Veranstaltungsort war
das Ham Yard Hotel in der Nähe von Picadilly Circus mitten in Londons Innenstadt.
Zu Beginn mussten sich alle eingeladenen Personen noch etwas gedulden, sodass
noch nicht direkt die Smartphones ausgehändigt wurden. Zunächst stand nämlich
das Mittagessen in Form von vielen kleinen Snacks an. In der Zwischenzeit konnten
sich Mitglieder von der Community untereinander oder mit Canonical-Mitarbeitern
unterhalten.

<a href="/graphics/ubuntuphone_lunch_klein.webp" data-lightbox="image" data-title="Mittagessen.">
	<img src="/graphics/ubuntuphone_lunch_klein.webp" style="margin:20px;">
</a>

<a href="/graphics/ubuntuphone_event_klein.webp" data-lightbox="image" data-title="Der Veranstaltungsraum.">
	<img src="/graphics/ubuntuphone_event_klein.webp" style="margin:20px;">
</a>

Nach etwa einer Stunde wechselten alle Anwesenden den Raum, in dem die offizielle
Veröffentlichung stattfand. Zunächst ergriff Cristian Parrino, Vice President
Mobile bei Canonical, das Wort. Dabei gab er einige Informationen über das
Telefon bekannt. So gibt es zwar (noch) kein WhatsApp für Ubuntu Phone, doch
gibt es den Messenger Telegram, welcher vorinstalliert ist. Der Verkauf des
Smartphones startet diese Woche, wann genau ist noch nicht bekannt. Die
genauen Daten werden über die Social-Media Kanäle von [Ubuntu](http://twitter.com/ubuntu)
und [bq](http://twitter.com/bqreaders) bekannt gegeben. Der Verkauf verläuft in
einem sogenannten Fire-Sale, sodass die Geräte nur innerhalb eines kurzen Zeitraums
käuflich zu erwerben sind. Sehr wahrscheinlich wird der Verkauf Dienstag oder Mittwoch 
losgehen. Ebenfalls ergriff ein Mitarbeiter von bq das Wort und lobte
die gute Zusammenarbeit mit Canonical. Als dritte und letzte Person gab auch noch
Jane Silber sich die Ehre. Jane Silber stellte heraus, das Ubuntu großes Potenzial
auf dem Smartphone hat, da es „anders“ denkt. Dieses „anders“ umfasst insbesondere
die Existenz der sogenannten Scopes, welches nicht Apps sind, doch Informationen
aus verschiedenen Apps und Ressourcen aus dem Internet thematisiert zusammenfassen
und darstellen.

<a href="/graphics/ubuntuphone_parrino_klein.webp" data-lightbox="image" data-title="Cristian Parrino bei der Veröffentlichung.">
	<img src="/graphics/ubuntuphone_parrino_klein.webp" style="margin:20px;">
</a>

So gibt es auf dem Home-Screen verschiedene Scopes, wie etwa den „Near By“-Scope
mit Informationen und Bildern aus der aktuellen Umgebung. Weiterhin gibt es unter
anderem auch noch Foto- und Video-Scopes, welche nicht nur die Dateien aus dem
Gerät darstellen, sondern auch aus Quellen wie YouTube oder Flickr ihre Daten beziehen.

<a href="/graphics/ubuntuphone_box_klein.webp" data-lightbox="image" data-title="Das Phone in der Box.">
	<img src="/graphics/ubuntuphone_box_klein.webp" style="margin:20px;">
</a>
<a href="/graphics/ubuntuphone_running.webp" data-lightbox="image" data-title="Das Phone im Betrieb..">
	<img src="/graphics/ubuntuphone_running.webp" style="margin:20px;">
</a>

Zum Schluss erfolgte die Übergabe der ersten Ubuntu Phones an die Mitglieder der
Community. Die Smartphones waren eingepackt in einer großen Box, in der nicht
nur das Smartphone selbst enthalten war, sondern ebenfalls Kopfhörer von Urbanears.
Mit dabei war ebenfalls ein Brief von Canonical-Gründer Mark Shuttleworth sowie
eine SIM-Karte inklusive 10 Pounds Aufladeguthaben. Die Box und dessen Inhalt ist
allerdings nur speziell an die Mitglieder der Community gerichtet und wird so nicht
in den Verkauf gehen. Die Verpackung war recht aufwendig, da das Smartphone in
einem Origami-Stil verpackt war.

Anschließend folgte der Spaß, sodass jeder nicht nur mit dem Smartphone herumspielen konnte,
sondern es gab ebenfalls eine Bowling-Bahn im Veranstaltungsort. Ich selbst spielte
mit einem Griechen, Franzosen und einem Polen. Immerhin wurde ich nicht Letzter.
Mitgespielt haben nicht nur Community-Mitglieder sondern auch Cristian Parrino und Jane Silber
auf der Nachbarbahn.

Am Abend löste sich die Gruppe allmählich auf, sodass ein großer Teil der Gruppe
verschiedene Pubs besucht hat.

#### Weitere Informationen

In einigen privaten Gesprächen auf der Veranstaltung haben die Verantwortlichen von
Canonical einige weitere Informationen mitgeteilt. So sollen auch weiterhin zuerst
ausgewählte Community-Mitglieder mit den neuesten Informationen versorgt werden,
bevor sie an die Presse gehen. Ebenfalls gab es Informationen zu dem ersten Smartphone
von Meizu. Hierbei handelt es sich um das Meizu MX4, welches deutlich leistungsstärker
ist als das bq Aquaris E4.5. Dieses soll im Idealfall nächsten Monat im März in China
und Europa erscheinen. Ein genauer Termin wurde allerdings noch nicht genannt.
Es kann durchaus sein, dass sich dieser Termin erneut verschiebt. Ebenfalls
geplant ist ein Start im US-Markt, welches mit Meizu erfolgen soll. Auch hierzu
wollte man sich nicht näher festlegen. Geplant sei aber wohl eine Veröffentlichung
mit dem Partner Meizu in der Mitte dieses Jahres.

### Der Tag danach

Der Tag nach dem Event war vollständig losgelöst vom Launch Event, sodass jeder
machen konnte was er möchte. Während man beim Frühstück noch einige Leute
traf, verteilte sich die Gruppe komplett. Ich selbst war mit [Jordan Keyes](https://plus.google.com/u/0/+JordanKeyes/)
in der Innenstadt von London unterwegs. wo wir vorallem auch das bq Ubuntu Phone
unter Realbedingungen getestet haben.

Am Abend saßen dann [Jono Bacon](https://plus.google.com/u/0/+JonoBaconProfile),
[Stuart Langridge](https://plus.google.com/u/0/+StuartLangridge), [Jordan Keyes](https://plus.google.com/u/0/+JordanKeyes/posts)
und ich zusammen. Was da geschah lasse ich lieber mal unkommentiert, man kann es allerdings
sehr gut an den Posts in den diversen Google+-Profilen entnehmen. ;)

Spätestens am Sonntag verließen die letzten London in Richtung Heimat.

### Fazit

Das Event war toll! Viel wichtiger für mich als das Smartphone selbst, war für
mich das Kennenlernen vieler neuer Leute die sich in der Ubuntu-Community
einbringen. Ich möchte mich an dieser Stelle ebenfalls bei Canonical für die
Einladung für das Event bedanken. Leider war viel zu wenig Zeit um sich mit allen
Leuten ausführlich zu unterhalten, aber dagegen kann man wohl eher nicht viel machen.
Ein paar weitere Bilder von der Veranstaltung habe ich auf meinem [Google+-Profil veröffentlicht](https://plus.google.com/u/0/+SujeevanVijayakumaran/posts/HvJZwV6TC9U).

Ich selbst habe jetzt noch einige Arbeit bezüglich des Smartphones vor mir. Insbesondere
eines ausführlichen Tests des Smartphones. Stay tuned!
