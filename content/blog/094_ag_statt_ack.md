---
title: "Suchen und Finden mit ag, statt ack (statt grep)"
categories: ["Shell"]
date: "2017-03-08 17:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Vor fast drei Jahren bloggte ich über [„Suchen und Finden mit Ack, statt grep“](https://svij.org/blog/2014/04/27/suchen-und-finden-mit-ack-statt-grep/#suchen-und-finden-mit-ack-statt-grep). Heute lautet der Titel
allerdings „Suchen und Finden mit ag, statt ack“.

Ag und Ack sind vom Prinzip her gleich: Mit beiden findet man Strings in Dateien,
ähnlich wie es grep auch kann. Im Gegensatz zu grep arbeitet ack und ag schneller
und ist mehr für Programmierer und Sysadmins ausgerichtet, da es diverse unnötige
Verzeichnisse (wie `.git`) nicht durchsucht. Auch die Ausgabe ist gleich. Ag ist
hingegen ein Klon von Ack, was in C statt in Perl geschrieben wurde. Der Fokus
bei ag liegt besonders auf die Geschwindigkeit. Wer dachte, dass ack schon schnell
ist, der findet mit ag ein noch schnelleres Tool. Laut Angaben des Projektes ist
es dabei 5-10x schneller als ack.

Auf meinen Systemen die ich selbst und alleine betreue und nutze, habe ich ack
durch ag ersetzt. Auf Systemen wo ich entweder nicht alleine drauf bin, verbleibt
weiterhin grep das Tool der Wahl, damit man nicht überall alles nachinstallieren muss
und ggf. auch garnicht erst sollte.

Der Paketname von ag ist nicht direkt nachvollziehbar, sofern man das Periodensystem
der Elemente nicht vollständig kennt. Silber ist nämlich das 47. Element mit dem Symbol "ag".
Unter Ubuntu heißt das Paket `silversearcher-ag` während es unter diversen 
anderen Distributionen einfach `the_silver_searcher` heißt.

Mehr Informationen zu Tools die in diversen Eigenschaften besser als ack sind,
finden sich auf [betterthanack.com](http://betterthanack.com/). Das GitHub-Repository
von mit mehr Informationen findet sich [hier](https://github.com/ggreer/the_silver_searcher).
