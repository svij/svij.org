---
title: Einblicke in Drupal
categories: ["Web"]
tags: ["Web"]
date: "2011-06-05T15:20:00+02:00"
toc: true
---

Im Januar des Jahres 2011 erschien eine neue Version des Content-Management-Systems [Drupal](http://	drupal.org). Die siebte Version wurde Anfang des Jahres nach dreijähriger Entwicklungsphase freigegeben. Dies ist eine gute Gelegenheit, einen Blick auf den aktuellen Stand des CMS zu werfen.

<strong>Hinweis: Der Artikel erschien bereits in der [Juni Ausgabe](http://www.freiesmagazin.de/20110605-juniausgabe-erschienen)  von [freiesMagazin](http://freiesMagazin.de).</strong>

Das Content-Management-System (kurz: CMS) Drupal wurde nach einer dreijährigen Entwicklungszeit in Version 7 von den Entwicklern freigegeben. Durch die neueste Veröffentlichung endet auch der offizielle Support von Drupal 5. Neben Drupal gibt es zahlreiche weitere Content-Management-Systeme wie zum Beispiel Joomla, Contao, Wordpress und Typo3. Sie dienen zentral der Organisation und Erstellung von Inhalten für eine Internetseite. Dabei arbeiten die verschiedenen Systeme unterschiedlich und setzen verschiedene Schwerpunkte. Drupal selbst setzt sich als Ziel ein ausgewogenes CMS zu sein. Sie geben in ihrer Dokumentation an, dass viele andere CMS entweder zu kompliziert für Einsteiger oder zu einfach gehalten sind, so dass man nicht sehr viel damit realisieren kann. Drupal selbst erhebt den Anspruch, sowohl leistungsstark als auch einfach für Einsteiger zu sein. Durch den Einsatz von vordefinierten Komponenten bildet Drupal sowohl ein CMS als auch ein Content-Management-Framework. Drupal ist komplett PHP 
geschrieben und ist mit zahlreichen Datenbanksystem kompatibel. Neben MySQL kann auch unter anderem PostgreSQl, Microsoft SQL und SQLite genutzt werden. Das CMS steht unter der GNU General Public License.

Ein deutschsprachiges [Drupal Forum](http://www.drupalcenter.de/forum) gibt es seit 2006, welches über 12.000 registierte Benutzer hat. Die internationale [Drupal Community](http://drupal.org/community) mit Forum hat insgesamt über 500.000 registrierte Benutzer.

Es gibt inzwischen viele Webseiten, die Drupal einsetzen. Zu den Bekannteren gehören unter anderem [MTV](http://www.mtv.co.uk/), [Ubuntu](http://ubuntu.com) oder auch [Greenpeace](http://greenpeace.org.uk).

### Die Entstehung von Drupal

Die Geschichte begann im Jahre 2000. An der Universität Antwerpen in Belgien entstand der erste Vorläufer von Drupal. Damals war es ein großes Privileg für Studenten eine permanente Internetverbindung zu besitzen. So bildeten acht belgische Studenten ein Netzwerk. Das einzige was in dem Netzwerk fehlte, war ein Plattform um die einfachsten Sachen zu teilen und zu diskutieren. Dies inspirierte Dries Buytaert eine kleine Newsseite zu programmieren, welche dann im Netzwerk genutzt werden konnte. Nachdem Dries die Universität verlassen hat, entschied sich die Gruppe, den Weblog online zu stellen. Zunächst war geplant gewesen, die Domain dorp.org zu registrieren. Dorp ist das holländische Wort für Dorf. Nach einem Tippfehler wurde jedoch die Domain Drop.org registriert. Im Januar 2001 wurde dann die Software unter dem Namen Drupal veröffentlicht. „Drupal“ stellt die englische Aussprache des niederländischen Wortes „Druppel“ dar, welches „Tropfen“ auf Deutsch heißt. Aus diesem Namen leitet sich daher auch das Logo 
ab, welches ein Tropfen darstellt.

### Der Aufbau des Frontends und Backends

![Administrationsmenü](/graphics/drupal_adminstrationsmenue.webp)

Die verschiedenen Content-Management-Systeme sind sehr häufig unterschiedlich aufgebaut. Viele Systeme haben getrennte Front- und Backends. Das Frontend dient dazu die Inhaltselemente einer Webseite darzustellen, während das Backend das Administrationsmenü ist. Dort lassen sich neue Artikel erstellen und bearbeiten, sowie zahlreiche weitere Einstellungen bearbeiten. Bei einigen Content-Management-Systemen sind die Frontends von den Backends getrennt. Bei Drupal gibt es hingegen einen fließenden Übergang zwischen Front- und Backend. Das heißt, sie sind nicht klar getrennt, wie es zum Beispiel bei dem CMS Contao der Fall ist. Das Adminstrationsmenü erreicht man in Drupal über die Leiste, die überhalb der Seite positioniert ist. Das Menü wird unterteilt in mehreren Kategorien.

#### Dashboard
Auf dem „Armaturenbrett“ liegen verschiedene „Blöcke“ die jeder sich nach seinen eigenen Vorlieben anpassen kann. Standardmäßig dargestellt werden die neuesten Inhalte, ein Suchformular, die neusten Mitglieder die sich registriert haben, als auch die Kommentare die auf Artikeln bzw. Seiten abgeben worden sind. Das Dashboard lässt sich über weitere Blöcke erweitern und auch die Anordnung der Blöcke darin, lassen sich ordnen.

#### Inhalt
Das Inhaltsmenü unterteilt sich in zwei weiteren Untermenüpunkten. Es lassen sich zum einen neue Inhaltsfelder einfügen, so gibt es die vorkonfigurierten Module „Inhaltsseiten“ und „Artikel“, womit man direkt einen Text veröffentlichen kann. Außerdem lassen sich auch weitere, selbst konfigurierte, Inhaltstypen auswählen. In einem weiteren Menüpunkt gibt es die Möglichkeit die bereits erstellten Seiten zu durchsuchen. Bei einer sehr großen Anzahl von Artikeln ist dabei auch die Filterfunktion sehr nützlich, womit man beispielsweise die erzeugten Artikel nach Typ, Sprache oder dem Status anzeigen lassen kann.

#### Struktur
Das Strukturmenü wird unterteilt in die Untermenüpunkte „Blöcke“, „Inhaltstypen“, „Menüs“ und „Taxonomie“.

#### Blöcke
Blöcke sind Boxen, die sich in verschiedene Bereiche einer Webseite verschieben lassen. So lässt sich beispielsweise per Drag-and-Drop die Navigation der Webseite von der linken Seite auf die rechte Seite verschieben. Es gibt viele vorkonfigurierte Blöcke, die mit der Installation von Drupal daher kommen. Praktisch für Anfänger ist auch die Vorschau der Blockregionen, dabei wird angezeigt, wo welche Region liegt. Dies ist besonders beim Einsatz von neuen Themes nützlich, da sich die Anordnung und Anzahl der Blöcke bei jedem Theme unterscheidet.

###### Inhaltstypen
In Drupal wird jeder Inhalt in einem „Node“ gespeichert. Jedes Node gehört zu einem bestimmten Inhaltstyp, welches verschiedene Einstellungen speichert. Darunter fallen unter anderem, ob standardmäßig die Artikel eines Inhaltstyps direkt veröffentlicht werden und beispielsweise auch, ob Kommentare möglich sind oder nicht.

Drupal bringt von Beginn an zwei Inhaltstypen mit. Dies ist zum einen der Inhaltstyp „Artikel“ und des Weiteren „Inhaltsseiten“. Die Artikel mit dem Inhaltstypen Artikel sind für häufig aktualisierte Artikel sinnvoll. Artikel bzw. Seiten mit dem Typ Inhaltsseiten sind für statische Seiten geeignet, die nicht, oder eher selten aktualisiert werden. Weitere Inhaltstypen mit angepassten Einstellungen lassen sich auch erstellen.

##### Menüs
In diesem Menüpunkt kann man die verschiedenen bereits vorhandenen Menüs der Webseite verändern, umbenennen oder die Rangfolge verändern. Es ist des Weiteren auch möglich neue Menüs zu erstellen. Die Menüs lassen sich dann als Block im Blockmenü auf der Webseite anordnen. Jedes Menü lässt sich mit weiteren Links erweitern. Die Sortierung der Links innerhalb des Menüs, auch innerhalb von Untermenüs, kann man sehr einfach per Drag-and-Drop verschieben.

##### Taxonomie
Die Taxonomie erlaubt das Identifizieren, Kategorisieren und auch das Klassifizieren von Inhalten. Die einzelnen Tags kann man in Gruppenzusammenfassen und damit Artikel kategorisieren.

##### Design
Auch die Konfiguration der Themes in Drupal ist sehr umfangreich. Nach der Installation sind drei umfangreiche Themes einsetzbar. In den Einstellungen des jeweiligen Themes kann man die Standardthemes individuell gestalten. Das Farbschema lässt sich besonders gut konfigurieren. So kann man entweder die Farbwerte für die verschiedenen Bereiche der Webseite ändern oder besonders bequem für Einsteiger, vordefinierte Farbschemen nutzen. Des Weiteren lassen sich diverse Seitenelemente einschalten oder auch abschalten. Außerdem kann man auch ein eigenes Logo und Favicon hochladen, welches dann von dem Theme genutzt wird. Nicht nur für das Frontend gibt es Themes. Es gibt auch Themes für das Administrationsmenü. Themes können auf der Projektseite {http://drupal.org/project/themes} von Drupal heruntergeladen werden.

#### Benutzer
Die Benutzer, die man als Administrator erstellt oder die sich selbst registriert haben, kann man verschiedenen „Rollen“ zuweisen. Die beiden Rollen „Gast“ und „Authentifizierter Benutzer“ sind gesperrte Gruppen, die man nicht löschen kann. Neben diesen beiden Gruppen gibt es standardmäßig auch die Administratorengruppe. Weitere Rollen lassen sich hinzufügen, und die verschiedenen Berechtigungen der Rollen lassen sich ebenfalls ändern. Jedem Benutzer lässt sich daher auch eine „Rolle“ zuweisen. Einzelne Benutzer kann man verständlicherweise auch sperren.

#### Module
Erweiterungen sind in vielen Projekten enorm wichtig, da sie den bestehenden Funktionsumfang deutlich ausweiten können. Ein klassisches Beispiel dafür ist der Browser Firefox. In Drupal werden Erweiterungen „Module“ genannt. Einige Module liefert Drupal im Kern mit, die zwar nach der Installation vorhanden sind, aber zunächst deaktiviert sind. Der Admin kann diese mitgelieferten Module selbstständig aktivieren und nutzen. Für Drupal in Version 7 gibt es etwas mehr als 1500 Module, die man installieren kann. Monat für Monat kommen weitere Erweiterungen hinzu. Kurz nach der Veröffentlichung von Drupal 7 Anfang Januar gab es knapp unter 1000 Module. Einige der Module für die aktuellste Version sind jedoch noch nicht in einer finalen und stabilen Version erschienen und befinden sich daher noch in der Entwicklungsphase, dies hängt damit zusammen, dass eben die Veröffentlichung von Drupal 7 noch nicht allzu lange zurückliegt.
Die Installation von Modulen ist sehr einfach gestaltet. Man kann entweder von einer URL installieren, oder man kann das entsprechende Paket direkt im Browser hochladen. Sehr gute und nützliche Module findet man, wie die Themes auch, direkt auf der Projekthomepage auf Modules {http://drupal.org/project/modules}.

#### Konfiguration, Berichte und Hilfe

![Drupal Startseite](/graphics/drupal_startseite.webp)

Das Konfigurationsmenü von Drupal enthält alle restlichen Einstellungen, die für die Webseite von Bedeutung sind, wie unter anderem Einstellungen für die Lokalisierung und das System. Darüber hinaus erstellt Drupal auch Berichte. Darunter fallen Benachrichtigungen über Aktualisierungen vom Drupal Kern und von Modulen, als auch Protokolle über die häufigsten Suchbegriffe, die häufigsten „Nicht-gefunden“- und „Zugriff-verboten“-Fehler. Im Hilfebereich findet man viele Informationen, die für den Umgang mit Drupal von Bedeutung sind. So umfasst die Hilfe viele Erklärungen zu den Modulen. Eine ausführlichere Hilfe bietet das [Drupal Handbook](http://drupal.org/handbooks) an, welches jedoch nur in der englischen Sprache vorhanden ist. Ein deutsches Handbuch beziehungsweise FAQ zu Drupal ist auf [Drupalcenter.de](http://www.drupalcenter.de/handbuch) zu finden. Darüber hinaus stellt die Webseite [Thoor.de](http://www.thoor.de/drupal/anleitungen/drupal-7-video-screencasts) einige Screencasts zur Verfügung, die den 
Umgang mit Drupal erläutern.


### Neues in Drupal 7
Drei Jahre hat es gedauert, bis das siebte Release von Drupal veröffentlicht wurde. Bei diesem Release wurde eine enorme Verbesserung des Adminstrations- bzw der Benutzeroberfläche durchgeführt. Neben der Verbesserung der Oberfläche sind nun auch noch flexible Inhaltsfelder möglich. Außerdem wurde die Barrierefreiheit in Drupal deutlich verbessert. Des Weiteren wurde die auch die Datenbankunterstützung optimiert, so dass man Drupal mit sehr vielen Datenbanksystemen nutzen kann.

### Ein Blick in die Zukunft
Mit der Veröffentlichung von Drupal 7 begann im März die Arbeit an der achten Version. Auf der DrupalCon in Chicago nannte Dries Buytaert die Schwerpunkte für die kommende Version. Zum einen soll die Unterstützung auf allen Geräten ausgeweitet werden, so dass besonders Mobile Geräte wie Smartphones und Tablets eine verbesserte Oberfläche erhalten. Eine Integration von Cloud-Diensten soll kommen, sowie eine verbesserte Trennung zwischen Inhalt und Konfiguration.
Neben einigen weiteren Punkten gilt auch hier, dass auch weiterhin die Benutzbarkeit und die Performance verbessert wird.


