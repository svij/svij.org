---
title: Statischer Pelican Blog mit statischen Kommentaren
categories: ["Web"]
date: "2017-03-22T22:45:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Seit nun mehr drei Jahren läuft mein Blog mit dem statischen Webseiten-Generator
[Pelican](https://blog.getpelican.com/). Einen Artikel zu Pelican hatte ich
[damals auch geschrieben](https://svij.org/blog/2014/07/02/statische-webseiten-mit-pelican-erstellen),
in dem ich die grundsätzlichen Features und Funktionen aufgelistet
habe. Kurz zusammengefasst kann man unter anderem in Markdown Blogposts
schreiben, die von Pelican als HTML-Datei(en) herausgeschrieben werden. Den
„output“ Ordner kann man dann auf beliebigen Webspace schieben. So war bislang
der Umzug des Blogs sehr einfach, da ich nur die generierten Dateien irgendwo
hinschieben musste.

Ein wesentlicher Nachteil ist von Pelican nur, dass es keine Kommentare erlaubt.
Wie auch, schließlich ist der Blog komplett statisch und soll es eben auch so sein.
Da ich in letzter Zeit vermehrt E-Mails zu meinen Blog-Posts bekommen habe, dessen
Informationen auch für anderen nützlich sein könnten, habe ich mich entschlossen,
eine Kommentarfunktion bereit zu stellen. Externe Dienste wie Disqus kamen für
mich nicht infrage, schließlich lädt das dann wieder externen Content nach und
in meinen Browser werden Dienste wie Disqus auch auf anderen Webseiten
von vorn hinein geblockt. Die Lösung, die ich jetzt einsetze und auf die mich
[Chris](https://c3vo.de/) hingewiesen hat, ist das
[Pelican Comment System](https://github.com/getpelican/pelican-plugins/tree/master/pelican_comment_system). Die Handhabung ist
hierbei interessant. Der Kommentator füllt wie gehabt ein Formular aus, der Inhalt
wiederum wird über das lokal installierte E-Mail-Programm an den Blogger gesendet.
Dieser muss dann nur noch die E-Mail in den Code der Webseite schieben, die Seite
neu generieren und dann wieder hochladen. Das ist gleichzeitig praktisch als auch
umständlich. Wesentlicher Vorteil ist, dass der Blogger, in dem Fall ich, die
weiterhin komplett selbst hostet und das Blog auch weiterhin statisch ist.
Nachteilig ist allerdings auch wiederum, dass
dies eine höhere Hürde für Kommentatoren bedeutet. Letzteres empfinde ich gleichzeitig
aber auch als Vorteil, damit nur diejenigen Kommentare abgeben, die auch wirklich
kommentieren wollen.

Das Verfahren geschieht aktuell noch vollständig händisch. Je nach Aufwand und der
Anzahl der eingehenden Kommentare, lässt sich das mit Skripten automatisieren.
Schließlich muss nur ein Ordner des Postfaches abgerufen werden, die Rohdaten
extrahiert werden, die dann nur noch in das richtige Verzeichnis geschoben werden,
um die Webseite neu zu bauen und hochzuladen.
