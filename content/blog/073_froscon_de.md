---
title: "FrOSCon"
categories: ["Events"]
date: "2015-08-25 20:20:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Vergangenes Wochenende, am 22. und 23. August, fand die [FrOSCon](https://froscon.de)
in St. Augustin statt und ich war dieses Jahr zum ersten Mal da. Kurz zusammengefasst:
Die Veranstaltung war super! Es gab sehr viele interessante Vorträge, viele davon
auch leider gleichzeitig, was sich nicht vermeiden lässt. Alle Vorträge wurden
dankenswerterweise [aufgezeichnet](https://media.ccc.de/browse/conferences/froscon/2015/index.html).

Ich selbst war zunächst am Samstagmorgen in den beiden Vorträgen, die ich gehalten
habe. Zunächst über Snappy Ubuntu Core und anschließend über Ubuntu Phone. Beide
Vorträge waren gut besucht, im Ubuntu Phone Vortrag waren aber letztendlich noch
ein wenig mehr Leute da als beim Snappy Vortrag. Der Ubuntu Phone Vortrag lässt sich
[hier](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1503-ubuntu_phone.html)
anschauen, während man den Snappy Vortrag [hier](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1523-snappy_ubuntu_core.html#video) findet.

Am Samstag war ich sonst in keinen anderen Vorträgen, am Abend gab es dann das Social
Event, wo es kostenlos Essen vom Grill gab. Auch der Eintritt war für jeden Besucher
in diesem Jahr frei!

Sonntag ging es dann los mit [„Access Without Empowerment“](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1645-access_without_empowerment.html) von Benjamin Mako Hill. Später war ich in dem
äußerst interessanten und auch unterhaltsamen Vortrag [„Was ist Cloud?“](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1693-was_ist_cloud.html) von Kristian Köhntopp, der sehr sehenswert ist.
Der Talk über [„Ansible“](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1512-ansible_basics.html#video) hat einen kurzen und guten Einblick in die Nutzung von Ansible als Deployment-Tool gegeben.
Die übrige Zeit stand ich vor oder hinter dem Ubuntu-Stand, hauptsächlich wollten
wo ich mit vielen Besuchern vor allem über Ubuntu Phone gesprochen habe. Auch 
gab es ein kleines Taskwarrior-Meetup mit Lynoure Braakman, Wim Schürmann, Dirk Deimeke
und mir; ein Foto davon gibt es bei [Dirk im Blog](http://www.deimeke.net/dirk/blog/index.php?/archives/3607-Mini-Taskwarrior-Meetup-....html).
Mit Niklas Wenzel und Christian Dywan hab ich zufällig auch noch zwei weitere Personen
aus der Ubuntu-Community getroffen, die ich bisher noch nicht getroffen habe.

Nächstes Jahr geht es jedenfalls für mich definitiv wieder dorthin. Bis dahin steht
dieses Jahr noch die [Ubucon](http://ubucon.de) und die [OpenRheinRuhr](http://openrheinruhr.de) an.
