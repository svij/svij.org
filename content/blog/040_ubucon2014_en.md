---
title: "Review of Ubucon 2014 Germany"
categories: ["Events"]
date: "2014-10-25 14:00:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Last weekend the german Ubucon took place in the town of [Katlenburg-Lindau](https://en.wikipedia.org/wiki/Katlenburg-Lindau).
It was the 8th Ubucon in Germany and it was the third time that I attented an Ubucon as
a visitor and as a speaker.

### Preparation

It was the second time that I participated in the organisation of the Ubucon.
Last year I was part of the organisation team for the event in Heidelberg.
This time the organisation was rather „silent“. In my opinion it was sometimes
too silent on the mailing list. The town where the event took place was rather
small, therefore there were fewer speakers and also fewer visitors compared to
the last two years. First I didn't expect that the event would be great, but luckily
I was wrong!

### The event

#### Friday

The first day of the event was friday. All visitors and speakers got a name plate with
their full name and their nickname at the front desk. Last year, my name was
[actually too long](https://plus.google.com/u/0/+SujeevanVijayakumaran/posts/iTDHcSdbxu4).
This time only one character was missing. Atleast I got used to mistakes in my name. :-)

<a href="/graphics/ubucon2014_namensschild.webp" data-lightbox="image" data-title="my nameplate">
	<img src="/graphics/ubucon2014_namensschild.webp" width="250" style="float:right; margin:20px;">
</a>

The opening keynote was hold by [Torsten Franz](https://wiki.ubuntu.com/TorstenFranz) who
was also the head of the organisation team. After this, he was talking about „10 years Ubuntu,
10 years Community“. Later a part of the visitors went to the first Social-Event which took
place at a castle next to the school. Personally I didn't go to this event.

#### Saturday

The second day started at 10 o'clock in the morning. It was the first time that I did a workshop, which
also started on that time. I talked about „Git for Beginners“. At the beginning we had a few issues
with the Wifi. This also affected my workshop, because it took a rather long time for the participants to download
and install git. Therefore, I changed a few things of my workshop, so afterwards the participants
didn't need a working internet connection. I planned about 3 hours, but we finished
after about 2,5h.

On the rest of the day, I didn't attend any talks. I rather talked to all the other
nice people :-). At the evening we had two Social-Events. A big part went to
[„Theater der Nacht“](http://theater-der-nacht.de) („Theatre of the night“). The
other smaller part stayed at the school, where two persons played Live-Music.
The Live-Music was quiet good, but all the other people who went to the Theatre said,
that it was really great. It seems that I missed something. Bernhard took a
few [really nice photos](https://www.flickr.com/photos/bhanakam/sets/72157648465004507/)
in there.

#### Sunday

On Sunday I only attended to talks. The first one was about LVM, the other one
about systemd. Both talks were hold by [Stefan J. Betz](http://blog.stefan-betz.net/).
and they were really informative and also a bit funny.

At the afternoon the Ubucon ended. We had really many people who helped to clean up
and pack everything. Therefore, many people could leave earlier than expected.

#### The location

The location was great! I didn't expect that a primary school was a good place
for a Ubucon, but it is! The technical infrastructure was really good. The school
had several „Smartboards“ with projectors. At the entrance area there was a big
hall, where you can sit and talk if you're not in hearing a talk. In this hall
there were several computers with different Linux-Distributions and Desktop-Environments.

It was the first time that we had a Gaming-Lounge. There were two rooms which
contained four Ubuntu-PCs with large TVs and also two Table football. The
idea was great and also the rooms were nice. There were many people who
played games there. I hope that we will have a similar Gaming-Lounge on future Ubucons.

All speakers got a nice gift-bag from the local organisation team. This bag
mainly contained several items of the region. In my bag there were a few sausages,
wine, beer and a sauce. Personally I don't eat and drink that stuff, but it was
a really good idea and gesture!

On all our Ubucons, the entrance fee of 10€ includes the money for food and drinks. On
the last few years we had only two or three different types of bread roll. This
time we also had bread rolls, but we also had [Bockwurst](https://en.wikipedia.org/wiki/Bockwurst)
and different types of soups. All of them were really tasty and everybody had
a bigger choice to eat something which they like.

### Conclusion

This years Ubucon was great! Compared to last years Ubucon we had a smaller amount
of attendees but this time the organisation team in Katlenburg was really good. They
had different really good ideas, like the Gaming-Lounge and the gift-bag for all
speakers.

I simply hope that next years Ubucon will as good as this years Ubucon. The place
is not fixed yet, we are going to search for another place for next years Ubucon.
By shifting the place of the Ubucon every year, all attendees will see different
cities and you can also meet different new nice people. The latter reason is my
main reason why I attend *and* help to organize the Ubucon.

If you're looking for a few nice photos of this years Ubucon, have a look [here](https://secure.flickr.com/photos/bhanakam/sets/72157648472883770/).
Bernhard Hanakam took some really good photos.
