---
title: "ShareLaTeX und minted auf dem eigenen Server"
categories: ["Server"]
tags: ["ubuntuusers-planet"]
date: "2015-02-27 19:45:00+02:00"
toc: true
---

Ihr kennt das sicherlich: Es gibt Probleme bei denen man stundenlang nach einer
Lösung sucht. Ich selbst stand diese Woche vor einem Problem: In einem LaTeX-Dokument
wollte ich das Paket "minted" nutzen. Minted ist dafür da, um Quellcode innerhalb
eines LaTeX-Dokumentes automatisch zu syntax-highlighten. Ich selbst nutzte dabei
allerdings nicht eine lokale LaTeX-Installation, sondern ShareLaTeX. Eine ausführliche
[Installations-Anleitung zu ShareLaTeX](https://svij.org/blog/2015/01/11/kollaborativ-an-latex-dokumenten-arbeiten-mit-sharelatex/)
habe ich im Januar veröffentlicht. Um nun das LaTeX-Dokument mit "minted" zu
kompilieren, muss man lokal folgenden Befehl ausführen:

    $ pdflatex --shell-escape main.tex

Lokal ist das natürlich kein Problem. Wenn man hingegen ShareLaTeX nutzt, kann
man diesen Aufruf nicht so einfach anpassen. Das Argument "--shell-escape" wird
benötigt, da ein externes Programm ausgeführt wird. Da es ein potenzielles
Sicherheitsrisiko ist, ist es standardmäßig so konfiguriert.

Ausschalten kann man das Verhalten in dem man die Datei "/usr/share/texlive/texmf-dist/web2c/texmf.cnf"
(unter Ubuntu) öffnet und dort "shell_escape = p" zu "shell_escape = t" ändert.
Anschließend lassen sich auch in ShareLaTeX Dokumente mit dem minted Paket
problemlos kompilieren.

Witzig ist auch, dass ich dann auf einen weiteren Fehler gestoßen bin. ShareLaTeX
mag es nicht, wenn die "main.tex" __nicht__ im Hauptverzeichnis des Projektes
liegt. Wenn es in einem Unterordner befindet, dann lässt sich ggf. das Dokument
nicht kompilieren. Immerhin gibt es dazu [auch ein Ticket](https://github.com/sharelatex/sharelatex/issues/171).

Fun-Fact: Beim Suchen nach einer Lösung zu genau diesem Problem, habe ich mich
lustigerweise auch auf meinem Blog verirrt. Wenigstens beim nächsten Mal finde
ich hier die Lösung.
