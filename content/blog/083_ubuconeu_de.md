---
title: "UbuCon Europe Planungen und Call for Contributions"
categories: ["Events"]
date: "2016-04-02 18:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Es ist schon ein Weilchen her, als ich über die erste [Ankündigung zur UbuCon Europe](https://svij.org/blog/2015/06/05/ubucon-europe-2016-in-essen/) in meinem Blog schrieb. Mittlerweile sind die Planungen weiter voran
geschritten. Es wird vom 18. bis zum 20. November im Essener [Unperfekthaus](http://unperfekthaus.de)
stattfinden. Wie auch die letzten Jahre begann auch schon die Suche nach [Ideen](http://ubucon.org/en/events/ubucon-europe/talks/)
für Vorträge, Workshops und Diskussionsrunden.

Jeder der eine Idee hat, was er gerne aus dem Ubuntu und Open Source Bereich auf
der UbuCon Europe hören möchte, darf sie gerne per E-Mail an idea@ubucon.eu schicken.
Die bisherigen Ideen finden sich bereits auf der [Wunschliste](http://ubucon.org/en/events/ubucon-europe/wishlist/).
Wenn du möglicher Vortragender bist, dann kannst du dir ein Thema von der Wunschliste
nehmen, ist natürlich keine Pflicht. Sprachlich planen wir aktuell sowohl deutsche
als auch englische Programmpunkte, sodass es für die deutschen Besuchern ohne gute
Englisch-Kenntnisse ebenfalls interessante Beiträge gibt.

Mehr Informationen befinden sich auf [UbuCon.eu](http://ubucon.eu) oder auf der
[deutschen UbuCon Seite](http://ubucon.de/2016/ubucon-europe-cfc).
