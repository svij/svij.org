---
title: Höhenverstellbarer Schreibtisch nach zwei Jahren im Einsatz
categories: ["Sonstiges"]
date: "2017-04-08T13:00:00+02:00"
toc: true
slug: "hohenverstellbarer-schreibtisch-nach-zwei-jahren-im-einsatz"
---

Vor fast zwei Jahren schrieb ich ein [Review über meinen Schreibtisch](/blog/2015/04/25/schreibtisch-review-stehen-und-sitzen-am-ikea-bekant/#schreibtisch-review-stehen-und-sitzen-am-ikea-bekant). Den elektrisch höhenverstellbaren Tisch
besitze ich nun seit über zwei Jahren, der Blogpost ist auch schon fast zwei
Jahre alt. Es wird Zeit ein kleines Fazit zu ziehen und eine Hinweise bzw. Tipps
für diejenigen zu geben, die ebenfalls einen solchen Tisch anschaffen wollen.

Eins vorweg: Ein höhenverstellbarer Tisch lohnt sich! Allerdings sollte man
eine wesentliche Sache beachten, die ich auch kurz in meinem alten Blogpost
angerissen habe: Die Füße tun am Anfang weh und wie ich später festgestellt habe,
tun sie eine ganze Weile später weiterhin weh. Insbesondere wirkt sich das auf den Fersen aus.
Nachdem ich die ersten Monate mit dem Tisch häufig stehend gearbeitet habe, wurde
die Zeit pro Tag die ich am Schreibtisch stand über die Monate immer weniger, bis ich lange Zeit
sehr selten den Tisch hochgefahren habe. Der hauptsächliche Grund war, dass eben
die Fersen weh taten, da ich in der Regel barfuß am Tisch stehe. In dem Zuge
kam es auch zu den Büro-Rückenbeschwerden, durch das ständige Sitzen.

Es musste also etwas gegen beide Probleme getan werden. Es lohnt sich ja
schließlich nicht, wenn man einen solchen höhenverstellbaren Tisch besitzt,
aber diesen kaum stehen nutzt. Um den Druck aus den Füßen und Fußgelenken
zu nehmen, gibt es diverse Anti-Ermüdungsmatten. Diese sind recht weich und es
lässt sich sehr angenehm drauf stehen. Diese Matten sind wohl vergleichbar wie
die „Gummiböden“ die es auf einigen Kinderspielplätzen gibt. Der Preis ist mit
ca. 50€ (und höher) nicht gerade günstig, bietet aber ein viel besseres und
angenehmeres Stehen als es einfache Teppiche oder Isomatten bringen würden.
Die letzten beiden Dinge habe ich ebenfalls erfolglos ausprobiert.

Im Endeffekt stehe ich dank einer Anti-Ermüdungsmatte deutlich länger am Tisch,
als ich es vorher tat. Wenn ich mich früh morgens an den Tisch stelle, fahre ich
den Tisch erst gegen Mittag runter. Demnach kann ich den Leuten, die einen
höhenverstellbaren Tisch besitzen oder einen anschaffen wollen nur empfehlen,
ebenfalls eine passende Matte mitzukaufen, damit man gleich den Tisch sinnvoll
nutzt und seinen Rücken schont. Einen höhenverstellbaren Tisch möchte ich jedenfalls
nicht mehr missen.
