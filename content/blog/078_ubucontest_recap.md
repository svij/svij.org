---
title: "UbuContest recapped"
categories: ["Ubuntu"]
date: "2015-10-15 19:30:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

The last weeks and months were very intensive in terms of my Ubuntu activities. I'm
not only doing the last preparations of this years Ubucon Germany where I'm the head
of the organisation team for the first time, also we successfully ran the first community-driven
app developer contest called UbuContest. The contest was initiated by SturmFlut
and me, and we thankfully got the support from Daniel Holbach, David Planella
and Canonical. Additionally, we had jury where I was part of!

I was really surprised by the variety of the [submitted entries](http://ubucon.de/2015/contest/submitted-entries),
but I was also sad, that we only had two developer teams, therefore we didn't have
a third place in the team entries. Also, we didn't have that many [submitted nominations](http://ubucon.de/2015/contest/submitted-nominations)
for the technical and non-technical prices. While this was a good idea from SturmFlut,
I think we didn't advertise this enough. It seemed that many people didn't really
understand our initial idea to support not only app developers. Anyway, this is
one of many good points to improve the contest in the future.

The UbuContest actually had two goals: bring some fresh new open source apps to
the store and advertise the Ubucon Germany, which takes place in Berlin on 23rd to
25th October. The first goal was achieved, many of the apps were new and I'm also
happy that three winners are coming to Ubucon! ___tl;dr___: Achievement unlocked! ;-)
The winners were announced [on the Ubucon website](http://ubucon.de/node/971).

If you're around Berlin next week, don't forget to visit the [Ubucon!](http://ubucon.de/2015)
