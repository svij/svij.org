---
title: "Erfahrungsbericht: Ein Jahr mit dem dynamischen Stromtarif von Tibber"
categories: ["Sonstiges"]
date: 2024-03-03T23:00:00+01:00
cover:
  image: "/graphics/tibber-header.jpeg"
---

Seit dem 1. März 2023 bin ich Kunde bei Tibber. Das ist nun also knapp über ein Jahr her. Zeit also ein erstes Fazit zu ziehen!

Tibber bietet dynamische Stromtarife an, bei dem der stündliche Strombörsenpreis an die Kunden je nach Stromverbrauch zu der jeweiligen Stunde, weitergereicht wird. Weitere Kosten sind ansonsten neben der benötigten Hardware auch noch eine Grundgebühr, aber dazu später mehr.

In diesem Blogpost gehe ich darauf ein, wie meine Erfahrung mit solchen dynamischen Stromtarifen sind und was ein paar Vor- und Nachteile sind, die mir erst während der Nutzung aufgefallen sind. Denn ein „No-Brainer“ ist das nicht unbedingt, denn es hängt von verschiedenen Faktoren ab, auf die ich in Kürze näher eingehen werde.

## Das Grundprinzip

### Stündlich variable Strompreise

![Tibber](/graphics/tibber2.jpeg)

Das Grundprinzip ist relativ einfach. Jeden Tag um 13 Uhr ist über die Tibber App ersichtlich, wie die Strompreise für jede Stunde am folgenden Tag ist. Dies sind die Strompreise an der Börse, die an die Kunden weitergegeben werden.
Das Erste was einem auffällt ist, dass die Gebühren und Steuern auf Strom ziemlich hoch sind. Im März 2024 belaufen sich die diese in meiner PLZ (in NRW) auf knapp 20ct/kWh. Das heißt: Wenn der Strom an der Börse „kostenlos“ mit 0ct/kWh ist, zahle ich trotzdem 20ct/kWh. Trotzdem kann der Strompreis auch niedriger ausfallen, aber dazu später mehr.

### Die Hardware

Als Hardware wird der „Tibber Pulse“ benötigt. Dieser kostet 100 €, Tibber Kunden bekommen allerdings, beim Einsatz eines Referral-Codes, 50 € Rabatt. Meinen findet ihr am Ende des Artikels. Dieses Gerät kann an diverse digitale Stromzähler magnetisch angedockt werden, sodass der aktuelle Stromverbrauch entsprechend vom Stromzähler per Infrarot abgerufen und auf die Server von Tibber übertragen wird.

Damit das ganze funktioniert, muss der Stromzähler einmal freigeschaltet werden, damit die Daten auch abrufbar sind. Dazu benötigt man ein Licht, sowie einen Freischaltcode, den man dann „eingeben“ muss. Mit der „Taschenlampe“ vom Smartphone geht so etwas problemlos. Den Freischaltcode musste ich mir hingegen vom Netzbetreiber besorgen. In meinem Fall, bei Westnetz, reichte ein Anruf und ich bekam den Code am Telefon mitgeteilt.

Wer keinen digitalen Stromzähler besitzt, der kann trotzdem Tibber nutzen, auch ohne den Tibber Pulse. Dann muss manuell der Zählerstand eingetragen werden, um den Durchschnittspreis über den entsprechenden Monat zu sichern. Da ich keinen Bedarf an diesen Use Case habe, habe ich entsprechend auch keine Erfahrungswerte dazu.

### Die Abrechnung

Untypisch für Stromrechnungen: Bei Tibber zahlt man keinen monatlichen Abschlag, sondern erhält jeden Monat eine Rechnung für den tatsächlich verbrauchten Strom und dieser Betrag wird mit vier bis 6 Wochen Verzug eingezogen. Jeder Monat ist also die Stromrechnung mal höher, mal tiefer.

## Die Realität

Die Theorie ist ja soweit einfach nachvollziehbar: Jeder Stunde gibt es einen anderen Strompreis, also ist es ja besser, wenn man dann den Strom verbraucht, wenn der Strom günstiger ist. Das ist zwar in der Theorie einfach, aber praktisch nicht ganz so, wie man sich es erwartet. Es gibt nämlich viele verschiedene Faktoren, die einen stärkeren oder schwächeren Einfluss haben.

### Der gängige Stromkostenverlauf am Tag

Wenn man sich die Strombörsenpreise für jeden Tag anschaut, dann sieht man diverse Muster, die nicht erstaunlich sind, wenn man in die Thematik schon mal tiefer eingestiegen sind.

Die Strompreise an einem „normalen“ Tag sind häufig in den Morgenstunden, zu den typischen Frühstückszeiten am teuersten, genauso wie am Abend, zur Abendessenszeit. Und da kommen wir auch schon zu einem ersten Knackpunkt: Die meisten Haushalte verbrauchen zu diesen Uhrzeiten den meisten Strom. Morgens laufen Kaffeemaschinen, Wasserkocher, Toaster und co., die alle ein paar kW an Leistung beanspruchen. Und abends eben, wenn man das Abendessen zubereitet.

Die Strompreise zu diesen Peaks sind tendenziell an jedem Tag über dem jeweiligen Tagesdurchschnitt. Langfristig ist also auch spannend zu sehen, ob man dann tatsächlich günstiger den Strom bezieht, als es bei einem „starren“ Stromtarif der Fall wäre. Einmal hatte ich abends den Backofen laufen, als der Strompreis bei 50ct/kWh lag. Das kam im letzten Jahr zwar selten vor, aber es kommt vor. Da fängt man direkt an überlegen, ob man vielleicht doch was „einfacheres“ zum Abendessen macht.

Wie zuvor schon angerissen, haben aber andere Faktoren einen noch stärkeren Einfluss.

### Faktor 1: Die Photovoltaikanlage

![Tibber](/graphics/tibber3.jpeg)

Mein erster wesentlicher Faktor ist die vorhandene Photovoltaik-Anlage. Zu meiner PV-Anlage habe ich schon vor ein paar Jahren [ein paar Worte hier im Blog verloren](/blog/2019/08/07/photovoltaik-anlage-auf-dem-dach/). 

Von Ende März bis Ende Oktober ist tagsüber tendenziell Sonne vorhanden und dieser Strom wird und sollte primär verwendet werden, um den externen Strombedarf möglichst gering zu halten.

Wenn man keinen Batteriespeicher besitzt, dann bezieht man im Sommer häufig nur später am Abend und in der Nacht Strom. Im Hochsommer ist das dann häufig nur noch die Grundlast des Haushalts. Praktisch ist dabei: Man sieht direkt in der App, wie viel Strom der Haushalt „im Stand-by“ verbraucht und kann entsprechend optimieren. Wer eine PV-Anlage besitzt, wird das schon kennen.

In der Praxis bedeutete es für mich, dass ich schon länger „Großverbraucher“ tendenziell dann anmache, wenn gerade die Sonne scheint. In meinem Fall ist es primär die Waschmaschine, die idealerweise bei Sonnenschein läuft.

Letztlich hieß es für mich daher auch: Die monatlichen Stromkosten sind im Sommer deutlich niedriger als im Winter. Aber die PV-Anlage ist ja nur ein Faktor.

### Faktor 2: Der Batteriespeicher

Im Laufe des Jahres habe ich dann noch einen Batteriespeicher nachrüsten lassen. Der Strom, der von der PV-Anlage generiert wurde, wird also, wenn es nicht direkt verbraucht wird, zum Laden des Akkus genutzt. Soweit nichts, was überrascht.

Das heißt allerdings dann auch: Es wird noch weniger Strom aus dem Netz benötigt und der Einfluss von Strombörsenpreis hat einen noch geringeren Einfluss auf die Stromrechnung. Wenn also im kommenden Sommer der Strompreis explodieren sollte, so wie es 2022 passiert ist, wäre ich in dem Fall nicht so stark betroffen. Ich hätte zwar keinen fixen Strompreis, allerdings brauche ich im Sommer auch nur wenig externen Strom.

Im September hatte ich so nur Stromkosten von ~17€. Das waren im Wesentlichen die Grundgebühren vom Netzbetreiber und Tibber.

Not so Fun Fact: Toll wäre eigentlich, bei sehr günstigen Strompreisen den Akku zu laden. Das wäre insbesondere im Winter praktisch, wenn ohnehin kaum Sonne da ist und der Batteriespeicher chronisch leer ist. Aber wegen irgendwelcher EEG-Regularien ist das leider nicht erlaubt. Einige Akkus lassen sich trotzdem aus dem Netz laden, weil das eine deutsche (oder europäische?) Eigenheit ist, sodass man nur den Betriebsmodus umändern kann. Das ist bei mir allerdings nicht möglich.

### Faktor 3: Saisonale Preisunterschiede

![Tibber](/graphics/tibber1.jpeg)

Wie man aus den beiden vorherigen Abschnitten schon ableiten konnte, benötige ich im Sommer kaum externen Strom. Im Winter sieht das allerdings anders aus. Entsprechend ist für mich viel wichtiger, wie der Strompreis im Winter ist, was auch damit zusammenhängt, dass ich im Spätsommer 2023 noch eine Wärmepumpe im Haus eingebaut bekommen habe, aber dazu später mehr.

Während im Sommer vor allem morgens und abends der Strompreis teurer ist, ist es im Herbst/Winter und Teile des Frühlings anders. Das ist auch nicht so verwunderlich, da der Strommix aus regenerativer Energie saisonal unterschiedlich ist. Im Winter ist es tendenziell windiger, was dazu führt, dass der Strompreis auch mal deutlich günstiger im Winter ist, ganz unabhängig vom lokalen und generellen Sonnenschein.

### Faktor 4: Großverbraucher: Die Wärmepumpe

Wirklich hilfreich sind die dynamischen Stromtarife eigentlich erst so richtig, wenn man Großverbraucher im Haus hat, die man zeitlich steuern kann. Die Küche ist es nicht, denn wenn ich Hunger habe, habe ich Hunger.

Seit Ende August habe ich eine Wärmepumpe, die nun mal etwas mehr Strom benötigt. Vorher besaß ich hier eine Gasheizung. Die Wärmepumpe ist allerdings noch *so* neu, dass diese noch gar nicht „smart“ ist, sodass zwar ein Netzwerkanschluss vorhanden ist, diese aber bislang noch funktionslos ist, bis es über ein Update nachgeliefert wird.

Dynamisch konnte ich bisher also nicht die Wärmepumpe steuern. Das Einzige, was ging, sind fixe Zeiten. Zu Beginn, kurz nach der Installation, brauchte ich hier nur warmes Brauchwasser, hierzu reichte es, dass ich die Wärmepumpe so konfiguriert habe, dass diese nur zwischen 10 und 18 Uhr laufen soll. Da man im Sommer und auch Teile des Herbsts nur wenig Warmwasser braucht, lief diese meist nur ca. 45min, und zwar ziemlich genau um 10 Uhr. Und da schien in der Regel ohnehin die Sonne.

Im Winter war eher das Problem: Keine Sonne, ein leerer Batteriespeicher … und die Wärmepumpe wird auch zum Heizen des Hauses benötigt. Der Stromverbrauch ist dann *deutlich* höher. Richtig dynamisch steuern kann ich das nicht. Nachtabsenkung habe ich konfiguriert, vom günstigeren Strompreis in der Nacht profitiere ich nicht, da die Wärmepumpe nachts kaum läuft

Zu den statistisch typisch teuren Stunden habe ich die Wärmepumpe „ausgeschaltet“. Ansonsten ist da zurzeit nicht viel machbar. Die restliche Zeit läuft sie schon häufig, ganz losgekoppelt von Tibber und dem Börsenpreis.

Schön wäre eigentlich, wenn die Uhrzeiten automatisch konfiguriert werden würden, wann die Wärmepumpe laufen sollte. Zudem wäre es womöglich praktisch, bei deutlichen Strombörsenpreisunterschieden an einem Tag, die Vorlauftemperaturen dynamisch hochzudrehen, damit man bei teuren Zeiten nicht die Wärmepumpe laufen lassen muss. Leider geht das beides (noch?) nicht. Aber genau dann würde es spannend.

### Faktor 5: Großverbraucher: Das (nicht existente) E-Auto

Ein E-Auto ist hier im Haushalt nicht vorhanden. Aber gerade bei einem E-Auto kommen die Stärken von dynamischen Stromtarifen zu tragen. E-Autos muss man tendenziell ja nicht jeden Tag von 0 auf 100 % laden, weswegen sich hier gut anpassen lässt, wann und wie schnell man das Auto lädt. Tibber hat ein paar kompatible Wallboxen, die das entsprechend dem Strompreis dynamisch laden. Total praktisch und hilfreich. Nur für mich bislang nicht. Erfahrungswerte damit habe ich demnach keine.

### Faktor 6: Der langfristige Strombörsenpreistrend

Ein Faktor, den ich im letzten Jahr praktisch fand, war es, den mittel- und langfristigen Strombörsenpreistrend mitzunehmen. Mein vorheriger Stromanbieter Naturstrom hatte im Januar 2023 die Strompreise auf knapp 54 ct/kWh hochgedreht. Diesen Preis habe ich dann eben für zwei Monate bezahlt. Mit Tibber habe ich direkt und ohne Verzögerung davon profitiert, dass der Strombörsenpreis über die Zeit deutlich sank.

Das war in meinem Fall der größte Vorteil, auf Tibber gesetzt zu haben. Wenn allerdings die Strombörsenpreise so explodieren, wie nach 2022, dann macht so etwas hingegen gar nicht so viel Spaß. Immerhin hat Tibber nur eine Kündigungsfrist von zwei Wochen.

Ich halte mir also vor, im Fall der Fälle, kurzfristig zu kündigen und auf einen berechenbaren fixen Tarif zu wechseln. Allerdings muss das auch nicht immer nützlich sein, etwa dann, wenn der Strompreis wieder nach unten geht. Dafür fehlt dann manchmal doch die Glaskugel und es fühlt sich dann ein wenig wie Trading an der Aktienbörse an.

Ich kann für mich nur sagen, dass ich sonst eher plane, eben *nicht* mehr regelmäßig den „günstigsten“ Stromanbieter zu suchen. Das wird idealerweise nun hinfällig und spart etwas Arbeit.

## Fun Facts

Neben den bloßen Fakten gibt es noch ein paar Fun Facts!

### Aus dem Strombörsenpreis das Wetter ableiten

Lustig fand ich, wie man über die Zeit ein Gefühl dafür bekommt, wie das Wetter am nächsten Tag ist, wenn man sich die Strompreise für den nächsten Tag anschaut. Insbesondere starken Wind über mehrere Stunden sieht man deutlich im Strompreis. Diese sind dann meist über den ganzen Tag verteilt ziemlich stabil *und* verhältnismäßig günstig.

Das sind dann Tage, in denen die Strompreise pro kWh dann eben bei 20 ct oder geringer liegen. Meistens nutze ich diese Erkenntnis dann auch, noch mal separat auf den Wetterbericht zu schauen, um bei der nächsten Laufrunde nicht vom Winde verweht zu werden.

Ach ja, wir benötigen übrigens generell mehr Windräder, dann wäre es wohl noch günstiger …

### Negative Strompreise sind toll – aber selten

Der Börsenpreis ist häufiger mal bei +-0 ct/kWh. 2024 sind die Abgaben, Steuern und Gebühren auf 20 ct/kWh angewachsen. 2023 war es noch eher bei so 17ct in meiner Region.

In der Regel gibt es sehr selten starke negative Strompreise. Aber manchmal kommt das schon vor. Als Verbraucher kann man davon aus profitieren.

Im Sommer 2023 war etwa einmal für knappe drei Stunden der Strompreis inklusive Abgaben negativ. Zur Mittagssonne rum, lag der Preis in diesen zwei Stunden zwischen -44ct und -5ct pro kWh, bevor es dann wenige Cent über 0 lag.

In dieser Zeit konnte man also für das Stromverbrauchen Geld verdienen. Ich dachte also direkt: Challenge accepted!

Leider fehlte ein E-Auto, sodass es keinen so großen Spaß gemacht hat. Aber das Kochen, das Staubsaugen, das Entkalken des Wasserkochers hat einiges an Strom verbraucht, sodass ich damit profitieren konnte. Allerdings musste ich dafür die PV-Anlage und den Akku abschalten, wo die PV-Anlage dann auch nicht 6–7 kWh eingespeist hat, … sonst hätte ich ja den eigenen Strom verbraucht, statt für das Stromnutzen Geld zu bekommen.

Das war für mich natürlich mehr ein Versuch zu schauen, ob das tatsächlich so funktioniert, wie es beworben wird. Und das ging entsprechend genauso wie erwartet. Auch hier wäre also ein E-Auto praktisch gewesen, genau dann zu laden. Dafür muss aber auch der Akku davon möglichst leer sein.

Aber wichtig: Solche negativen Strompreise kamen nur sehr selten vor. Ich meine, es waren vielleicht drei Tage, in denen der Strom kostenlos oder günstiger war. Und an diesen Tagen eben nur für maximal drei Stunden.

## Not so Fun Facts

Es gibt aber auch ein paar „Not so Fun Facts“ …

### Ständiger Kontrollwahn

Den mentalen Aspekt darf man auch nicht unbedingt vernachlässigen. Einige sind dafür vielleicht weniger anfällig als andere.

Für mich hieß es, dass ich – primär zu Beginn – ständig nachgeschaut habe, wie der Preis ist. Ich habe allerdings keine so große Möglichkeit den Stromverbrauch anzupassen. Das erhöhte bei mir tatsächlich manchmal den „Mental Load“ weil man doch irgendwie „ständig“ nachschaut, wie der Strompreis gerade so ist. Vor dem Anschalten des Backofens oder der Waschmaschine prüft man also … genauso wie den mittel- und langfristigen Trend.

Ich kann auch noch sehen, wie hoch der Stromverbrauch pro Tag ist, das ist praktisch, vor allem wenn man etwas optimieren und datenbasiert prüfen möchte, ob es auch zu Änderungen geführt hat. In meinem Fall waren es Änderungen der Konfiguration der Wärmepumpe. Das ist zwar super, aber ständig herumfrickeln ist auf Dauer, für den Kopf, doch etwas „anstrengend“. Da ist dann manchmal ein stupider fixer Stromtarif doch besser.

Das ist jetzt natürlich deutlich negativer ausgedrückt, als ich mich tatsächlich fühle. Gehört aber letztlich auch zur gesamten Wahrheit solcher Stromtarife. Und da ich Daten mag, schaue ich da (natürlich) häufiger drauf, als es nützlich und hilfreich ist.

### Die Frage nach dem Datenschutz

Aus Datenschutzsicht ist ein solcher Anbieter natürlich nicht so toll. Meine Stromverbrauchsdaten liegen auf den Servern von Tibber. So hat Tibber einen gewissen Einblick in mein Privatleben und sieht theoretisch auch, wenn niemand zu Hause ist. Weil dann eben der Stromverbrauch ziemlich niedrig ist.

Für mich ist das in Ordnung, aber das muss nicht für jede Person bzw. Haushalt der Fall sein. Letztlich sollten keine fremden Menschen Zugang zum Tibber-Account bekommen, denn sonst können diese die Daten abzapfen und sehen, wann man das Haus ausrauben kann.

## Fazit

Ob sich dynamische Stromtarife lohnen oder nicht, ist, finde ich, nicht ganz so offensichtlich. Es spielen viele Faktoren rein, die einen größeren oder kleineren Einfluss auf den Strompreis hat.

Was ich ein wenig ausgeklammert habe, sind die Grundgebühren von Tibber von knapp 6 €/Monat. Wenn man nur sehr wenig Strom verbraucht, dann muss man diese Gebühren auf den Stromverbrauch aufrechnen. Bei höherem Stromverbrauch ist das vernachlässigbar, allerdings haben auch viele andere Stromanbieter eine Grundgebühr, zusätzlich zum verbrauchsabhängigen Preis.

Unabhängig von dem unmittelbaren Partizipieren an fallenden (und steigenden) Strombörsenpreisen auf lange Sicht, lässt sich schon sagen, dass sich Tibber eher lohnt, wenn man automatisierbare Großverbraucher hat. Je weniger, das der Fall ist, desto weniger „lohnt“ sich Tibber.

Ich habe übrigens auch einen Tibber Affiliate Link, [den man hier abrufen kann](https://invite.tibber.com/xl6yaia4). In dem Fall würden beide Seiten jeweils 50 € Guthaben für den Tibber Store bekommen.
