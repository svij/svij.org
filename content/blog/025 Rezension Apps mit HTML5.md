---
title: "Rezension: Apps mit HTML5 und CSS3 für iPad, iPhone und Android"
categories: ["Rezension"]
tags: ["Mobile", "Web"]
date: "2014-02-15T04:00:00+02:00"
slug: "rezension-apps-mit-html5-und-css3-fur-ipad-iphone-und-android"
toc: true
---

Das Buch „Apps mit HTML5 und CSS3 für iPad, iPhone und Android“ ist 2013 in 
einer zweiten aktualisierten und erweiterten Auflage erschienen und behandelt 
dabei grundsätzlich die WebApp-Entwicklung mit den neuesten Web-Technologien 
HTML5, CSS3 sowie diversen JavaScript Frameworks für iOS, Android und auch 
Windows Phone.

## Was steht drin?

Das Buch ist in zehn Kapitel unterteilt und umfasst dabei 524 Seiten. 
Geschrieben wurde das Buch von den zwei Autoren Florian Franke und Johannes 
Ippen.

Die ersten Kapitel behandeln die Grundlagen der WebApp-Entwicklung. Zunächst 
erfolgt im ersten Kapitel der direkte Einstieg zum Thema Apps, in dem die 
grundlegendsten Unterschiede zwischen nativen Apps und WebApps erläutert werden. 
Im Anschluss folgt im zweiten Kapitel der Einstieg in das Thema HTML5, CSS3 
sowie JavaScript. Es wird dabei ausführlich darauf eingegangen, welche neuen 
HTML-Tags HTML5 mit sich bringt und wie sie arbeiten. Zudem wird verdeutlicht, 
wie sie sich auf die einzelnen Plattformen – iOS und Android – auswirken. Der 
ausführlichste Teil vom zweiten Kapitel ist allerdings der CSS-Teil. Im 
JavaScript-Unterkapitel wird anschließend das JavaScript-Framework Zepto.js 
vorgestellt, welches speziell für mobile Geräte entwickelt wurde.
Kapitel 3 und 4 gehen einen Schritt weiter, sind aber ebenfalls noch erweiterte 
Grundlagenkapitel. Im dritten Kapitel geht es zunächst um die Konzeption sowie 
um Gestaltungsprinzipien von WebApps. Dabei handelt es sich grundsätzlich um das 
Design der graphischen Oberfläche von Apps und was man dabei beachten sollte: 
etwa nicht zu kleine Buttons wählen oder auch die Nutzung von Registern. Das 
vierte Kapitel thematisiert anschließend „HTML5 als Designwerkzeug“. Es wird 
unter anderem anschaulich erklärt, wie man mit dem Less CSS Framework arbeitet 
und damit ein Magazin-Layout mittels Rastern kreiert. Dazu kommen allerdings 
auch noch weitere HTML5-Designwerkzeuge zum Einsatz, wie der Einsatz von 
Schriften, die Einbindung von Audio und Video sowie Animationen mit CSS.
Kapitel 5 befasst sich ausschließlich und ausführlichst mit der 
Positionsbestimmung. Schritt für Schritt wird hierbei anhand einer Beispiel-App 
erläutert, wie man die aktuelle Position mittels GPS und der WLAN-Lokalisierung 
ermittelt. Im darauf folgenden siebten Kapitel wird erklärt, wie man eine WebApp 
offline verfügbar macht.

Im achten Kapitel geht es endlich an das Eingemachte. Dieses ausführliche 
Kapitel umfasst die Entwicklung von WebApps mit Frameworks. Konkret werden dazu 
drei Beispiel-Applikationen entwickelt und erklärt. Die Frameworks sind jQuery 
Mobile, Sencha Touch sowie Twitter Bootstrap.

Das neunte und somit vorletzte Kapitel behandelt die Entwicklung von WebApps in 
nativen Anwendungen, die dann nicht mehr eine bloße Webseite darstellen, sondern 
über einen AppStore von Apple oder den Play Store von Google bezogen werden 
können. Das Kapitel beginnt mit dem Einsatz des Frameworks PhoneGap und endet 
mit einer Anleitung, wie man die fertige App veröffentlicht.
Im letzten Kapitel wird noch kurz die Plattform Windows Phone 8 angerissen. Auch 
hier wird eine kleine WebApp geschrieben, bei der besonders auf die Eigenheiten 
des Internet Explorers eingegangen wird.

## Wie liest es sich?

Das Buch setzt Grundkenntnisse und Erfahrung mit HTML, CSS und JavaScript 
voraus. Absolute Einsteiger dürften daher diverse Probleme haben, alles 
vernünftig verstehen und anwenden zu können. Mit geringen HTML-, CSS- und 
JavaScript-Kenntnissen und etwas Erfahrung sollte dies allerdings kein großes 
Problem darstellen.

Das Buch ist allgemein in einem guten, lockeren Ton geschrieben. Die Beispiele 
sind gut gewählt und lassen sich gut und einfach nachvollziehen. Empfehlenswert 
ist es natürlich, die Apps selbst nachzuprogrammieren. Im Buch wird großzügig 
Code abgedruckt. Die wichtigsten Code-Zeilen sind dabei immer vorhanden, meist 
aber auch mehr als nötig.

Die beiliegende DVD besitzt neben dem vollständigen Code der Beispiel-Apps 
zusätzlich auch einige Video-Trainingslektionen.

## Kritik

Das Buch ist grundsätzlich empfehlenswert, da die Thematik gut und anschaulich 
erläutert wird. Es macht Spaß, das Buch zu lesen und die Apps zu entdecken. 
Allerdings gehe ich davon aus, dass komplette Neueinsteiger Schwierigkeiten 
haben werden, alle Beispiele und Skripte korrekt nachzuvollziehen. Vor allem der 
Einstieg in die Funktionsweise von CSS wird nur oberflächlich angerissen. 
Stattdessen stürzt man sich in diesem Buch sehr schnell tief in CSS.
Man merkt, dass beide Autoren Erfahrung in der WebApp-Entwicklung besitzen. 
Allerdings scheint es so, als ob sie mehr Erfahrung in der Entwicklung mit iOS 
haben als mit Android. So beziehen sich die Beispiele immer nur auf den 
Standard-Android-Browser, nicht aber auf andere verfügbare und verbreitete 
Browser unter Android. Dem langen de-facto-Standard Chrome wurde in dem Buch 
keinerlei Beachtung geschenkt. Mittlerweile ist Chrome auch schon echter 
Standard für native WebApps geworden.

Schade ist zudem, dass kein Blick auf die möglichen neuen Plattformen geworfen 
wurde. Firefox OS wäre zum Beispiel ein guter Kandidat, da es komplett auf 
Web-Technologien setzt. Leider wurde es mit keinem Wort erwähnt, was allerdings 
sehr wahrscheinlich damit zusammenhängt, dass gedruckte Bücher selten 
hochaktuell sind. Die Veröffentlichung dieses Buches liegt immerhin auch schon 
einige Monate zurück.

In [freiesMagazin 07/2012](http://www.freiesmagazin.de/freiesMagazin-2012-07)
rezensierte Michael Niedermair bereits die erste 
Ausgabe dieses Buches. Er kritisierte dabei u. a., dass auf der Rückseite des 
Buchcovers „Für Windows und Mac“ vermerkt ist, obwohl alles sehr wohl auch unter 
Linux funktioniert. Dieser Vermerk ist in der aktualisierten Ausgabe auch 
weiterhin enthalten; so existiert die Installations-Anleitung von XAMPP im Buch 
nur für Windows und Mac OS X.

Zum Schluss lässt sich noch sagen, dass der Umfang im Vergleich zu der 
vorherigen Ausgabe etwas gestiegen ist – von knapp 440 Seiten auf 524 Seiten. 
Wer Spaß und Interesse an der Entwicklung von HTML5-Apps besitzt und auch 
Erfahrung in HTML, CSS und JavaScript besitzt, kann sehr wohl auf das Buch 
zurückgreifen. Für 29,90 Euro bekommt man ein ausgewogenes Buch mit Hardcover 
und einem Lesebändchen. Das Buch ist komplett in schwarz-weiß gedruckt, was dem 
Inhalt allerdings keineswegs schadet.

Buchinformation:

*   Titel: [Apps mit HTML5 und CSS3 für iPad, iPhone und Android](http://www.galileocomputing.de/katalog/buecher/titel/gp/titelID-3330)
*   Autor: Florian Franke, Johannes Ippen
*   Verlag: Galileo Press, 2013
*   Umfang: 524 Seiten
*   ISBN: 978-3-8362-2237-2
*   Preis: 29,90€
