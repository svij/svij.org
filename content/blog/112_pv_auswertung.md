---
title: "Photovoltaik-Anlage auf dem Dach"
categories: ["Sonstiges"]
date: "2019-08-07 20:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg02.met.vgwort.de/na/fa036002c250479bb6c6983363e68d41"
cover:
  image: "/graphics/pv-anlage.webp"
---

Hier mal ein thematisch etwas anderer Blogpost. Vor etwas über einem Jahr ließ
ich mir eine Photovoltaik-Anlage auf dem Dach des Hauses montieren. Einige Tage
nach der Montage ging die Anlage auch an das Stromnetz. Das ist heute auf dem
Tag genau ein Jahr her. In diesem Blogpost zeige ich ein wenig, wie mäßig die
Software ist, wie man selbst Herr über die Daten wird und wie weit die Schätzungen
vom Ertrag von der Realität abweichen.

![PV-Anlage auf dem Dach…](/graphics/pv-anlage.webp)

Die Photovoltaik-Anlage besteht aus 20 Modulen à 335W. Es hat somit eine
Peak-Leistung von 6700 Wh. Theoretisch sollte also keine höhere Peak-Leistung
bei Idealbedingungen möglich sein, in der Realität sah ich ein paar mal Peak-Leistungen,
die darüber lagen.

## (Smarter) Wechselrichter

Neben den Modulen, Gestellen, Kabeln und anderen Kleinigkeiten, besteht so eine
Photovoltaik-Anlage noch aus einem Wechselrichter, der den Gleichstrom in
Wechselstrom umwandelt. Konkret besitze ich einen Kostal PIKO 7.0. Viele der
neueren Wechselrichter sind „smart“. Sie besitzen eine LAN-Buchse und kommen
also mit Web-Interface und App daher, mit diversen Vor- und Nachteilen, was die
Daten angeht, da man diese optional aus der Hand geben kann, was ich tue.

Die Qualität der Software ist allerdings optimierungsbedürftig. Der Wechselrichter
erfasst einiges an Daten. Das umfasst unter anderem den aktuellen Stromverbrauch des Hauses,
die Generatorleistung (Ertrag) der PV-Module, eine Eigenverbrauchsquote oder
ein Einspeisung des Stroms in das öffentliche Netz. Der Wechselrichter sammelt
diese Daten für ein paar Stunden und lädt sie in (un)regelmäßigen Abständen innerhalb eines Tages auf
den Server vom Anbieter hoch. Mir ist bewusst, dass dadurch der Anbieter Daten
hat, um zu sehen, wann etwa die Kaffeemaschine angeschaltet wird, oder wann gekocht wird.

![Tages-Ansicht im Web](/graphics/pv-anlage-online.webp)

Die hochgeladenen Daten können über die Web-Oberfläche von Kostal oder über die
App eingesehen werden. Leider ist hier schon ein Problem da, denn manchmal verschluckt
der Wechselrichter scheinbar die Daten und lädt diese nicht zuverlässig hoch.
Das führt zu Lücken in den Graphen des Tages und es lässt sich keine zuverlässigen
Auswertungen zu. Für den Monat Juli waren etwa an vier Tagen fast keine Daten
hochgeladen worden, was die hinterlegte Statistik für die einzelnen Tage/Monat/Jahre
verfälscht und unbrauchbar macht.

![Tages-Ansicht in der App](/graphics/pv-anlage-app.webp)

Die App kann nicht nur die Daten von den Servern laden und mittelmäßig darstellen,
sondern auch Live-Daten anzeigen. Dazu muss sich das Handy aber im selben Netzwerk
wie der Wechselrichter befinden. Dargestellt wird dann der aktuelle Ertrag der
PV-Anlage, die aktuelle Einspeisung ins Netz, der aktuelle Verbrauch des Hauses
und Füllstand und Lade/Entlade-Kapazität von einer Batterie. Da ich keine Batterie
habe, ist die entsprechend immer bei 0.

Die Qualität der App war auch eher mittelmäßig. Häufig hat die App die Zugangsdaten
vergessen und man musste sich diese immer wieder neu eintippen. Auch die grafische
Darstellung war eher schlecht, da sich einige Zahlen in den Graphen überlappen,
wenn das Smartphone im Hochformat verwendet wird. Prinzipiell praktisch ist
hingegen die Möglichkeit pro Stunde, Tag, Woche, Monat und Jahr die Verbrauchs-
und Ertragsdaten darstellen zu lassen. Aufgrund der sporadischen Aussetzer war
dies mir allerdings auch nicht zufriedenstellend genug.

Neben dem Online-Portal und der App hat der Wechselrichter noch einen Webserver
laufen, über den man einige Daten abrufen kann. Dies ist nur im lokalen Netz
erreichbar und nutzt dieselbe Schnittstelle, wie die App.

## Eigene Datenhaltung

Aufgrund der eher mäßigen Qualität der Software musste also eine eigene
Datenhaltung her. Nicht, weil ich unbedingt genaue Daten brauche, sondern weil es geht
und weil ich es kann.

Die zuvor angesprochene Web-Oberfläche des Wechselrichters sind lediglich ein paar einfache
HTML-Seiten mit JavaScript. Um an die Daten des Wechselrichters zu kommen, musste
also ein wenig „Reverse-Engineering“ durchgeführt werden. Das hab ich bewusst
in Anführungsstrichen geschrieben, weil das, was ich getan hab, viel zu einfach
ist, als es Reverse-Engineering zu nennen.

Der JavaScript Code sendet alle paar Sekunden HTTP-Anfragen an den Wechselrichter,
in dem es lange IDs hinschickt und Antworten als Key-Value Paare zurückerhält. Aus
den IDs und somit auch Keys der Antworten, lässt sich nicht aus Anhieb erkennen,
was für ein Wert das ist. Mit ein wenig logisches Denken, kann man das allerdings
sehr schnell mit den Werten aus der Web-Oberfläche verknüpfen. Dazu war auch
ein Blick in den Netzwerk-Tab der Web-Konsole hilfreich. Kompliziert war das nicht,
eher ein wenig nervige Handarbeit.

Um nun die aktuellen Daten auszulesen, habe ich ein Python-Skript geschrieben,
welches anhand der IDs die Daten über die REST-Schnittstelle abruft und in eine
PostgreSQL-Datenbank schreibt. Das Skript ist im GitHub-Repository [svijee/kostal-dataexporter](https://github.com/svijee/kostal-dataexporter)
zu finden. Komplex ist das ganze nicht. Das Skript läuft bei mir innerhalb
eines Docker Containers und schreibt es in die Datenbank. Das Skript ist nicht
wirklich schön – aber es funktioniert!

Die Daten möchte ich natürlich auch noch visualisieren. Dazu habe ich (natürlich)
Grafana im Einsatz, wofür ich ein Dashboard gebaut habe. Zum jetzigen Zeitpunkt
hat es allerdings nicht die gleichen Features wie die App bzw. das Web-Portal von
Kostal. Ich kann nämlich bislang nur die Daten von einem Tag darstellen und
es findet noch keine praktisch-nutzbare Aggregierung der Daten über mehrere Tage,
Wochen, Monate oder Jahre statt. Ich kann daher etwa nicht anschauen, wie viel
Strom pro Tag oder Woche erzeugt wurde.

![PV-Anlage über Grafana](/graphics/pv-anlage-grafana.webp)

In dem Screenshot lässt sich einiges Interessantes herauslesen. Oben links ist
der Graph für den Gesamtertrag der Anlage und unten links der Graph für die Einspeisung in
das öffentliche Netz. Wie man daraus erkennen kann, läuft der untere Graph gegen
eine Grenze bei fast 5 kW. Dies hängt damit zusammen, dass der Netzbetreiber (in
meinem Fall Westnetz) für die EEG-Vergütung nur bis zu 70% der Peak-Leistung der
Anlage annehmen müssen. Das entspricht bei meiner Anlage eine maximale Leistung
bei der Einspeisung von 4690 W. Der Eigenverbrauch des Hauses wird da nicht
mit eingerechnet, sodass alles über 70% Leistung reduziert wird, d.h. es eine
künstliche Leistungsreduzierung stattfindet, sofern der Strom nicht selbst verwendet
wird. Alles über 70% der Peak-Leistung ist somit kostenfreier Strom, da dieser
sonst „verpufft“. Aus ökologischer Sicht ist diese 70% Regelung natürlich Unfug,
da sauberer Strom künstlich reduziert wird, nur, damit der Netzbetreiber nicht alles
an produzierten Strom ankaufen muss. Als PV-Anlagen Betreiber heißt das dann natürlich:
Möglichst viel Strom tagsüber verbrauchen.

Zurück zum technischen! Die übrigen Daten, die auf dem Dashboard zu sehen sind,
sind alles Daten, die direkt von der REST-Schnittstelle vom Wechselrichter
kommen. Viel Magie passiert da also nicht.

Rechts sieht man noch zwei Graphen: den Stromverbrauch im Haus, sowie darunter
pro Phase. Letzteres interessiert mich in der Praxis nicht wirklich. Aus dem
Stromverbrauch im Haus lassen sich diverse Informationen auslesen. Man erkennt
gut, wann der Herd an war und man erkennt häufig auch sehr gut, wenn die Personen
im Haushalt aufstehen, da der angeschaltete Wasserkocher ebenfalls erkennbar ist.
An Tagen wo keiner zu Hause ist, erkennt man auch sehr gut den laufenden Kühlschrank,
der in regelmäßigen Abständen für eine gewisse Zeit kühlt und dann in den Standby wechselt.
Für mich zeigte das deutlich, dass man aus diesen Metadaten einiges erkennen kann,
was auch ein wesentlicher Kritikpunkt von smarten Stromzählern sind.

Ein weiterer Punkt, der aus dem Graphen zu erkennen ist, dass Nachts keine Erfassung
des Stromverbrauchs erfolgt. Dies hängt damit zusammen, dass der Sensor, der den
Stromverbrauch des Hauses misst, nur dann läuft, wenn Strom aus der Photovoltaik-Anlage
bekommt. Der Hauptzweck des Sensors ist die Berechnung, wieviel Strom in das Netz
eingespeist werden kann, wenn gleichzeitig Strom im Haus verbraucht wird. Dies hängt
erneut mit der 70% Regelung zusammen. Kostal hält es jedenfalls nicht für notwendig,
dass man den Sensor so nutzen kann, dass man 24h den Stromverbrauch im Haus messen kann. Um
dies zu ermöglichen, bräuchte ich eine Batterie, die wiederum aktuell noch zu teuer
ist.

## Auswertung

Spannend war für mich die Auswertung der Daten nach einem Jahr. Die Firma, die mir
die Photovoltaik-Anlage montiert hat, hat über ein Programm ausgerechnet, wie
hoch die möglichen Erträge für meine Anlage ist. Das war keine einfache Excel-Tabelle
(wie sie teilweise andere Firmen genutzt haben), sondern ein Programm, was diverse
Faktoren einkalkuliert hat. Darunter die Sonnenstunden der letzten Jahre auf
den Koordinaten, den Winkel der Anlage auf dem Dach und der Winkel zur Sonne
und mögliche Schattierungen von Bäumen in der Nähe.

Relevant für die Amortisation ist sowohl der Strombedarf für einen selbst, als auch
ein möglichst hoher Eigenverbrauch, da dann weniger Strom vom Stromanbieter bezogen
werden muss, da dies deutlich teurer ist.

Unser Jahresverbrauch im Vorjahr lag bei uns bei etwa 3800 kWh bei einem 2,5 Personen
Haushalt. Dies diente dann als Berechnungsgrundlage für den Amortisierungsplan.
Folgende Leistungsdaten berechnete das Programm von der Firma:

 * Ertrag der PV-Anlage: 6410 kWh
  * davon 1680 kWh Eigenverbrauch
  * davon 4730 kWh Netzeinspeisung

 * Gesamtverbrauch: 3800 kWh
  * davon 1680 kWh durch PV
  * davon 2128 kWh durch Netz

Die Realität ein Jahr später sieht spannend aus:

 * Ertrag der PV-Anlage: 7405 kWh (+995 kWh , +13,4%)
  * 1600 kWh Eigenverbrauch (-80 kWh, -5%)
  * 5805 kWh Einspeisung (+1075 kWh, +18,5%)

Der Gesamtverbrauch lag fast genau bei der Schätzung:

 * Gesamtverbrauch: 3793 kWh (-8 kWh, -0,01%)
  * 1600 kWh durch PV (-80 kWh, -5%)
  * 2193 kWh durch Netz (+65 kWh, +3%)

Wie man sieht, sind die geschätzten Werte nahezu insignifikant höher oder niedriger
als geschätzt, mit einer krassen Ausnahme: der Gesamtertrag der PV-Anlage und somit
auch die Einspeisung in das öffentliche Netz. Da schien die Sonne wohl deutlich mehr
als die Jahre zuvor.

Und hier noch ein paar Funfacts:

 * Der Monat mit dem höchsten Ertrag war der Juni mit über 1000 kWh
  * Photovoltaik-Anlagen arbeiten besser, wenn es kühler ist. Der Juli war deutlich zu heiß, das sieht man deutlich an den Ertragsgraphen
 * Der Tag mit dem höchsten Ertrag war der 15. Mai mit 46,4 kWh
  * Die Screenshots oben sind von dem Tag
 * Der Tag mit dem geringsten Ertrag war der 16. Dezember mit 0,7 kWh
  * Das war einer der wenigen Tage mit 100% Eigenverbrauchsquote

## Fazit

Rein finanziell lohnt sich so eine Photovoltaik-Anlage für mich. Aus technischer
Sicht machte das schon viel Spaß sich damit näher auseinander zu setzen, und damit
mein ich sowohl generell das Thema Photovoltaik, als auch die Erfassung, Auswertung
und Visualisierung der Daten. Persönlich fehlt mir ja noch eine Batterie, um den
Eigenverbrauch zu steigern. Dazu lag mir zwar auch ein Angebot vor, doch 33% Aufpreis
bei einer längeren Amortisationsdauer von +50% war nicht wirklich sinnvoll. Da
warte ich lieber auf hoffentlich günstigere Angebote in Zukunft für Batterien, da diese sich
ja mit überschaubaren Aufwand nachrüsten lassen.
