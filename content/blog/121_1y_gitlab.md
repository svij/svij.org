---
title: "One year at GitLab: 7 things which I didn't expect to learn"
date: "2021-04-12T21:15:00+02:00"
categories: ["Sonstiges"]
vgwort: "https://vg08.met.vgwort.de/na/70a435ac0ac44fef968753bed84f70be"
tags: ["ubuntu-planet"]
cover:
  image: "/graphics/svij_gitlab.webp"
---

One year ago I joined GitLab as a Solution Architect. In this blog post I do not
want to focus on my role, my daily work or anything pandemic related. Also, there
won't be a huge focus in regard to all remote working. I rather want to focus on
my **personal experiences** in regard to the work culture. I'll focus on things
which I certainly did not think about before I joined GitLab (or any other
company before).

Before joining GitLab I worked for four German companies. As a German with a
Sri-Lankan Tamil heritage I was always a minority at work. Most of the time it wasn't
an issue. At least that's what I thought. At all those previous companies there
were mostly white male and with very few (or even none) non-males especially in
technical **and** leading roles. Nowadays, I realize what a huge difference a globally
distributed company makes with people from different countries, cultures,
background and gender. 

There were sooo many small things which makes a difference and which opened my
eyes.

## People are pronouncing my name correctly

Some of you might (hopefully) think:

> Wait that was an issue!?

Yes, yes it was. And it was super annoying. Working in a globally distributed
companies means that the default behavior is: People are asking how to
correctly (!) pronounce the full (!) name. It's a simple question, and it
directly shows respect even if you struggle to pronounce it correctly on the
first time. My name is transcribed from Tamil to English. So the average
colleagues simply tries to pronounce it in English, and it's perfect and that
includes the German GitLab colleagues. In previous jobs there were a lot of
colleagues who didn't ask, and I was "the person with the complicated name",
"you know who" or some even called me "Sushiwahn". One former colleague
referenced me to the customer in a phone call as "the other colleague". That
was not cool.  If you wonder on how to pronounce my name: I uploaded a
recording on my profile website on
[sujeevan.vijayakumaran.com](https://sujeevan.vijayakumaran.com).
I should've done that way earlier.

## The meaning/origin of my name

I never really cared about the meaning of my name. So many people have asked me
if my name has a meaning or what the origin was. I didn't know, and I also didn't
really care. My mum always simply told me "Your name has style". My teammate
[Sri](https://srirangan.net/) one day randomly dropped a message in our team
channel:

> If you break down your name into the root words, it basically translates to "Good Life (Sujeevan) Prince Of Victory (Vijayakumaran).

That blew my mind 🤯.

## #BlackLivesMatter and #StopAsianHate

So many terrible things happened in the last year in the world. When these two
movements appeared it was a big topic in the company. Even with messages in our
[#company-fyi](https://about.gitlab.com/handbook/communication/#posting-in-company-fyi-1)
channel which is normally used for company related announcements. While
#BlackLivesMatter was covered in the German media, #StopAsianHate was not
really covered in the German media at all.

Around the time of #BlackLivesMatter my manager asked in our team meeting how/if it
affects us even if we - in our EMEA team – are far away from the US.  I had the
chance to share stories from my past
which wouldn't have happened for the average white person in a white country.
This never happened in any other company I worked before.  When the [Berlin truck
attack at a Christmas market](https://en.wikipedia.org/wiki/2016_Berlin_truck_attack) (back in 2016)
happened it was a big topic at lunchtime with the colleagues. When a racist
[shot German Migrants in Hanau](https://en.wikipedia.org/wiki/Hanau_shootings)
in February 2020 it was not really a topic at work. At one of the attacks Germans were
the victims in the other it were migrants. Both shootings happened before I
joined GitLab. When there was a shooting in Vienna in November 2020 colleagues
directly jumped into the local Vienna channel and asked if everyone is somewhat
okay.  *See the difference!*

## Languages
We have a #lang-de Channel for German (the language not the country!) related
content. There are obviously many other channels for other languages. What
surprised me? There were way more people outside of Germany without a German
background who are learning or trying to learn German. It's a small thing but
it's cool! There were many discussions about word meanings and how to
handle the German language. Personally that got me thinking if I should pick up
learning French again.

## Meanings of Emojis
There are a lot of emojis. Specially in Slack. At the beginning it was somewhat
overwhelming, but I got used to it. One thing which confused me right after I
joined was the usage of the 🙏🏻 emoji.  Interestingly the name of the emoji is
simply ["folded hands"](https://emojipedia.org/folded-hands-light-skin-tone/).
But what is the meaning of it? When I first saw the use of it I was somewhat
confused. For me as a Hindu it's clearly "praying". The second meaning which
comes to my mind is use of it as a form of greeting – see
[Namaste](https://en.wikipedia.org/wiki/Namaste). However, there are so many
colleagues who use it for some sort of "thanks". Or even "sorry". *Emojis have
different meanings in different cultures!*

## Different People – Different Mindsets
Since GitLab is an all-remote company our informal communication is happening
in Slack channels and in [coffee
chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats)
in Zoom calls. In the first weeks I scheduled a lot of coffee chats to get to
know my teammates and *some* other colleagues in other teams. The most useful
bot (for me) in Slack is the [Donut
Bot](https://about.gitlab.com/company/culture/all-remote/informal-communication/#the-donut-bot)
which randomly connects two persons every two weeks. I don't have to take care
to randomly select people from different teams and department. And honestly I
most likely would be somewhat biased when I would cherry-pick people to
schedule coffee chats.

So every two weeks I get matched with some "random" person. This lowers the
bar to talk to someone from some other department where I (shamefully)
thought: "Oh that role sounds boring to me." But if it sounds boring to me
that's the first sign that I should talk to them. Without the Donut Bot I
would've most likely not talked to someone from the Legal department, just to give
one small example. And there were also a lot of engineers who didn't really
talk to someone from Sales, like I am part of the Sales team. Even though we do
not need to talk about work related stuff I generally learned something new when
I leave the conversation at the end.

However, the more interesting part is to get to know all the different people
in the different countries and continents with different cultures. There are
many colleagues who left their home country and live somewhere else. The
majority of these people are either in the group "I moved because of my
(previous) work" or "I moved because of my partner". The most surprising
sentence came from a Canadian colleague though:

> I'm thinking of relocating
to Germany for a couple of years since it's easily possible with GitLab. All my
friends here are migrants and I really want to experience how it is to learn a
new language and live in another country.

*That* was by far the most
interesting reason I heard so far!  Besides that my favorite question I ask
those people who moved away from their home country is what they're
missing and what they would miss if they moved back. This also leads to some
fascinating stories. Most of them are related to food, some to
specific medicine and some reasons are even "I like the $specific\_mentality
over here which I would miss".

I left out the more obvious parts of a globally distributed team like getting
to know how life is in the not-so-rich countries of the world. Also, I finally
understood what the difference between the average German is compared to the
average Silicon Valley person. The latter is way more open to a visionary
goal while the average German wants to keep their safe job for a long time
(yes, even in IT).

## Mental Health Awareness

We have a lot of content related to [Mental Health](https://about.gitlab.com/company/culture/all-remote/mental-health/)
which I still need to check out. It's a super important topic on so many different
levels. At all my previous employers this was not a topic at all. I might even
say it's generally a taboo topic. One thing which I definitely did not expect
was the introduction of the [Family and Friends day](https://about.gitlab.com/company/family-friends-day/)
which was introduced in May 2020 shortly after I joined the company and it happened nearly
every month since then and was introduced because of the COVID-Lockdowns. On that
day (nearly) the whole company has a day off to spend time with their family
and friends. My German friends reaction to that was something like: 

> Wait didn't you join a hyper-growth startup-ish company? That doesn't
sound like late-stage capitalism what I would have expected!

In addition to that, there's also a [#mental-health-aware Slack channel](https://about.gitlab.com/company/culture/all-remote/mental-health/mental-health-awareness-learning-path/#id-like-to-learn-more-from-my-gitlab-team--where-can-i-go-to-talk-about-my-mental-health) where everyone can talk about their problems.
I was really surprised to see so many team members to share their problems
and what they are currently struggling with. I couldn't have imagined that
people share very personal stories in the company and that includes people sharing
their experince with getting help from a therapist.

As someone who is somewhat an introvert who struggles to talk to a lot of people
in big groups in real life this past year (and a few more months) has been
relatively easy to handle in this regard as I only met four team members
in person so far. However, the first in-person company event is coming up, and I'm
pretty confident that getting in touch with a lot of mostly unknown people will
be easier than at other companies I've worked for so far.

## Things which I totally expected

There are still things which I expected to work as intended. Here's a short list:

 * All Remote and working async works pretty damn good and I really don't want to go back to an office
 * Spending company money is easy and definitely not a hassle
 * Not having to justify how and when I exactly work is a *huge* relief
 * Not being forced to request paid time off is an unfamiliar feeling at the beginning, but I got used to it pretty quickly
 * Working with people with a vision who can additionally identify with the company is great
 * No real barriers between teams and departments
 * [Values matter](https://about.gitlab.com/handbook/values/)

For me personally GitLab has set the bar for companies I might work for in the
future pretty high. That's good and bad at the same time ;-). If you want to read
another story of "1 year at GitLab" I can highly recommend [the blogpost of dnsmichi](https://dnsmichi.at/2021/03/02/my-1st-year-all-remote-at-gitlab/)
from a month ago.
