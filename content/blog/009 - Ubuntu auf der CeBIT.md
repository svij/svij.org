---
title: Ubuntu auf der CeBIT 2012
categories: ["Ubuntu"]
date: "2012-03-12T22:30:00+01:00"
toc: true
---

Letzte Woche fand die CeBIT 2012 in Hannover statt. Zum ersten Mal war ich dabei als Besucher dort. Nachdem ich circa drei Stunden durch einige wenige Hallen gewandert bin, habe ich mich gegen Mittag zur Standbetreuung zum Ubuntu-Stand begeben um dort meinen [ubuntuusers.de](http://ubuntuusers.de) Kollegen [Torsten Franz](http://torstenfranz.wordpress.com/) auszuhelfen.

Der Stand bestand aus einem kleinen Tisch worauf ein Asus Eee PC platziert war, auf dem die aktuelle Beta 1 von Ubuntu 12.04 Precise Pangolin lief. Insgesamt war der Stand an dem Samstag ziemlich gut besucht, sodass es nur wenige ruhige Minuten gab, in denen kein Besucher an dem Stand stand. Ich selbst hatte bereits Erfahrung an einem Ubuntu-Stand von der OpenRheinRuhr, die im November 2011 stattfand. Im Gegensatz zur [OpenRheinRuhr](http://openrheinruhr.de/) erschienen am Ubuntu-Stand auf der CeBIT auch einige Leute, die generell keine Erfahrung zu Ubuntu hatten. So kamen viele Leute zusammen, die zwar bisher Ubuntu vom Namen her kannten, bisher aber noch gar keinen Kontakt zu Ubuntu beziehungsweise Linux hatten und sich generell darüber informieren wollten, also eben typische Windows Nutzer. Besonders bei diesen Vorstellungen sammelten sich nach und nach eine größere Gruppe von fünf bis sieben Leute am Stand an, die zugehört haben.

Generell überwiegt hingegen die Zahl derer Leute, die Ubuntu nutzen und sich so darüber unterhalten wollten und fragen hatten. Viele wollten die Neuerungen von Precise Pangolin sehen oder hatten generelle Fragen über deren Probleme mit Ubuntu. Die meisten dieser Probleme waren jedoch so speziell, dass sie nicht ohne den besagten PC oder Laptop gelöst werden konnten. So konnte ich auch oft nur den Hinweis auf das tolle Wiki von ubuntuusers.de geben ;-). Neben Fragen und Problemen rund um Ubuntu, gab es es auch eine sehr hohe Nachfrage zu weiteren Informationen zu „Ubuntu for Android”. Da wir lediglich einen Community-Stand hatten, gab es leider von unsere Seite aus keine weiteren Informationen hierzu.

Interessant war auch, dass auch einige ausländische Besucher Fragen auf Englisch gestellt haben. Zu Irritationen kam es hingegen dann, wenn deutsche Besucher mit mir auf Englisch geredet haben, obwohl ich sehr wohl Deutsch kann, und wir es jeweils erst nach einigen wenigen Minuten gemerkt haben. Dieser Fall trat schon das ein oder andere Male auf.

Fazit: Für mich war die CeBIT die erste große Messe, die ich besucht habe, die ich dann auch direkt mit einer Standbetreuung für Ubuntu kombiniert habe. Viel gesehen hab ich von der CeBIT nicht, die Standbetreuung hat aber ziemlichen Spaß gemacht, da man Kontakt zu vielen verschiedenen Menschen hat und auch neue Leute kennen lernt.
