---
title: "Canon-DSLR als Webcam auf Linux-Systemen verwenden"
categories: ["Sonstiges"]
date: 2020-11-22T15:50:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: "https://vg02.met.vgwort.de/na/473f11ffecca41a99d72c27ae32ba910"
---

Ganz unabhängig von der aktuellen Weltlage arbeite ich mittlerweile seit April
2020 in einem All-Remote-Job. Was da jeder braucht, ist eine Webcam. Ich wollte
eigentlich zu Beginn des Jobs eine neue Kamera kaufen. Blöd nur, dass die überall ausverkauft
waren und wenn sie lieferbar waren, dann erst Monate später zu Wucherpreisen.

Für mich musste also kurzfristig eine Alternative her, die sich auch mittelfristig
durchgesetzt hat. Die im Laptop verbaute Webcam ist leider ziemlich unbrauchbar.

Meine Alternative ist die Nutzung meiner DSLR, die ich eigentlich zum
Fotografieren besitze. Konkret besitze ich eine Canon EOS 700D. Diese Anleitung
hier dokumentiert wie man diese Kamera als Webcam unter Linux zum Laufen
bringt. Leider ist das nicht ganz so einfach und hat ein paar Einschränkungen,
aber dazu gleich mehr.

Die Kamera habe ich per Mini-USB an meinen Rechner angeschlossen. Zusätzlich wird
noch eine dauerhafte Stromzufuhr gebraucht. In diversen Online-Shops existieren
Batterie-Adapter, die man per USB mit Strom füttern kann. Dieses Teil kostet
so knapp 20 bis 30 Euro.

Auf der Seite der Software werden prinzipiell drei verschiedene Anwendungen
gebraucht. Die Paketnamen beziehen sich auf ArchLinux, bei anderen Distributionen
sind die Paketnamen aber ähnlich.

- `gphoto2`
- `gstreamer` und `gst-plugins-bad`
- `v4l2loopback-dkms`

Um genau zu sein, wird `gphoto2` gebraucht, um das Kamerabild abzugreifen. Das
muss dann an `gstreamer` gepipet werden, was den Videoinhalt dann in ein Video4Linux2
Loopback-Sink schickt. Anschließend ist die Kamera dann als Webcam verfügbar. Aber
zunächst der Reihe nach.

Nachdem man sich die oben genannten Pakete installiert hat, wird durch die
Installation von `v4l2loopback-dkms` ein Kernel-Modul kompiliert. Dies wird
benötigt, damit man V4L2 Loopback Devices nutzen kann. Dies muss anschließend
auch noch geladen werden:

    $ sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="Canon 700D" exclusive_caps=1

An dieser Stelle habe ich nicht nur das reine Modul geladen, sondern habe noch ein
paar Optionen mitgegeben. `devices` gibt lediglich an, dass es sich um ein einzelnes Gerät
handelt. Mit `video_nr=10` gebe ich an, dass das Gerät unter `/dev/video10`
im System verfügbar ist. Mit `card_label` gebe ich hingegen schlicht den Namen
an. Wichtig ist noch der Parameter `exclusive_caps=1`: Ohne diesen Parameter
erkennt der Chrome die Kamera nicht als Webcam. Warum das so ist, hab ich nicht
ganz verstanden, ist mir aber egal, solange es funktioniert. Im Firefox und in
Zoom funktioniert das auch ohne diesen Parameter.

Nachdem also das Kernel-Modul geladen ist, muss ich folgenden Befehl verwenden,
um das Gebastel zum Laufen zu kriegen:

    $ gphoto2 --stdout --capture-movie | gst-launch-1.0 fdsrc ! decodebin3 name=dec ! queue ! videoconvert ! v4l2sink device=/dev/video10

Ein Nachteil dieser Methode ist, dass `gphoto2` von der Kamera per USB nur das
Preview-Bild holt und es dem Rechner rüberreicht. Das Preview-Bild ist das Bild,
was auch auf dem eingebauten Display dargestellt wird. Das heißt in meinem Fall,
dass das kein HD-Bild ist, sondern nur eine Auflösung von 960x640 hat. Für den
Einsatz als Webcam ist das allerdings nicht weiter schlimm, da das Bild immer
noch besser ist als bei einer Standard-Full-HD-Webcam. Meistens findet dann
sowieso noch ein Herunterrechnen des Bildes statt.

Eine Alternative zu dieser Methode wäre die Nutzung einer HDMI-Capture-Card
die das Signal vom HDMI-Ausgang in voller Auflösung abgreift. Da gescheite
HDMI-Capture-Cards über 100 € kosten, hab ich getrost darauf verzichtet.

Um meinen Workflow ein wenig zu vereinfachen, lade ich das Kernel-Modul
beim jeden Reboot automatisch und hab einen Alias gesetzt, um den langen Befehl
schnell aufrufen zu müssen. Einfaches Plug-and-Play ist das nicht, komfortabel
auch nicht. Aber es war eine einfache Lösung um den Kauf einer überteuerten
und nicht-lieferbaren Webcam zu umgehen, da der Rest der Hardware schon vorhanden
war. In der Dokumentation von gphoto auf [gphoto.org](http://www.gphoto.org/doc/remote/)
sind alle Kamera-Modelle aufgelistet, die unterstützt werden.
