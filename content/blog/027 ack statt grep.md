---
title: "Suchen und Finden mit Ack, statt grep"
categories: ["Shell"]
date: "2014-04-27 09:30:00+02:00"
toc: true
---

Nutzer der Kommandozeile suchen häufig nach Suchmustern in Dateien. Das gängigste
Tool ist wohl grep. Mit „ack“ gibt es hingegen eine Alternative die besonders
für Programmierer geeignet ist.

Ack ist komplett in Perl geschrieben und nutzt grundsätzlich den Funktionsumfang
der Regulären Ausdrücke von Perl. Der Schwerpunkt von ack liegt in der Optimierung
für Programmierer, denn ack beachtet im Standard bereits schon einige Dinge
um nicht unnötige Verzeichnisse zu durchsuchen.

Die Anwendung ack ist in den gängigen Paketquellen der Linux-Distributionen
enthalten. Unter Fedora oder ArchLinux heißt das Paket schlicht „ack“, in
Debian-basierten Distributionen hingegen „ack-grep“.

## Ack nutzen

Ack sucht im Standard rekursiv. Als Nutzer muss man also, nicht wie
bei grep, ein "-r" für rekursives Suchen mit angeben. Die Funktionsweise
von Ack wird hier an Hand diesen [Repositories](https://github.com/svijee/taskwarrior-androidapp)
deutlich gemacht.

Wenn man nun beispielsweise in einem Quellcode nach „Database“ suchen will,
reicht folgender Befehl:

    ack Database

![Suche nach Database mit grep (links) und ack (rechts)](/graphics/ack_grep.webp)

Ack sucht rekursiv nach dem String „Database“. Das Ergebnis wird dabei mit
Hilfe von verschiedenen Farben hervorgehoben. Sichtbar sind drei Dinge:
Dateiname, Zeilennummer, sowie Zeilen-Inhalt mit hervorgehobenen Suchbegriff.
Im Gegensatz zu grep ist die Standard-Ausgabe von ack deutlich besser lesbar
als die von grep.

Weiterhin dürfte einigen noch ein Detail auffallen: Bei der Suche mit grep
werden Suchergebnisse auch aus dem ".git"-Unterverzeichnis gefunden, was bei
ack nicht der Fall ist. Ack sucht nämlich nicht in Verzeichnissen von
Versionsverwaltungssystemen. So werden unter anderem ".git" oder ".svn" Verzeichnisse
nicht durchsucht, da man schließlich in echten Dateien suchen will und nicht
in Repository-Dateien. Ebenfalls durchsucht ack keine Backup-Dateien, wie etwa
"beispiel~" oder Binärdateien. Die Macher von ack begründen dies um falsche
Such-Treffer zu verhindern bzw. auszublenden.

Mein Beispiel-Repository ist ein Android-Projekt, in dem nicht nur Java-Code
enthalten ist, sondern auch XML-Dateien und einige Binärdateien und Java-Klassen
die während der Entwicklung automatisch generiert werden. Mit ack ist es auch
sehr einfach möglich bestimmte Suchbegriffe auf einzelne Programmiersprachen
zu beschränken.

Wenn man nun nach „Database“ nur in Java-Dateien suchen will, kann man einfach

    ack --java Database

ausführen. Wenn man hingegen nur in XML-Dateien suchen möchte, geht auch
folgender Befehl:

    ack --xml Database

Besonders in größeren Software-Projekten ist dies praktisch, besonders dann
wenn mehrere verschiedene Programmiersprachen zum Einsatz kommen. Die Dateityp-basierte
Suche lässt sich ebenfalls umdrehen, sodass man beispielsweise explizit nicht
in XML-Dateien suchen möchte.

    ack --noxml Database

Ack nutzt in dieser Funktion nicht nur die Datei-Endung der einzelnen Dateien
in der Ordner-Struktur, sondern prüft auch die [Shebang-Zeile](https://de.wikipedia.org/wiki/Shebang),
so dass auch Dateien oder Skripte ohne Dateiendung aber mit Shebang-Zeile
richtig erkannt und durchsucht werden.

Weiterhin unterstützt ack die alt-bekannten Kommandozeilen Optionen von grep,
wie etwa `-i` für case-insensitive Suchen oder `-v` für eine invertierte Suche.

Ack besitzt noch zahlreiche weitere Optionen, die sich in der [Dokumentation](http://beyondgrep.com/documentation/)
nachlesen lassen. Programmierer dürften ack mit dessen Funktion durchaus
einiges abgewinnen, da es schnell sucht und übersichtliche Ergebnisse liefert.
Nichtsdestotrotz ist und bleibt grep ein Tool, welches man weiterhin nutzen
kann, da es schließlich auf jedem unixoiden Betriebssystem im Standard vorhanden
ist.
