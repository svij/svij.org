---
title: Freie Webanalytik mit Piwik
categories: ["Web"]
tags: ["Web"]
date: "2011-08-09T21:00:00+02:00"
toc: true
---

Eine freie Webanalytik-Software, die in PHP geschrieben ist und MySQL als Datenbank nutzt, ist Piwik. Mitte Juni wurde Piwik in Version 1.5 veröffentlicht. Die Software ist unter der GPL v3 lizenziert und soll eine freie Alternative zu Google Analytics darstellen.

<strong>Hinweis</strong>: Der Artikel erschien bereits in der [August Ausgabe 2011](http://www.freiesmagazin.de/20110807-augustausgabe-erschienen) von [freiesMagazin](http://freiesMagazin.de).

### Allgemeines
Eines der wichtigen Instrumente für einen Webseitenbetreiber ist die Webanalytik. Die Bekannteste Webanalytik-Software kommt aus dem Hause Google. Google Analytics bietet die umfangreichste Webanalytik auf dem Markt und ist daher auch sehr weit verbreitet. Der große Nachteil von Google Analytics ist, dass alle Daten direkt an die Google Server übermittelt und auch dort verarbeitet werden. Eine freie Alternativsoftware wie Piwik, ermöglicht ebenfalls eine gute Webanalytik. Durch Webanalytik-Software ist es möglich das Verhalten der Webseitenbesucher zu analysieren. Die Software zeichnet viele Statistiken eines Webseitenbesuchers auf. Neben der Besuchshäufigkeit und Besuchsdauer der eigenen Webseite wird auch die IP-Adresse und Herkunft des Benutzers gespeichert. Einen Einblick in die Analysemöglichkeiten von Piwik soll dieser Artikel geben.
Piwik erzeugt aus den gespeicherten Daten sehr viele Berichte, die in verschiedene Kategorien eingeordnet werden. Die Daten werden für jeden einzelnen Besucher erhoben, die je nach Statistik zusammen oder getrennt aufgelistet werden.

Piwik nutzt wie andere Webanalytik-Software auch einen JavaScript-Code zum Aufzeichnen des Verhaltens der Webseitenbesucher. Der JavaScript-Code muss auf jeder einzelnen Seite eines Webauftritts eingebunden sein, damit alle Daten ordnungsgemäß und vollständig erhoben werden können. In den meisten Content-Management-Systemen lässt sich dieser Code sehr einfach in das Template des Footer einbinden. Es wird empfohlen den Standard JavaScript-Code zu verwenden, es ist jedoch auch möglich den Code anzupassen. So lassen sich einige Funktionen ein- bzw. ausschalten. Eine Alternative für die Benutzer, die JavaScript im Browser deaktiviert haben, gibt es ebenfalls. Piwik realisiert dieses mit einen Image Tracker Code, der logischerweise kein JavaScript verwendet. Nachteilig ist hierbei jedoch, dass einige wichtige Funktionen nicht aufgezeichnet werden können. Darunter fallen unter anderem Suchschlüsselwörter, Browser-Plugins und Bildschirmauflösungen.

##### Installation & Updates
Die Installation von Piwik lässt sich zügig in etwa fünf Minuten erledigen und erfolgt ziemlich ähnlich der Installation von diversen Content-Management-Systemen. Verfügbare Aktualisierungen erscheinen im Administrationsmenü von Piwik. Diese lassen sich mit wenigen Klicks direkt im Browser installieren.

##### Übersicht

![Piwik Übersicht](/graphics/piwik-uebersicht.webp)

Piwik unterteilt die Bedienoberfläche in die Seiten „Besucher“, „Aktionen“, „Verweise“ und „Ziele“. Auf jeder Unterseite lassen sich die Daten der überwachten Webseite für einen speziell ausgewählten Zeitraum erfassen. So kann man die Statistiken zu jeden einzelnen Tag, Monat, und Jahr getrennt aufrufen. Neben diesen vordefinierten Zeiträumen, kann man ebenfalls seinen eigenen Zeitraum definieren.
Die Übersichtsseite, die in drei Spalten gegliedert ist, in denen die wichtigsten Widges liegen, bildet gleichzeitig auch die Startseite von Piwik. Standardmäßig wird dort unter anderem die Statistiken zu den letzten Besuchern, Referrer-Webseiten und die Besucher-Browser angezeigt. Der Administrator kann die Widgets  komplett nach seinen eigenen Belieben anpassen. So ist es möglich vorhandene Widgets zu löschen oder neue hinzufügen, außerdem lassen sie sich per Drag and Drop frei verschieben und neu anordnen.

##### Besucher

Piwik zeichnet sehr viele Informationen zu jeden einzelnen Besucher auf. Auf der Besucher-Übersichtsseite lässt sich die Benutzerentwicklung nachvollziehen und es werden des Weiteren noch Zahlen zur durchschnittlichen Aufenthaltsdauer, Absprungrate der Benutzer oder auch die Besucheranzahl angezeigt.

Das Besucher-Log zeigt genaue Informationen für jeden Besucher an. Als Beispiel dient hier ein Besucher einer Webseite, der die Fotogalerie aufgerufen hat. Neben der IP-Adresse, die hier anonymisiert gespeichert ist, wird auch die sekundengenaue Uhrzeit angezeigt, zu der der Besucher die Seite besucht hat. Sofern sich aus der IP Adresse der Provider herauslesen lässt, wird auch der Provider angezeigt, welches in diesem Beispiel Citykom sein soll. Weiterhin geht aus der Tabelle hervor, dass der Besucher aus Deutschland kommt, Windows als Betriebssystem verwendet und den Internet Explorer 9.0 als Browser nutzt. Piwik erkennt an Hand von einem abgespeicherten Cookie, ob der Besucher die Webseite bereits besucht hat, oder ob es ein neuer Besucher ist, und markiert dies in den Statistiken in Form eines Symbols. Ebenso werden die verwendeten Browser-Plugins, wie Flash, PDF, Java oder Quicktime, erfasst und ausgewertet.Einzig die Plugins von Besucher die den Internet Explorer verwenden, werden, wie hier im Beispiel,
 nicht gespeichert. Nur die Verwendung von Java wird ausgegeben. Weitere Informationen etwa über die Version des Betriebssystems, der verwendeten Version des Browsers und der genutzten Auflösung wird in einem Mouse-Over über den dazugehörenden Symbolen angezeigt.
 
![Piwik Besucherlog](/graphics/piwik-besucherlog.webp)

Darüber hinaus merkt sich Piwik auch die „Herkunft“ des Besuchers, sofern die Referrer-Seite ermittelt werden konnte, ansonsten zeigt Piwik an, dass der Besucher direkt auf die Seite zugegriffen hat. Ergänzend ist es mit Piwik auch möglich zu sehen was der Besucher genau auf Webseite getan hat, welche Unterseiten er besucht hat, und wie lang er sich insgesamt auf der gesamten Webseite aufgehalten hat. Zudem werden auch Downloads von beispielsweise Bildern aufgezeichnet.
Die weiteren Unterkategorien von „Besucher“ fassen einzelne Informationen thematisch zusammen. Unter „Standorte und Provider“ werden Graphen und Zahlen von Besuchern sortiert in Kontinente, Länder und Provider dargestellt. Im weiteren Punkt „Einstellungen“ werden die verwendeten Browser und PlugIns aufgelistet, als auch die Betriebssysteme und die Bildschirmauflösungen. Das Datenmaterial wird teilweise in Kreisdiagrammen angezeigt, aber auch in Listen mit absoluten Werten und stellenweise Prozentwerte. Die Anzeigemethoden sind jedoch nicht fest definiert. Der Anwender kann nach eigenen Belieben zwischen den verschiedenen Ausgabeoptionen wie Tabellen und Diagrammen wechseln.
Neben diesen Informationen werden, wie bereits erwähnt auch die Zeiten  gespeichert, die ebenfalls nochmal getrennt in einem Diagramm visualisiert sind. Unter der Unterkategorie „Engagement“ wird unter anderem die Durchschnittliche Aufenthaltsdauer auf der Webseite dargestellt, als auch die Menge der wiederkehrenden Besucher.
Nützlich sind besonders die zahlreichen Exportoptionen von Piwik. Sie ermöglicht das Exportieren eines Datensatzes als CSV, TSV, XML als auch in Json, PHP und RSS.
Neben diesen Informationen werden, wie bereits erwähnt auch die Zeiten gespeichert, die ebenfalls nochmal getrennt in einem Diagramm visualisiert sind. Unter der Unterkategorie „Engagement“ wird unter anderem die Durchschnittliche Aufenthaltsdauer auf der Webseite dargestellt, als auch die Menge der wiederkehrenden Besucher.

#### Aktionen
In die Kategorie „Aktionen“ fallen alle Daten über die einzelnen Seiten. Unterteilt wird die Kategorie in „Seiten“, „Eingangsseiten“, „Ausstiegsseiten“, „Seitentitel“, „Ausgehende Verweise“, und „Downloads“. In der Unterkategorie „Seiten“ wird unter anderem dargestellt welche Teile der Webseite die meisten Seitenansichten hat, wie hoch die Absprung- und Ausstiegsrate ist, als auch die durchschnittliche Verweildauer  auf der Seite. In „Eingangs-“ und „Ausstiegsseiten“ wird nun differenziert dargestellt, welche Einzelseite die Besucher zuerst öffnen, und wiederum bei welcher Einzelseite sie die Webseite verlassen haben. Während bei „Seiten“ die Daten nach der Struktur und den Dateinamen einzelnen Seiten geordnet sind, werden bei „Seitentitel“ die Titel der Einzelseiten aufgezählt und mit den gleichen Fakten veranschaulicht.
Mit den aufgezeichneten Daten unter „Ausgehende Verweise“ kann der Webseitenbetreiber leicht erkennen an welchen Links die Besucher am häufigsten die Seite verlassen. Der letzte Unterpunkt unter „Aktionen“ ist der Bereich „Downloads“. Dort wird ziemlich simpel aufgelistet welches Datenmaterial und wie oft heruntergeladen wurde.

#### Verweise
Die Kategorie „Verweise“ wertet hauptsächlich die Herkunft der Besucher aus. Unterteilt in „Suchmaschinen und Suchbegriffe“ sammelt Piwik die Anzahl der Besuche unterteilt in den verschiedenen Suchmaschinen, zusätzlich wird auch noch erfasst, welche Suchbegriffe die Besucher verwendet haben, um auf die Webseite zu gelangen. Neben der Herkunft der Besucher von Suchmaschinen kommen diese ebenfalls von anderen Webseiten. Unter „Webseiten“ wird hier aufgezählt welche Seiten zum besonderen Maße die Besucher auf seine eigene Seite weiterleitet.

#### Ziele
Eigene Statistiken lassen sich unter „Ziele“ erstellen. Der Webseitenbetreiber kann so beispielsweise ein Ziel für Registrierung von neuen Benutzern erstellen oder die Aufrufanzahl von „Bitte Lesen“ Posts verfolgen.

#### Weiteres
Piwik bietet neben dem Webauftritt auch die Möglichkeit der E-Mail Benachrichtigung an. Die Berichte lassen sich individuell gestalten und an mehrere Leute senden. Die einzelnen Datensätze lassen sich aktivieren und deaktivieren. So ist es möglich nur Teile der Daten an weitere Personen zu senden, während der Administrator selbst eine E-Mail mit vollständigen Datensätzen bekommt.
Eigene Funktionen lassen sich mit Hilfe der API realisieren. Die bereits enthaltenen Funktionen sind als Plugins in Piwik eingebunden. In den Plugin-Einstellungen ist es möglich einige der Funktionen ab- und an zuschalten, so dass man nur einiges speichern lässt.

### Datenschutz
Ein weiterer Punkt den man bei der Webanalytik nicht außer Acht lassen darf, ist der Datenschutz. Problematisch ist hierbei die Erfassung Personen spezifischer Daten wie beispielsweise die IP-Adresse womit eine eindeutige Identifizierung möglich ist.
Piwik arbeitet aus diesem Grund mit dem unabhängigen Landeszentrum für [Datenschutz](http://de.piwik.org/blog/2011/03/unabhangiges-landeszentrum-datenschutz-uld-piwik-datenschutzkonform-einsetzbar/) zusammen. Die angeregten Verbesserungen wurden bereits in der Version 1.2 eingepflegt. Das ULD hat ein Dokument veröffentlicht in welchem genannt werden, wie man Piwik nutzen datenschutzrechtlich einwandfrei nutzen kann. Darunter fallen unter anderem die Anonymisation von IP Adressen als auch die OptOut-Funktion.
Die OptOut-Funktion ermöglicht es die Besucher entscheiden zu lassen, ob Piwik seine Daten speichern darf oder nicht. Standardmäßig werden die Daten jedoch automatisch gespeichert. Der Admin hat die Möglichkeit mittels eines iframes dem Besucher die Möglichkeit zu geben die Speicherung seiner Daten zu widersprechen. Durch die Deaktivierung seitens des Besuchers wird dann der eindeutige Webanalyse-Cookie deaktiviert.

![Piwik Android App](/graphics/piwik-androidapp.webp)

### Piwik Mobile

Eine Mobile-Seite für den Abruf der Statistiken von einem Smartphone oder Tablet gibt es bisher nicht. Stattdessen gibt es ein offizielles und kostenloses App unter dem Namen „Piwik Mobile“ für Android und iOS, welches ebenfalls unter der GPL lizenziert ist. Die einzelnen Statistiken sind in Gruppen zusammengefasst und in einer Liste geordnet. Sie bietet ziemlich den gleichen Funktionsumfang wie auch die eigentliche Webseite an sich. Jeder Unterpunkt der gesammelten Informationen wird in einem Kreisdiagramm dargestellt, des Weiteren werden auch die absoluten Zahlen in einer Tabelle angezeigt. In der oberen Leiste gibt es neben der Möglichkeit den Zeitraum individuell umzustellen und somit eine veränderte Statistik abzurufen, auch noch die Möglichkeit zwischen mehreren Piwik-Datenbanken zu wechseln.

### Geschichte von Piwik
Piwik entstand aus der Grundlage von phpMyVisites. Nach der Einstellung des Projekts phpMyVisites wurde daraus Piwik. Die Version 0.1 wurde im März 2009 veröffentlicht und seitdem stetig weiterentwickelt. August 2010 erschien Piwik in Version 1.0, danach folgten zügig weitere Veröffentlichungen. Die aktuelle Version 1.5 wurde Mitte Juni veröffentlicht. Piwik wurde mittlerweile über 675.000 mal heruntergeladen und wird von zahlreichen Webseiten Webseitenbetreibern genutzt.

### Neues in Piwik 1.5
Mit der neuen Version von Piwik kamen neben Fehlerkorrekturen und weiteren kleineren Änderungen auch neue Funktionen. Als neue Funktion wurde unter anderem [E-Commerce](http://piwik.org/docs/ecommerce-analytics/2) und [benutzerdefinierte Variablen](http://piwik.org/docs/custom-variables/) eingeführt. Des Weiteren wurde die Nutzung von Flash für die Anzeige von Graphen abgeschafft. Die Graphen werden nun mittels einer Kombination aus Canvas und JavaScript erzeugt. Einzig für die Weltkarte der Besucher wird noch Flash benötigt.

### Ausblick
Nach der [Roadmap](http://piwik.org/roadmap/) ist es die Mission von Piwik eine führende Open Source Webanalytik-Software zu entwickeln, die den Zugang zur gesamten Funktionalität durch eine offene API und offenen Komponenten gewährleisten soll. Unter anderem ist geplant, dass man benutzerdefinierte Benachrichtigungen auf Aktionen erstellt, so dass der Administrator jedes Mal via E-Mail benachrichtigt wird, wenn dieser Fall eintritt. Des Weiteren soll auch noch die Länder und Städte Lokalisation mittels der GeoIP Datenbank eingebunden werden. Alle Weiteren Funktionen die bis zur Version 2.0 eingefügt werden sollen, kann man in der Roadmap nach lesen.
