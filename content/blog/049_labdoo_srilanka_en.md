---
title: "Labdoo Laptop-Donation to a school in Sri Lanka"
categories: ["Sonstiges"]
date: "2015-02-05 20:45:00+02:00"
tags: ["ubuntu-planet"]
toc: true
lang: "en"
---

Labdoo is an international project which focuses to bring used laptops to
schools in developing countries where those laptops can be used by the pupils.

### Labdoo

Every year many people are buying new laptops. This is especially the case
in industrialized countries. Consequently, there are also many still working
laptops which have been used for a couple of years, but no one uses them anymore
because they are too slow. At the same time there are many countries
where young people don't have access to computers and the internet for
educational purposes.

Labdoo is a project which aims to collect those used laptops and brings it to
educational institution, like schools, in developing countries.

The laptops are collected in so called „hubs“.
In those „hubs“ the laptops get prepared, e.g. defective parts are replaced and
Edubuntu with a collection of educational software, such as an offline wikipedia
and learning software, are installed.

So for example if there are some defect parts, they got replaced and
all laptops get Edubuntu with a collection of educational Software, like an Offline-Wikipedia
and Learning-Software.

The delivery of the laptops to a school is done by volunteerslike all the other work, too.
There are many people travelling around the world. Costs are covered by the travelling
person, but usually it comes down to just needing some available space in your
luggage, which you tend to have. For the Labdoo project it is important that the
delivery is as friendly to the environment as possible, thus taking laptops along
with your luggage is preferred to sending parcels. Also by delivering these
laptops in person you might be able to see happy children.

If you have a laptop you can contact one of the [Labdoo-Hubs](http://labdoo.org/hubs).
Most of these hubs are located in Germany, Switzerland and Austria, but there
are also a few hubs in Spain, USA, UK and a couple other countries. You can donate
Laptops (at least Pentium 4), eBook Readers, Tablets and other IT-Devices or parts
such as network-hardware, RAM, keyboards, mouses and so on.

Everyone who donates a laptop gets regular updates about the current status via
E-Mail. You e.g. get a notification if the laptop is ready for delivery, on the
way to a school or successfully delivered and in use at a school. On the website
you can view photos of the delivery, so you can be sure that your donated laptop arrived at a school.

### My Labdoo-Cycle

Personally I heard about Labdoo back in 2012 in an [article on ubuntuusers.de](http://ubuntuusers.de/ikhaya/2787/).
Additionally I talked to Ralf Hamm, a highly involved person in Germany,
at the OpenRheinRuhr in Oberhausen in 2013 and 2014. There he got me motivated to
help the project.

My parents are from Sri Lanka and we were planning a trip to Sri Lanka anyway.
This was a perfect situation to contribute to the Labdoo project. I decided to
do as much as possible by my own.

#### Step 1: Get some laptops

Firstly I needed a couple of laptops which I could bring to a school in Sri Lanka. This
was the easiest part because I knew that I had an old laptop in my office.
Therefore I asked my employer, the [otris software AG](http://otris.de) from Dortmund,
whether we could donate a few used laptops. This got accepted easily, so my employer
donated two laptops. All companies who donated laptops are listed on a
[donation page](http://www.labdoo.org/de/content/spender) on the website of the Labdoo project.

#### Step 2: Find a school

Interestingly enough this was the hardest part. Many schools in Sri Lanka in or around bigger
cities already had computers, because the government are slowly rolling out funds
for computers. At this step I got help from my mother, who supported and
likes my idea. After a few weeks of making many phone calls, my mother finally
found a school who really needed Laptops.

The school „Thadchanamaruthamadhu G.T.M.S“ is far away from the bigger cities
in Sri Lanka. It is located roughly 100km air-line distance from Jaffna, in
the northern province of Sri Lanka. The school is located in the small town Madhu,
which got visited by Pope Francis a few days before I went there.

#### Step 3: Preparing the laptops

Preparing the laptops wasn't hard. The Installation is image-based, so I simply
had to download the image from the website and used CloneZilla to install the
Edubuntu system.

#### Step 4: Transport to Sri Lanka

The fourth step is the transfer to Sri Lanka. I securely packed the laptops in
our luggage. The weight of the laptops and the docking-station was roughly 8kg.

#### Step 5: Transport to the school

The delivery of the laptops took nearly twelve hours. In the early hours of the
day we started our trip at 7 am in Jaffna, after a few sight-seeing stops on the
way we arrived at the school at midday. The journey was long and stony. Many streets
in the area of the school didn't have a solid pavement. It was also laced with
road holes. You could say that the school was „in the middle of nowhere“. The
way to school was also long for the pupils, because there weren't many houses
around the school.

<a href="/graphics/labdoo_way_to_school.webp" data-lightbox="image" data-title="The way to the school.">
	<img src="/graphics/labdoo_way_to_school.webp" style="margin:20px;">
</a>

After we arrived at the school, we were welcomed by the principal. After a short
introduction we prepared the handover of the laptops. To my surprise the school
made a great effort to say thanks. The whole school met at the schoolyard.
This included roughly 300 pupils and 20 teachers. There a teacher and the
principal thanked us for the laptops. After that, the principal wanted us to have
a word wth the pupils. Due to the basic knowledge of the Tamil language, my dad held
a short speech. At the end the pupils thanked us with a "Thank you, Sir". After
that I gave the science teacher a short introduction to Ubuntu.

<a href="/graphics/labdoo_meetup.webp" data-lightbox="image" data-title="Meetup.">
	<img src="/graphics/labdoo_meetup.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_handover.webp" data-lightbox="image" data-title="My sister and me handing over the laptops.">
	<img src="/graphics/labdoo_handover.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_pupils.webp" data-lightbox="image" data-title="The pupils.">
	<img src="/graphics/labdoo_pupils.webp" style="margin:20px;">
</a>

### Conclusion

<a href="/graphics/labdoo_classroom.webp" data-lightbox="image" data-title="The classroom.">
	<img src="/graphics/labdoo_classroom.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_using.webp" data-lightbox="image" data-title="The pupils having fun using the laptop.">
	<img src="/graphics/labdoo_using.webp" style="margin:20px;">
</a>


The donation and the delivery of the laptops were really interesting. I really
want to encourage everybody to help the Labdoo project in any way possible. Everyone
who has an old, unused laptop can donate it. Compared to the „One Laptop
Per Child“ project, Labdoo focuses on old and already used laptops instead of
producing new laptops. It was really cool to see all these happy pupils at the school.
I will try to continue to support the Labdoo project personally. 
One part where everybody can help can help the project is spreading the word.
Don't throw away old laptops, donate them! This is not only important for techy-people,
it is also good to know for non-techy people.
