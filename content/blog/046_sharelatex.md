---
title: "Kollaborativ an LaTeX-Dokumenten arbeiten mit ShareLaTeX"
categories: ["Server"]
date: "2015-01-11 13:45:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Die Web-Anwendung ShareLaTeX ermöglicht das kollaborative Arbeiten an LaTeX-Dokumenten
in einem Team. Die Oberfläche läuft vollständig im Browser und die Dokumente lassen sich
sowohl im Browser bearbeiten, kompilieren als auch anzeigen.

### Allgemeines

ShareLaTeX begann ihren Dienst zunächst ausschließlich über ihre Webseite
[ShareLaTeX.com](https://www.sharelatex.com/) anzubieten. Es folgt dem „Freemium“
Prinzip, sodass man auf der Webseite zwar LaTeX-Projekte anlegen kann, diese
in der kostenfreien Variante allerdings an einigen Funktionen beschnitten ist.
Gegen Zahlung kann man sich hingegen ein Premium-Account mieten, in dem man
dann mehr Funktionen nutzen kann.

Anfang 2014 stellten die Entwickler die Software von ShareLaTeX unter der AGPL v3
Lizenz, sodass man es auch auf seinen eigenen Server installieren kann. ShareLaTeX
ist in [CoffeeScript](https://de.wikipedia.org/wiki/CoffeeScript) geschrieben und
läuft auf [node.js](http://nodejs.org/). Als Datenbank wird [Redis](https://de.wikipedia.org/wiki/Redis)
und [MongoDB](https://de.wikipedia.org/wiki/MongoDB) verwendet.

### Installation unter Ubuntu 14.04

Leider ist die Installation von ShareLaTeX auf dem eigenen Server nicht ganz
so einfach. Die Dokumentation im Wiki des [ShareLaTeX-Repositorys](https://github.com/sharelatex/sharelatex/wiki)
ist zum Beispiel nur die Installation mittels eines Deb-Pakets unter Ubuntu 12.04
oder die Installation unter ArchLinux beschrieben. Außerdem wird in der [offiziellen](https://github.com/sharelatex/sharelatex/wiki/Production-Installation-Instructions)
Installations-Anleitung die Installation von Fremd-Quellen vorgeschlagen, welches
man durchaus vermeiden kann.

Voraussetzung für diese Installations-Anleitung ist ein System auf dem Ubuntu
14.04 läuft. Wichtig ist, dass der Server möglichst mindestens 1GB an Arbeitsspeicher
hat, sofern auf dem Server ausschließlich ShareLaTeX laufen soll. Die Installation
auf einem Server mit 512MB Arbeitsspeicher läuft zwar durch, allerdings kann es durchaus passieren,
dass man sich hinterher nicht mehr per SSH verbinden kann, da der Arbeitsspeicher
vollständig gefüllt ist.

Zu Beginn muss man erst die Paket-Quellen aktualisieren und dann einige Pakete
installieren die benötigt werden, um die Installation durchzuführen.

    $ sudo apt-get update
    $ sudo apt-get install git build-essential curl python-software-properties zlib1g-dev zip unzip ruby-dev

Für den Produktiv-Betrieb werden, wie oben schon kurz erwähnt, node.js, redis
und mongodb benötigt

    $ sudo apt-get install nodejs npm redis-server mongodb

ShareLaTeX benötigt schließlich auch noch die LaTeX-Pakete. Diese können
entweder über die offizielle Anleitung der TeX Live Distribution(https://www.tug.org/texlive/quickinstall.html)
erfolgen, alternativ kann man auch die TeX Live Pakete von Ubuntu installieren:

    $ sudo apt-get install texlive latexmk texlive-lang-german texlive-latex-extra

Sofern man weitere spezielle LaTeX-Pakete braucht, kann man diese natürlich je
nach Verwendungszweck problemlos nachinstallieren. Problematisch über die
Paket-Verwaltung könnte sich erweisen, dass ShareLaTeX das Paket latexmk
mindestens in der Version 4.39 benötigt. In den Paket-Quellen von Ubuntu ist
latexmk allerdings nur in Version 4.35 enthalten. Bei der Arbeit an nicht
allzu komplexen Dokumenten erwies sich dies im Nachhinein allerdings nicht als
ein Problem.

Nichtsdestotrotz werden trotzdem einige externe Pakete benötigt, die mittels
npm und gem installiert werden müssen:

    $ sudo  npm install -g grunt-cli
    $ gem install fpm

Anschließend liegt Arbeitsverzeichnis ein "tmp"-Ordner, der problemlos gelöscht
werden darf. Danach muss man das Git-Repository clonen, zu Beachten ist, dass
man den Release-Branch nutzt und nicht den Master-Branch.

    $ git clone -b release https://github.com/sharelatex/sharelatex.git

Im Anschluss müssen einige Pakete über den Paketmanager von node.js npm installiert,
sowie letztendlich die ShareLaTeX-Dienste installiert werden.

    $ npm install
    $ grunt install

Beide Befehle dauern relativ lange, je nach verfügbaren Bandbreite dauert der
Download seine Zeit.

Nachfolgend muss noch mit dem Tool grunt geprüft werden, ob alles erfolgreich
durchgelaufen ist:

    $ grunt check --force

Der Parameter "--force" ist notwendig, da grunt an dieser Stelle feststellt,
dass das latexmk Paket veraltet ist, sofern man es sich wie oben beschrieben
aus den Ubuntu Paketquellen installiert hat. Danach fehlt lediglich das
Bauen des Deb-Paketes. Allerdings muss hierfür die Datei "Gruntfile.coffee"
händisch angepasst werden. Konkret findet sich im Quellcode dieser Datei die
Zeile:

      "--depends", "mongodb-org > 2.4.0"

Dort muss "mongodb-org" durch "mongodb" ersetzt werden, da das Paket von mongodb
unter Ubuntu eben mongodb heißt und nicht mongodb-org. Die Zeile sollte im Anschluss
so aussehen:

      "--depends", "mongodb > 2.4.0"

Jetzt kann das Deb-Paket mittels grunt gebaut werden und anschließend installiert
werden.

    $ grunt build:deb
    $ sudo dpkg -i sharelatex_0.0.1_amd64.deb

Nach der Installation ist ShareLaTeX lauffähig, und kann unter der IP des Servers bzw.
der Domain aufgerufen werden. Allerdings läuft es im Standard unter Port 3000. Man
muss daher http://IP_des_servers:3000 aufrufen, um auf die Web-Oberfläche von 
ShareLaTeX zu gelangen. Um ShareLaTeX unter Port 80 und somit ohne Port-Angabe
im Browser erreichen zu können, kann man den Webserver nginx installieren, der
als Reverse-Proxy dient. Danach muss man lediglich das Laden der Standard-Konfigurationsdatei
von nginx unter "/etc/nginx/sites-enabled/default" unterbinden und nginx
die die Konfigurationsdateien neuladen lassen.

    $ sudo apt-get install nginx
    $ sudo rm /etc/nginx/sites-enabled/default
    $ sudo service nginx reload

### Die Basis-Einrichtung

Nach dem Abschluss der Installation kann man mit der Nutzung beginnen. Ein
normales Benutzerkonto kann man direkt erstellen ohne, dass man seine E-Mail
Adresse bestätigen muss. Wer jetzt allerdings eine Admininistrations-Oberfläche
sucht, der wird erstmal nicht fündig. Administrator-Rechte kann man lediglich
über die Datenbank vergeben. Dazu muss man folgendes in der Kommandozeile
ausführen:

    $ mongo
    MongoDB shell version: 2.4.9
    connecting to: test
    > use sharelatex
    switched to db sharelatex
    > db.users.update((email:"mail@svij.org"), ("$set":(isAdmin:true)))

Im letzten Befehl muss da natürlich die eigene E-Mail Adresse angegeben werden,
um sich selbst die Rechte zu geben. Anschließend kann man unter http://IP_des_servers/admin
auf die Admin-Funktionen zugreifen.

Außerdem kann man noch den Versand von E-Mails konfigurieren. Hierfür kann man
sich an die Anleitung von ShareLaTeX [selbst](https://github.com/sharelatex/sharelatex/wiki/Configuring-SMTP-Email)
halten.

### Was kann es?

Nachdem man sich ein Benutzerkonto erstellt und eingeloggt hat, bekommt man
eine Übersicht über seine angelegten Projekte. Zu Beginn sind da natürlich keine,
man kann sich allerdings sehr einfach ein Projekt anlegen. Man kann entweder 
ein vollständig leeres Projekt angelegen, ein Beispiel-Projekt erstellen lassen
oder ein bereits existentes Projekt als Zip-Paket hochladen.

<a href="/graphics/sharelatex_login.webp" data-lightbox="image" data-title="Eine Übersicht über die angelegten Projekte.">
    <img src="/graphics/sharelatex_login.webp" style="margin:20px;">
</a>

Wenn man nun ein Projekt angelegt hat, kann man dies öffnen und man sieht eine
drei geteilt Übersicht. Auf der linken Seite findet sich eine schmale Spalte
in welchem der Verzeichnisbaum des Projektes angezeigt wird. Dort kann man
alle vorhanden Dateien ansehen, umbenennen, hochladen oder auch löschen. Das
gleiche gilt für Unter-Ordner.

In der mittleren Spalte wird der Quellcode der geöffneten Datei angezeigt, welches
in der Regel die ".tex" Dateien sind, aber auch Bilder lassen sich anzeigen.
In der rechten Spalte sieht man die Vorschau des kompilierten Dokumentes. Diese
Vorschau lässt sich mittels des "Recompile" Button oberhalb des Dokumentes
beliebig oft neugenerieren. Daneben finden sich noch Buttons um Warnungen vom
LaTeX-Compiler anzuzeigen und es besteht ebenfalls die Möglichkeit das Dokument
herunterzuladen.

<a href="/graphics/sharelatex_projekt.webp" data-lightbox="image" data-title="Die Standard-Oberfläche in einem Projekt.">
    <img src="/graphics/sharelatex_projekt.webp" style="margin:20px;">
</a>

Zwischen der mittleren und der rechten Spalte findet sich eine nützliche Funktion
in der man entweder von einer Quellcode-Position zu der entsprechenden Seite
im Dokument springen kann. Alternativ funktioniert das auch anders herum, sodass
man von der entsprechenden Stelle im Dokument an die richtige Stelle im Quellcode
springen kann.

Am oberen Rand der Web-Oberfläche finden sich ebenfalls noch ein paar weitere
Buttons, hinter denen sich eine nützliche Funktionen verbergen. Ganz rechts
befindet sich ein Button um eine Chat-Leiste einzublenden. Dort ist ein simpler
Chat enthalten, damit sich Projekt-Mitarbeiter dort unterhalten können. Daneben
kann man auch die letzten Änderungen anzeigen lassen, die im Projekt durchgeführt
worden sind. Es werden nicht nur die eigenen Änderungen angezeigt, sondern
die aller Projekt-Mitarbeiter. Diese Projekt-Mitarbeiter müssen natürlich
zuvor hinzugefügt werden. Daher verbirgt sich hinter dem Share-Button die Möglichkeit
eine E-Mail Adresse anzugeben, damit ein Nutzer das Projekt entweder nur Lesen
oder auch Lesen und Änderungen durchführen kann. Wenn der E-Mail Versand korrekt
eingerichtet worden ist, wird eine E-Mail versendet, alternativ kann sich der
Nutzer mit der angegebenen E-Mail Adresse auch selbst registrieren, um auf das
Projekt zugreifen zu können.

Am linken Rand der oberen Leiste in der Web-Oberfläche befindet sich ein Menü
um weitere Einstellungen am Projekt durchzuführen. Da kann man etwa den Compiler
anpassen, das Theme des Editors anpassen oder auch die Schrift-Größe anpassen.

<a href="/graphics/sharelatex_menu.webp" data-lightbox="image" data-title="Das Menü innerhalb eines Projektes.">
    <img src="/graphics/sharelatex_menu.webp" style="margin:20px;">
</a>

### Fazit

ShareLaTeX bietet eine gute Möglichkeit um kollaborativ an LaTeX-Dokumenten
zu Arbeiten. Die Änderungen jedes Projekt-Mitarbeiters können einfach nachvollzogen
werden und auch ein Chat ist integriert um sich auf kurzem Wege zu unterhalten.
Sinnvoll ist der Einsatz auch, wenn man häufig an verschiedenen Rechnern sitzt
und nicht überall die gleiche LaTeX-Installation vorzuweisen hat. Ebenfalls
kann man, wenn man etwa an akademischen Arbeiten sitzt, einfach andere Leute
einladen um etwa Korrekturen zu machen. Nachteillig ist nur die etwas umständliche
Installation, sowie die relativ hohe Last auf dem Server, da schon bei einem Nutzer
knapp 750MB an Arbeitsspeicher für ShareLaTeX (inklusive des Systems) gebraucht werden.
