---
title: "Rezension: Git - Verteilte Versionsverwaltung für Code und Dokumente"
categories: ["Rezension"]
date: "2015-07-05 15:45:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Das Buch „Git - Verteilte Versionsverwaltung für Code und Dokumente“ von den
Autoren Valentin Haenel und Julius Plenz gibt einen ausfürhlichen Einblick in die
Nutzung von Git. Das Buch erschien im Open Source Press Verlag.

### Was steht drin?

Das Buch umfasst 336 Seiten auf insgesamt neun Kapiteln sowie einen Anhang. Diese
werden zudem in drei Kategorien „Grundlagen“, „Fortgeschrittene Szenarien“
und dem Anhang aufgeteilt.

Die Grundlagen unterteilen sich in sechs Kapiteln. Zu Beginn zunächst die
Einführung, in der die ersten Schritte mit Git erläutert werden. Dies umfassst
im wesentlichen die notwendigen Begriffe und deren Erläuterungen, um mit Git
arbeiten zu können. Weiterhin wird das Anlegen eines ersten Repositorys mit den ersten
Commits thematisiert. Außerdem wird die grundsätzlichen Git Konfigurationen genannt und erläutert.

Im zweiten Kapitel folgen die gängigen Git-Kommandos die für das tägliche Arbeiten
mit Git gebraucht werden, sowie die Erläuterungen des Objektmodells. Im anschließenden
dritten Kapitel dreht es sich hauptsächlich um das Arbeiten mit Branches und Tags.
So wird erklärt, wie man Branches erstellt und wie man diese wieder zusammenführt.

Das vierte Kapitel behandelt fortgeschrittene Konzepte, wie etwa das Rebasen, inklusiver
Tipps wie und wann man Rebasen sollte und wann nicht. Weiterhin wird vermittelt,
wie man Dateien ignoriert und was die Befehle "git stash" und "git blame" machen.

Erst im fünften Kapitel geht es um die verteilten Git-Repositorys, also wie man
Remote-Repositorys hinzufügt, die Daten herunterlädt und deren Branches in die
eigenen Entwicklungsbranches mergt und wie man Commits wieder hochlädt. Außerdem
wird noch thematisiert, wie man Patches per E-Mail versenden kann und wie man Submodules
einbindet und verwaltet. Die Grundlagen schließen mit einem kurzem Kapitel zu
Git-Workflows ab.

Nach den Grundlagen-Kapitel folgen die fortgeschrittenen Szenarien, worin es
zunächst in Kapitel 7 um das Hosten von Git auf dem Server geht. Darin werden
verschiedene Optionen mit ihren Vor- und Nachteilen genannt, darunter etwa der Einsatz
von Gitolite. Das achte Kapitel umfasst die Automatisierungsfunktionen von Git, also
Attribute, Hooks und das Schreiben von eigenen Git-Kommandos. Das letzte Kapitel
erläutert im wesentlichen das Zusammenspiel von Git mit Subversion, mittels des
"git-svn" Tools.

Im Anhang befinden sich weiterführende Texte, darunter die Installation von Git,
die Struktur eines Repositorys, die Shell-Integration sowie einige Seiten über GitHub.

### Wie liest es sich?

Das Buch bietet einen tiefen und detailreichen Einstieg in Git. Die Erläuterungen
sind in der Regel klar und deutlich, sodass man die Vorgehensweise mit Git
gut verstehen kann. Das Buch richtet sich nicht auf Um- oder Einsteiger aus, sodass
wenige Vergleiche zu anderen Versionskontrollsystemen gezogen werden. Der
Leser erfährt dadurch auch keine Unterschiede zu anderen Systemen und worin Git besser
oder schlechter ist.

Die Autoren legen großen Wert auf das Verständnis des Git-Objektmodells, so referenzieren
sehr viele Erläuterungen auf das Kapitel über das Git-Objektmodell. Der Leser
erfährt dadurch nicht nur, wie man mit Git arbeitet, sondern auch, wie Git selbst
arbeitet was für das tägliche Arbeiten mit Git durchaus sinnvoll sein kann.

### Kritik

Während die Erläuterungen in dem Buch sehr detailreich sind, wird hin und wieder
nicht klar, warum ein Funktion sinnvoll ist und wie oft man es im täglichen
Nutzen von Git braucht. Bereits am Anfang des Buches werden sehr viele
Details erläutert. Um die Sinnhaftigkeit von Funktionen besser darzustellen, bietet
es sich meiner Meinung nach besser an, mehr auf Praxis-Beispiele zu beziehen, die
nicht nur bloß genannt werden, sondern auch beispielhaft vorgeführt werden.

Der Leser muss bereits am Anfang sehr viel lesen, um mit Git sinnvoll
starten zu können. Dadurch, dass häufig keine Praxisbeispiele am Anfang genannt
werden, muss der Leser selbst herumexperimentieren um sich mit Git vertraut zu machen.
Durch die starke Bindung an das Git-Objektmodell sind viele Begriffe Englisch und
technische Bezeichner. Die Erklärungen werden dadurch manchmal nicht ganz so einfach
verständlich, was vor allem für Einsteiger problematisch sein kann. Schade ist auch,
dass nur wenig auf Workflows eingegangen wird. Das vorhandene Kapitel hierzu ist
nur wenige Seiten lang.

Nichtsdestotrotz bietet das Buch viele Details, die selbst ein erfahrener Git-Nutzer
nicht kennt. Es werden häufig auch Tipps und Tricks eingeworfen die für das tägliche
Arbeiten mit Git hilfreich sind. Das sind in der Regel Aliase die gesetzt werden
können, um lange Befehle mit vielen Parametern abzukürzen.

### Buchinfo:

**Titel:** [Git - Verteilte Versionsverwaltung für Code und Dokumente](http://www.opensourcepress.de/de/produkte/Git/33227/978-3-95539-119-5)

**Autor:** Valentin Haenel und Julius Plenz

**Verlag:** Open Source Press

**Umfang:** 336 Seiten

**ISBN:** 978-3-95539-120-1

**Preis:** 29,90€ (broschiert), 24,99 (E-Book)

Da es schade wäre, wenn das Buch bei mir im Regal verstaubt, wird es verlost. Dies
ist „drüben“ bei [freiesMagazin](http://www.freiesmagazin.de/20150705-juliausgabe-erschienen)
in den Kommentaren möglich.
