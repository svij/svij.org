---
title: "What's coming next…"
categories: ["Sonstiges"]
tags: ["ubuntu-planet"]
date: "2015-04-21 18:20:00+02:00"
toc: true
---

Yesterday I have finally finished my Bachelor (yay!) after roughly 3,5 years.
Therefore, I've got a bit more time to focus my work on different open source projects (next
to my full time job). In this blog post I'm sharing my current plans.

### Taskwarrior for Android

In 2012 I've started the development of a [Taskwarrior](http://taskwarrior.org/)
compatible app for Android. Taskwarrior is a really cool CLI tool to manage your
todo list. After I didn't have much time to continue the development, I've stopped
developing the app in late 2013. 

A few days back I finally got time to restart the development. I've already done
a [few commits](https://github.com/svijee/taskwarrior-androidapp/commits/develop).
Anyway, I started from scratch - again. This happened for several reasons, mainly
the code was a big mess and the Android API changed in different ways. One bigger
mistake was, that I wanted to include too many features of the original Taskwarrior.

My current plan is, that the app can sync with the task server and have a nice,
simple to use user interface with the most important Taskwarrior features. The
minimum required Android Version will be 4.0.3 (Ice Cream Sandwich).

Time will tell, when the first release will be ready…

### Taskwarrior for Ubuntu Phone

After I've got my Ubuntu Phone at the Ubuntu Phone Insiders Launch Event, it's
a good reason to also develop a Taskwarrior app for Ubuntu Phone. This might happen
after I released the Android-App. Hopefully not too far away from today. :-)

### Ubucon Germany

It's the first time that I will be the head of the organization team of the
German Ubucon. Sadly we don't have any date or place to announce yet, so we
might don't have an Ubucon this year in Germany. The Ubucon took place in different
places in Germany since 2007. This year would be the 8th Ubucon.

### Anything else?

Beside of the above new projects I will continue my work on my other projects,
like my team membership at the German Ubuntu website [ubuntuusers.de](http://ubuntuusers.de).
And I will also travel to different events to support the Ubuntu Booth. :-)
