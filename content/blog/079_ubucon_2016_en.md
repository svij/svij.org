---
title: "This was the Ubucon 2015 in Berlin"
categories: ["Ubuntu"]
date: "2015-11-01 20:15:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Last weekend the 9th Ubucon in Germany took place in Berlin. It was the first
time that I was the head of the organisation team for the vent. After 2012, it
was the second time that we were guests at [HWTK](http://www.hwtk.de) in the
Center of Berlin. It was as always hosted by the [ubuntu Deutschland e.V.](http://verein.ubuntu-de.org).

### Friday

Traditionally, the Ubucon begins on a Friday. Personally, I was in Berlin in the
early morning, so I had time to see a bit of Berlin. In this case I've
met [SturmFlut](sturmflut.github.io) for the first time and [Riccardo Padovani](rpadovani.com)
for the second time. After we visited a couple of things in Berlin, I went to
the venue to set up the rooms for the Ubucon. Thankfully we had many people who
helped! After (nearly) everything was setup, we went over to the museum ["Story of Berlin"](http://story-of-berlin.de).
There, we had a really interesting tour through a fallout shelter and the museum.
We did learn how they would organise the bunker, if there would be a worst case
scenario. In the museum itself, we learned many things about the history of Berlin.
We were roughly 20 people in the museum, later on, we moved to the Restaurant
called "Route 66" where 40 people came together for the social event.

<a href="/graphics/ubucon2015_route66.webp" data-lightbox="image">
    <img src="/graphics/ubucon2015_route66.webp" width="400" style="margin:10px;">
</a>


### Saturday

We opened the registration booth of the Ubucon at 9 o'clock in the morning. At the same
time the catering-service delivered the canapé, sandwiches, bread rolls and salads.
Funnily, these guys were also happy Ubuntu users. :-)

As the main organiser, I opened the Ubucon at 10 o'clock. The schedule of the
event did contain many Ubuntu topics - that was different in the last years. We
had 28 talks and workshops in 4 to 5 tracks. Personally I didn't attend any talks
before lunch. Later Simon (aka SturmFlut, the panda guy) and me did the talk
about the UbuContest, where we handed the prizes of the contest to the winners.
Luckily Riccardo Padovani, Michał Prędotka from the FallDown Team and Jorik van
Nielen from "Click the Cookie" made their way to Berlin. Simon made some really
cool videos for the UbuContest, which you should watch: [Intro Video](https://www.youtube.com/watch?v=9xexCCAWbDs),
[the unofficial Ubucon-Video](https://www.youtube.com/watch?v=jj4yAImbJ8w),
[Falldown](https://www.youtube.com/watch?v=gVZ04kNqpE8), and
[Monster Wars](https://www.youtube.com/watch?v=LZTPaR2MO-8).

<a href="/graphics/ubucon2015_david_planella.webp" data-lightbox="image">
    <img src="/graphics/ubucon2015_david_planella.webp" width="400" style="margin:10px;">
</a>

After our talk, David Planella gave a speech about "The Ubuntu Phone and the
road to convergence", which was interesting and also contained some new information
which I didn't know yet. We had a camera guy, who sadly only got managed to record
the second half of the talk. The video might be online in the next weeks. My second
talk was my actual first English talk about Snappy Ubuntu Core with Daniel Holbach
and Oliver Grawert. The day ended with the "Linux-Quiz" where the most funny part
was how often the quiz application has crashed. ;-)

After the talks roughly 50 people headed over to the Cum Laude Restaurant for the
second Social Event, which was actually a candle light dinner ;-).

### Sunday

Due to the clock change the night to Sunday was an hour longer. Sunday is mostly
the day where fewer people are coming over. Also, the day is a bit shorter, because
the last talks ends at 3 PM. At 9 o'clock in the morning, the most motivated people
where the two guys from the catering service during the delivery of the food.
Personally, I visited some talks again. This time the systemd-Daemons talk from
Stefan Betz and the "What is Cloud?" talk from Kristian Köhntopp. After the lunch,
Simon talked about "Linux and Supercomputing" and in the last slot I organised
a quick discussion round to gather ideas of the next Ubucon, which will be the
[UbuCon Europe](http://ubucon.eu)!

<a href="/graphics/ubucon2015_owncloud-vortrag.webp" data-lightbox="image">
    <img src="/graphics/ubucon2015_owncloud-vortrag.webp" width="400" style="margin:10px;">
</a>

### Conclusion

It was the fourth Ubucon I've attended, the third where I was part of the organisation
and the first as the head of the organisation team. It was definitely more stress-free
than I would have expected. This was possible because we had a great team who
help whereever they could. Thanks to Torsten Franz and Dominik Wagenführ for the
work before the event. Thanks to Peter and Adrian who helped a lot in Berlin. A big
thanks goes also to Lynxis, who brought his hardware and setup the whole network,
which was working flawless.

We had nearly 120 attendees this year. The number is nearly the same as in the
last years. If you want to see some photos, David Planella has uploaded [a few on flickr](https://www.flickr.com/photos/david-planella/sets/72157659954860749).

For the next year we're hoping to get a bit bigger. The 10th
Ubucon in Germany will be the UbuCon Europe, taking place in Essen, Germany! There
is already a website on [ubucon.eu](http://ubucon.eu) and a [Launchpad-Group](https://launchpad.net/~ubuconeu)
where all the organisation stuff will be discussed. During the Ubuntu Online
Summit, we will also have a [session](http://summit.ubuntu.com/uos-1511/meeting/22619/ubucon-europe-planning/)
to gather more and new ideas. If you want to help or bring up some ideas, feel
free to join us!
