---
title: "Mozilla Stumbler ausprobiert"
categories: ["Mobile"]
date: "2014-11-01 12:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Vorgestern bin ich bei Golem über den Artikel [über Mozilla Stumbler](http://www.golem.de/news/stumbler-herumstolpern-fuer-mozillas-geolokationsdienst-1410-110203.html)
gestolpert. Die App für Android hatte ich vor etwa einem Jahr mal kurz angetestet, doch
ergab sich hier wieder die Gelegenheit es mal erneut auszuprobieren.

### Was ist es?

Mit Mozilla Stumbler möchte Mozilla einen freien Geolokationsdienst erschaffen, der mit den diversen
kommerziellen Angeboten konkurrieren kann. Der bekannteste Vertreter unfreier Gelokationsdienste ist
wohl Google. Erfasst werden dabei nicht nur die GPS-Position, sondern vor allem auch die an dem Ort
verfügbaren WLAN-Netze und Mobilfunk-Zellen. Sinn der Sache ist, dass man auch ohne einem verfügbaren GPS-Signal
möglichst genau geortet werden kann, etwa in geschlossenen Gebäuden. Dies funktioniert unter Android
beispielsweise mit den Google-Diensten gut und schnell. Diese Art der Ortung wird [Assisted GPS](https://de.wikipedia.org/wiki/Assisted_Global_Positioning_System)
genannt.

Mozillas Dienst nennt sich schlicht [„Mozilla Location Service“](https://location.services.mozilla.com/).
Alle Daten stehen unter [Public Domain](https://creativecommons.org/publicdomain/zero/1.0/) und können
[heruntergeladen](https://location.services.mozilla.com/downloads) werden.

__[Update]__

Korrektur: Es stehen nicht alle Daten unter Public Domain. Ganz im Gegenteil: Es werden
lediglich die Positionsdaten vom Mobilfunk freigegeben. WLAN-Positionsdaten werden *nicht* herausgegeben,
da es hier anscheinend Probleme bezüglich des Datenschutzes gibt. Nähere Informationen hierzu finden
sich [in einem GitHub-Issue](https://github.com/mozilla/ichnaea/issues/330) und [hier](https://news.ycombinator.com/item?id=8525635).
Das ganze trübt leider den Service, da er so nun nicht wirklich frei ist, aber es als frei beworben wird.

Danke an Matthias (per E-Mail) und [Markus](https://twitter.com/kamikazow/status/528732600291323904) für die Hinweise.

__[/Update]__

### Wie funktioniert es?

Das Mitmachen ist sehr einfach. Am einfachsten geht es, wenn man die Android App [„Mozilla Stumbler“](https://play.google.com/store/apps/details?id=org.mozilla.mozstumbler)
aus dem Google Play Store installiert. Danach muss man lediglich die App starten und an die frische
Luft und loslaufen.

<a href="/graphics/mozstumbler1.webp" data-lightbox="image" data-title="Mozilla Stumbler im ersten Lauf.">
	<img src="/graphics/mozstumbler1.webp" width="250" style="margin:20px;">
</a>
<a href="/graphics/mozstumbler2.webp" data-lightbox="image" data-title="Die erfassten Daten werden dargestellt.">
	<img src="/graphics/mozstumbler2.webp" width="250" style="margin:20px;">
</a>

Ich hab die App in der Dortmunder Innenstadt mal ausprobiert. Wie man am ersten (linken) Screenshot
sieht, waren zu dem Zeitpunkt einige Teile der Innenstadt noch nicht erfasst. Erfasste
Gegenden werden mit hellblauen Punkten übermalt. Wenn man los läuft, dann erfasst die App in regelmäßigen
Abständen die verfügbaren WLAN-Netze und Mobilfunk-Zellen. In meinem Fall wurden innerhalb von 15 Minuten
knapp 40 Berichte angefertigt. Interessanterweise fand die App während des Spaziergangs
knapp *900*(!) WLAN-Netze.

Die [Map](https://location.services.mozilla.com/map#2/15.0/10.0) wird einmal am Tag aktualisiert.
Meine erfassten Daten wurden am darauffolgenden Tag auch direkt in der Map angezeigt (siehe Screenshot 2).

Interessant ist auch, dass es ein kleines [„Leaderboard“](https://location.services.mozilla.com/leaders)
gibt. Innerhalb der App kann man seinen Namen eintragen, um anschließend dort gelistet zu werden.

### Fazit

Wer mit einfachen Möglichkeiten an dem Projekt teilnehmen möchte, der kann sich recht einfach
die App installieren und los legen. Für den dauerhaften Betrieb ist es wohl eher nicht geeignet,
außer man kann damit leben, dass der Akku sich schneller leert. Ich finde das Projekt unterstützenswert,
da man hier einen offenen Dienst sehr einfach unterstützen kann der nicht von einer kommerziellen Firma
geführt wird. Für den praktischen Einsatz ist es insbesondere für Firefox OS und auch anderen freien
Systemen und Apps relevant.

