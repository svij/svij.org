---
title: "Video Hands on bq Aquaris E5 Ubuntu Edition"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-06-25 20:30:00+02:00"
toc: true
---

Gestern habe ich mein insgesamt drittes Ubuntu Phone bekommen. Das Marketing-Team
von bq war so nett und hat mir für zwei Wochen das *bq Aquaris E5 Ubuntu Edition*
überlassen, was ich testen kann.

Wie auch beim [Meizu MX4](/blog/2015/06/21/video-hands-on-meizu-mx4-ubuntu-edition/)
habe ich ein kurzes Video aufgenommen, wo ich im wesentlichen die Unterschiede zum
Schwester-Modell dem Aquaris E4.5 aufzeige. Ein ausführliches Review folgt natürlich
auch noch.

Das Video habe ich auf YouTube hochgeladen und lässt sich [hier ansehen](https://www.youtube.com/watch?v=tcyf1Fe_jGM)

<iframe width="420" height="315" src="https://www.youtube.com/embed/tcyf1Fe_jGM" frameborder="0" allowfullscreen></iframe>
