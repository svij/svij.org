---
title: "Cristian Parrino from Canonical about the release of Ubuntu Phone and future plans"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2015-02-11 07:45:00+02:00"
toc: true
---

On the 6th February I've attended the "Ubuntu Phone Insider Launch Event" which
took place in London. There I had the chance to talk to Cristian Parrino. Thankfully
he agreed for an interview, where he talked about the current state of Ubuntu
Phone and a few feature plans, which weren't publicly known yet.

__Hi Cristian and thanks for joining the interview. First of could you tell me a few words about yourself?__

I'm Cristian Parrino and I head up the mobile program at Canonical. So all things
Ubuntu Phone.

__When did you join Canonical?__

Exactly five years ago next week!

__What did you do at the beginning?__

I headed up Online Services, so created the team which created Ubuntu One.

__Then you shut it down.__

I shut it down (laughs). It's a full life-cycle of a product.

__Hopefully you won't shut down Ubuntu Phone?__

No, I think there's a very very slim chance for that. We want people to love it.
It will take a life of its own.

__Today you launched the very first Ubuntu Phone, what is your favourite feature?__

It has to be scopes, which is the almost achieving answer because there are so
many features all in one place. But scopes are essentially redefining how people are
interacting with their phone. You know where the confident services that you used to finding
behind apps and app icons all the sudden comes alive directly on the screen. You
know where the information are richer, more engaging, faster and unfragmented.

__Are you already using your Ubuntu Phone as your daily phone?__

I am, yeah, absolutely!

<a href="/graphics/ubuntuphone_parrino_klein.webp" data-lightbox="image" data-title="Cristian Parrino bei der Veröffentlichung.">
    <img src="/graphics/ubuntuphone_parrino_klein.webp" style="margin:20px;">
</a>

__The bq Phone or another one?__

You actually have the first real bq phone. I still have an Android Version of the
bq phone reflashed with Ubuntu. Still a bq phone, but you actually have even
before anybody at Canonical the very first production of Ubuntu Phones.

__A few people asked me, if they can buy Android Version of the bq phone
 and flash it with Ubuntu?__

They could. The problem is that … obviously we have a version of Ubuntu which you
can always download from the web. Unlike on the desktop where
you encourage people to download and use it every day, on the phone side it's
for developers. It is the same codebase, but what a manufacturer does with scopes
is that they create an experience thats unique to the device. So for example the
bq device is launching with an experience that has a news scope, photos scope,
video scope, music scope, NearBy scope for location services, that has a much richer
experience than the version you download from the web that has an app scope
and a whole bunch of blank scopes.

It's a developer build which we have on the web, while the user build is the one that
goes into the devices. So we actually can't fully appreciate what the Ubuntu Phone
is all about unless you get your hands on a preloaded Ubuntu Phone from bq.

__bq is the first one after that … Meizu?__

It's Meizu, yeah.

__… next month?__

Well, we're in February, so I think the best-case scenario is next month. What
definitely is going to happen next month is that we will show the Meizu devices
at MWC in the first week of March. We don't know yet for sure but we are hoping
that we can sell those devices in March as well. It could be March, it could be April.

__ So what happened last year, when you said that Canonical will release the
first Phone in 2014 ;-). What happened there?__

I hope we get a little better this time. I think we are a little bit more accurate
now.

__What were the main problems of the really long delay?__

Tons of stuff, right. You're doing something for the very first time and you wanted
to get it just right. Also, predictable things like supply-chain related, production
chain sustainability were building a screen with the android icons. How long does
it take to turn around actually the production line for the same screen but without
the Android buttons. Just a myriad of different things which you just resolve as they
come up.

__So the Hardware of both phones with Android and Ubuntu are not exactly the same?__

Yes, the only difference is the screen. It has the same resolution but one has
icons for Android and the other one doesn't, because Ubuntu doesn't require
physical buttons.

__Well even Android doesn't require physical buttons.__

Yeah, it has this overlay three icons.

__Yeah, it depends on the manufacturer…__

__How many people are currently working on Ubuntu Phone?__

Oh my … that's a difficult question. I'm gonna guess about 350 to 400 people 
just inside Canonical, but obviously there is a community that contribute across
the board from development to usability to a whole number of things. Ubuntu is always
much more than just Canonical.

__If I want to contribute to Ubuntu Phone, where should I go?__

Depends, what do you want do. Are you a developer, a community contributor or…?

__Let's say: As a developer.__

As a developer I think the best thing to do, for you individually as a developer,
but also to increase the value of Ubuntu Phone, is to start to use the scope toolkit. That's
the truly revolutionary about this phone which makes it incredible easy to use
any API to create an experience that is app-wide that doesn't cause a fraction
like an app does.

__Which programming language or any other thing do I need to know to start developing
an app?__

Just about anything you know. JavaScript, C++, Go…! We're using a UI toolkit, so
you are essentially mapping API of your own service or own content or whatever
it is to a toolkit that gives you a lot of flexibility.

__Other smartphone operating systems support running Android apps on
their phone. Why isn't Ubuntu supporting Android apps?__

Because I don't think any platform that just aims to be a "me too" has a chance
to succeeding. From a user perspective what's the motivation of buying a phone
that gonna give you an Android-Clone but not quiet?

__Regarding to the future, what are the future plans for Ubuntu Phone in the
next two years?__

In the next two years, if we fast forward to the end of that timeline is the
dream of convergence. You know a phone that does all the things which we talk
about today that changes the way users interact with their phones. But that
also becomes the heart of all your personal computing devices. So back to what
we hinted in the Ubuntu Edge campaign where your phone becomes to your pc, when
you dock it.

__So you are planning around two years to have an Ubuntu Edge like phone with
all the features which were announced back then?__

Well hopefully we will be faster than two years, but two years ahead I want that
phone which is also my pc in the market. Even this bq phone is a significant
step in that direction. There are foundations in the phone that makes us closer
to that conversion vision. But right now we focus on teaching the world that
there is another way to enjoy phone instead of just apps and app icons. It's time
for a change.

__Personally I think Ubuntu Phone has better ideas than Firefox OS which has
just another system with webapps.__

Right. From a user perspective they don't care if it's a webapp or not. On Ubuntu
Phone you get a richer experience with an impressive amount of services. You get
your scopes on Ubuntu so the experience will be richer. And it slowly gonna get
better. Thats a starting point of thousand scopes and apps without even selling
a single phone is an amazing amout of course.

__Let's go back to the topic about manufacturers. Next to bq and Meizu, are there
any other manufacturer in the pipeline?__

Yes, but forgive me if I don't mention them yet.

__Ohh!__

They won't be very happy if I do. (laughs)

But you are actually going well, because there is a group which we call insiders,
which you are part of, which means that as soon as we are ready to talk
about next manufacturer, you are first to know about it.

__Is there a timeline?__

I would say before June we will probably name another manufacturer for a market
that we haven't yet touched.

__That means? ;)__

That means it's a market where we haven't yet offered a solution – it's the US.
So we hope to announce something for the US in the next six months.

__Alright, thanks for the interview!__

You're welcome, thanks for being here, it is really great to have you here.

__Thanks!__
