---
title: "Aufzeichnung und Folien des Git-Talks & -Workshops auf der FrOSCon 2017"
categories: ["Events"]
date: "2017-08-22T19:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/180779b98acf48ad9e68199299ba7aae"
---

Letztes Wochenende fand die 12. [FrOSCon](https://www.froscon.de) in St. Augustin
statt. Für mich war es die dritte FrOSCon in Folge. Ich selbst habe diesmal
zwar nicht den Ubuntu Stand betreut, stattdessen habe ich einen
„Git für Einsteiger Workshop“ durchgeführt, sowie den Vortrag „Nicht alltägliche
Git-Funktionen“ gehalten.

### Nicht alltägliche Git-Funktionen

Der Talk war schönerweise gut besucht und wurde auch, wie zahlreiche anderen
Talks, aufgezeichnet. Die „Folien“ des Talks finden sich auf meinem
[Speakerdeck-Account](https://speakerdeck.com/svij/nicht-alltagliche-git-funktionen).

<script async class="speakerdeck-embed" data-id="87ea46c8bc1047739fa47f42212ddf33" data-ratio="1.78086956521739" src="//speakerdeck.com/assets/embed.js"></script>

Alle Aufzeichnungen der FrOSCon finden sich auf [media.ccc.de](https://media.ccc.de/c/froscon2017).
Meinen Vortrag findet man [hier](https://media.ccc.de/v/froscon2017-1919-nicht_alltagliche_git-funktionen).

<iframe width="1024" height="576" src="https://media.ccc.de/v/froscon2017-1919-nicht_alltagliche_git-funktionen/oembed" frameborder="0" allowfullscreen></iframe>

Thema des Talks ist die Übersicht von Git-Funktionen, die man explizit nicht jeden
Tag braucht, aber immer mal wieder gebrauchen kann. Einige sind dabei alltäglicher
als die anderen Befehle. Details lassen sich dazu in dem Talk ansehen, wie zuvor
verlinkt.

### Git-Workshop für Einsteiger

Während der Talk sich an fortgeschrittene Git-Nutzer richtete, war der Git-Workshop
für „blutige“ Anfänger. Auch hier waren erfreulicherweise genau die passenden
Leute da, die entweder noch nie ein Versionskontrollsystem verwendet haben, oder
die von Subversion umsteigen bzw. umsteigen wollen.

Die „Folien“ sind hier ohne Erläuterungen, da ich diese mündlich erfolgten. Die
Folien finden sich ebenfalls [auf meinem Speakerdeck-Account](https://speakerdeck.com/svij/git-workshop-fur-einsteiger-1).

Alternativ lohnt sich auch hier wieder mein [Git Tutorial](/blog/2014/10/25/git-fur-einsteiger-teil-1/#git-fur-einsteiger-teil-1)
auf meinem Blog bzw. auch mein [Buch](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/#mein-git-buch-beim-mitp-verlag-ist-da).
Das Buch basiert nämlich auf meine Git Tutorial Reihe und der Workshop basiert
ebenfalls darauf.

<script async class="speakerdeck-embed" data-id="123c82a7d6e5452796ebd6ac60bca3f5" data-ratio="1.78086956521739" src="//speakerdeck.com/assets/embed.js"></script>

### Fazit

Unabhängig von meinem Vortrag und Workshop fand ich die FrOSCon wieder einmal
sehr schön. Auch in diesem Jahr merkt man, wie sich die Organisation der Veranstaltung
sich jedes Jahr Schritt für Schritt verbessert, auch wenn sie schon zum 12. Mal
hintereinander stattfindet. In diesem Sinne: Vielen Dank an das Organisationsteam
der FrOSCon für die tolle Konferenz! Nächstes Jahr bin ich dann wohl wieder dabei. :)
