---
title: "Canonical CEO Jane Silber about Ubuntu Phone"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2015-02-13 12:00:00+02:00"
toc: true
---

During the "Ubuntu Phone Insider Launch Event" I had the chance to do an interview
with Cristian Parrino (Vice President Mobile Canonical) and with Jane Silber,
who is the CEO of Canonical. I've already [published the interview](/blog/2015/02/11/cristian-parrino-from-canonical-about-the-release-of-ubuntu-phone-and-future-plans/)
with Cristian Parrino.

__Hi Jane and thanks for joining the interview. Could you please tell me a few words about yourself?__

I'm Jane Silber and I'm the CEO of Canonical.

__Canonical just launched the first Ubuntu Phone today, what is your favourite feature of the phone?__

There are certain apps which I really like. I really like the gallery app, I find
it a pleasing experience to use. And in terms of the user experience I love the
right edge swipe, I love the way you can swipe from the right edge and see all
the open apps and flick up to close them. It pleases me! Sometimes I open a bunch
of apps and play with that thread just because I think it's just beautiful.

__Are you already using Ubuntu Phone as your daily phone?__

I use it as a daily phone I really admit that I also carry an Android Phone. I
have a Nexus Phone which I use as my Ubuntu Phone, I don't have one of the new
bq. I'm looking forward to getting one of those. And I also have some friends
who are addicted to WhatsApp so I use my Android phone for the one purpose to talking
to friends on WhatsApp. But I'm slowly, slowly, slowly are converting them to Telegram.
So I message them on Telegram and they're starting to get it. And Telegram is a
great app so it works fine.

__Do you have any feature on your Android Phone which is not present on Ubuntu Phone yet?__

No, there's nothing which I miss other than WhatsApp. There is no specific feature
on Android which I don't have on Ubuntu.

<a href="/graphics/ubuntuphone_janesilber.webp" data-lightbox="image" data-title="Cristian Parrino bei der Veröffentlichung.">
    <img src="/graphics/ubuntuphone_janesilber.webp" style="margin:20px;">
</a>

__Let's talk about the business. What is the business model of Ubuntu Phone, how is Canonical making money?__

There is a unit charge that OEMs shipping Ubuntu Phone are paying us. And depending
on the region and the services included there is revenue share on some of their
services, if a user buys different things. The exact details of that we keep confidential
as a piece of commercial information. But it is a system where both parties are
collaborating and work together. And both parties benefit.

__How about developers? Let's say I want to create an app for Ubuntu Phone to make money?__

It's the same sort of revenue share program for developers for publishing apps
and scopes. It is small now because there is a limited user group right now.

__What is the main difference between Ubuntu Phone, Android and iOS?__

I think for the Ubuntu Phone what users will notice most is the experience.
You have a difference experience of bringing content to the user in the form
of scopes as supposed to a grid of app icons. And on that people can react positively
and negatively because of the change. But if they give it a try and use it, it is
a very pleasing experience. Additionally you get very used to aggregated content from different
sources. There are many differences under the cover but I think from the user
perspective it is the new experience that scopes can bring with bringing content.

But now, I have a question for you! From what you have seen so far what do you think
the people appreciate the most?

__I also think that scopes are a pretty nice feature. It's not comparable to
systems like Firefox OS which has just apps. In my opinion Ubuntu Phone has a
better idea, so let's see what happens.__

What also scopes are allowed to do, or what hardware partners can do is to
differentiate between the phone to highlight different services. They
have customisation opportunities so people are able to choose and have a meaningful
difference between different phones from different operators. So you will
potentially get a different experience with Meizu and bq.

__So what are the differences between …__

Right now if you buy an Android Phone the operators try to do something different then other operators. So they mess around with Android. They customize
it in different ways and you get a lot of fragmentation. And that is also very
difficult for developers.

What we can do with scopes and frameworks is to allow a same baseline for developers.
In different regions we can also offer an experience for that region or country.
If an operator for instance has a set of services which they want to highlight.

__Services like…?__

Services like Cloud Storage, Music, Video, so there can be customisation on that
level. And the user can also say "I don't want to get my music from grooveshark".
It allows a very high level of customisation without fragmentation that has haunted
some other ecosystems.

__So different OEMs will probably have different pre installed apps and scopes?__

Yes. And users can also change them in a very consistent way and developers can
target them in a consistent way without having that fragmentation that other
systems have.

__This new bq Phone with Ubuntu is launching in Europe next week, what about the US market or asia?__

In asia we will launch with Meizu. We will launch Meizu phones both in Europe and
in China. The US will follow after that but we are not ready to announce anything.
We have plans.

__So no date? Or first half of the year or second half of the year?__

No dates. ;-) Meizu will absolutely start first and the US we are not announcing
yet.

__Alright thanks for the interview!__
