---
title: German Ubucon in Katlenburg
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2014-02-15T12:10:00+02:00"
toc: true
---

This year the 8th German Ubucon takes place in [Katlenburg](https://de.wikipedia.org/wiki/Katlenburg-Lindau),
which is a rather small city next to Göttingen. The last Ubucon took place in
Heidelberg and that was the first time that I joined the organization team.
So this year I am also a member of the organization team of the Ubucon. Personally
I attended two Ubucons: 2012 in Berlin and 2013 in Heidelberg. Last year
we hade the slogan „Build your conference“. Everybody was invited to make requests
for talks. The actual [list](http://ubucon.de/2013/themenwunschliste) was quiet
long. Many speakers picked up some topics, so a big part of the Ubucon
was built by the attendees. This worked out very well, so we do it again! :-)
Nevertheless we have another slogan for this years Ubucon: „10 years Ubuntu,
10 years Community“.

We started the organization at the end of last year. The Ubucon starts in eight
months, so we have plenty of time to organize everything. We already started our
[„Call for Papers“](http://ubucon.de/2014/10-jahre-ubuntu-10-jahre-community-call-for-papers-beginnt)
and we also got our [first submission](http://ubucon.de/2014/programm) for a talk.

I am really looking forward to this years Ubucon. The team in Katlenburg, who
organizes the local parts of the event, are really motivated. There will be some
cool stuff, which I cannot announce now ;-). The location of the event is a primary
school, which is rather unusual. The last Ubucons usually took place in a public
or private university. An interesting fact is, that this primary school has an
„open source background“: They migrated the schools computers from Windows to Ubuntu. :-) 
