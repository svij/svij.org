---
title: "Programm der Ubucon 2013 steht fest"
categories: ["Ubuntu"]
date: "2013-09-21 12:30:00+01:00"
toc: true
---

Am letzten Dienstag erschien das [Programm](http://ubucon.de/2013/programm-der-ubucon-2013-veroeffentlicht) der Ubucon 2013. Ich selbst
bin diesmal mit 2,5 Vorträgen vertreten. Am Freitag, direkt nach der Eröffnung, gibt es den ersten „Vortrag“ von mir. Letztendlich
ist es kein richtiger Vortrag sondern ein „Hands on“ für die beiden mobilen Betriebssysteme „Ubuntu Touch“ sowie „Firefox OS“. Die
Besucher dürfen in der Zeit sich die Geräte mit den Systemen anschauen und sich selbst ein Bild von den beiden Systemen machen. Ubuntu
Touch läuft dabei auf zwei Nexus 4 Smartphones, die freundlicherweise von Canonical zur Verfügung gestellt werden. Firefox OS wird
hingegen auf einem (eventuell auch zwei) Nexus S laufen.

Am Sonntag folgen dann meine beiden richtigen Vorträge. Um 10 Uhr folgt zunächst der Vortrag „Mobile Open-Source-Systeme im Überblick“
in dem ich die grundlegenden Eigenschaften und Unterschiede zwischen Android, Firefox OS, Ubuntu Touch und Tizen. Direkt danach folgt
dann ein ausführlicher Vortrag über Ubuntu Touch. Beide Vorträge werde ich hier spätestens nach der Ubucon veröffentlichen.

Da ich auch als einer der Organisatoren mithelfe, freue ich mich um so mehr auf die diesjährige Ubucon. :-)
