---
title: "Visiting bq: How bq tests and develops their devices"
categories: ["Ubuntu"]
date: "2015-10-11 16:30:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Recently, I got an invitation from bq for the launch of their first Android One
in Spain and Portugal. The event took place in Madrid. A couple of journalists
and bloggers from Germany (including me) and France were also invited for a tour
through the offices and labs from bq in their headquarters in Madrid. There, they
showed us some details about the development of their smartphones and how they
test the hardware. While the presentation of the Android One Smartphone wasn't
that much interesting for me, the latter part of the event was even more interesting!

Transparency side note: bq paid me the trip to Madrid!

# Company Presentation

<a href="/graphics/bq0.webp" data-lightbox="image">
    <img src="/graphics/bq0.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq1.webp" data-lightbox="image">
    <img src="/graphics/bq1.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq2.webp" data-lightbox="image">
    <img src="/graphics/bq2.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq2-.webp" data-lightbox="image">
    <img src="/graphics/bq2-.webp" width="195" style="float: left; margin:10px;">
</a>

At the beginning the deputy CEO Rodrigo del Prado, who I also met in London at
the Ubuntu Phone Launch Event, presented the recent developments of bq. The company
itself is pretty young and widely unknown to "normal" people outside from Portugal
and Spain. While they started with selling Ebook-Readers, they now focus and
Smartphones and Tablets running Android. As a side-project they're also selling
the first both Ubuntu Phones, which shouldn't be news for the Ubuntu Planet ;-).

Currently bq focuses on the European market. They develop the hardware and software
in Madrid, but they're manufactured in China. There are currently no plans for
enter the north-american market - so you probably can't expect a bq Ubuntu Phone
in the US in the near future.

# Development

<a href="/graphics/bq3.webp" data-lightbox="image">
    <img src="/graphics/bq3.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq4.webp" data-lightbox="image">
    <img src="/graphics/bq4.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq5.webp" data-lightbox="image">
    <img src="/graphics/bq5.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq6.webp" data-lightbox="image">
    <img src="/graphics/bq6.webp" width="195" style="float: left; margin:10px;">
</a>

BQ did tell us a lot details about their development process. In a couple of charts,
they showed us how long they need to develop a device from scratch: 9 months.
In the first two months they mainly focus on working at the first prototype. Two
teams do work in this stage: the visual designers and the mechanic designers.
The first group of designers are responsible for the appearance of the device, the
mechanic desigerns work on the internal hardware design. All hardware components,
like the board, the screen, etc. must fit together, so both teams has to work
closely together. In this early stage of the development only nine people work
on the prototypes.

After finishing the first prototypes, the first test-production starts with only
80 devices. In the next three months bq works on the "Engineering Verification Testing",
where they test the general functionality of the hardware. They're also implementing
corrections and making improvements on the hardware. This phase also includes
the first basic tests in power consumption, temperature, camera and many more. In
the mean time, the software developers are starting their work on the kernel and
porting the software.

The third phase is the "Design Verification Process", where they continue to improve
the devices. The number of devices is a bit higher now, with roughly 140 sample
devices. This is also the stage, where the software developer are working on
the full stack - not only the hardware support any more.

After that, the bigger tests are starting in the "Process Verification Test". This
is an unofficial beta-release of the device, where all the developments and tests
from the previous phases. The main target of this stage is, to find as many bugs
as possible. The hardware of the device is nearly fixed, if they find bigger
problems in this stage, they have to go back and nearly have to start again.
The overall number of testing devices are 2000. In the 8th and 9th month, the device
will finally go into the mass production. After that, only OTA-Updates will follow.

# Lab-Tour

<a href="/graphics/bq7.webp" data-lightbox="image">
    <img src="/graphics/bq7.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq8.webp" data-lightbox="image">
    <img src="/graphics/bq8.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq9.webp" data-lightbox="image">
    <img src="/graphics/bq9.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq10.webp" data-lightbox="image">
    <img src="/graphics/bq10.webp" width="195" style="float: left; margin:10px;">
</a>

After the presentation, we had a guided tour through the offices and labs from bq.
This was by far the most interesting part of the day. We had the opportunity to
talk to the Engineers and other Employees working on the phones and tablets. At
the beginning, we visited the designers. Before we entered the room, they covered
the walls, because there were a few new devices, which are not announced yet. While
I did see some details about new devices, I sadly didn't see any signs of the
new convergent device running Ubuntu, which was announced to release this year.

Anyway, there were only two young designers, who work on the appearance of the devices.
They work closely with the mechanic designers and the bosses to create a good
looking device. On their desks there were many drafts of phones and also dummy
phones.

<a href="/graphics/bq11.webp" data-lightbox="image">
    <img src="/graphics/bq11.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq12.webp" data-lightbox="image">
    <img src="/graphics/bq12.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq13.webp" data-lightbox="image">
    <img src="/graphics/bq13.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq14.webp" data-lightbox="image">
    <img src="/graphics/bq14.webp" width="195" style="float: left; margin:10px;">
</a>

The mechanic designers are responsible for the internal hardware design. This
includes the board with the CPU, RAM, WiFi and all other hardware components. The
board also has two micro SIM slots and a micro sd slot. When you look on the pictures,
you actually see how much space these three things are consuming. Knowing that,
it isn't surprising, that many companiess doesn't want micro sd slots and dual sim.
The rest of the device is essentially the battery, the screen, the chassis and
cables and antennas. The hardware designers are also responsible for the power
consumption and the quality of the radio antennas. The production of the device
is completely done in China, while the QA-Process is done in Spain.

<a href="/graphics/bq15.webp" data-lightbox="image">
    <img src="/graphics/bq15.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq16.webp" data-lightbox="image">
    <img src="/graphics/bq16.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq17.webp" data-lightbox="image">
    <img src="/graphics/bq17.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq18.webp" data-lightbox="image">
    <img src="/graphics/bq18.webp" width="195" style="float: left; margin:10px;">
</a>

BQ runs seven different tests in their labs. The "Vibration Test Machine" doesn't
test the vibration motor, but they put the device into the machine, which vibrates
heavily. The goal is to make sure that no parts of the device are getting loose.
This test will run several hours. The other test checks the connections and buttons
of the device. So it will plug and unplug USB-Cables, Headphones and press the volume
and power buttons. With this test, they want to make sure that the quality is good
enough for many years. The third test could be named the "Bendgate"-Test. If you
remember the Bendgate from the iPhone 6+, you might remember, that the device got
bend, when you put it in your back pocket of your jeans. BQ are running those tests
on their devices, also for a couple of hours. Most people break the screen or the
glass of the screen on their device, here they execute two different drop tests.
The simple one just drops the device from a small height, like you drop your phone
onto the table. The "Tumbling Test Machine" on the other side simulates dropping
your phone from a higher height. The last machine is a huge machine, which raises
much dust. With this test they're making sure that there won't be any dust in the
device, like under the screen or on the camera lens.

# Conclusion

<a href="/graphics/bq19.webp" data-lightbox="image">
    <img src="/graphics/bq19.webp" width="195" style="float: left; margin:10px;">
</a>
<a href="/graphics/bq20.webp" data-lightbox="image">
    <img src="/graphics/bq20.webp" width="195" style="float: left; margin:10px;">
</a>

<iframe width="560" height="315" src="https://www.youtube.com/embed/PYc5Gj2JqpY" frameborder="0" allowfullscreen></iframe>

We had a good view into the production and testing details of bq. They were also
open to any questions. BQ answered the question, why they're also selling Ubuntu
Phones and Android One Phones, that they want to be open for other opportunities
to don't miss any new options. Therefore, they also said, that they were quite surprised
by the amount of sold Ubuntu Phones. During the visit I also made a video about
all the testing machines, which you can watch [here on YouTube](https://www.youtube.com/watch?v=PYc5Gj2JqpY).
The video is in German though, but you'll get the most of the stuff, if you read
this article ;-).
