---
title: "Tooltipp: Diagramme mit Mermaid"
categories: ["Kurztipps"]
date: 2023-05-20T03:45:00+02:00
tags: ["ubuntuusers-planet"]
---

Es gibt ja viele verschiedene Tools, mit denen man Diagramme erstellen kann.
Zuletzt setzte ich vor allem auf [draw.io](https://www.drawio.com/).

Praktisch an drawio finde ich, dass das grafische Tool recht einfach zu bedienen ist
und damit beziehe ich mich vor allem darauf, wie gut die gleichen Abstände zwischen
den Elementen eines Diagramms abbildbar sind.

Viel lieber hätte ich allerdings ein Tool, womit ich die Diagramme als Code definieren
kann. Hierzu bin ich neulich auf [mermaid.js](https://mermaid.js.org/) gestoßen.

Mermaid.js hat diverse Diagrammtypen, die verwendet werden können. Darunter Flowcharts,
Gantt Charts oder Pie Chart. Persönlich finde ich Git Graph nützlich, Mindmaps,
sowie Timeline. Die ganze Liste findet sich in der [Dokumentation](https://mermaid.js.org/intro/).

Besonders praktisch ist es vor allem, wenn man sich die [Integrationen](https://mermaid.js.org/ecosystem/integrations.html#productivity) anschaut
und genau dabei bin ich auch auf Mermaid gestoßen: Durch die Ablage der Dateien
in GitLab oder auch GitHub, wird es direkt im Browser angezeigt. Das beinhaltet
auch die Nutzung und Darstellung in Issues.

Beispiel-Code für die Darstellung von GitLab Flow:

    %%{init: { 'logLevel': 'debug', 'theme': 'base', 'gitGraph': {'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'production'}} }%%
    gitGraph
        commit
        commit
        branch main
        commit
        commit
        branch feature/foobar
        commit
        commit
        checkout main
        merge feature/foobar
        checkout production
        merge main

Der Output:

![GitLab-Flow](/graphics/gitlab-flow.webp)

Damit man aber nicht mühselig die Dokumentation lesen muss, existiert auch noch
ein Editor im Browser unter [mermaid.live](https://mermaid.live/). Dort kann man
dann auch viel einfacher die Diagramme erstellen und dort hin rüberkopieren, wo
man diese braucht.

Vielleicht hilft es ja der ein oder anderen Person!
