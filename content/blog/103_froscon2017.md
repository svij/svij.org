---
title: Git-Talk und Git-Workshop auf der FrOSCon 2017
categories: ["Events"]
date: "2017-07-20T15:15:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/a17eb425835f4a179055ca03ea99b3ba"
---

Am 19. und 20. August findet in St. Augustin bei Bonn die [FrOSCon](https://www.froscon.de/)
statt. Für mich ist es mittlerweile eine der Open Source Veranstaltung neben
der OpenRheinRuhr und der Ubucon, die ich möglichst nicht verpassen will. Dieses
Jahr bin ich so da zum (erst) dritten Mal in Folge.

Das Programm der diesjährigen FrOSCon [veröffentlicht](https://programm.froscon.de/2017/).
Ich selbst bin dieses Mal mit einem Vortrag und einem Workshop vertreten.

### Nicht alltägliche Git-Funktionen

Im letzten Slot am Samstag um 17:45 findet mein Talk mit dem Titel „Nicht alltägliche Git-Funktionen
– Committen, Pushen, Mergen kann jeder – aber was gibt es darüber hinaus?“ statt.

Wie der (Unter)Titel schon verrät, geht es hier eben nicht um die alltäglichen
Funktionen von Git, sondern die Funktionen, die man eher nicht so oft nutzt
und eben auch nicht jeden Tag braucht. Darunter fallen Funktionen wie die
Fehlersuche mit Git, das Neuschreiben der kompletten Historie, Wiederherstellen von
kaputten und „verlorenen“ Commits und noch einige weitere Funktionen.

Der Talk richtet sich an diejenigen, die Git schon kennen und neue nützliche
Kenntnisse in der Nutzung gewinnen wollen. Der Link zum Programmpunkt findet
sich [an dieser Stelle](https://programm.froscon.de/2017/events/1919.html).

### Git-Workshop für Einsteiger

Für die Personen, die noch keine Erfahrung und Ahnung von Git haben, biete ich auch
noch einen Einsteiger-Workshop an. Dieser findet am Sonntag von 14 bis 18 Uhr
statt. In den vier Stunden wird es einen grundlegenden Einstieg in die Versionsverwaltung
mit Git. Konkret fällt darunter: Was ist Git und worin unterscheidet es sich von
anderen Versionsverwaltungssystemen? Wie erstelle ich ein Repository und füge
Änderungen hinzu? Wie erstelle ich Branches und führe sie wieder zusammen?

Vier Stunden sind wahrlich nicht viel für einen Workshop. Je nach Zeit wird die
Arbeit mit Remote-Repositorys auf das nötigste beschränkt. Der Workshop empfiehlt
sich daher wirklich nur für die Leute, die wirklich noch nicht die wesentlichen
Git Funktionen kennen. Der Link zum offiziellen Programmpunkt findet sich
[an dieser Stelle](https://programm.froscon.de/2017/events/1918.html).

Der Workshop basiert auf mein [Git-Buch](https://svij.org/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da).
Eben dieses Buch wird auch – wenn nichts dazwischen kommt – nach dem Workshop
und nach dem Talk verlost.

Unabhängig von meinen Talk und Workshop lohnt sich natürlich auch ein Blick
auf die anderen Vorträge und Workshops [im Programm der FrOSCon](https://programm.froscon.de/2017/)!
