---
title: "Dritte Auflage meines Git-Buches erschienen!"
date: "2021-03-03T09:15:00+01:00"
categories: ["Git"]
vgwort: "https://vg01.met.vgwort.de/na/959e2a3a72aa4483b29f1ea63dc3f2ac"
cover:
  image: "/graphics/git-buch-v3-cover.webp"
---

Im August 2016 erschien die [erste Auflage meines Git-Buches](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/),
welches in Zusammenarbeit mit dem [mitp-Verlag](https://mitp.de) erschienen ist.
Im Mai 2019 folgt anschließend die zweite Auflage.

Seit Anfang März 2021 ist nun auch schon die dritte Auflage erhältlich!

## Was ist neu?

Mich fragten schon vor Erscheinen des Buches einige Leute: „Lohnt es sich für mich das
Buch zu kaufen, wenn ich schon die erste oder zweite Auflage habe?“ Und die Antwort ist
dafür recht klar: „Höchstwahrscheinlich nicht.“

Wie so häufig bei Neuauflagen sind sehr viele Kleinigkeiten korrigiert, die nicht
sonderlich spannend zu berichten sind. Ja schön, weniger Tipp- und Rechtschreibfehler,
toll, nicht? Dafür natürlich auch an der ein oder andere ein paar neue.

Zu den interessanteren Änderungen gehören einige Änderungen die von Git über
die Zeit kommen: Statt sehr vieles nur mit `git checkout` zu machen, gibt es nun
auch die Möglichkeit `git switch` und `git restore` zu verwenden. Das Buch setzt
nun vollständig auf die neuen Befehle. Einige neue Tipps und Tricks sind zudem
in Kapitel 9 enthalten, etwa Abschnitte zu Git LFS oder `git subtree`.

Zudem sind die Kapitel zum Hosting von Git-Repositorys grundlegend überarbeitet
worden. In den letzten Jahren hat sich sowohl bei GitHub als auch bei GitLab
einiges getan und beides sind schon länger nicht mehr nur für das reine Hosten
von Git-Repositorys nützlich. So ist der Abschnitt zu Jenkins rausgeflogen und
stattdessen ein neuer Abschnitt zu GitHub Actions reingeflogen. GitLab CI/CD hat
in der Runde ebenfalls ein wenig mehr Liebe bekommen.

Ganz neu ist das jetzt neue zwölfte Kapitel über DevOps. Dies gibt einen Einstieg
in DevOps und soll aufzeigen, was das ist und was für Vorteile es hat. Die einen
mögen sich vielleicht fragen, was es in einem Git-Buch zu suchen hat, aber das
ist relativ klar: Als äußerste Grundlagentechnologie ist Git auf nahezu allen Ebenen
in der DevOps-Kultur unausweichlich.

## Was steht drin?

Der Umfang des Buches hat über die Auflagen hinweg immer ein wenig zugenommen.
Mittlerweile sind es 328 Seiten bei nun 13 Kapitel. Im Vergleich zur zweiten
Auflage ist ein neues Kapitel über DevOps hinzugekommen.

 * **Einführung:**
    Im ersten Kapitel geht es zunächst um den Einstieg in das Thema von
    Versionsverwaltung. Konkret also was eine Versionsverwaltung ist, wofür sie
    gut ist und welche Arten es gibt.

 * **Die Grundlagen:**
    Das zweite Kapitel befasst sich mit den Grundlagen, also wie man ein
    Repository anlegt, wie Commits man tätigt und wie Git intern grob arbeitet.

 * **Arbeiten mit Branches:**
    Im Anschluss folgt auch schon das Kapitel zum Branching mit Git. Erklärt
    wird, wie man Branches erstellt, wofür sie gut sind und wie sie
    zusammengeführt werden können. Dabei werden sowohl normale Merges mit und
    ohne Merge-Konflikten behandelt als auch das Rebasen von Branches.

 * **Verteilte Repositorys:**
    Verteilte Repositorys kamen in den ersten drei Kapitel noch nicht vor. In
    diesem Kapitel wird erläutert wie man mit Remote-Repositorys arbeitet und
    wie ein kleiner Workflow mit drei Repositorys aussieht.

 * **Git Hosting:**
    Das fünfte Kapitel befasst sich mit dem Einsatz von Git auf dem Server. Im
    Wesentlichen wird der Workflow GitHub und GitLab erläutert, sodass am Ende
    bekannt ist, wie man ein kleines Repositorys maintaint und wie man Änderungen
    mittels Pull-Requests bei anderen Projekten beiträgt und was dabei zu beachten
    ist. Kurz thematisiert werden auch GitHub Actions und GitLab CI/CD,
    die beide zur kontinuierlichen Integration und zum kontinuierlichen Deployment
    nützlich sind.

 * **Workflows:**
    Wichtig beim täglichen Einsatz von Git in einem Projekt ist die Auswahl
    eines passenden Workflows. Hierbei werden verschiedene Workflows
    beispielhaft genannt, die in einem Projekt eingesetzt werden können, etwa
    Git Flow.

 * **Hooks:**
    Mit Hooks lassen sich einige Dinge automatisieren, wenn etwa Commits
    getätigt werden. Zweites Nebenthema sind Git-Attribute.

 * **Umstieg von Subversion:**
    Wenn der Leser nicht gerade völliger Neuling ist, dann ist der Umstieg von
    Subversion wahrscheinlich. Das Kapitel behandelt die wesentlichen Unterschiede
    in den Konzepten und in den Kommandos und bringt auch eine Anleitung wie man
    ein Subversion-Repository in ein Git-Repository überführt.

 * **Tipps und Tricks:**
    Das Kapitel befasst sich mit einer Sammlung von Tipps und Tricks die
    man mehr und weniger regelmäßig gebrauchen kann. Thematisiert werden unter
    anderem die Fehlersuche mit Bisect, das Arbeiten mit Patches und das
    Neuschreiben der Historie mit Filter-Branch.

 * **Grafische Clients:**
    Während die übrigen Kapitel kaum auf grafische Clients eingeht, ist dies bei
    diesem Kapitel anders. Hier erfährt man einen groben Überblick über einige der zur
    Verfügung stehenden grafischen Clients von Git.

 * **Nachvollziehbare Git-Historien:**
    Das erste der beiden neuen Kapiteln befasst sich mit meinem aktuellen Lieblingsthema:
    Wie muss man vorgehen, um eine nachvollziehbare Git-Historie zu erstellen.
    Das Kapitel umfasst sowohl Tipps und Hinweise, wie die Commit-Messages
    auszusehen haben sollten, als auch wie groß bzw. klein die Commits sein sollten.

 * **DevOps:**
    Neu in der dritten Auflage ist das 12. Kapitel. Hier wird das Thema
    DevOps kurz und kompakt zusammengefasst, wofür Git das grundlegende
    Werkzeug ist.

 * **Frequently Asked Questions:**
    Dieses Kapitel stellt einige Problemstellungen vor, die bei Git-Nutzern
    regelmäßig zu Irritationen führen und häufiger nachgefragt werden. Für
    viele Probleme gibt es mehrere Lösungen und teilweise wurden diese Lösungen
    schon diverse Male in den vorherigen Kapiteln behandelt. Trotzdem soll es
    hier einen kurzen Überblick über allgemeine Probleme zum Nachschlagen
    geben, der auf die entsprechenden Stellen im Buch für nähere Erläuterungen
    verweist.

## Leseprobe und Kauf

Eine Leseprobe gibt es natürlich auch. Diese findet sich [hier](https://www.mitp.de/out/media/9783747503041_Leseprobe.pdf)

Bücher lassen sich bekanntlicher weise an vielen Stellen kaufen zu einem festen
Preis. Es kostet auch in der dritten Auflage in der Print-Ausgabe 29,99 € und 25,99 €
in der digitalen Ausgabe. Wer ein Bundle aus beiden haben möchte, zahlt 34,99 €.
Die Bundle-Veriante gibt es allerdings nur auf Verlagswebseite.

Wer sich das Buch kaufen möchte, dem kann ich ein Kauf direkt auf der [mitp-Verlagsshop](https://www.mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git-oxid.html)
nahe legen, da dies für den Verlag und dem Autor (also mich) am besten ist.

Auf [Amazon findet man das Buch natürlich auch](https://amzn.to/2O02L7k).
Es ist sowohl dort als auch bei anderen Händlern ab dem 1. März 2021 verfügbar.
