---
title: "Labdoo Laptop-Spende an eine Schule in Sri Lanka"
categories: ["Sonstiges"]
date: "2015-02-05 20:45:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
slug: "labdoo-srilanka"
---

Labdoo ist ein weltweit organisiertes Projekt um bereits genutzte und nicht mehr
gebrauchte Laptops an Bildungseinrichtungen in Entwicklungsländer zu bringen,
wo sie dann von Schülern genutzt werden können.

### Labdoo

Jedes Jahr werden vor allem in den Industrieländern sehr viele Laptops gekauft.
Nach und nach liegen viele Laptops, die nicht mehr genutzt werden, in der Ecke
rum und verstauben. Währenddessen gibt es zahlreiche Länder in denen viele
jungen Menschen der Zugang zu Computern und dem Internet für Bildungszwecke fehlt.

Labdoo setzt sich dafür ein, dass genau diese Laptops, die in den Industrieländern
schon ausgedient haben, zu Bildungseinrichtungen in Entwicklungsländern gebracht
werden. Die Laptops werden zunächst in sogenannten „Hubs“ weltweit eingesammelt.
Dort werden sie dann aufbereitet. Darunter fällt unter anderem der Austausch
von möglichen defekten Teilen und das Aufspielen von Edubuntu mit zahlreichen
Software-Paketen die für die Schüler nützlich sind, etwa einem Offline-Wiki
und Lern-Software.

Im dritten Schritt folgt die Übergabe der Laptops an die Schule. Dies wird, wie
auch die anderen Aufgaben, von freiwilligen Mitarbeitern übernommen. Menschen,
die um die Welt reisen, können sich bei Labdoo registrieren um Laptops an eine
Schule zu bringen. Die Kosten für den Transport trägt die jeweils reisende
Person. Meistens wird allerdings sowieso das zur verfügungstehende Gepäck genutzt,
um die Laptops zu transportieren. Wichtig im Labdoo-Projekt ist auch, dass der
Transport möglichst umweltfreundlich abläuft. Daher wird bevorzugt, den Platz
im Koffer zu nutzen, statt ein Paket abzuschicken. Darüber hinaus sieht man bei der
persönlichen Übergabe der Laptops noch viele fröhliche Kinder.

Es können eine Vielzahl an Laptops gespendet werden. Geeignet sind Laptops ab
dem Baujahr 2001 (Pentium 4), eBook Reader, Tablet-PCs und auch solche Geräte,
die noch funktionsfähig sind, aber eine Entsorgung zu schade wäre.

Neben den vollständigen Geräten werden auch Ersatzteile gebraucht, wie Arbeitsspeicher
in jeglicher Variante (für Laptops), Laptop-Festplatten, Tastaturen und Mäuse, Netzwerk-Geräte
oder auch Ladegeräte für Laptops und Taschen.

Die Laptops oder andere Teile können an sogenannten „Labdoo Hubs“ abgegeben werden,
wo die Laptops dann weiter aufbereitet werden und anschließend an die Schulen gehen.
Eine Liste der Hubs in Deutschland findet sich [auf der Labdoo-Webseite](http://www.labdoo.org/de/hubs?country=de).
Besonders im Rhein-Ruhr-Bereich in Nordrhein-Westfalen sind viele Hubs vorhanden.
Jeder Spender bekommt regelmäßig Status-Akualisierungen, damit jeder Spender
Bescheid weiß, in welchem Status sich der Laptop gerade befindet. Der Spender
bekommt mit, wenn der Laptop fertig eingerichtet ist, auf dem Weg zur Schule ist,
oder auch, wenn es final an einer Schule angekommen ist. Auf der Webseite sind
dann Fotos zu sehen, wie die Laptops an der Schule übergeben worden sind und wie
sie im Einsatz ist. Der Spender kann daher sicher gehen, dass der Laptop auch
dort angekommen ist, wo es hinsollte.

### Mein Labdoo-Zyklus

Ich selbst habe zum ersten Mal im Jahr 2012 in einem [Ikhaya-Artikel](http://ikhaya.ubuntuusers.de/2012/02/01/hilfsprojekt-www-labdoo-org-hilft-elektroschrott-zu-vermeiden-alte-laptops-fuer-schulen-in-entwi/)
vom Projekt Labdoo gelesen. Im November 2013 und 2014 habe ich dann nochmal
jeweils mit Ralf Hamm vom Labdoo-Projekt auf der OpenRheinRuhr in Oberhausen
gesprochen. Ralf mat mich dort dann direkt motiviert, doch auch mitzuhelfen.
Da meine Eltern aus Sri Lanka stammen und für uns sowieso im Januar 2015 eine
Reise nach Sri Lanka anstand, war die Situation natürlich ideal. Ich selbst habe
den Weg gewählt, möglichst alles selbst zu erledigen.

#### Schritt 1: Laptops beschaffen

Zunächst mussten also Laptops beschafft werden. Dies war zum Glück die leichteste
Aufgabe, da sowohl in meinem Büro als auch im Büro meines Arbeitskollegen
jeweils ein alter Arbeits-Laptop stand, die nicht mehr in Benutzung waren. Eine
kurze Anfrage beim Arbeitgeber hat gereicht, damit diese Laptops gespendet
werden dürfen, was ohne Probleme akzeptiert wurde. Mein Arbeitgeber, die [otris software AG](http://www.otris.de/otris/de/home.jhtml)
hat somit zwei ausgediente Laptops zur Verfügung gestellt. Im Gegenzug werden
Firmen auf der [Spenden-Seite bei Labdoo](http://www.labdoo.org/de/content/spender) genannt.

#### Schritt 2: Schule finden

Das Finden einer geeigneten Schule war interessanterweise das Schwierigste des
ganzen Projektes. In Sri Lanka sind einige Schulen in den größeren Städten meistens
schon mit Computern ausgestattet, da die Regierung dafür Mittel zur Verfügung
stellt. Bei diesem Schritt bekam ich Hilfe von meiner Mutter, welche die Idee
vom Projekt ebenfalls gut findet und mich dabei unterstützte. Nach einigen Wochen
herumtelefonieren fand meine Mutter schließlich über eine frühere Lehrerin den
Kontakt zur Schule und dessen Schulleiter. 

Die Schule liegt abseits von den Großstädten, etwa 100 km Luftlinie der Stadt
Jaffna im Norden Sri Lankas. Die Schule „Thadchanamaruthamadhu G.T.M.S“ liegt
in der Kleinstadt Madhu. Die selbe Stadt wurde etwa eine Woche vor meinem Besuch
von Papst Franziskus besucht. 

#### Schritt 3: Laptops aufbereiten

Da beide Laptops sich in einem guten Zustand befanden, musste lediglich das System
aufgespielt werden. Die Installation verläuft Image-basiert, welches im [FAQ von Labdoo](http://www.labdoo.org/de/node/1725#FAQ03)
beschrieben ist. Konkret wird ein Edubuntu-System aufgespielt, in dem einige
Software-Pakete für Bildungszwecke vorinstalliert ist.

#### Schritt 4: Transport ins Zielland

Im vierten Schritt folgte der Transport in das Zielland Sri Lanka. Sicher verpackt
im Reisegepäck zwischen der eigenen Kleidung. Bei einer Reise von drei Personen
und 90 kg Reisegepäck nehmen zwei ältere Laptops inklusive Docking-Station schon
fast 8 kg ein.

#### Schritt 5: Transport zur Schule

Der Transport zur Schule inklusive An- und Abfahrt nahm insgesamt an die 12 Stunden
in Anspruch. Früh morgens um 7 Uhr ging es auf dem Weg von Jaffna in die Stadt
Madhu. Mit einigen Umwegen und einigen Besichtigungen auf dem Weg, kamen wir
schließlich zur Mittagszeit an der Schule an. Der Weg dorthin war lang und steinig. Viele Straßen
in der Nähe der Schule hatten keinen festen Belag oder waren übersäht von Schlaglöchern.
Rund um die Schule sind fast keine Häuser zu sehen. Man könnte schon sagen, dass
die Schule „mitten im nirgendwo“ liegt. Auch die Schüler haben somit einen weiten
Weg zur Schule.

<a href="/graphics/labdoo_way_to_school.webp" data-lightbox="image" data-title="Der Weg zur Schule war übersäht mit Schlaglöchern.">
	<img src="/graphics/labdoo_way_to_school.webp" style="margin:20px;">
</a>

Angekommen an der Schule, wurden wir zunächst vom Schulleiter begrüßt.
Nach einer kurzen Vorstellung folgte dann die Übergabe der Laptops. Die Schule hat,
zu meiner Überraschung, sich sehr viel Mühe gegeben. Zunächst folgte einer Versammlung
der ganzen Schule auf dem Hof. Dies umfasste knapp 300 Schüler und Schülerinnen
sowie etwa 20 Lehrkräfte. Dort folgte zunächst eine Dankensrede von einem Lehrer
sowie des Schulleiters. In der Rede bedankten sich beide Personen bei uns, für
die Übergabe der Laptops. Kurz danach sollten auch wir ein Wort an die Schüler
richten. Auf Grund meiner eher mittelmäßigen tamilischen Sprachkenntnisse ist
zum Glück mein Vater eingesprungen und hat ein paar Worte an die Schüler gerichtet.
Die Schüler bedankten sich dann mit einem „Thank you, Sir“ im Chor.
Anschließend folgte noch die Einweisung in die Nutzung der Laptops an einen
Science-Lehrer durch mich.

<a href="/graphics/labdoo_meetup.webp" data-lightbox="image" data-title="Versammlung.">
	<img src="/graphics/labdoo_meetup.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_handover.webp" data-lightbox="image" data-title="Übergabe der Laptops durch meiner Schwester und mir.">
	<img src="/graphics/labdoo_handover.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_pupils.webp" data-lightbox="image" data-title="Die Schüler.">
	<img src="/graphics/labdoo_pupils.webp" style="margin:20px;">
</a>

### Fazit

<a href="/graphics/labdoo_classroom.webp" data-lightbox="image" data-title="Der Klassenraum ist eher bedürftig eingerichtet.">
	<img src="/graphics/labdoo_classroom.webp" style="margin:20px;">
</a>

<a href="/graphics/labdoo_using.webp" data-lightbox="image" data-title="Die Schüler nutzen den Laptop.">
	<img src="/graphics/labdoo_using.webp" style="margin:20px;">
</a>

Die Spende der Laptops inklusive des Vorbeibringen der Laptops an der Schule
war sehr interessant. Ich kann nur jeden dazu ermutigen, ebenfalls zu helfen.
Jeder der einen alten, nicht mehr genutzten Laptop zur Verfügung hat, sollte
sich Gedanken machen, ob man diesen nicht spendet. Gegenüber anderen Projekten wie
„One Laptop per Child“ liegt hier die Besonderheit, dass nicht extra Hardware
produziert werden muss, sondern bereits ausgediente Laptops wiederverwendet werden.
Der Transport verläuft dabei grundsätzlich unter geringem finanziellen Aufwand.
Es ist ebenfalls toll zu sehen, wie sich die Schule und die Schüler freuen, wenn
sie Laptops bekommen. Wichtig ist zudem, dass möglichst viele von dieser Möglichkeit
der Spende wissen. Das umfasst nicht nur technik-affine Personen, sondern
auch „normale“ Personen, die alte Laptops ungebraucht zuhause liegen haben.
