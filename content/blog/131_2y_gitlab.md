---
title: "Year 2 at GitLab: Let's talk about Growth"
date: "2022-04-11T16:50:00+02:00"
categories: ["Sonstiges"]
---

For some reason, it's somewhat common to write about another blog post after
another anniversary at GitLab. I can certainly recommend [dnsmichi's
blog post](https://dnsmichi.at/2022/03/02/2-years-all-remote-and-2022-vision/)
for second year at GitLab which he published about a month ago.

Last year I posted [my blog post for my first
year](/blog/2021/04/12/one-year-at-gitlab-7-things-which-i-didnt-expect-to-learn/)
covering seven things which I didn't expect to learn. I certainly did *not*
expect to get so much feedback from inside and outside the company.  For
example, I criticized former colleagues who referred to me in a professional
context as "the other colleague" to other customers as they weren't bothered to
learn to pronounce my full name properly. To my surprise: One previous coworker
reached out and apologized as he recognized himself!

Anyway: Some close and not so close friends were jokingly asking me recently
if I'm already looking for my next job because I didn't last longer than 1,5y
in the last four of five jobs I had before joining GitLab.

One of the reasons, but not necessary the most important topic, why I switched
jobs frequently in the past is also the topic of this blog post: **Growth**.

In my career, I generally want to focus on learning and growing my skills. There
are two types of growth: the growth of the company itself and how my personal
growth is valued and is supported.

## Growth: Personal Growth

As I mentioned earlier, it's really important for me that the company is interested
in growing the skills of their employees. There are multiple relevant ways:

- give them  *time* to develop themselves
- give them *budget* to develop themselves
- managers should talk generally and regularly about career development plans

In previous companies I didn't get that much support as I hoped. Sure, there were
some sorts of support here and there. However, none of them was really strategically
and most of the time it was only handled as a cost center and not as an investment.

From one of my previous employers, I even heard something like: "If we individually
support you, you'll use the gained knowledge to go somewhere else!". And guess
what: I didn't get the overall support in general … so I eventually left.

What many companies still miss, though, is that they get a return on investment
over time. Happy employees who are being developed, who learn new stuff, who
then work more efficiently and with a better focus and will deliver better results.

GitLab supported me one way and the other. While we do have access to good
self-paced learning content like on [LinkedIn Learning](https://www.linkedin.com/learning/)
and [O'Reilly Learning](learning.oreilly.com/) which I could basically watch,
listen and read all the day. We also do have a [Growth and Development Benefit](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/),
which I have not used for now.

It's important to mention though that it's not only time and money. Getting bills
paid and access to these kinds of services is one thing. The other thing is the
actual support from your manager and the manager's above.

As I've written earlier, it can have a positive impact for the company to support the development
of employees. In my case, that resulted in different skills which were directly
usable when engaging with customers. Additionally, I think these
are also the reasons why I ended up being awarded the **Top Solutions Architect**
at GitLab for our last fiscal year at our annual Sales-Kick-Off.

I personally will not join a company which doesn't care in the development of
their employees – regardless of their age and role!
