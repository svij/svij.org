---
title: "Jenkins 2.0 Talk auf der committerconf 2016"
categories: ["Events"]
date: "2016-10-18 22:30:00+02:00"
tags: ["ubuntuusers-planet"]
slug: "jenkins-20-talk-auf-der-committerconf-2016"
toc: true
---

Am 17. und 18. Oktober 2016 fand im Essener Unperfekthaus die dritte [committerconf](https://committerconf.de)
statt. Ich selbst war zum ersten Mal da, durch den relativ kurzen Anfahrtsweg und
der verfügbaren Zeit war es zum Glück relativ einfach möglich.

Die committerconf ist im Gegensatz zu den anderen Open Source Konferenz die ich
sonst besuche eine kommerzielle Veranstaltung. Ein normales Ticket kostet da 400€,
dementsprechend waren die Besucher auch eher welche die das Ticket von ihrem Arbeitgeber
bezahlt bekommen haben. Die Konferenz verfolgt ein interessantes Konzept: Jeder der
ein Ticket kauft, bekommt "Punkte", die an eingereichten Talks vergeben werden können. Je nach
dem ob Talks gehört werden wollen, werden die Talks kurz vorher angenommen.

Ich selbst hatte einen Talk eingereicht mit dem Titel "Automation mit Jenkins 2.0"
wo ich zunächst einen Einstieg in Jenkins (1.x) gegeben habe, gefolgt von den
neuen Features die Jenkins 2.0 eingeflossen sind. Die Slides des Talks habe
ich auf [Speakerdeck](https://speakerdeck.com/svij/automation-mit-jenkins-2-dot-0)
hochgeladen. Wer den Talk noch live sehen will: Den werde ich schon bald am 5. November
auf der [OpenRheinRuhr](http://programm.openrheinruhr.de/2016/events/458.de.html)
halten, gefolgt von der [UbuCon Europe](http://ubucon.org/en/events/ubucon-europe/talks/#jenkins)
am 19. November. Die OpenRheinRuhr findet wieder in Oberhausen statt, die UbuCon Europe ebenfalls
im Unperfekthaus in Essen. Beide Veranstaltungen kann ich nur empfehlen, letzteres
nicht nur (oder vor allem?), weil ich es organisiere. ;)

<script async class="speakerdeck-embed" data-id="d9d6d99cbe204e5891600a80507bffc7" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

Zurück zur committerconf: Die Besucheranzahl war mit geschätzt 40 Leuten
vergleichweise klein, dafür konnte man umso mehr mit vielen verschiedenen Leuten
sprechen und sich austauschen. Auch das [Programm](https://committerconf.de/2016/schedule)
war eher übersichtlich. Nichtsdestotrotz konnte ich einiges neues lernen, vor allem
in den Einsteiger-Workshops zu Vim (nicht im Programm gelistet) und systemd, wo
ich eigentlich dachte, ich hätte genug "Einsteigerwissen". So kann man sich irren! ;)
