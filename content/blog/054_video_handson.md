---
title: "Video Hands on bq Aquaris E4.5 Ubuntu Edition"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-02-16 18:30:00+02:00"
slug: "video-hands-on-bq-aquaris-e45-ubuntu-edition"
toc: true
---

Nach knapp über eine Woche im Besitz des „bq Aquaris E4.5 Ubuntu Edition“, also
des ersten Ubuntu Phones, habe ich gestern ein kleines „Hands on“ Video aufgenommen.
In knapp 17,5 Minuten gebe ich dort einen Überblick in den Aufbau der graphischen
Oberfläche und die Gesten. Darunter auch ein genauer Blick auf die Scopes die
im Standard aktiviert sind. Ich konnte natürlich nicht alle Einzelheiten abdecken in dem
Video, denn sonst wäre es vorallem zu lang geworden.

Ich denke, die meisten werden davon einen guten Einblick in das System bekommen,
wie es läuft, wie es funktioniert und wie die gängige Nutzung ausschaut. Das
Video habe ich [hier auf YouTube](https://www.youtube.com/watch?v=k6SkaqtKtbg)
hochgeladen.

<iframe width="560" height="315" src="https://www.youtube.com/embed/k6SkaqtKtbg" frameborder="0" allowfullscreen></iframe>

Ein erster ausführlicher Test-Bericht folgt im Laufe dieser Woche.
