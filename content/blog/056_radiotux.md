---
title: "Ubuntu Phone und ich bei RadioTux"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-02-26 19:00:00+02:00"
toc: true
---

Am 6. Februar war ich beim [Ubuntu Phone Insider Launch Event](/blog/2015/02/09/impressionen-vom-ubuntu-phone-insider-event/),
wo ich bekanntlich auch das Smartphone in die Hände bekommen habe. Mittlerweile
habe ich auch schon meinen langen Testbericht in Ikhaya [veröffentlicht](http://ubuntuusers.de/ikhaya/6623/).
Es ist ziemlich lang geworden, also nehmt euch Zeit beim Lesen.

Wer nicht so viel Lesen mag und lieber etwas hört, dem kann ich die [Februar-Ausgabe](http://www.radiotux.de/index.php?/archives/7994-RadioTux-Sendung-Februar-2015.html) von RadioTux empfehlen. Zu Beginn erzählt
Leszek von Tizen und das erste Tizen Smartphone, dem Samsung Z1. Im zweiten Teil,
ab 1:08:53 melde ich mich zu Wort. Mit Ingo habe ich dann über das Launch Event
in London, über das System, sowie über das Smartphone selbst gesprochen.

Für mich war es das erste Mal bei einem Podcast. Da ich selbst nur ein relativ
günstiges Mikrofon bzw. Headset besitze, ist die Sound-Qualität dementsprechend
nicht allzu gut. Zu Beginn war ich vorallem nervös, das hat sich hinterher mehr
oder minder gelegt. Bitte verzeiht mir übrigens die vielen "ähms" ;-). Insgesamt beträgt
die Länge meines Segments über Ubuntu Phone etwa eine Stunde. Inhaltlich habe ich
größtenteils nicht viel neues erzählt, sofern man meine Blog-Posts und Ikhaya-Artikel
bereits gelesen hat.

Viel Spaß beim Hören!
