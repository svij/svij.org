---
title: "Canon-DSLR als Webcam mit HDMI-Output und Magic Lantern"
categories: ["Sonstiges"]
date: 2021-08-29T18:50:00+02:00
tags: ["ubuntuusers-planet"]
vgwort: ""
---

# Problem

Vor etwas weniger als [einem Jahr](/blog/2020/11/22/canon-dslr-als-webcam-auf-linux-systemen-verwenden/)
bloggte ich bereits über mein Webcam-Setup, wo ich beschrieb, wie man eine
Canon DSLR per Mini-USB unter Linux zum Laufen bringt. Das funktioniert zwar
technisch, hatte aber ein paar Einschränkungen und Dinge, die mich nervten,
weshalb ich das Setup wieder umgebaut habe. Hauptproblem: Das Setup braucht
ein geladenes Kernel-Modul und bei jedem Anschalten der Kamera musste eine lange
Befehlskette neu gestartet werden. Zudem war es nur das Preview-Bild was
abgegriffen wurde. Das entsprach dann einer Auflösung von lediglich 960x640.

# Hardware

Praktischer ist daher die Nutzung des HDMI-Ausgangs über ein HDMI-Capture Stick.
Ich nutze dafür den Elgato Cam Link 4K. Der Stick wird direkt als Webcam identifiziert
und kann ohne Probleme in jegliche Videokonferenztools genutzt werden.

Theoretisch wäre dieser Blogpost schon zu Ende. Allerdings fiel nach dem ersten
Betrieb schon ein Problem auf: Über den HDMI-Ausgang wirft meine Canon 700D kein
sauberes Bild aus, da dort typische Kamera Einstellungen und Darstellungen auf
dem Bildschirm dargestellt werden, die auch über HDMI ausgegeben werden. Größeres
Problem allerdings: Die Kamera schaltet den Live-View Modus nach 30min automatisch
ab. Somit ist es nicht sonderlich gut nutzbar, dies war über USB nämlich kein
Problem.

# Magic Lantern

Zu dem Problem gibt es aber eine weitere Lösung: Mit [Magic Lantern](https://magiclantern.fm/),
eine Open Source Firmware für diverse Canon Kameras. Die Firmware wird dabei
von der SD-Karte gestartet, nachdem man zuvor das passende Firmware-Update für
die Kamera selbst installiert hat. Scheinbar gibt es aber für Magic Lantern selbst
seit 3 Jahren keine Updates mehr. Für meinen Zweck ist das noch verkraftbar.

In den Einstellungen von Magic Lantern lassen sich dann extremst viele Konfigurationen
einstellen, um mehr aus der Kamera herauszuholen zu können. In meinem Fall war ich
da lediglich daran interessiert, das Auto-Abschalten des Live-Views zu deaktivieren.

# Einschränkungen

Auch mit diesem Setup gibt es ein paar Einschränkungen, sonst wäre es ja langweilig.
So hat das Live-View Bild im P-Modus der Kamera eine schwarzen Kasten, der auch so
über HDMI ausgegeben wird. Im Automatik-Modus ist das zum Glück nicht der Fall,
aber auch da hat man zumindest am linken und rechten Rand schwarze Balken, was
dem Kamera-Format geschuldet ist. Dies kann man, wenn man möchte, über Software wie
OBS croppen, aber das möchte ich in meinem Fall nicht.

Reguläre Webcams sind immerhin mittlerweile lieferbar zu bezahlbaren Preisen, im
Gegensatz zum letzten Jahr. Ich bleibe nun erstmal bei dem Setup, alles andere
wäre schließlich zu einfach (und zu günstig).
