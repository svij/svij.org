---
title: "Certified Kubernetes Application Developer: Prüfungsvorbereitung und Absolvierung"
categories: ["Sonstiges"]
date: 2021-12-12T18:00:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: "https://vg02.met.vgwort.de/na/e9841ad23ecd425ba43381e9ddca8f19"
---

Wer meinen Blog ein wenig liest und mich ein wenig kennt, weiß, dass ich aktuell
relativ viel mit Kubernetes „herumspiele“. Siehe meinen Blogpost zum [Homeserver-Setup
in 2021](/blog/2021/11/28/homeserver-setup-2021-k3s-mit-flux-und-helm/).

Dieser Blogpost geht ein wenig darauf ein, warum ich die Zertifizierung
„Certified Kubernetes Application Developer“ (CKAD) gemacht habe, wie ich mich
darauf vorbereitet habe und warum das Drumherum um die Prüfung total nervig
war.

Die CKAD-Zertifizierung ist die Zertifizierung, die sich eher an Entwickler
richtet, während die „Certified Kubernetes Administrator“ Zertifizierung (CKA,
ohne D) und „Certified Kubernetes Security Specialist“ Zertifizierung (CKS)
sich eher an Administratoren richtet.

Der Spaß kostet mittlerweile $375 und findet Online statt. Es gibt immer allerdings
immer wieder Angebote, wo es weniger kostet.

# Warum eigentlich eine Zertifizierung?

Die allgemeine Frage, warum man eine Zertifizierung braucht und auch für wen
sich eine Zertifizierung lohnt, ist immer so eine Frage. So einfach lässt sich
das nicht beantworten, da dort viele Faktoren reinspielen. Es gibt einige
(vor allem junge) Leute, die mehrere Zertifizierungen „sammeln“ ohne
je praktische Erfahrung gesammelt zu haben. Sieht zwar schön aus, hat nur keinen
wirklichen Mehrwert. Die Praxis ist wichtig! Andersrum gilt das gleiche:
Wer tagtäglich über mehrere Jahre mit Tools arbeitet, für die es
Zertifizierungen gibt, braucht es vermutlich auch eher nicht.

Ich, für meinen Teil, hatte verschiedene Gründe, weshalb ich mich ran gewagt habe:

 * Ich wollte neben meiner praktischen Erfahrung für den Heimgebrauch wissen,
   was mir grundsätzlich noch fehlt.
 * Ich muss generell für meinen Job wissen, wie Kubernetes grundsätzlich
   funktioniert und wie DevOps-Teams ihre Anwendungen in den Cluster bekommen.
 * Ich hab nach Absolvierung meines Studiums vor ein paar Jahren
   Prüfungssituationen vermisst. (Wer hier Ironie findet, darf sie gern
   behalten.)
 * Es ist generell bei uns auf der Arbeit gewünscht, sich fortzubilden, die
   Kosten des Kurses wurden auch übernommen.

# Prüfungsvorbereitung

Am wenigsten Zeitaufwand für die Vorbereitung auf die Prüfung hat man, wenn man
gute Erfahrung in Kubernetes hat. Welch Überraschung. Konkret geht es allerdings
in der Prüfung ja darum, vor allem Anwendungen auf ein Kubernetes-Cluster zu
deployn und auch Änderungen von deployten Anwendungen durchführen zu können, wo
etwa ein fehlerhaftes Deployment schon liegt.

Die CNCF überarbeitet regelmäßig die Curricula für alle Zertifizierungen, und
ist somit meist recht aktuell für eine aktuelle Kubernetes Version. Die
aktuellen Informationen, inklusive der Gewichtung aller Themenblöcke
finden sich [in diesem GitHub-Repository](https://github.com/cncf/curriculum).

Meine Prüfung fand im Juni 2021 statt, war also noch ein etwas älterer Stand.

Zur Prüfungsvorbereitung habe ich zwei Dinge genutzt: [ein Video-Kurs von
Sander van
Vugt](https://www.sandervanvugt.com/course/certified-kubernetes-application-developer-ckad-video-course/)
und das Buch [Certified Kubernetes Appliaction Developer Study
Guide](https://www.oreilly.com/library/view/certified-kubernetes-application/9781492083726/).
Ich habe Zugriff über den Arbeitgeber auf [O'Reilly Learning Plattform](https://www.oreilly.com/),
wo beides enthalten war. Weiteren Kosten sind daher nicht entstanden.

Ich hab mir zwar beides angeschaut, allerdings vieles nur überflogen, um möglichst
herauszufinden, was es bei der Prüfung zu beachten gibt und die Übungen zu machen.
Das eigentliche Doing war mir zumindest größtenteils bekannt. Ein paar Tipps
meinerseits folgen gleich.

Wenn man die Grundlagen und die Themen aus dem Curriculum so weit beherrscht, empfehle
ich strengstens ein paar Übungsaufgaben zu machen und die Zeit dazu auch zu stoppen.
In der Prüfung hat man zwei Stunden Zeit, das ist knapp und man hat nur
wenig Zeit bei Problemen etwas zu debuggen.

Folgende zwei GitHub-Repositories mit Beispielaufgaben und -lösungen habe ich
verwendet, um zu verifizieren, dass ich es so weit kann:

 * [dgkanatsios/CKAD-exercises](https://github.com/dgkanatsios/CKAD-exercises)
 * [bmushko/ckad-prep](https://github.com/bmuschko/ckad-prep)

Wer diese Aufgaben ohne Probleme in der Zeit lösen kann, sollte keine Probleme
bei der Prüfung haben.

# Prüfung

Zur Prüfung gibt es zwei Themen zu bereden:

 * Die „Vorbereitung“ auf die technische Durchführung der Prüfung ist super ätzend
 * Tipps und Hinweise zur Prüfung selbst

Nun, die Prüfung wird komplett Online durchgeführt. Man muss so gut wie alles
vom Tisch und den Wänden nehmen, damit man nicht spicken kann. Das wird ausführlich
überprüft, in dem man von einer Person im Chat unterhält. Leider ist die Person
wenig verständlich gewesen bei mir, sodass sich das ganze etwas gezogen hat und
ich ganze 30 Minuten damit beschäftigt war zu „beweisen“, dass ich einen leeren
Tisch und leere Wände habe.

Zusätzlich muss man seinen Desktop komplett freigeben über eine Chrome
Extension, weil nur ein Tab mit der Kubernetes Dokumentation offen sein darf
und die Prüfung selbst. Andere Anwendungen dürfen nicht laufen. Ja, auch das
war super nervig, da ich den „Taskmanager“ zeigen sollte, um zu zeigen, dass
nichts läuft. Ich sollte dann noch „alle Anwendungen“ beenden, obwohl nur noch
KDE/Plasma Desktop-Teile liefen. Die hätte ich gerne beenden sollen. Ach ja und
die Lautsprecher, die bei mir quasi fix montiert sind, sollte ich auch
runternehmen. Ich durfte dann immerhin über die Webcam zeigen, dass ich es
abgestöpselt habe.

Nach diesen 30 Minuten hatte ich schon echt kaum Lust auf die Prüfung, aber da
musste ich dann durch. Die Prüfung ist komplett praktisch. Sodass man diverse
Umgebungen gestellt bekommt, wo man die Übungen absolvieren muss. Das hat immerhin
problemlos funktioniert.

Folgende Tipps allerdings noch meinerseits, damit die Prüfung erfolgreich absolviert
werden kann:

 * Versucht so viel YAML-Code durch `kubectl` generieren zu lassen wie möglich.
  Das spart sehr viel Zeit, die sonst fehlen würde. Außerdem wollt ihr nicht
  YAML korrigieren wollen in so einer Situation. (Und auch sonst nicht.)
 * Wenn ihr Probleme mit dem Lösen von Aufgaben habt, überspringt es zunächst
 und macht alles, was ihr ohne Probleme schafft.
 * Je mehr Übungen man vorher macht, desto einfacher wird es in der Prüfung selbst.
 * Macht euch gewohnt an die Kubernetes Dokumentation und wie man da über die
 interne Suche was findet. Googeln darf man nicht.

Innerhalb von 24h bekommt man die Resultate. Man braucht 66 von 100 Punkten. Ich
habe 87 erreicht. Unabhängig von der eher nervigen Prüfungsorganisation vor
dem Ablegen der Prüfung, habe ich gut was gelernt, da ich in meinem Homelab ja
auch nicht alles nutze und brauche.

