---
title: Bessere Git Diffs im Terminal mit delta
categories: ["Git", "Kurztipps"]
date: "2022-01-23T21:10:00+01:00"
tags: ["ubuntuusers-planet"]
---

Vor über vier Jahren schrieb ich einen Blogpost über [„Schönere Git Diffs im Terminal mit diff-so-fancy“](https://svij.org/blog/2018/01/12/schonere-git-diffs-im-terminal-mit-diff-so-fancy/).
Mit [diff-so-fancy](https://github.com/so-fancy/diff-so-fancy) ist es möglich
schönere bzw. bessere Diffs mit Git anzeigen zu lassen. Als Vergleich
dient hier natürlich das klassische `diff` Kommandozeilentool, was vom System
mitgeliefert wird und nur sehr wenig optisch die Unterschiede zwischen zwei
Versionen hervorhebt.

Kürzlich bin ich auf [delta](https://github.com/dandavison/delta) gestoßen, was
noch einmal deutlich besser als diff-so-fancy ist und zwar für mich vor allem
für die bessere farbliche Hervorhebung und für die Nutzung von Syntaxhervorhebung,
die abhängig der verwendeten Programmiersprache ist. Darüber hinaus gibt es auch
noch Zeilennummerierungen und ein optionales Side-by-Side View.

![Ein git diff mit delta](/graphics/delta-diff.webp)

Ich kann jeder Person, die `git diff` auf der Kommandozeile ausführt, nur empfehlen
mal ein Blick darauf zu werfen. [Die Dokumentation](https://dandavison.github.io/delta/introduction.html)
und das [GitHub-Repository](https://github.com/dandavison/delta) listen alle
Funktionen auf, zeigen mehr Screenshots und listet auf, wie man es installiert.
Unter den gängigen Linux-Distributionen, Windows und macOS ist das Tool unter
`delta` bzw. `git-delta` in den Paketquellen enthalten. Einzig in Ubuntu und Debian
muss man sich das Debian-Paket händisch installieren. Mehr dazu in der Dokumentation
zur [Installation](https://dandavison.github.io/delta/installation.html) und
[Konfiguration](https://dandavison.github.io/delta/configuration.html).
