---
title: "Fragen und Antworten zu Ubuntu Phone"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-02-16 19:45:00+02:00"
toc: true
---

Am 5. Februar hatte ich in [einem Artikel](/blog/2015/02/05/ubuntu-phone-insider-event-in-london-am-6-februar/)
meinen Besuch beim „Ubuntu Phone Insider Launch Event“ angekündigt. Dort habe
ich angeboten, dass ich offene Fragen zu Ubuntu Phone und zu dem bq Aquaris E4.5
beantworte. Einige E-Mails mit Fragen sind vor und auch kurz nach dem Event
eingetroffen. Zeit sie zu beantworten hatte ich bislang nicht sonderlich, weshalb
ich es hier mal tue.

__Ist es möglich, Ubuntu Phone auf dem bq Aquaris E6 zu Installieren?__

Nein, zumindest nicht offiziell. Der Hersteller bq hat gegenüber OMG! Ubuntu!
[bestätigt](https://twitter.com/omgubuntu/status/566981897893732353), dass sie keine Pläne
für weitere Ubuntu Phones haben.

__Erscheint ein Ubuntu Phone mit einem Display, welches größer ist als 4,5 Zoll?__

Ja, das Meizu MX4 wird mit (u.a.) mit Ubuntu ausgeliefert. Siehe auch [das Interview mit Cristian Parrino](https://svij.org/blog/2015/02/11/cristian-parrino-from-canonical-about-the-release-of-ubuntu-phone-and-future-plans/).
Erscheint März oder April.

__Lässt sich der Akku vom bq tauschen?__

Nein, dieser ist wie bei vielen anderen aktuellen Smartphones fest-verbaut. Ausnahmen betätigen die Regel.

__Kann ich kontrollieren und/oder begrenzen, ob bzw. welche Daten eine 
App versendet? Und an wen? Bzw. mir Beispiel-Datensätze anzeigen lassen?__

Das ist leider nicht möglich.

__Läuft das GPS unabhängig von WLAN- oder Mobilfunkzellen?__

Es lässt sich in den Einstellungen die "Standortbestimmung" und "GPS" einzeln
aktivieren. Es scheint also für mich kein großer Unterschied zu den anderen
mobilen Betriebssystemen bezüglich A-GPS zu herrschen.

__Kann man das Gerät dann in Castrop-Rauxel mal besichtigen?__

Natürlich. :-)

__Kommt Meizu und LeTV auch nach Europa?__

Meizu: ja. Von LeTV hab ich bis jetzt noch gar nichts gehört.

__Ist es gerootet oder einfach rootbar, um apt-get nutzen zu können?__

Man kann den Modus „umschalten“ sodass man apt-get nutzen kann. Empfohlen wird
das allerdings nicht, da man dann keine System-Updates erhält, da diese Image-basiert
ausgeliefert werden.

__Wird das BQ mit neu geflashten Versionen später auch als PC am
Bildschirm benutzbar sein?__

Nein.

__Gibt es eine Möglichkeit ein Hotspot zu erstellen?__

Nein, auch das gibt es leider nicht.

__Wie will eine offene Navi-Software ernsthaft mit Google Maps konkurrieren?__

Diese Frage lässt sich eher schwierig beantworten. HERE Maps ist vorinstalliert,
bietet allerdings zum jetzigen Zeitpunkt keine Turn-by-Turn Navigation.

__Wie kontaktiere ich meine WhatsApp-Freunde ohne Client?__

Garnicht. Einen WhatsApp Client gibt es zur Zeit nicht. Stattdessen ist der
Messenger [Telegram](https://telegram.org/) verfügbar.

__Wird Ubuntu den Weg von Tizen gehen und eine Möglichkeit anbieten, Android-Apps zu nutzen, oder muss auf all die bekannteren und unbekannteren Apps der Android-Welt verzichtet und darauf gehofft werden, dass gute Alternativen entwickelt werden?__

Eine Unterstützung von Android Apps ist nicht vorhanden und wird auch nicht kommen.

__Warum werden die Ubuntu Phones nicht normal über den Store verkauft?__

Meine Meinung: Marketing. Durch häufigere Flash-Sales bleibt Ubuntu Phone
häufiger im Gespräch, als wenn es „leise“ in den Verkauf geht. Natürlich mit allen
Vor- und Nachteilen die ein Flash-Sale mit sich bringt. bq hat auf [Twitter beantwortet](https://twitter.com/bqreaders/status/567013970733957120), dass es eine Entscheidung von Canonical sei, auch wenn bq
die Geräte selbst verkauft.

__Wird es später auch das E5 mit Ubuntu geben?__

Nein, siehe oben.

__Könnte man ein E4.5 kaufen und Ubuntu selbst installieren?__

Ja, bq hat angekündigt Images bereitzustellen.

__Im SDK hatte ich keinen E-Mail-Client gefunden, ist einer installiert?__

Nein. Es gibt allerdings die E-Mail App „Dekko“, die noch in Beta ist und noch
eingeschränkte Funktionen hat. So kann man nur den Posteingang abrufen, allerdings
keine anderen Ordner im IMAP-Konto. Daneben gibt es noch einige Stabilitätsprobleme.


Das waren alle Fragen die ich bekam. Einige habe ich ausgelassen, wenn ich sie
etwa nicht beantworten konnte. Darunter waren zwei/drei technische Fragen.
