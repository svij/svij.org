---
title: "Tschüss freiesMagazin!"
categories: ["Open Source"]
date: "2016-12-04 10:50:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Heute Morgen ist die [letzte Ausgabe](http://www.freiesmagazin.de/20161204-dezemberausgabe-erschienen) von [freiesMagazin](http://freiesMagazin.de) erschienen. Ich habe
mich selbst nochmal mit einem letzten Artikel [über Jenkins 2.0](https://svij.org/blog/2016/12/04/automatisierung-mit-jenkins-20/) verewigt.

Es ist schade, dass das Projekt nun eingestampft wird, es war aber auch schon
vor einigen Jahren absehbar, dass die Aktivität immer weiter abnahm. Ich selbst
war von Herbst 2012 bis Herbst 2013 für etwa ein Jahr im Redaktionsteam, was ich
aus Zeitgründen damals dann verließ. Ich blieb aber bis zur letzten Ausgabe
mehr oder weniger regelmäßiger Autor, mit Beginn im Jahr 2011. Was nun auch fünf
Jahre sind.

An dieser Stelle möchte ich mich an das Team hinter freiesMagazin bedanken. Neben
meiner Mitarbeit und dem Team von ubuntuusers.de habe ich bei freiesMagazin ebenfalls
sehr viel gelernt und auch viel Zeit investiert, die sich definitiv gelohnt hat.
Sorry an Dominik, dass ich manchmal nicht ganz der schnellste war, aber beim Nachhaken,
wo denn die versprochenen Artikel und Rezensionen bleiben, war ich nicht der einzige. ;)
Ohne freiesMagazin hätte ich zudem wohl nie [mein Git Buch](https://svij.org/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/) geschrieben, was auf die Git-Tutorium
Teile aus [Teil 1 in freiesMagazin 12/2014](http://www.freiesmagazin.de/freiesMagazin-2014-12), [Teil 2 in freiesMagazin 01/2015](http://www.freiesmagazin.de/freiesMagazin-2015-01), [Teil 3 in freiesMagazin 02/2015](http://www.freiesmagazin.de/freiesMagazin-2015-02) und mit etwas Verzug [Teil 4 in 06/2015](http://www.freiesmagazin.de/freiesMagazin-2015-06)
fußt.

Also ja, mit mehr oder weniger einfachen Arbeiten die man bei Open-Source-Projekten
erledigt, kann man auch beruflich sinnvoll nutzen. Nicht nur als Referenz, dass man
was in Open-Source-Projekten tut, sondern auch mit den Kenntnissen die man durch
die Arbeit gewonnen hat. Deshalb kann ich nur ermutigen: Macht mit! Egal ob Coden,
Schreiben oder Supporten. Was am Ende bei rum kommt, kann jedenfalls nicht schaden.
Der erste Anlaufspunkt, den ich nur empfehlen kann, ist [ubuntuusers.de](https://ubuntuusers.de). Vor allem beim Newsblog [Ikhaya](https://ikhaya.ubuntuusers.de/) kann man einfach
mitmachen. Entweder die [„Artikel vorschlagen“-Funktion](https://ikhaya.ubuntuusers.de/suggest/new/) nutzen oder beim [Ubuntu-Wochenrückblick](https://wiki.ubuntuusers.de/ubuntuusers/Ikhayateam/UWR/Mitmachen/) mitmachen. Die [Mitmachen-Seite](https://wiki.ubuntuusers.de/Mitmachen/) im Wiki zeigt auch noch weitere Möglichkeiten, wie man Mitmachen kann.
Mich kann man bei Fragen zum Mitmachen natürlich auch anschreiben!

In dem Sinne: Danke und Tschüss freiesMagazin! Es war schön, dass es das Magazin
gab, und es ist schade, dass es nun zu Ende geht!
