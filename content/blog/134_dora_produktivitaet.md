---
title: "DevOps-Metriken: Nicht nur für das Management interessant"
categories: ["DevOps"]
date: 2022-11-03T10:00:00+01:00
tags: ["ubuntuusers-planet"]
---

In meiner Karriere arbeitete ich in verschiedenen Aufgabengebieten in
verschiedenen Firmen, teils intern, teils extern, teils in kleinen, teils in
größeren Firmen. Von Anfang an waren mir dabei zwei Dinge wichtig: Ich wollte
zum einen möglichst zügig meine Aufgaben erledigen, aber ich wollte zum anderen
auch, dass die Qualität stimmt. Letztendlich geht beides Hand in Hand: Wenn die
Qualität stimmt und alles ordentlich implementiert wurde, dann sind die
Aufgaben auch zügig erledigt. Wenn nicht, dann holten einem die fehlerhaften
Implementierungen schneller ein, als einem lieb war.

Die Realität sah allerdings häufig so aus: Die verschiedenen Kollegen in den
verschiedenen Firmen haben sich häufig nur auf den ersten Punkt: Hauptsache die
Arbeit ist schnell erledigt. Das hieß konkret, dass jeder für sich die
Änderungen implementiert und direkt in das Repository gepusht hat, ohne dass
ein Code-Review stattgefunden hat. Auf den ersten Blick funktioniert es ja,
also ist alles kein Problem, schließlich wurde so ja schon seit 20 Jahren
gearbeitet.

Die Probleme türmten sich häufig allerdings nachgelagert auf. Zu häufig wurde
Code mit geringer Qualität implementiert. Zu häufig führten die Änderungen zu
Problemen nach dem Ausrollen, weil viele Tests, wenn überhaupt, nur halbherzig
umgesetzt wurden. Für mich und die anderen Kollegen, denen gute Qualität auch
wichtig war, führte das dazu, dass man erst angefangen hat, die eigentlichen
Probleme zu umschiffen, bevor man dann versucht hat das Team und die
Teamleitung zu überzeugen, lieber einen Schritt langsamer zu machen, damit man
hinterher weniger Probleme hat. Der ersten und einfachsten Schritte, die wir
jeweils vorgeschlagen haben, waren die Einführung von Code-Review, sowie
durchgängiges automatisiertes Testen aller Änderungen, bevor diese die den
Hauptentwicklungsbranch gemergt werden. Erst im zweiten Schritt ging es dann an
das kontinuierliche Ausrollen der Software.

Die erste Herausforderung war es also, sowohl das Team, als auch die
Teamleitung und ggf. auch weitere Führungskräfte zu überzeugen, hier einen
Schritt langsamer zu gehen, um dafür mittel- und langfristig weniger Probleme
zu haben, um somit produktiver zu sein. Die angeführten Ideen führten bloß
selten auf Anhieb auf Begeisterung. Gängige Argumente waren dann eher dieser
Natur:

  - „Das haben wir doch schon immer so gemacht.“
  - „Wir haben so viel zu tun, wir haben keine Zeit für Code-Review.“
  - „Dieser Mehraufwand kommt doch gar nicht beim Kunden an.“

Teilweise konnte man die Bedenken ja auch gut nachvollziehen, schließlich lief
das Business ja schon ein bis zwei Jahrzehnte. Stellenweise konnten die
Bedenken zumindest für das einzelne Team ausgeräumt werden. Mal halfen einfache
Argumente, mal wurde eine Testphase gestartet. Zu Problemen kam es dann bei
diesen Testphasen allerdings auch, denn nicht selten blieben Reviews liegen
oder wurden einfach blind durchgewunken, sodass sich scheinbar nur Nachteile
gebildet haben.

Gerade diese Woche hatte ich eine Diskussion mit einem Software-Engineer eines
Kunden, der ebenfalls der Meinung war, dass Code-Reviews nicht gebraucht wird,
schließlich arbeiten sie ja schon immer so und es ist “gut”. Auf die Frage,
was denn “gut” heißt, hieß es nur: “Wir releasen alle zwei Wochen!”. Eine
nachvollziehbare Metrik ist das hingegen nicht, schließlich kann alle zwei
Wochen Code mit schlechter Qualität ausgeliefert werden, der zudem sehr
ineffizient entwickelt wurde.

Was also in der Praxis gefehlt hat, sind Fakten. So stellte man sich häufig die
Fragen: Hat es sich verbessert oder verschlechtert? Wie sehr hat es sich
verbessert oder verschlechtert? Was genau ist denn nun besser oder eben
schlechter?

Die Bewertung erfolgte letztendlich häufig aus einem Bauchgefühl heraus und
weniger nach klaren Fakten. Und genau hier haben damals, wie teilweise auch
heute, Tatsachen gefehlt. Für diese Fakten gibt es einige Metriken, wodurch man
als Team besser entscheiden kann, wo es etwas zu verbessern gibt und wo
vielleicht einige Experimente nicht von Erfolg gekrönt waren.

### Die DORA-Metriken

Damals, als ich vor den zuvor erläuterten argumentativen Problemen stand, war
DevOps als Kultur noch kein bekanntes Thema. Viele kulturelle Aspekte von
DevOps waren allerdings schon vor der Entstehung des Begriffs schon relevant.
Was allerdings aus dem DevOps-Movement entstanden ist und mir damals für die
Argumentation geholfen hätte, sind die [DORA](https://www.devops-research.com)-Metriken.

Die DORA-Metriken sind vier Metriken, womit sich der Erfolg von DevOps messen
lassen. Entwickelt und benannt sind sie von der “DevOps Research and
Assessment”. Folgende vier Metriken gibt es:

  1. Deployment Frequency
  1. Lead Time
  1. Change Failure Rate
  1. Time to Restore Service

Diese Metriken werden vor allem vom Management herangezogen, um zu schauen, wie
gut die DevOps-Prozesse denn in dem Unternehmen funktionieren. Es lohnt sich
allerdings, diese Metriken auch mal aus einem anderen Blickwinkel anzuschauen,
der häufig vergessen wird, nämlich der Entwicklerproduktivität.

**Deployment Frequency**: Diese Metrik gibt an, wie oft ein Deployment stattfindet.
Hier gilt grundsätzlich: Je häufiger, desto besser, denn so landen die
Änderungen, die implementiert wurden, schnell bei den Endbenutzern. Für die
Entwickler heißt es letztendlich, dass diese langsam daran hingeführt werden
müssen, damit sie das Vertrauen in das eigene Projekt aufbauen können, um
häufig deployen zu können. Und um das umzusetzen, müssen weitere Methoden und
Prozesse eingeführt werden, um das Vertrauen eben zu stärken. Genau hier
spielen Punkte wie Code-Review hinein. Diese Metrik komplett alleine sagt
allerdings noch nicht viel aus.

**Lead Time**: Zu Beginn dieses Artikels ging es ja um das zügige Abschließen
der zugewiesenen Arbeit. Das ist auch ein Teil, der für die Lead Time relevant
ist. Diese gibt an, wie lange es dauert, bis ein Commit, der erstellt wurde,
auf die Produktivumgebung ausgerollt wurde. Dies geht also auch Hand in Hand
mit der Deployment Frequency daher.

**Change Failure Rate**: Bisher unbeachtet waren bei den ersten beiden Metriken
war, was passiert, wenn Fehler auftreten. Eine hohe Deployment Frequency und
eine schnelle Lead Time ist schön und gut, allerdings bringt das alles nicht so
viel, wenn viele Fehler auftreten. Genau dafür steht die dritte Metrik da, die
Change Failure Rate, bei der gemessen wird, wie hoch die Fehlerhäufigkeit nach
Deployments ist.

**Time to Restore Service**: Falls nun Fehler auftreten, sollte diese möglichst
zügig korrigiert werden. Mit der Time to Restore Service Metrik wird daher
angegeben, wie lange es dauert, bis der Dienst bei einem Fehler wieder zur
Verfügung steht.

### Fazit

Für die einen oder anderen dürften die DORA-Metriken nichts Neues sein.
Allerdings wird es meiner Einschätzung nach häufig eben nur mit dem Blick des
Managements für die Endnutzer draufgeschaut und weniger aus dem Blick des Teams
und deren Produktivität. Dabei lassen sich gerade diese Metriken auch dafür
wunderbar nutzen, etwa um Entscheidungen im Entwicklungsprozess zu begutachten,
ob es nun etwas besser oder schlechter gemacht hat. Dabei ist wichtig, dass man
nicht nur diese Metriken kennt, sondern diese auch Tool-unterstützt einfach
herauslesen kann. Anschließend kann es nicht nur für die DevOps-Produktivität
verwendet werden, sondern auch als Vehikel für Entwicklungsprozesse verwendet
werden.

Mein Appell lautet daher: Einfach mal überlegen und schauen, wie der
Entwicklungsprozess verschlankt und effizienter gemacht werden kann, damit
nicht nur die Effizienz der Personen gesteigert wird, sondern auch deren
Zufriedenheit. Denn das Wichtigste ist am Ende ja, dass die
Software-Entwicklungsteams genau das tun, wofür sie angestellt wurden: Software
entwickeln, ohne zu viele Steine in den Weg gelegt zu bekommen.

