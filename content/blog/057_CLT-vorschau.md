---
title: "Chemnitzer Linux-Tage…"
categories: ["Open Source"]
tags: ["ubuntuusers-planet"]
date: "2015-02-27 19:15:00+02:00"
toc: true
---

Am 21. und 22. März finden die Chemnitzer Linux-Tage statt. Ich selbst bin diesmal
erstmals auch dort am Start. Das [Programm](https://chemnitzer.linux-tage.de/2015/de/programm/plan)
der Veranstaltung ist breit gefächert. Immerhin sechs parallele Vorträge sowie
zusätzlich noch drei parallele [Workshops](https://chemnitzer.linux-tage.de/2015/de/programm/workshops).

Ich selbst werde, wie bei fast jeder OpenSource-Messe die ich besuche, am Ubuntu-Stand
vertreten sein. Bis zur Veranstaltung werden wir auch mindestens eins, wahrscheinlich
eher zwei Ubuntu Phones dabei haben. Die wurden im letzten Jahr relativ häufig
angefragt.

Vorträge werde ich auch Besuchen, konkret habe ich mir folgende rausgesucht,
die ich am Samstag höchstwahrscheinlich besuchen werde: [Git Workflows im Alltag](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/306), [Aktuelle Entwicklungen im Linux-Kernel](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/271),
[Qt: ein Code, alle Betriebssysteme](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/248),
[An introduction to Amazon Web Services](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/194).

Am Sonntag plane ich wenigere Vorträge, nämlich: [Docker](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/441),
[Embedded Web mit Django](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/303)
und eventuell noch [Admin's Diary: Wie ich lernte, systemd zu lieben ;-)](https://chemnitzer.linux-tage.de/2015/de/programm/beitrag/269).

Wer übrigens eine gute Bus-Verbindung nach Chemnitz sucht, der wird vielleicht
auf [Freedom Tours](http://freedom-tours.de/), dem quasi „Linux-Bus“, eine Fahrt
finden. Jedenfalls fahre ich mit dem Bus direkt nach Chemnitz. Je mehr Leute
mitfahren, desto günstiger wird es. [Plätze sind aber noch frei!](http://freedom-tours.de/letztes-hotelbett-weg/531)

