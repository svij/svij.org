---
title: "Rezension: Android - Einstieg in die Programmierung und Entwicklungsumgebung"
categories: ["Rezension"]
date: "2014-09-07 14:00:00+02:00"
toc: true
---

Das Buch „Android - Der schnelle und einfache Einstieg in die Programmierung
und Entwicklungsumgebung“ von den Autoren Dirk Louis und Peter Müller
erschien in der ersten Auflage im Hanser Verlag.

### Was steht drin?

Das Buch umfasst insgesamt 20 Kapitel, abgedruckt auf 474 Seiten. Alle Kapitel
werden dann noch zusätzlich in die Teile „Einführung“, „Grundlagen“ und
„weiterführende Themen“ unterteilt.

Neben den 20 Kapiteln gibt es auch noch einen Anhang mit zusätzlichen Material,
sowie einer DVD. Letztere beinhaltet neben der benötigten Software auch noch
weitere Anleitungen wie etwa einem Java-Tutorium.

Der erste Teil führt in die absoluten Basis-Grundlagen in die Android-Entwicklung
ein. Das erste Kapitel behandelt die erste Einrichtung von Eclipse, der 
Installation von JRE, sowie die Nutzung des Android-SDK-Managers. Die
Installationsanleitung thematisiert lediglich die Windows-Installation. Wie
man genau die Einrichtung unter Linux und Mac OS durchführt, wird daher
in dem Buch leider nicht thematisiert.

Bereits im zweiten Kapitel wird die erste kleine „Hallo Welt!“-App entwickelt.
Es wird dabei Schritt für Schritt sowohl der Umgang mit Eclipse als auch dem 
Ausführen der App in einem Emulator erklärt. Das dritte und letzte Kapitel
der Einführung erläutert ausführlich den Aufbau eines Android-Projekts.

Mit dem vierten Kapitel starten die Einführungskapitel. Zunächst wird der
allgemeine Umgang mit Eclipse erklärt, welches Spezialitäten von Android
nicht enthält. Darin wird nicht nur die grundlegende Bedienung dargestellt,
sondern es werden auch zahlreiche Tipps und Tricks gegeben, welches das Arbeiten
mit Eclipse deutlich erleichtert. Im fünften Kapitel folgt die Einweisung
in das Layouten von Android-Apps. Es ausführlich erklärt welche Design-Prinzipien
man beachten sollte, und auch wie man diese umsetzen kann. Das anschließende
sechste Kapitel behandelt die Nutzung von Ressourcen, also etwa die Nutzung von
Grafiken, Größen-Definitionen oder auch Strings. Das siebte Kapitel enthält
das Thema der Anwender-Interaktion. Fragen was Views sind und wie sie arbeiten
werden genauso beantwortet wie die Nutzung von normalen OnClick-Listenern oder
auch die Implementierung von Wisch-Gesten.
Ein sehr wichtiges und teilweise besonders für Anfänger kompliziertes Thema
ist der Lebenszyklus einer Android-App. Dies ist das Thema vom achten Kapitel,
welches zudem noch einige Grundlagen erläutert, was sich unter der Haube einer
Android-App befindet.

Der dritte Teil des Buches behandelt weiterführende vertiefende Themen. Kapitel 9
erläutert zuerst die Nutzung von Canvas innerhalb von Apps, während anschließend
das zehnte Kapitel die Nutzung von Menüs und Dialogen beinhaltet. Das elfte Kapitel
gehört zwar auch zu den weiterführenden Themen, behandelt aber ein wichtiges
Thema: die Intents. Kapitel 12 beinhaltet wiederum ein klassisches Java-Thema,
nämlich dem Arbeiten mit Dateien. Dabei wird auch auf die Ansdroid-spezifischen
Sonderfälle eingegangen, wie dem Schreiben auf externem Speicher. Im 13. Kapitel
wird erstmals eine vollständige App entwickelt. An Hand einer Quiz-App werden
die vorher erlernten Dinge gesammelt nochmals erläutert. Die darauffolgenden
Kapitel behandeln weiterhin die Themen Multimedia, die Einbindung von Sensoren,
den Einsatz von SQLite Datenbanken sowie die Geolokation.

Das 18. Kapitel fasst nochmals viele erlernte Dinge aus den vorheringen Kapitel
in einer Beispiel-App zusammen. Dieses Mal in Form eines Tic-Tac-Toe Spiels.
Interessanterweise landete die Thematik rund um Fragments, die in Apps genutzt
werden können fast zum Schluss im 19. Kapitel, obwohl dies mittlerweile eines
der wichtigsten Elemente jeder App sind. Im letzten Kapitel werden schlußendlich
noch einige Tipps und Tricks genannt.

Zum Schluss wartet noch der Anhang, der ebenfalls in drei Teile unterteilt ist.
Dort weiterführende Themen behandelt, die nach der Fertigstellung einer App
warten, etwa dem Veröffentlichen der App im Google Play Store.

### Wie liest es sich?

Das Buch setzt keine Kenntnisse mit der Android-Entwicklung voraus. Nichtsdestotrotz
werden gute Kenntnisse in Java benötigt um mit dem Buch sinnvoll arbeiten zu
können. Der Umgang mit der Entwicklungsumgebung Eclipse wird hingegen nicht
vorausgesetzt. Die Nutzung wird an vielen Stellen ausführlich genug erläutert
und mit zahlreichen Screenshots verbildlicht. Java-Neulinge sollten sich vor
dem Anschaffen dieses Buches zunächst auf anderem Wege Java erlernen. Viele
Dinge welches die Programmierung mit Java betrifft wird trotzdem häufig genug
am Rande erläutert oder es wird auf die Buch-DVD verwiesen. Dort ist nämlich
auch ein Java-Tutorium enthalten. 

Die Erklärungen in dem Buch sind grundsätzlich gut beschrieben und werden
ausführlich erläutert, sodass erfahrene Java-Entwickler keine Probleme in den
Einstieg in die Android-Programmierung haben sollten. Die wichtigsten Themen
rund um die Android-Entwicklung werden grundsätzlich abgedeckt. Das Buch
umfasst auch zahlreiche Bilder, welche häufig wichtig sind, um etwa die
richtigen Schaltflächen schnell zu finden.

### Kritik

An diesem Buch gibt es wenig auszusetzen. Für knapp 30€ findet man mit diesen Buch
einen guten Einstieg in die Entwicklung von Android Apps. Die Buchseiten
sind komplett schwarz-weiß, farbige Seiten werden in dem Buch allerdings
sowieso nicht gebraucht. Weiterhin bietet das Buch eine gute Abwechslung
zwischen Theorie und Praxis. Besonders die beiden Praxis-Beispiele an Hand
der beiden Apps bieten dadurch nicht nur eine gute Abwechslung sondern fassen
den vorher gelernten Inhalt sinnvoll zusammen. Allwissend wird man durch dieses
Buch allerdings nicht. Wer eine App vollständig entwickeln will, kommt auf
weitergehende Lektüre nicht herum, was auch nicht Ziel des Buches ist.
Das Ziel einen schnellen und einfachen Einstieg in die Android-Entwicklung
wird das Buch durchaus gerecht.

#### Buchinfo:

**Titel:** [Android - Der schnelle und einfache Einstieg in die Programmierung und Entwicklungsumgebung](http://www.hanser-fachbuch.de/buch/Android/9783446438231)

**Autor:** Dirk Louis, Peter Müller

**Verlag:** Hanser, 2014

**Umfang:** 474 Seiten + DVD

**ISBN:** 978-3-446-43823-1 (Buch), 978-3-446-43831-6 (E-Book)

**Preis:** 29,99€ (broschiert), 23,99€ (E-Book)

Da es schade wäre, wenn das Buch bei mir im Regal verstaubt,
wird es verlost. Dies ist „drüben“ bei [freiesMagazin](http://www.freiesmagazin.de/20140907-septemberausgabe-erschienen)
in den Kommentaren möglich.
