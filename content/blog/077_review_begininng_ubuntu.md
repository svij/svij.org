---
title: "Review: Beginning Ubuntu for Windows and Mac Users by Nathan Haines"
categories: ["Rezension"]
date: "2015-10-12 22:25:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

Fellow Ubuntu Member Nathan Haines has recently published his first book
"Beginning Ubuntu for Windows and Mac Users". Nathan asked me to review his
book, I couldn't decline it.

### What is in it?

The book is divided into six different chapters and two appendices. It was written
by Nathan Haines, who is a long time Ubuntu User, Ubuntu Community Contributor
and organisator of UbuCon at SCaLE. The book was published by Apress and has a
length of 244 pages.

The book starts with some information about the author, about the technical reviewer
and the acknowledgments. In the introduction Nathan gives a decent introduction
into Ubuntu, where he covers the main topics of Ubuntu. This includes the special
parts of Ubuntu, the philosophy and some information about Linux itself.

The first chapter deals with the installation of Ubuntu. It not only covers the
standard Ubuntu Desktop Installation with Unity, but also the different flavors.
This includes Kubuntu, Xubuntu, Lubuntu, Ubuntu GNOME and also Ubuntu Server.
He gives detailed instructions how to install the system and what the effects of
the different actions are. The title of the book is saying, that it is targeted
for Windows and Mac users. Therefore, the whole instructions are written in way
that Windows and Mac users see the differences between the systems and they can
also create the installation DVDs and USB-Sticks from their system. It also covers
the installation of a dual-boot installation, either with Windows or with running
Mac OS X alongside Ubuntu.

The introduction and installation already covered roughly a quarter of the book.
In the second chapter, the topic is how to getting started with Ubuntu. In this
part, it gives an introduction how to use the Unity Desktop, where you find the
Dash, what the indicators are and showing how to use the Unity-HUD. It also covers
the installation of software and updating the system using the Software Center. The book is not only about
Ubuntu , it also gives a guidance how to set up your Ubuntu Desktop in a network
with Windows, especially how to connect to a Windows-Machine using RDP or how to
access files in the local network using the SMB-Protocol.

The third chapter is all about productivity at home and work. It showcases all
the different applications that are either pre-installed or are installable and
helps you to get things done at work and at home. Precisely, it covers basic instructions
of FocusWriter, HomeBank, Evolution, Thunderbird with Lightning, using Google
with the Online Accounts feature, Empathy, FreeMind and others. 

The fourth chapter handles the media entertainment. This basically covers the
usage of DVDs, VLC and how you can organize your music and podcasts with Rhythmbox.
Additionally, it also covers the applicactions Brasero, DeVeDe, Shotwell, Audacity
and a few more programs. The topic of the fifth chapter is the command-line.
The author gives an introduction on how to use the basic commands of the
command-line, which tools are available and why it might be good option to use it.
On the following pages, there are also explanation how to use nano to edit
files, alpine to read mails and use irssi to use IRC.

The last chapter is all about power user tools. This includes the topics how
to manage processes, disks and thumb drives or how to use multiple workspaces
properly. Additionally, the reader gets some information about the commands to
install applications through the command line or how to work with virtual machines.

The book ends with two appendices covering the Ubuntu release strategy and the
future of Ubuntu, with Snappy and Ubuntu Phone.

### How well is it written?

As the title of this book already say, this book is mainly targeted for beginners
who already used Windows or Mac OS X. The explanation are always straightforward
and mostly not complex. The reader of the book gets a good overview of Ubuntu
and how to use most of the pre-installed software. Nearly every application is
also presented by a screenshot.

As a beginner, you shouldn't have any problems to getting started with Ubuntu
with this book. You see that the author is very experience with Ubuntu, which
is actually true ;-).

### Critique

The most parts of the book covers topics on how to use different applications.
Mostly one application per category was picked, which were then explained. All
the explanations were neither too short nor too long. Nathan always mentioned
other similar programs of the same category. Anyway, sometimes the selection
was a bit surprising. In my opinion one of my important programs for Ubuntu beginners
is the LibreOffice Suite, which isn't covered in this book. Instead of LibreOffice,
FocusWriter was focused. On the other side there were also a few applications
which I wouldn't expect, like the command-line mail client alpine. This is the
only small minus point for me. Except this, the book is perfect for beginners!
As an experienced Ubuntu users, you probably don't learn many new things. Even
though, I found a couple of applications which I never heard of.

### Book-Details:

**Title:** [Beginning Ubuntu for Windows and Mac Users](https://www.apress.com/9781484206096?gtmf=s)

**Author:** [Nathan Haines](http://nhaines.com)

**Publisher:** Apress

**Pages:** 244
