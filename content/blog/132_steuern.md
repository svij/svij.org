---
title: "Versteuerung von US-Aktienoptionen und -paketen in Deutschland"
date: "2022-09-16T12:00:00+02:00"
categories: ["Sonstiges"]
---

Wenn man bei einem jungen Unternehmen aus der Start-up Branche arbeitet, was im Laufe der
Zeit ggf. auch an die Börse geht, dann gibt es häufiger diverse
Aktienbeteiligungsprogramme, um die Mitarbeitenden am Wachstum der Firma zu
beteiligen. Vor allem wenn man für amerikanische Firmen arbeitet, kann dies
komplizierter sein, da viele Gewinne und Verluste selbst dem Finanzamt gemeldet
werden müssen.

Für den Otto Normaldeutschen, mit Steuersitz in Deutschland, mit entweder wenig
Ahnung von Steuern oder wenig Ahnung von Aktien generell, kann dies sehr
schnell, sehr unübersichtlich, kompliziert und überfordernd sein. Je früher man
sich mit den steuerlichen Aspekten beschäftigt, desto besser ist es eine
fundierte Entscheidung zu treffen, was Risikoabwägung und Steuerlast angeht.

Dieser Blogpost geht ein wenig auf die Themen ein, in die ich mich selbst, mit
diversen Arbeitskollegen, eingearbeitet habe, um das ganze ein wenig durchschaubarer
zu machen. Dies ist natürlich weder eine Anlageberatung, noch eine Steuerberatung.
Im Zweifel also eine Steuerberatung zurate ziehen. Die Informationen zu den
Steuern entsprechen dem Stand vom Jahr 2022.

Es gibt verschiedene Arten von Beteiligungsprogrammen, am gängigsten sind folgende:

- Aktienoptionen (Englisch: "[stock options](https://www.investopedia.com/terms/e/eso.asp)")
- [Restricted Stock Units (RSUs)](https://www.investopedia.com/terms/r/restricted-stock-unit.asp)
- [Mitarbeiteraktienkaufpläne (Employee Stock Purchase Program, kurz: ESPP)](https://www.investopedia.com/terms/e/espp.asp)

## Vesting

Wichtig für das Verständnis der Versteuerung der einzelnen Pakete ist zu
wissen, was es heißt, wenn von "Vesting" gesprochen wird. In der Regel wird ein
Paket dem Mitarbeitenden zugewiesen, was über einen gewissen Zeitraum "vestet".
Das heißt, dass einem erst nach Ablauf dieser Vesting-Periode alles aus
dem zugewiesenen Paket gehört. Häufig sind das vier oder fünf Jahre, damit die Mitarbeitenden
auch längerfristig an die Firma gebunden werden. Meistens gibt es dann noch ein sogenannten Vesting-Cliff,
sodass nach erst nach einem festgelegten Zeitraum ein größerer Anteil vestet, bevor
es dann regelmäßiger vonstattengeht.

Beispiel: Bei einer Firma werden Aktienoptionspakete mit einer Laufzeit von vier
Jahren verteilt. Das Vesting-Cliff beträgt ein Jahr. Danach erfolgt ein monatliches
Vesting. Bei 10.000 Aktienoptionen über vier Jahre, vesten nach einem Jahr
also 25% der Optionen, also 2500. Anschließend vesten jeden Monat ~208,33 Optionen.

Das Vesting hat, je nachdem ob es Options oder RSUs sind, unterschiedliche steuerliche
Effekte, aber dazu später mehr. Vesting-Dauer, Vesting-Cliff und Vesting-Periode
können bei jedem Unternehmen unterschiedlich sein und auch abhängig sein, ob
es sich um Aktienoptionen oder RSUs handelt.

## Aktienoptionen ("Stock options")

Aktienoptionen, auf Englisch Stock Options, sind ein häufiges Vehikel, um vor
einem Börsengang oder Verkauf einer Firma den Mitarbeitern die Möglichkeit zu geben, mit geringerem
Risiko am Wachstum der Firma zu profitieren.

Aktienoptionen sind letztendlich nicht viel mehr als eine Möglichkeit zu einem
bestimmten festgelegten Preis in der Zukunft Aktien der Firma zu
beziehen. Sofern die Aktieneoptionen nicht ausgeübt werden – hier spricht man dann
häufig von "exercisen" oder eben ausüben der Optionen – dann gibt es zunächst
keine Steuern zu bezahlen. Wenn man allerdings die Aktienoptionen zieht bzw. nach
einem eventuellen Börsengang der Firma die Optionen direkt verkauft, dann gilt
dies als geldwerter Vorteil, sofern natürlich Rabatte gegeben worden sind.

Folgendes Beispiel macht das Ganze vermutlich etwas klarer:
- 10.000 Optionen werden zum Start dem Mitarbeitenden gegeben
- Vesting über 4 Jahre mit einem Vesting-Cliff von einem Jahr, anschließend monatliches vesting
- Optionspreis von $1
- Firma ist privat gehalten, also nicht an der Börse

Es gibt nun mehrere Möglichkeiten:
- Early Exercise "all-in": Direkt zum Start des Jobs alle Optionen ziehen
- partly Early Exercise: Direkt zum Start ein Teil der Optionen ziehen
- late exercise: Nach einigen Monaten/Jahren ein Teil/alle Optionen ziehen
- no exercise: keine Optionen ziehen

All diese Möglichkeiten haben sowohl unterschiedliche steuerliche Effekte, als
auch ein differenziertes Risiko-Verhältnis.

### Early Exercise

Beim Early Exercise werden entweder alle oder ein Teil der Optionen direkt zum
Start des Jobs ausgeübt. Häufig entspricht der Optionspreis auch dem sogenannten
"Fair Market Value" (kurz: FMV) der Firma – also dem aktuellen Wert der Firma. Für den
Early Exercise gehen wir in diesem Beispiel mal davon aus, dass der Wert
der Option dem aktuelle FMV entspricht, also beides $1.

Wenn man nun also die Optionen direkt alle zieht, würden $10.000 fällig,
die bezahlt werden müssen. Das Geld ist dann zunächst einmal "weg", denn so
schnell kommt man da nicht mehr ran, da die Firma zu dem Zeitpunkt in diesem
Beispiel ja noch nicht an der Börse ist. Generell sollte man Geld nur in Firmen,
sei es an der Börse oder nicht, nur dann investieren, wenn man damit leben kann,
an das Geld eine sehr lange Zeit, also etwa über 10 Jahre, nicht ran zu müssen.

Das Vorgehen vom Early Exercise ist allerdings steuerlich am einfachsten und
günstigsten: Da der Optionspreis dem FMV der Firma entspricht, gibt es schlicht
keinen Rabatt, sodass hier keine zusätzlichen Steuern fällig werden.

Aktien gehören einem direkt nach dem Exercisen allerdings auch noch nicht, da
das Vesting nach einem Jahr erst beginnt und dann monatlich erfolgt. Bei jedem
Vesting gehören dann einem die vollständigen Aktien, sodass einem ein Teil der Firma
gehört, auch vor dem Börsengang. 

Verkauft werden können die Anteile allerdings
auch noch nicht so einfach, da es sich noch um eine privat-gehaltende Firma
handelt. Eine Ausnahme ist dann nur, wenn die Firma verkauft wird, Aktien nach einem
Börsengang verkauft werden können oder bei einem sogenannten Secondary an Investoren
verkauft werden können.

Beim Verkauf werden Steuern fällig, dazu weiter unten mehr. Das Early Exercise
ist steuerlich die günstigste Art am Wachstum beteiligt zu werden. Es ist gleichzeitig
aber auch die risiko-reichste: Die $10.000 müssen gedanklich abgeschrieben werden. Viele
Start-ups gehen pleite. Laut [Investopedia schaffen es 90% der Start Ups](https://www.investopedia.com/articles/personal-finance/041315/risk-and-rewards-investing-startups.asp) nicht an
die Börse. Es ist ungewiss, ob und wann ein Exit der Firma stattfindet
und ob dann überhaupt Gewinn daraus gezogen werden kann.

Ein weiteres Risiko ist, dass einem alle Optionen erst nach Ablauf der Vesting-Periode
gehören. Wenn man innerhalb der Probezeit entlassen wird oder man aus anderen 
Gründen die Firma verlässt, müssen die übrigen Optionen, die noch nicht gevestet sind,
nicht zwangsläufig zurückgekauft werden.

### Late Exercise

Eine Alternative, um das Risiko ein wenig abzumildern, ist ein späteres Exercisen
der Optionen. Dies ist zum Beispiel dann relevant, wenn man entweder nur gevestete
Optionen ziehen möchte oder wenn man wartet, bis einem ein wenig klarer ist, wie
der (finanzielle) Stand der Firma ist, sofern man das überhaupt beurteilen kann.

Die Wahrscheinlichkeit ist hoch, dass der Fair Market Value der Firma sich
verändert hat. Dieser kann nach unten oder nach oben gehen. Dies ist etwa dann
der Fall, wenn eine neue Finanzierungsrunde stattgefunden hat.

Um das Beispiel weiterzudrehen, sollen nach einem Jahr Firmenzugehörigkeit
die gevesteten 2500 Optionen gezogen werden, weiterhin gilt hier der Optionspreis
von $1. Wesentlicher Unterschied nach einem Jahr: Der Fair-Market-Value beträgt
nun $5.

Steuerlich hat dies natürlich einen Effekt: Man erhält, sofern man denn wirklich
die Optionen zieht, Aktien im Wert von $5 für $1. Hierbei handelt es sich dann
um einen geldwerten Vorteil von $4 durch eben diesen Rabatt.

- Geldwerter Vorteil je ausgeübter Option: $4
- ausgeübte Optionen: 2500
- Optionspreis gesamt: $2500
- demnach geldwerter Vorteil: $10.000

Der geldwerte Vorteil muss dann vom Arbeitgeber bei der Gehaltsabrechnung mit
angegeben werden, sodass ca. 5000€ netto weniger Gehalt ausgezahlt werden
dürfen, wenn man von einem USD:EUR Wechselkurs von 1:1 ausgeht und einem
vereinfachten Steuersatz von 50%. Je nach Firma und Gehalt muss hier also das
Geld zur Firma überwiesen werden, damit diese das zum Finanzamt abführen.

Das Ziehen der Optionen nach einem Jahr hat in diesem Beispiel somit $2500
für die Optionen + 5000€ Steuern gekostet, sodass man auf eine Belastung von ~7500€ kommt.
Im Vergleich zum Early Exercise zahlt man also deutlich darauf, hat immer noch diverse
Risiken, wenn auch einige weniger.

### No Exercise

Die dritte Möglichkeit ist, dass man die Optionen einfach liegen lässt. Hierfür
muss dann kein Geld aufgewendet werden, hat also hier kein Risiko. Steuern
werden beim Vesten von Optionen auch nicht fällig. Da man diese Optionen ja auch
nicht zieht.

Risikotechnisch ist das das risikoärmste: Wenn die Firma pleite geht, oder andersweitig
verscherbelt wird, erleidet man keine Verluste, da man kein Geld reingesteckt hat.
Man tauscht hier also das Risiko eines Totalverlusts gegen höhere Steuern im Falle
einer Gewinnmitnahme.

Interessant wird es bei diesem Beispiel eher, wie das nun aussieht, wenn man die
Optionen direkt verkauft. Aber dazu unten mehr.

Wichtig bei Optionen generell: Diese haben ein Ablaufdatum, das liegt meist 10 Jahre
nach der initialen Zuweisung in der Zukunft. Nach dieser Zeit verfallen
diese ersatzlos, also nicht vergessen! Weiterer Punkt: Viele Optionen lassen
sich nur exercisen, wenn man noch in der Firma angestellt ist. Sobald man die
Firma verlässt, verfallen die Optionen nach einer Dauer ebenfalls. Das ist allerdings
abhängig von der Firma. So manch ein Mitarbeiter der ersten Stunde hat
sicherlich Optionen verfallen lassen, die Jahre später sehr viel mehr
Wertzuwachs hatten.

## Restricted Stock Units (RSUs)

[Restricted Stock Units](https://en.wikipedia.org/wiki/Restricted_stock) sind
Aktien, die einer Sperre unterliegen, die unter festgelegten Aktion erst transferierbar
sind. Für normale Mitarbeitende, die RSUs erhalten, ist dies meist ein "normales"
Vesting, wie es auch bei Optionen der Fall ist. Im Gegensatz zu Optionen erhält
man bei RSUs allerdings direkt Aktien, ohne den Umweg einer Option, die man später
ziehen kann.

RSUs werden in der Regel dann verteilt, wenn die Firma an der Börse gelistet
ist. Hier macht es also keinen großen Unterschied, ob es sich um eine junge oder
alte Firma handelt. Viele große und ältere US-Techfirmen verteilen so Aktienpakete
an ihre Mitarbeiter.

Wie auch Optionen vesten RSUs über einen Zeitraum, häufig auch mit einer Vesting-Cliff
und in regelmäßigen Abständen. Anders als Optionen sind RSUs sofort zu versteuern,
sobald diese gevestet sind. Das hat einige Implikationen. Um das Ganze zu verdeutlichen,
erneut ein Beispiel:

- RSU Grant: $100.000
- RSU Stock value: $25
- Total RSUs: 4000
- Vesting über 4 Jahre mit einem Vesting-Cliff von einem Quartal, anschließend quartalsweises Vesting

In diesem Beispiel wird dem Mitarbeitenden ein RSU Grant von $100.000 verteilt.
Diese Summe wird allerdings nur einmal für die Berechnung der Anzahl der RSUs
genutzt. Genau genommen wird ein Aktienwert festgelegt, häufig ein Mittelwert
über einen bestimmten Zeitraum und einmalig durch die Summe des RSU Grants geteilt. Der
Aktienwert ist also schwankend und die genaue Berechnung abhängig von der Firma.
In diesem Beispiel nehme ich ein einfaches Beispiel mit $25, sodass 4000 RSUs
letztendlich vergeben werden, die über vier Jahre vesten und das jedes Quartal.
Jedes Quartal vesten also 250 RSUs.

Wie oben geschrieben werden beim Vesten der RSUs einem direkt die Aktien zugewiesen,
die solange gehalten werden können, wie man mag. Dadurch muss auf diese Aktien
normal Einkommenssteuer gezahlt werden. Die zuvor erwähnten $100.000 haben keine
Relevanz: Es geht lediglich um den aktuellen Wert bei Einbuchung der Aktien.

- Wert der Aktie beim Vesten: $30
- Anzahl gevesteter RSUs: 250
- Zu versteuerndes Einkommen: $7500

Grundsätzlich gibt es zwei Möglichkeiten die Steuern zu bezahlen: Entweder wird
es wie normales Gehalt behandelt und vom Lohn abgezogen oder es werden automatisch
Anteile verkauft, um die Steuern zu decken. Letzteres wird häufig auch "sell to cover"
genannt, um die Steuern zu decken. Dies ist inbesondere dann nützlich, um ein
sehr geringes Netto-Gehalt zu vermeiden und Risiken beim Halten der Aktien zu
verringern. Als Mitarbeiter hat man nicht unbedingt die Möglichkeit, zwischen
den Optionen zu wählen.

Interessant beim "Sell to cover" ist allerdings zu beachten, welcher Steuersatz
beim Verkauf gewählt wird. Normalerweiser sollte ja der persönliche Einkommenssteuersatz
gewählt werden. Je nachdem ob an Single oder Verheiratet ist und ob man wenig,
viel oder sehr viel verdient, ist dieser natürlich höher oder tiefer.

Tatsächlich ist es aber so, dass der Broker, wo die Aktien liegen, keinen Einblick
in die persönlichen Steuersätze haben. Hier hängt es also ggf. auch am Arbeitgeber,
ob die persönlichen Steuersätze genutzt werden, um die Aktien zu verkaufen, um
die Steuern zu decken. Relevant ist dies allerdings eh fast nur, wenn man plant,
die Aktien länger zu halten.

Wenn man Pech hat, ist ein maximaler Steuersatz eingetragen, der nicht verändert
werden kann. Der maximal höchste Steuersatz in Deutschland ist 51,525%. Dieser
Wert setzt sich aus drei Steuersätze zusammen:

- 45% Einkommenssteuer auf Gehälter über 277.826€ (als Alleinstehender im Jahr 2022)
- Solidaritätszuschlag von 5,5% auf die Summe der Einkommenssteuer
- Kirchensteuer von 9% auf die Summe der Einkommenssteuer

Zusammengerechnet entspricht dies 51,525%. Erreichen dürften diesen Satz die wenigsten
Leute. Wenn nun aber dieser Wert unverändert darin steht, dann werden entsprechend
(mindestens) 51,525% der gevesteten RSUs automatisch verkauft, um die Steuer
zu decken. Bei dem Beispiel oben wären das dann 129 Aktien, die automatisch verkauft
werden.

In der nächsten Gehaltsabrechnung werden dann Zahlen wieder richtig gerückt, da
eben die wenigsten wirklich 51,525% Einkommenssteuer zahlen müssen. Über die
Gehaltsabrechnung wird dann das Geld, was durch die zu viel verkauften Aktien
einbehalten wurde, wieder nachberechnet und entsprechend ausbezahlt.

Es gibt allerdings noch ein paar kleine Stolpersteine, die man beachten sollte:
Zwischen dem Vesting und dem tatsächlichen Einbuchung der Aktien, sowie dem
automatischen Verkauf der Aktien können Stunden oder Tage vergehen. Dann kann
es zu einem kuriosen Zustand kommen:

- Vesting-Datum der RSUs: 15. August bei Marktschluss
- Einbuchung der RSUs: 19. August zur Markteröffnung
- Sell-to-cover der RSUs: 19. August zwei Stunden nach Markteröffnung

Das ganze kann dann dazu führen, dass die Werte anders sind, zum Beispiel:

- Wert beim Vesting: $30
- Wert bei der Einbuchung: $31
- Wert bei Verkauf durch Sell-to-cover: $31,50

**Wichtig:** Immer brav alle Dokumente abspeichern und absichern! So kann man
sichergehen alle Daten auch in Zukunft nachvollziehbar zu haben.

Für die Gehaltsabrechnung selbst sind zwei Werte hier relevant: Der Wert beim
Vesting, sowie der Wert der durch den Verkauf durch Sell-to-cover eingenommen
wurde. Tatsächlich wurde allerdings hier noch ein Kapitelertrag zwischen $30
und $31,50 erwirtschaftet, auf den Kapitalertragssteuer gezahlt werden muss.
Dies passiert allerdings nicht automatisch, sondern muss selbst über die
Steuererklärung in der Anlage KAP für das Jahr eingereicht werden, da es sich
ja um einen US-Broker handelt, der keine Steuern automatisch abführt. Das sind
zwar womöglich Peanuts, aber trotzdem müssen hier Steuern gezahlt werden. Es
geht übrigens auch in die andere Richtung, dass Verluste gemacht werden. Die
Verluste lassen sich gegen andere Gewinne gegenrechnen, aber begeht man hier
nicht aus Versehen Steuerhinterziehung im kleinen Rahmen.

## Employee Stock Purchase Program (ESPP)

Die Ausgestaltung eines ESPPs kann sehr verschieden sein und ist stark vom
Arbeitgeber abhängig. Von folgenden Möglichkeiten habe ich schon gehört:

- Mitarbeiter können grundsätzlich Mitarbeiteraktien mit einem Rabatt beziehen, müssen diese aber eine Zeitlang halten
- Mitarbeiter können einen Teil vom Netto-Gehalt zur Seite legen, damit Aktien mit einem Rabatt zu einem Stichtag bezogen werden können

Egal, wie die Ausgestaltung letztendlich ist: Der Rabatt ist immer mit dem
persönlichen Steuersatz zu versteuern.

Beispiel:
- Aktien im Gegenwert von $10.000 werden eingekauft
- Aktien werden dank 15% Rabatt für $8500 bezogen
- Geldwerter Vorteil: $1500

Hier gilt es wie bei RSUs auch: Entweder erfolgt ein Abzug auf der Gehaltsabrechnung
vom Lohn, oder die Steuerlast wird mittels Sell-to-cover gedeckt.

Im Gegensatz zu RSUs steckt man beim ESPP mehr eigenes Geld in die Aktien des
eigenen Arbeitgebers. Der Rabatt von 15% zum Beispiel ist gut, birgt aber auch
ein paar Risiken, jenachdem ob es Haltefristen gibt, oder eben nicht.

Häufiges Argument für den zeitnahen Verkauf von Aktien, die über ein ESPP gekauft
worden sind, ist, dass zwischen dem Arbeitnehmer und Arbeitgeber eine zu hohe
Abhängigkeit bilden kann. Insbesondere geht es dabei darum, dass wenn es der Firma
schlecht geht und man als Arbeitnehmer entlassen wird, dann geht es mit großer
Sicherheit auch dem Aktienkurs nicht so gut, sodass man unter dem Einstiegswert
landen kann. Hier gelten ähnliche Risiken wie beim frühen Exercisen von Options,
die man sich bewusst sein muss.

## Freibetrag für Mitarbeiterbeteiligungen

In den bisherigen Abschnitten wurden noch keinerlei Freibeträge thematisiert.
Seit [dem 1. Juli 2021 gibt es einen höheren Freibetrag](https://www.haufe.de/personal/entgelt/fondsstandortgesetz-zur-mitarbeiterkapitalbeteiligung_78_533572.html)
auf Mitarbeiterkapitalbeteiligungen in Höhe von 1440€. Dies kann genutzt werden,
wenn alle Mitarbeitenden der Firma Aktienoptionen oder RSUs erhalten haben.
Im konkreten heißt es also, dass jedes Mal, wenn ein geldwerter Vorteil beim
Ziehen von Aktienoptionen, beim Vesting von RSUs oder beim Rabattierten Kauf
via ESPP entsteht, dann sind die ersten 1440€ steuerfrei. Falls der Arbeitgeber
das über die Gehaltsabrechnung nicht selbst durchführt, kann dies über die
Steuererklärung nachgeholt werden. Hier fragt das Finanzamt ggf. nach einer
Bestätigung dafür, dass alle Mitarbeitenden das jeweils erhalten haben.

## Verkaufen - Steuerliche Ereignisse

Bisher gar nicht betrachtet habe ich den Verkauf von Optionen und Aktien.
Im Falle eines Verkaufs von Aktien wird Kapitalertragssteuer fällig: In dem Fall
muss auf die Gewinne 25% Kapitalertragssteuer + 5,5% Solidaritätszuschlag
auf die Summe des Kapitalertrags + ggf. Kirchensteuer entrichtet werden. Wichtig:
Die Steuer muss man selbst abführen, da höchstwahrscheinlich der Broker ein US-Broker
ist, der die Steuer nicht automatisch abführt. Hier muss es dann in der
Steuererklärung in der Anlage KAP deklariert werden.

Beispiel für RSUs:
- RSU Vesting am 15. August: 122 Aktien zu je $30
- RSU Vesting am 15. November: 122 Aktien zu je $35
- Verkauf von 244 Aktien zu je $40
- zu versteuernder Gewinn mit Kapitalertragssteuer: ($40 - $35) * 122 + ($40 - $30) * 122 = $1830

Wichtig ist hier, dass der Wert zählt zum Tag des Vestings. Dieser ist aber auch
im Broker hinterlegt und sollte entsprechend sichtbar sein.


Ein weiteres Beispiel für early exercised Options, aus dem Beispiel am Anfang
des Blogposts:
- Optionspreis: $1
- FMV am Exercise Datum: $4
- Exercised Options: 2500
- Verkauf der 2500 Aktien zu je $40
- zu versteuernder Gewinn mit Kapitalertragssteuer: 2500 * ($40 - $4) = $90.000

Da der Fair Market Value beim Exercise $4 betrug, muss die Differenz für die Kapitalertragssteuer
zwischen $40 und $4 gebildet werden. Der Optionspreis hat zur Berechnung der
Kapitalertragssteuer keine Relevanz mehr, da die Differenz zwischen $1 und $4
ja schon versteuert wurde, mit dem persönlichen Steuersatz.

Anders sieht es aus, wenn man Optionen direkt verkauft: Je nach Broker können die
Optionen quasi direkt verkauft werden: In dem Fall wird auch hier ein
geldwerter Vorteil berechnet. Auch hier gilt: Differenzbetrag zwischen Verkaufswert
und Optionswert. Es ähnelt also grundsätzlich sehr dem normalen ausüben der
Optionen. Das Ziehen der Optionen und der Verkauf der daraus resultierenden
Aktie wird also in einem Schritt vollzogen. 

**Wichtig:** Überall da, wo Kapitalertragssteuer anfällt, muss eine Steuererklärung
gemacht werden. Das gilt natürlich auch für Dividenden. Dieser Hinweis ist
für US-Broker wichtig, die die Kapitalertragssteuer nicht automatisch dem deutschen
Fiskus übermitteln.

Beim Verkauf von Aktien – nicht Optionen! – kann ebenfalls noch der Sparerpauschbetrag
in Anspruch genommen werden. Bei deutschen Banken und Brokern erfolgt die Abführung
automatisch und kann mit dem Freistellungsauftrag eingerichtet werden. Für Singles
beträgt der Freibetrag 801€, bei Eheleuten 1602€. Für das Jahr 2023 ist eine Erhöhung
auf 1000€ bzw. 2000€ angedacht. Auch diese Verrechnung läuft über die Steuererklärung,
da ein US-Broker es nicht beachten wird.

## Fazit

Das richtige Verständnis der möglichen Steuerlast gepaart mit einem gesunden
Risikobewusstsein ist wichtig für kluge Handlungen, um Risiko gegen Steuerlast
sinnvoll abzuwägen. Dieser Blogpost fasst meine Erfahrungen der letzten ~2,5 Jahre
zusammen. Viele haben nur geringes Verständnis des deutschen Steuersystems und
vieles ist nun mal komplex, ansonsten wäre dieser Artikel auch relativ kurz.

Häufig beschäftigen sich viele nicht mit den Optionen die ihnen zur Verfügung
steht. Am Ende, bei einem Börsengang wird sich geärgert, weil man ja hätte viel
Steuern sparen können mit einem gewissen Risiko, wenn man sich frühzeitig damit
beschäftigt hätte.

Um auch ein Negativbeispiel zu nennen, um deutlich zu machen, was so ein unüberlegtes
und nicht richtig verstandenes Beteiligungspaket bewirken kann, lohnt sich ein
Blick auf die Bank Klarna. [FINANCEFWD](https://financefwd.com/de/klarna-mitarbeiterbeteiligung/)
hat einen Artikel veröffentlicht, wie es bei Klarna schieflief.
Obwohl Klarna nicht an der Börse ist, hat Klarna an ihre Mitarbeiter RSUs
verteilt, die beim Vesten nun mal direkt zu versteuern sind. Dadurch, dass es eben
nicht an der Börse gelistet ist, kann auch kein Sell-to-cover durchgeführt werden.
Stattdessen haben viele Mitarbeiter es mit ihrem normalen Gehalt versteuert, also
deutlich weniger Nettogehalt ausgezahlt bekommen. Leider ist dann die Firmenbewertung
um 85% gravierend zusammengebrochen, sodass die Mitarbeiter viele Steuern für
Aktien bezahlt haben, die heute nur sehr wenig Wert sind. Da sie nicht an der Börse
sind, konnten sie auch zu keinem Punkt eingreifen und nach ihrem Willen verkaufen.
Solche RSU Pakete sind zum einen nicht optimal, aber auch optional: Man muss
sie nicht annehmen. Options wären hier etwa besser geeignet gewesen, aber auch
hier hätten viele Mitarbeiter womöglich viele Verluste erlitten.

Meine Key-Takeaways hierfür sind daher:
- Wer geringes oder kein Risiko eingehen will, sollte Optionen nicht ziehen und
direkt verkaufen, um somit das Risiko mit einer potenziellen höheren Steuerlast
später abzusichern.
- Wer hohes Risiko eingehen will, langfristig im optimalen Fall Steuern sparen
will oder auf die Summe verzichten kann, sollte möglichst früh exercisen
- Wer RSUs bekommt, sollte auf sell-to-cover achten, um böse Überraschungen wie
bei Klarna zu vermeiden, bei öffentlich gelisteten Unternehmen ist das hingegen
ein netter Zusatz zum Gehalt
- Wer an ESPP teilnehmen kann, sollte sich überlegen, wie viel und wie lange er
Geld da reinsteckt, um mögliche Schwierigkeiten später ausschließen zu können.
Stichwort hier: Diversifikation

Wenn man alles verstanden hat und richtig abwägt, sind es am Ende nur noch
"Champagner-Probleme": Zu viel Steuern zahlen heißt nämlich auch immer: Man hat
guten Gewinn gemacht.
