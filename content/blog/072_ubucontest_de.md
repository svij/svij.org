---
title: "UbuContest startet!"
categories: ["Ubuntu"]
date: "2015-07-18 12:25:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Gestern haben wir auf der Ubucon-Webseite die große Ankündigung gemacht: [Der Start des UbuContests](http://ubucon.de/2015/announcement-ubucontest-de). Der Wettbewerb läuft in Zusammenarbeit mit Canonical, welche
(fast) alle Gewinne zur Verfügung stellt, darunter das ein oder andere Ubuntu Phone
von bq und Meizu, Produkte aus dem Ubuntu Shop und eine Reiseunterstützung zur Ubucon nach
Berlin.

Insgesamt gibt es fünf verschiedene Kategorien, die sich nicht nur an Entwickler
richtet, sondern auch an Leute, die etwa als fleißiger Tester, Übersetzungen oder
auch als Designer aktiv sind. Auch die Community hat die Möglichkeit seine eigenen
Favoriten zu nominieren.

Der Wettbewerb endet am 18. September. Danach wird die Jury aktiv, die ebenfalls (mit einer Ausnahme)
aus Mitgliedern der Community bestehen. Das sind konkret Carla Sella aus Italien,
Laura Cowen aus dem Ubuntu UK Podcast, Simos Xenitellis aus Griechenland, Michael
Zanetti von Canonical und meiner Wenigkeit.

Ich bin gespannt, welche neuen Apps oder Scopes eingereicht werden und hoffe auf
eine rege Beteiligung! :)

Nähere Informationen finden sich [hier](http://ubucon.de/2015/contest).
