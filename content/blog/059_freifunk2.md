---
title: "Mehr Freifunk!"
categories: ["Open Source"]
tags: ["ubuntuusers-planet"]
date: "2015-02-28 11:30:00+02:00"
toc: true
---

Im August 2014 habe ich [meinen ersten Freifunk-Knoten](/blog/2014/08/10/mein-erster-freifunk-knoten/)
bei mir Zuhause aufgestellt. Mittlerweile hat sich einiges getan und auch bei
mir steht jetzt ein zusätzlicher zweiter Router.

Beide Freifunk-Router laufen über [Freifunk-Ruhrgebiet](http://freifunk-ruhrgebiet.de/).
Interessant ist, dass Freifunk letzten August vorallem im Ruhrgebiet kaum verbreitet
war. So gab es insgesamt um die 100 Knoten und in Dortmund etwa gar keine Freifunk-Nodes.
Mittlerweile sind einige Knoten dazugekommen. Alleine im [Ruhrgebiet](http://map.freifunk-ruhrgebiet.de/counts/) finden sich über 1000 Knoten.

Ich selbst habe im vorherigen Artikel beschrieben, wie man ein privates
WLAN-Netz mit dem Freifunk-Images aufsetzen kann. Mittlerweile ist das nicht mehr
notwendig, da es im offiziellen Image eingeflossen ist und man es über das Web-Interface
konfigurieren kann.

Das Freifunk-Netz nutze ich selbst eher selten, stattdessen scheint es der
ein oder andere Nachbar zu nutzen, schließlich gab es über meinen Router
einen Traffic-Volumen von ~26,5 GB in den letzten 37 Tagen.

<a href="/graphics/freifunk1.webp" data-lightbox="image" data-title="Meine beiden Freifunk-Knoten.">
    <img src="/graphics/freifunk1.webp" style="margin:20px;">
</a>

Freifunk fungiert bei mir größtenteils als Gäste-WLAN. Die Router eignen sich
gleichzeitig eben auch, um das eigene private WLAN-Netz im Haus zu erweitern. Diesmal
griff ich zu einem TP-Link WDR3600 für knapp 35€. Jetzt ist auch im ganzen Haus
überall sowohl mein privates WLAN-Netz als auch Freifunk verfügbar.

<a href="/graphics/freifunk2.webp" data-lightbox="image" data-title="Meine beiden Freifunk-Knoten beim meshen.">
    <img src="/graphics/freifunk2.webp" style="margin:20px;">
</a>

Wer Freifunk nicht kennt, der kann auf einen der zahlreichen Freifunk-Communities
nachlesen, etwa auch bei [Freifunk-Ruhrgebiet](http://freifunk-ruhrgebiet.de/).
