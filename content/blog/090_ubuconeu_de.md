---
title: "UbuCon Europe in der Retrospektive"
categories: ["Events"]
date: "2016-11-27 12:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Letztes Wochenende fand die erste UbuCon Europe statt. Die UbuCon ist eine
Ubuntu-Konferenz die an verschiedenen Orten auf der Welt stattfindet, so zum
Beispiel auch in Deutschland. Die UbuCon Europe war die erste der Art, die sich
an die internationale europäische Ubuntu Community richtete. Ich selbst war seit 2012
auf jeder deutschen UbuCon, seit 2013 im Orga-Team und seit 2015 Hauptorganisator
für die UbuCon 2015 in Berlin und der UbuCon Europe 2016 in Essen.

Was alles auf der UbuCon Europe geschah, habe ich bereits in einem [Artikel auf ikhaya.ubuntuusers.de](https://ubuntuusers.de/ikhaya/7853/) geschrieben. Das kann man also
da nachlesen. Dieser Artikel geht mehr auf meine persönlichen Erfahrungen bei der
Organisation und der Teilnahme ein.

Die initiale Idee eine europäische UbuCon auszurichten stammt von [Costales](https://plus.google.com/u/0/+MarcosCostales/posts/jLzUVbFmHuv) die wir dann im UbuCon Team
aufgegriffen haben und daraus die UbuCon Europe in Essen formten. Das Unperfekthaus in
Essen war zwar Plan B, allerdings auch nur, weil dort höhere Kosten herrschten.
Sonst hatten wir meist kostenlos zur Verfügung gestellte Räume in Schulen oder
Universitäten bekommen. Im Nachhinein war es sehr gut, dass wir die Veranstaltung
im Unperfekthaus ausgerichtet haben, da es schließlich ein interessanter Ort ist und
das hat sehr vielen Leuten gefallen. Aus organisatorischer Sicht war es ebenfalls einfacher,
so mussten wir vom Orga-Team uns nicht um das Essen und die Getränke kümmern. Im
Gegenteil, diesmal gab es sogar nicht nur stumpf belegte Brötchen und Sandwiches,
sondern gleich richtige Mahlzeiten als Büffet. Auch das WLAN wurde vom Unperfekthaus
gestellt und das funktioniert auch sehr gut, das waren wir schon von unseren
vorherigen Konferenzen gar nicht mehr gewohnt.

<a href="/graphics/ubucon2016_audience.webp" data-lightbox="image">
  <img src="/graphics/ubucon2016_audience.webp" style="margin:10px;">
</a>

Eine Sorge, die ich am Anfang hatte war, dass fast nur Deutsche erscheinen und
nur wenige Leute aus dem Ausland den Weg nach Essen antreten. Am Ende wurden wir allerdings
positiv überrascht: Es kamen Teilnehmer aus 17 Ländern und drei Kontinenten.
Insgesamt kamen 132 Teilnehmer, wovon 55 % Deutsche waren. Die Zahl war geringer
als wir erwartet haben, da sonst auch in etwa 120 Personen zu unseren rein deutschen
UbuCons kamen. Geschadet hat es hingegen nicht. Auch hier fehlte leider die Zeit
mit jeden sich zusammenzusetzen, um zu quatschen!

Bei den Talks war auch ein guter Mix zwischen Einsteiger- und Fortgeschrittenen-Themen
vorhanden. Schön war auch, dass Canonical CEO Jane Silber meiner Einladung für
eine Keynote gefolgt ist und den Samstagmorgen auch anwesend war. Neben ihr waren
auch noch ein paar Canonical-Mitarbeiter da und natürlich auch zahlreiche weitere von der Community. Die Slides und ein paar wenige Aufnahmen der Talks sind auf der [Webseite](http://ubucon.org/en/events/ubucon-europe/talks/) verlinkt.

Da bleibt fast nur noch das übrig, was nicht richtig oder gut funktioniert hat.
Prinzipiell hat – recht überraschend – das meiste gut funktioniert. Einzig bei
der Bezahlung und der Kommunikation mit Besuchern und Speakern gibt es
Verbesserungspotenzial, denn da war vielen nicht klar, wann wer wie viel und wo zahlen muss. Am Ende
hat es aber dann doch noch ganz gut geklappt. Das Social-Event am Samstag-Abend
war letztendlich auch nicht ganz so gut besucht wie erwartet. Von 140 interessierten
kamen dann doch nur 80 Leute. Da hatte ich eher mit 100 gerechnet.

An dieser Stelle möchte ich mich noch bei einigen Personen für die gute Unterstützung
und Hilfe im Team bedanken. Zunächst an David Planella, Daniel Holbach, Alan Pope
und Michael Hall von Canonicals Community-Team die bei Fragen und Anregungen immer
da waren und stets unterstützt haben. Weiterhin gilt mein Dank an Ilonka O. und
Marius Quabeck die mit mir jede Woche beim Meeting mit dem Community-Team teilgenommen
haben und ebenfalls einiges an Arbeit und Ideen eingebracht haben. Nicht zu vergessen
weiterhin Jonathan Liebers und Jens Holschbach welche die UbuCon überhaupt erst
nach Essen gebracht haben, auch wenn der ursprüngliche Plan nicht geklappt hat.
Mein Dank gilt auch Veit Jahns, der mit Ilonka sich um die eingehenden Talks
der Speaker gekümmert hat. Weiterhin natürlich noch Sarah, Peter und Philipp vom
ubuntuusers.de Team die zur falschen Zeit am falschen Ort standen und dann spontan
vor Ort die Arbeit am Registration Desk übernommen haben ;). Last but not least
natürlich noch Torsten Franz und Thoralf Schilde und dem ubuntu Deutschland e.V.
die das ganze finanzielle und rechtliche getragen und unterstützt haben. Falls
ihr die UbuCon und andere Tätigkeiten des ubuntu Deutschland e.V.s unterstützen wollt, dann
[kann man ganz einfach dem Verein beitreten](http://verein.ubuntu-de.org/node/46).

Unabhängig vom eigentlichen Orga-Team gilt der Dank natürlich noch den Speakern
und Besuchern die erst das Programm und die Veranstaltung Inhalt gegeben haben!

Nächstes Jahr findet die UbuCon Europe in Paris statt. Die französische Ubuntu
Community ist sehr aktiv und ich bin schon sehr gespannt was sie da aus dem Hut
zaubern werden. Ob eine deutsche UbuCon nächstes Jahr stattfindet, ist hingegen noch nicht
klar.
