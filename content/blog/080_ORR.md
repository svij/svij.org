---
title: "Besuch bei der OpenRheinRuhr 2015"
categories: ["Ubuntu"]
date: "2015-11-09 21:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Letztes Wochenende fand in Oberhausen die [OpenRheinRuhr](http://openrheinruhr.de)
statt. Die OpenRheinRuhr ist für mich als Ruhrgebiet-Mensch meine Heimmesse, da
ich in knapp einer Stunde von zuhause anreisen kann. Seit 2010 war ich auch brav
jedes Mal da und habe mich dieses Mal nicht nur am Ubuntu Stand beteiligt, sondern
habe auch selbst zwei Vorträge gehalten.

Am Samstag habe ich nur drei Vorträge besucht:

 * [foreman - Wie man heutzutage Systeme deployt](http://programm.openrheinruhr.de/2015/events/367.de.html) war
    ein Vortrag von Julius Bloch. Julius hat in seinem Vortrag ein paar Witze
    eingestreut und foreman vorgestellt. Für mich gab es letztendlich die Erkenntnis,
    das ich das Tool wohl eher weniger zur Zeit brauchen werden.
 * [Taskwarrior - Todo-Liste für Geeks](http://programm.openrheinruhr.de/2015/events/372.de.html) war
    ein Vortrag von Oleg Fiksel. Als langjähriger Taskwarrior-Nutzer gab es da für
    mich nichts neues zu lernen, was abzusehen war. Es erinnerte mich jedenfalls
    stark daran, dass ich mein [altes Projekt](https://github.com/svijee/taskwarrior-androidapp)
    mal wiederbeleben muss.
 * Am meisten habe ich mich auf den Vortrag von Dirk Deimeke über [Zeit- und Selbstmanagement](http://programm.openrheinruhr.de/2015/events/359.de.html) gefreut, da ich da gerade noch nach meiner besten Möglichkeit
   suche das Privatleben, das Studium, den Job und die Arbeit an den diversen
   Open Source Projekten unter einem Hut zu bringen. Jetzt werde ich definitiv
   einige der genannten Methoden ausprobieren, um effektiv und möglichst stressfrei
   meine Aufgaben in den Griff zu kriegen. Danke Dirk!

Am Sonntag habe ich keine Vorträge besucht, sondern meine beiden Vorträge gehalten. Um 10
Uhr hielt ich zunächst den [Ubuntu Phone](http://programm.openrheinruhr.de/2015/events/348.de.html)
Vortrag, den ich in ähnlicher Fassung auch auf der FrOSCon vor zwei Monaten gehalten
habe. Der Vortragsraum war soweit fast voll. Interessanterweise gab es auf der
FrOSCon viel mehr kritische Nachfragen als es auf der OpenRheinRuhr der Fall war.
Überraschend auch, dass sich fast 10 Leute am Ende gemeldet haben, die sich ein
Ubuntu Phone kaufen wollten, obwohl es explizit keine Marketing-Veranstaltung war ;).
Die Vortragsfolien finden sich [hier](https://github.com/svijee/ubuntu-phone-froscon/raw/master/output/Ubuntu_Phone_ORR.pdf).

Ein paar Stunden später hielt ich dann den zweiten Vortrag über [Snappy Ubuntu Core](http://programm.openrheinruhr.de/2015/events/349.de.html). Nicht ganz so voll wie im vorherigen Vortrag, aber besser besucht
als ich gedacht hatte. Wie ich auch am Ubuntu Stand feststellte, ist recht wenig
Wissen bei den Leuten über Snappy Ubuntu Core vorhanden, obwohl da die Zukunft
von Ubuntu liegen wird. Leider spielte das Netzwerk in der Situation nicht ganz
so schön mit, sodass ich die Live-Demo ausfallen lassen musste. Die Vortragsfolien
kann man sich [hier anschauen](https://github.com/svijee/snappy-ubuntu-froscon/raw/master/output/snappy-orr.pdf).

Die restliche Zeit verbrachte ich entweder am Ubuntu Stand mit den Kollegen oder
habe mich mit einen der vielen bekannten Gesichter unterhalten. Ob und wo die
Veranstaltung nächstes Jahr stattfindet, ist noch nicht klar. Hierfür sucht
das Orga-Team [nach einem neuen Ort](http://blog.openrheinruhr.de/2015/10/11/call-for-location-openrheinruhr-2016/)
im Rhein-Ruhr-Gebiet. Ich drücke jeden Falls die Daumen, dass es auch nächstes
Jahr wieder die Veranstaltung geben wird!
