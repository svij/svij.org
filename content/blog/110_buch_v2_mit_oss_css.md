---
title: "Open und Closed Source Software beim Schreiben des Git-Buches (2. Auflage)"
categories: ["Sonstiges"]
date: "2019-05-12 17:30:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg02.met.vgwort.de/na/ba98a6c797534d6f8d917dd264d7ec99"
---

2016 erschien die [erste Auflage meines Git-Buches](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/),
bei dem ich auch darüber [schrieb](/blog/2016/08/03/ein-buch-mit-open-und-closed-source-software-schreiben/),
welche Open und Closed Source Software ich verwendet habe, um das Buch zu schreiben.

In diesem Artikel geht es um das gleiche Thema wie letztes Mal, mit dem Unterschied,
dass ich über die Arbeit an der [zweiten Auflage des Buches](/blog/2019/05/10/zweite-auflage-meines-git-buches-erschienen/)
welches jetzt erschien. Nähere Informationen zu dem Buch finden sich hinter dem Link.

Allgemein geht es in diesem Artikel die Tools die ich selbst verwendet habe und
nicht die, die beim Verlag eingesetzt wurde. Wie auch im letzten Mal habe ich
versucht so viel Open-Source-Software wie möglich einzusetzen.

## Das Schreiben

Bei der ersten Auflage schrieb ich den ersten Entwurf zunächst in Vim und
überführte es dann nach LibreOffice Writer, wo die Dateien dann vom Verlag
weiterverarbeitet wurden.

Für die zweite Auflage arbeitete ich direkt in LibreOffice weiter, wo ich die
Änderungen eingepflegt habe. Um ein Diff zwischen den Versionen anschauen zu
können, verwendete ich **odt2txt**, was ich in meine Git-Konfiguration eingebunden
habe. Das funktioniert zwar nicht so gut wie bei reinen Text-Dateien, aber besser so,
als gar kein Diff zu haben.

Beim Verlag bzw. beim Satz des Buches wurde FrameMaker verwendet. Berührungspunkte
hatte ich allerdings damit keine.

## Die Grafiken

Im Buch muss ich diverse Dinge visualisieren. Dazu gehören sowohl Darstellungen
der Git-Historie, als auch Veranschaulichungen bei Workflows beim Arbeiten mit
Remote-Repositorys.

In der ersten Auflage griff ich nach einigen Experimenten mit Tools wie [Graphviz](http://www.graphviz.org)
auf [yEd Graph Editor](https://www.yworks.com/products/yed). Hauptgrund für die
Auswahl von yEd war, dass ich ohne größeren Anstrengungen und mit wenig Aufwand
solche Grafiken erstellen konnte. Nachteil von yEd: es ist ein kein quelloffenes
Tool und die enthaltenen Abbildungen empfand ich zum Teil als altbacken.

Als neues Tool setzte ich dieses Mal auf [draw.io](https://www.draw.io/). Das
funktioniert sowohl im Browser bzw. Web, als auch als Electron Anwendung auf dem
Desktop. Ich griff dabei auf die Installation über das [Flatpak-Paket](https://flathub.org/apps/details/com.jgraph.drawio.desktop)
zurück. Das Tool steht zwar unter der Apache 2.0 Lizenz, die Entwicklung geschieht
allerdings größtenteils im Verborgenen. Das sieht man auch sehr stark, wenn
man sich die [Git-Historie](https://github.com/jgraph/drawio/commits/master) auf
dem master Branch anschaut: Bis auf wenige Ausnahmen enthält jeder einzelne Commit die
Änderungen einer kompletten Version. Eine nachvollziehbare Historie ist dies
hingegen nicht.

Eine Migration der mit yEd erstellen Grafiken war allerdings nicht möglich, sodass
ich alle Grafiken neu erstellt habe.

## Statistiken

Bei der ersten Auflage konnte ich noch eine kleine Statistik erstellen
in dem ich die Anzahl und Uhrzeiten der Git-Commits ausgewertet habe. Dies ging
bei der zweiten Auflage nicht ganz so gut, weil viel weniger Commits notwendig
waren. Ich begann die Arbeit an der zweiten Auflage am Heiligabend 2018 und endete
am 26. April 2019. Insgesamt wurden es 53 Commits. Effektiv gearbeitet habe ich
in diesem Zeitraum natürlich nicht jeden Tag, da ich fast ausschließlich am
Wochenenden, Urlaubstagen und in Zügen der Deutschen Bahn daran gearbeitet habe.
Zum Vergleich: Für die Fertigstellung der ersten Auflage lagen zwischen dem ersten
und dem letzten Commit 308 Tage und 287 Commits, also knapp über 10 Monate.

## Und nun?

Mit Abschluss meines Masterabschlusses Anfang Dezember und Fertigstellung der
zweiten Auflagen des Buches sind meine Großprojekte der letzten Jahre erstmal
abgeschlossen. Mal schauen was ich mir jetzt ans Bein binde. ;)
