---
title: "Ubucon 2013: Mobile Open Source Systeme im Überblick"
categories: ["Mobile"]
date: "2013-10-14 21:00:00+02:00"
slug: "ubucon-2013-mobile-open-source-systeme-im-uberblick"
toc: true
---

Letztes Wochenende fand vom 11. bis zum 13. Oktober die diesjährige Ubucon statt.
Diesmal war sie in Heidelberg. Wie auch letztes Jahr in Berlin, war ich auch
dieses Mal mit dabei. Ich hielt dieses Jahr 2,5 Vorträge, wobei man eher von
zwei echten Vorträgen reden kann. Beide fanden am Sonntag, dem letzten Ubucon-Tag
statt. Zum einen „Mobile Open Source Systeme im Überblick“ und zum anderen
einen Vortrag über „Ubuntu Touch“. Am Freitag hatte ich allerdings bereits
ein „Hands On“ zu Firefox OS und Ubuntu Touch geleitet, bei dem die Besucher
mit zwei Nexus 4 Smartphones mit Ubuntu Touch sowie einem Nexus S mit Firefox OS
herumspielen durften. Sowohl das Hands On, als auch die beiden Vorträgen waren
mit 20-30 Besuchern gut besucht.

Es gibt allerdings nur zum Vortrag „Mobile Open Source Systeme im Überblick“
Vortragsfolien. Diese finden sich [hier](/uploads/MobileOSS-ubucon2013.pdf). Beim Ubuntu Touch Vortrag
habe ich eine Video-Kamera aufgebaut, und darüber das System über den Beamer
präsentiert.

Die Ubucon hat dieses Jahr wieder sehr viel Spaß gemacht, und ich hoffe, dass
ich auch nächstes Jahr wieder dabei sein kann. Ein Besuch lohnt sich auf jeden
Fall!
