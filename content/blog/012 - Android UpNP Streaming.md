---
title: Multimedia-Streaming von einem UPnP-Server auf Android
categories: ["Android"]
date: "2012-12-02T18:30:00+02:00"
toc: true
---

Da ich nun seit einigen Wochen [das Nexus 10](/blog/2012/11/16/ein-blick-auf-das-nexus-10/) besitze, wollte ich nun einmal probieren wie ich meine Videos von meinem Desktop-Rechner auf das Tablet streame.
Die einfachste Möglichkeit die ich gefunden habe ist, dass ich einen [UPnP-Server](http://de.wikipedia.org/wiki/UPnP) aufsetze. Dies hab ich mit der Software uShare gelöst. Genaue Konfigurationsmöglichkeiten lassen sich hierfür im [ubuntuusers.de-Wiki](http://wiki.ubuntuusers.de/uShare) nachlesen.

Das einzige was ich an der Konfiguration in der Datei ''/etc/ushare/ushare.conf'' geändert habe, ist der Pfad zu den Multimedia-Inhalten.

	…
	USHARE_DIR=/media/files/
	…


Der USHARE_DIR-Variable setzt man nun den Pfad in dem seine Multimedia-Inhalte liegen. In meinem Fall ist es ''/media/files''. uShare kann man entweder als Dienst starten oder schlicht im Terminal starten. Die Ausgabe im Terminal sieht dann in etwa wie folgt aus:


	% ushare
	Interface eth0 ist down.
	Überprüfe uShare's Konfiguration und versuche es erneut!
	uShare (version 1.1a), a lightweight UPnP A/V and DLNA Media Server.
	Benjamin Zores (C) 2005-2007, für das GeeXboX Team.
	Für Updates gehe auf http://ushare.geexbox.org/.
	Listening on telnet port 1337
	Initialisiere UPnP Subsystem ...
	UPnP MediaServer lauscht auf 192.168.2.186:65535
	Sende UpnP Advertisement für Gerät...
	Lausche auf Control Point Verbindungen...
	Building Metadata List ...
	Looking for files in content directory : /media/files
	Found 55066 files and subdirectories.


Somit ist der UPnP MediaServer gestartet und lauscht an Port 65535. Was noch fehlt ist die passende Android-App um die Inhalte abzurufen. Bei der Suche nach einer guten App bin ich dann auf [MediaHouse](https://play.google.com/store/apps/details?id=com.dbapp.android.mediahouse) gestoßen.
MediaHouse erkennt automatisch alle laufenden UPnP-MediaServer im LAN. Mein uShare-UPnP-Server wurde automatisch erkannt und ich konnte ziemlich einfach durch die Dateien browsen und auch Filme streamen. Dabei kann man auch seinen Video-Player seines Vertrauens nutzen, zum Beispiel den VLC-Media-Player. Das Streamen testete ich direkt einmal mit einem HD-Film mit VLC und es klappte wunderbar.
Verzögerungen oder irgendwelche anderen Störungen traten dabei nicht auf.
