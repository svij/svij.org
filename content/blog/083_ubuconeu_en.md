---
title: "UbuCon Europe Plannings and Call for Contributions"
categories: ["Events"]
date: "2016-04-02 18:00:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

It's been a while, since my first announcement about [UbuCon Europe 2016](https://svij.org/blog/2015/06/05/announcing-ubucon-europe-2016/) which will take place from the 18th to 20th November 2016 in Essen, Germany. That means:
Mark your calendars! Check out our [travel page](http://ubucon.org/en/events/ubucon-europe/travel-and-accommodation/)
on how to get to Essen! And finally: Submit your [talks and ideas](http://ubucon.org/en/events/ubucon-europe/talks/)
for talks or workshops!

While we were running the UbuCon Germany in the last years, we learned that many
speakers picked a talk which were requested by the audience. So be sure to also
submit your idea for a talk or workshop, which you would like to hear to idea@ubucon.eu!

We have a few talks already in our pipeline, sadly nothing fixed yet. I hope that
we can announce the first speakers in the next weeks. If you want to join and help
our team to organise the event, just send me an email (svij ät ubuntu.com).

More information are on [UbuCon.eu](http://ubucon.eu)!
