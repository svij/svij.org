---
title: "Mein Git-Buch beim mitp Verlag ist da!"
categories: ["Git"]
date: "2016-08-03 12:00:00+02:00"
toc: true
---

Im Herbst 2014 bis Frühling 2015 veröffentlichte ich auf meinem Blog [ein bisher vierteiliges Git-Tutorial](https://svij.org/blog/category/git/).
Die Reihe diente ungeplant als Vorlage und Basis für ein Git-Buch, welches ich im
Verlaufe der letzten ~11 Monate geschrieben habe und im [mitp-Verlag](http://mitp.de)
jetzt erschienen ist.

<a href="/graphics/gitbuch.webp" data-lightbox="image">
  <img src="/graphics/gitbuch.webp" width="200" style="margin:10px;">
</a>

## Der Inhalt

Insgesamt fasst das Buch mit allen drum und dran 272 Seiten. Aufgeteilt ist es
in insgesamt 10 Kapitel.

* **Einführung**: Im ersten Kapitel geht es zunächst um den Einstieg in das Thema
von Versionsverwaltung. Konkret also was eine Versionsverwaltung ist, wofür sie gut
ist und welche Arten es gibt.

* **Die Grundlagen**: Das zweite Kapitel befasst sich mit den Grundlagen, also wie man
ein Repository anlegt, wie Commits man tätigt und wie Git intern arbeitet.

* **Arbeiten mit Branches**: Im Anschluss folgt auch schon das Kapitel zum Branching
mit Git. Erklärt wird, wie man Branches erstellt, wofür sie gut sind und wie sie
zusammengeführt werden können. Dabei werden sowohl normale Merges mit und ohne Merge-Konflikten
behandelt als auch das Rebasen von Branches.

* **Verteilte Repositories**: Verteilte Repositories kamen in den ersten drei
Kapitel noch nicht vor. In diesem Kapitel wird erläutert wie man mit Remote-Repositories
arbeitet und wie ein kleiner Workflow mit drei Repositories aussieht.

* **Git Hosting**: Das fünfte Kapitel befasst sich mit dem Einsatz von Git auf
dem Server. Im Wesentlichen wird der Workflow GitHub und GitLab erläutert, sodass
am Ende bekannt ist, wie man ein kleines Repositorys maintaint und wie man Änderungen
mittels Pull-Requests bei anderen Projekten beiträgt und was dabei zu beachten ist.
Kurz thematisiert werden auch externe Tools wie Jenkins oder Travis-CI, die zur
kontinuierlichen Integration nützlich sind.

* **Workflows**: Wichtig beim täglichen Einsatz von Git in einem Projekt ist die
Auswahl eines passendes Workflows. Hierbei werden verschiedene Workflows beispielhaft
genannt, die in einem Projekt eingesetzt werden können, etwa Git Flow.

* **Hooks**: Mit Hooks lassen sich einige Dinge automatisieren, wenn etwa Commits
getätigt werden.

* **Umstieg von Subversion**: Wenn der Leser nicht gerade völliger Neuling ist, dann
ist der Umstieg von Subversion wahrscheinlich. Das Kapitel behandelt die wesentlichen
Unterschiede in den Konzepten und in den Kommandos und bringt auch eine Anleitung
wie man ein Subversion-Repository in ein Git-Repository überführt.

* **Tipps und Tricks**: Das vorletzte Kapitel befasst sich mit einer Sammlung
von Tipps und Tricks die man mehr und weniger regelmäßig brauchen kann. Thematisiert
werden unter anderem die Fehlersuche mit Bisect, das Arbeiten mit Patches und
das Neuschreiben der Historie mit Filter-Branch

* **Grafische Clients**: Während die übrigen Kapitel kaum auf graphische Clients
eingeht, ist dies bei diesem Kapitel anders. Hier erfährt man einen Überblick über
einige der zur Verfügung stehenden grafischen Clients von Git.

Wer mehr zum Inhalt lesen möchte, findet [die Leseprobe hier](https://mitp.de/out/media/9783958452268_Leseprobe.pdf).

Das Buch kostet in der Print-Ausgabe 29,99€ oder 25,99€ in der digitalen Ausgabe.
Es lässt sich direkt auf der [Verlagsseite](https://mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git.html),
bei [Amazon](https://www.amazon.de/dp/3958452264) oder bei einem der anderen
zahlreichen Buchhändlern.
