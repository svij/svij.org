---
title: "Rezension: Skalierbare Container-Infrastrukturen"
categories: ["Rezension"]
date: 2019-11-25T08:30:00+01:00
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg02.met.vgwort.de/na/fa60fddb86d04b6cb078c59565a3282d"
---

In dieser Rezension bespreche ich das Buch [„Skarlierbare Container-Infrastrukturen“](https://www.rheinwerk-verlag.de/skalierbare-container-infrastrukturen_4676/)
vom Autor Oliver Liebel vom Rheinwerk-Verlag.

**Hinweis:** Der Rheinwerk-Verlag stellte mir für die Rezension ein Rezensionsexmplar
frei zur Verfügung.

### Was steht drin?

Viel! Sehr viel sogar. Das Buch umfasst insgesamt 1380 Seiten, ist nicht nur
von außen betrachtet ein echt umfangreiches Buch. Es unterteilt sich in 5 Teile
mit insgesamt 25 Kapiteln.

Der Autor beginnt den ersten Teil „Brave New World?“ mit dem allgemeinen
Einstieg in das Thema der Container: Was ist es und wofür braucht man es?
Er beantwortet zudem die Frage, ob es wirklich alles so einfach ist, wie behauptet wird.

Die Grundlagen werden im zweiten Teil „Single-Node Container-Systeme“ vermittelt.
Hier geht es vor allem, aber nicht nur, um Docker. So wird ein Einstieg gegeben
und die grundlegenden Konzepte und Kommandos an Hand von Beispielen erläutert.

Teil 3 „Skaliarbare Container-Cluster und Container-Orchestrierung“ behandelt
im Wesentlichen Kubernetes und etcd. Dies ist zudem auch der umfangreichste
Teil: Es macht mit 672 Seiten knapp die Hälfte des Buches aus. Kein Wunder: Kubernetes
ist sehr komplex und auf die Einzelheiten wird näher eingegangen. Darunter etwa
auch, wie man Kubernetes ohne Docker betreibt.

Im vierten Teil „High-Level-Orchestrierungstools für Container-Infrastrukturen
(on Premise und in der Cloud)“ setzt auf den dritten Teil auf und behandelt
im wesentlich OpenShift, was die Kubernetes-Distribution von Red Hat ist.

Der fünfte und letzter Teil „Software-Defined Storage für verteilte
Container-Infrastrukturen“ befasst sich mit Software-Defined-Storage, darunter
Ceph. Zusätzlich werden noch Checkpunkte für vor den Aufbau von Container-Infrastrukturen
besprochen, sowie ein Ausblick und Fazit zur Container-Welt gegeben.

### Kritik

Meine Gefühle für das Buch sind ein wenig gemischt. Keine Frage: Fachlich ist
das Buch sehr gut. Es ist nicht nur beim Blick auf die Seitenanzahl sehr
umfangreich, sondern auch beim tatsächlichen Inhalt. Wichtig ist, dass an sehr
vielen Stellen erkennbar ist, dass der Autor viele Praxis-Kenntnisse in dem Bereich
besitzt. Das führt dazu, dass an sehr vielen Stellen praxis-relevante Hinweise
gegeben werden, wie gut oder vor allem schlecht etwas funktioniert.

Und genau der letzte genannte Punkt ist zeitgleich mein größter Kritikpunkt.
Prinzipiell find ich es gar nicht so schlecht, wenn in Büchern „Klartext“
geschrieben wird und die Probleme richtig genannt werden, da nicht alles
so schön einfach ist, wie immer behauptet wird. Mein wesentliches Problem ist
der Ton. An sehr vielen Stellen liest sich die Kritik, etwa wenn etwas nicht
wie angeworben funktioniert, als wenn der Autor **sehr** frustriert ist.
Stellenweise hatte ich als Leser das Gefühl, dass der Autor seine Frust an
Entscheidungsträgern und einzelnen Design- und Software-Entscheidungen nur im
Buch auslässt.

Gepaart wird dies durch den hohen Einsatz von Umgangssprache, was meinen Eindruck vom
Buch weiter senkt. Zu Beginn fand ich den Schreibstil ja ganz nett: Die Probleme
und Irrtümer im Umgang mit Container-Infrastrukturen werden klar, deutlich und auch hart
benannt. Auf die Dauer verbreitete sich bei mir das Gefühl, dass immer weiter Frust
abgeladen wird. Zwei Beispiele für zu viel Slang, die sehr oft vorkommen: „Nö“ und
„Würgaround“. „Nö“ wird sogar erschreckend oft in Überschriften von Absätzen
verwendet. „Würgaroung“ (statt Workaround) kenne ich eher nur von alten Forenposts,
wo es heutzutage auch kaum einer mehr verwendet. Meiner Meinung nach ist so ein Slang
in einem professionellen Buch fehl am Platz und macht zumindest bei der Häufigkeit
wie hier, keinen guten Eindruck.

Unabhängig von der Sprache ist der fachliche Umfang tadellos: Es ist sehr umfangreich
und geht in sehr viele tiefe Eigenschaften, Eigenheiten und auch Probleme im
Umgang mit den Technologien ein. Der Preis mit 79,90 €
ist nicht gering, aber bei dem Umfang durchaus nachvollziehbar. Problematisch
bei dem Themengebiet ist zusätzlich, dass sehr vieles in dem Buch sehr schnell
veraltet. Dafür kann der Autor und der Verlag nichts, da die Welt der Container
sich sehr schnell weiterentwickelt. Das Buch kam in der ersten Auflage im April
2017 und die von mir gelesene zweite Auflage im Oktober 2018 auf dem Markt. Ist also
schon ein Jahr alt. In der Zwischenzeit ist viel passiert, so ist mittlerweile
OpenShift 4 erschienen, was sich schon deutlich vom behandelten OpenShift 3
unterscheidet. Man muss sich also zusätzlich zu dem Buch noch separat auf den
aktuellen Stand der Technik bringen.

Würde ich das Buch trotzdem empfehlen: Ja! Wer eine sehr gute Einführung in
die Welt der Container möchte, kommt an dieses Buch nicht vorbei. Wichtig ist
allerdings noch hervorzuheben, dass (sehr) gute Linux- und Netzwerk-Kenntnisse
unabdingbar ist. Grundlagen zu diesen Themen werden nicht thematisiert, die muss man
sich selbst aneignen. Wer sich dem Thema widmen sollte und nur grundlegende
Kenntnisse mitbringt, wird es schwierig haben – und das ganz unabhängig von der
verwendeten Lektüre.

### Buchinfo:

**Titel:** [Skalierbare Container-Infrastrukturen](https://www.rheinwerk-verlag.de/skalierbare-container-infrastrukturen_4676/)

**Autor:** Oliver Liebel

**Verlag:** Rheinwerk

**Umfang:** 1380 Seiten

**ISBN:** 978-3-8362-6385-6

**Preis:** 79,90 €
