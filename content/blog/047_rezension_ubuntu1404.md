---
title: "Rezension: Ubuntu 14.04 für Ein- und Umsteiger"
categories: ["Rezension"]
date: "2015-01-11 15:15:00+02:00"
tags: ["ubuntuusers-planet"]
slug: "rezension-ubuntu-1404-fur-ein-und-umsteiger"
toc: true
---

Das Buch „Ubuntu 14.04 - Für Ein- und Umsteiger“ ist ein Taschenbuch welches
sich speziell an Einsteiger und Umsteiger richtet. Es soll einen guten Einblick
in die tägliche Nutzung von Ubuntu geben.

### Was steht drin?

Das Buch beinhaltet insgesamt 267 Seiten, die in 13 Kapiteln unterteilt sind.
Zusätzlich dazu beginnt das Buch mit einem einleitenden Vorwort. Das Buch
bietet einen Überblick über die Installation, Nutzung von Ubuntu und die Nutzung der
vorinstallierten Programmen. Dem Buch beigelegt ist ebenfalls eine Live-DVD von Ubuntu 14.04.

Im einleitenden Kapitel geht es zunächst auf wenigen Seiten um die Frage,
was Linux bzw. Ubuntu ist und für wen es geeignet ist. Es wird dabei kurz
die Philosophie von Ubuntu erläutert, die existierenden Ubuntu-Derivate
aufgelistet, sowie Konzeption des Buches und der Inhalt der DVD beschrieben.

Im ersten echten Kapitel wird zunächst erklärt, wie und wo man Ubuntu herunterladen
kann und wie man dazu das passende Boot-Medium, also DVD oder USB-Stick, 
unter Windows erstellt.

Das zweite Kapitel befasst sich mit der Live-Session von Ubuntu, welche man
mittels der beiliegenden DVD, der selbstgebrannten DVD oder mittels eines
USB-Sticks starten kann. Anschließend folgt eine Beschreibung, wie man die
gängigen Daten unter Windows sichern kann, um anschließend die Basis-Installation
durchzuführen, die im Anschluss daran erklärt wird.

Im dritten Kapitel folgt der Einstieg in Unity, der grafischen Oberfläche von Ubuntu.
Dort wird ein guter Einstieg in die Komponenten des Unity-Desktops gegeben, etwa
wie der Launcher und die Dash funktioniert und was man alles im oberen Panel findet.

Im Anschluss wird im vierten Kapitel dargelegt, wie man seine Ubuntu-Installation
einrichtet. Darunter fällt, wie man sich mit einem WLAN-Netzwerk verbindet und
was man zu tun hat, wenn etwa der WLAN-Stick nicht automatisch erkannt wird. Weiterhin
wird auch noch thematisiert, wie man einen UMTS-Stick in Betrieb nimmt. Neben dem
Internet-Zugang wird auch noch genannt, wie man das System aktualisiert, fehlende
Sprachpakete nachinstalliert und die Grafikkarte einrichtet. Zum Schluss des
Kapitels werden noch die Drucker- und Scanner-Einrichtung sowie die Installation von
Software aus dem Software-Center und auch außerhalb des Software-Centers beschrieben.

Das fünfte Kapitel befasst sich mit der Nutzung von Firefox, Thunderbird, Empathy
und Skype. Mit dabei ist auch eine Anleitung, wie man seine Daten aus dem Firefox-
und Thunderbird-Profil aus dem Windows-System überträgt und in Ubuntu nutzbar
macht.

Im sechsten Kapitel dreht es sich darum, wie man Windows-Programme unter Ubuntu
benutzt. Dabei wird nicht nur Wine genannt, sondern auch wie man sich mit VirtualBox
eine Windows-VM aufsetzen kann.

Das siebte Kapitel widmet sich vom Einsatz von Ubuntu in einer Gruppe, also
die Erstellung und Verwaltung mehrere Benutzerkonten auf einem System.

Anschließend folgt im achten Kapitel ein ausführlicher Überblick über LibreOffice.
Konkret wird mit vielen Screenshots bebildert die gängige Nutzung von Writer,
Calc, Impress und Draw beschrieben.

„Mit Ubuntu in der Cloud“ lautet das Thema des neunten Kapitels. Hierbei geht es
grundsätzlich um verschiedene Cloud-Speicherdienste. So wird erklärt, wie man
Dropbox, Copy, Wuala und Spideroak unter Ubuntu installiert und einrichtet.

Das zehnte Kapitel beleuchtet die Sicherheit des Systems. Allerdings ist die
Beschreibung zum Sicherheitskonzept eher kurz. Stattdessen ist eine Empfehlung
für sichere Passwörter enthalten und eine kurze Beschreibung wie man die Chronik
in Firefox deaktiviert. Weiterhin wird noch erläutert, wie man seine Dateien
mit dem Programm Déjà Dup sichert.

Etwas in die Tiefe geht das elfte Kapitel, in dem die Verzeichnisstruktur von
Linux-Systemen erläutert wird. Außerdem folgt ein recht kurzer Einblick in die
Nutzung des Terminals.

Das zwölfte Kapitel leitet das Ende des Buches ein, in dem die vorinstallierten
Programme in Ubuntu kurz beschrieben werden. Anschließend folgt das letzte Kapitel,
in dem erläutert wird, wie man eine VPN-Verbindung zu der heimischen Fritz!Box
einrichten kann.

### Wie liest es sich?

Wie es der Titel bereits beschreibt, eignet sich das Buch wirklich nur für
Einsteiger und Umsteiger. Insbesondere richtet sich das Buch auch an Einsteiger,
die bislang nur wenig einen Computer benutzt haben. Die Beschreibungen in dem
Buch sind in der Regel ausführlich, auch wenn nur auf die gängigsten Funktionen
eingegangen wird. Außerdem werden sehr viele Screenshots verwendet. Die Anleitungen
lassen sich dadurch sehr gut und einfach nachvollziehen. Das Buch ist in der
Ich-Perspektive geschrieben und enthält auch einige persönliche Meinungen des
Autors.

### Kritik

Das Buch ist der Nachfolger des Buches „Ubuntu 12.04 - Für Ein- und Umsteiger“,
welches in [freiesMagazin 10/2012](http://www.freiesmagazin.de/freiesMagazin-2012-10 freiesMagazin 10/2012)
rezensiert wurde. Es lohnt sich also ein Blick zurück zu werfen, um zu schauen,
ob die angebrachte Kritik umgesetzt wurde. Leider wurden damals schon eine Vielzahl
an Dingen bemängelt, die in dieser Ausgabe größtenteils wieder auftauchen.

In der vorherigen Ausgabe des Buches war nicht bekannt, welche Kenntnisse und wie viel
Erfahrung der Autor im Umgang mit Ubuntu und Linux im Allgemeinen besitzt.
Das hat sich in dieser Ausgabe nicht geändert, da keinerlei Informationen zum
Autor dem Buch zu entnehmen ist.

Ungünstig umgesetzt waren damals die Screenshots, da diese teilweise vom Bildschirm
abfotografiert wurden, schlecht angeschnitten wurden, sowie die Abbildungen
viel zu dunkel geraten sind. In dieser Ausgabe sind zwar eine Vielzahl an
abfotografierten Bildschirm-Abbildungen verschwunden, einige wenige sind allerdings
immer noch enthalten. Die Abbildungen sind teilweise immer noch zu dunkel, was
auch daran geschuldet ist, dass es gänzlich in Schwarz-Weiß gedruckt ist.
Schlecht und merkwürdig angeschnittene Screenshots sind allerdings weiterhin enthalten, sodass
man einige findet, in denen Teile der Titelleiste eines Fensters fehlen
oder einige Buchstaben im Nichts verschwinden. Ebenfalls fallen einige Screenshots
negativ auf, die im Vergleich zu anderen Screenshots deutlich verschwommener sind
als der Rest. Diese Fehler sind optische Feinheiten, die man sehr einfach hätte beheben können.

Bereits im August 2011 wurde die [Hardwaredatenbank](http://ubuntuusers.de/ikhaya/2292/ Hardwaredatenbank)
von ubuntuusers.de eingestellt, da sie nicht nur schlecht gewartet sondern auch
nicht sonderlich aktuell war. Obwohl dies bereits in der vorherigen Rezension
kritisiert wurde, fand auch dieser Teil den Weg in das Buch mit dem Zusatz,
dass die Hardwaredatenbank „aktuell und gut gepflegt“ ist.

Gravierend sind außerdem die zahlreiche inhaltlichen Fehler, die sich in diesem
Buch finden. So wird diesmal wenigstens kurz erwähnt, dass es 32-Bit- und 64-Bit-Systeme 
gibt und wie man prüfen kann, ob man ein 64-Bit-fähige Hardware vorhanden
ist. Allerdings wird auch diesmal empfohlen die 32-Bit-Variante zu nutzen, wenn
man sich nicht sicher ist. Skandalös ist allerdings die Aussage des Autors, 
dass angeblich nicht alle Entwickler erkannt haben, dass sich die 64-Bit-Architektur
durchgesetzt hat. Dazu schreibt der Autor, dass Erfahrungen zeigen, dass die
32-Bit-Versionen angeblich stabiler und nahezu störungsfrei laufen, was bei
der 64-Bit-Version nicht der Fall sei, da sie mehr Probleme verursache. Dies führt
bei Unwissenden, an welche das Buch ja gerichtet ist, eher dazu, dass sie Angst
haben und die 32-Bit-Version installieren, obwohl die 64-Bit-Version sehr wohl
stabil ist.

Wie bereits erwähnt, ist das Buch übersät von zahlreichen Fehlern. Unter anderem
heißt an einer Stelle der Desktop nicht mehr „Unity“ sondern „Utility“. Außerdem
wird auf die Downloadseite auf unbuntu.com verlinkt. Der Tippfehler führt interessanterweise
trotzdem auf die richtige Webseite auf ubuntu.com. An anderer Stelle wurde statt
auf ubuntuusers.de auf ububtuusers.de verlinkt, ebenfalls ein Tippfehler, der
eigentlich bei den Korrekturen auffallen hätte müssen. Ungünstig ist auch, dass ein
paar Befehle falsch aufgeführt werden, ein '''Sudo''' oder '''Apt-get''' mit beginnenden
Großbuchstaben führen nämlich zu einem Fehler. An andere Stelle wird zwar in der
Kapitelbeschreibung erwähnt, dass die Nutzung von Gwibber erklärt wird, zu finden
ist sie allerdings nicht. Interessant ist auch, dass bei der Anleitung zur
Installation von Grafikkarten-Treibern scheinbar ein Absatz oder gar eine ganze
Seite fehlt, da der Teil inhaltlich einfach keinen Sinn ergibt.

Ebenfalls glänzt der Autor mit gefährlichen Halbwissen, so ist es angeblich
durch den Bug [Heartbleed](http://heartbleed.com/) möglich, den Quellcode
der „Sicherheitssoftware SSL“ anzugreifen. Außerdem bringt
der Autor zwar ein einigen Stellen Datenschutz ein, schreibt allerdings größtenteils
auch nur, dass man die Chronik der besuchten Webseiten im Firefox abschalten sollte und
Cookies nach dem Beenden von Firefox automatisch löschen lassen sollte. Wichtigere
Dinge wie etwa das Verhindern des Trackens von Webseiten-Besuchern mittels Plug-ins wie Ghostery oder Tor
werden in keinem Wort erwähnt.

Inhaltlich fehlen meiner Meinung nach noch ein paar wichtige Dinge im Umgang
mit Ubuntu. Zwar wurde zwar wenigstens diesmal ganz kurz erwähnt, dass es andere
Derivate gibt, allerdings wäre da ein paar genauere Informationen schon sinnvoll
gewesen, da Ubuntu nicht nur Unity, sondern eben unter anderem auch Kubuntu, Xubuntu und
Lubuntu ist. Ein eigenes Kapitel wäre hierfür eher angebracht, als das Kapitel
zur Installation und Einrichtung eines VPN zur heimischen Fritz!Box. Dieses Kapitel
wird merkwürdigerweise mit Windows-Software angeleitet.

Weiterhin fehlen jegliche Informationen zu den LTS- und STS-Versionen von Ubuntu.
So wird der Leser nicht schlau, wann er sein System aktualisieren sollte, wie
lang die Unterstützung für das System läuft und in welchen Rhythmus die LTS-Versionen
erscheinen. Weiterhin wäre es für Ein- und Umsteiger eher angebracht, mehr auf
äquivalenten Windows-Programme unter Linux einzugehen. Stattdessen wird ausführlich
auf Wine eingegangen und geschrieben, dass es mit einem großen Teil von nicht
allzu komplexen Windows-Programmen funktionieren soll. Sinnvoller wäre es daher,
nicht nur Firefox, Thunderbird und LibreOffice ausführlich zu erklären, sondern
auch etwa die Verwaltung von Fotos mit entsprechenden Programmen oder die Verwaltung
von Dateien mit Nautilus.

Ebenfalls schwach ist, dass das Konzept der Paketverwaltung kaum zur Geltung kommt.
Es wird zwar erwähnt, dass man im Software-Center sehr viele Programme findet,
dazu fehlt allerdings die Information zur dahinter liegenden Paketverwaltung und
wie es mit den Aktualisierungen der Programme über die Aktualisierungsverwaltung
aussieht. Fatal ist auch, dass sowohl Skype als auch VirtualBox als Deb-Paket
über die Hersteller-Seite heruntergeladen werden, statt das Standard-Repository
beziehungsweise das Partner-Repository zu nutzen. Zwar wird einige Kapitel später
die Installation von Skype nochmals angeschnitten und das Canonical-Partner-Repository
erwähnt, allerdings erfolgte vorher die Installation des völlig veralteten Skype-Pakets
in Version 2.2. Hier scheint man einfach das Kapitel aus dem Vorgänger-Buch
ohne Korrekturen übernommen zu haben.

Die Nutzung des Terminals wird zwar in dem Buch kurz angeschnitten, allerdings
nur sehr sehr oberflächlich und mit dem Zusatz, dass das Terminal ja nur
von Profis genutzt wird. Ein positiver Blick auf die sinnvollen Funktionen
eines Terminals fehlt völlig, stattdessen wird es als Tool für Profis negativ
abgestempelt.

### Fazit

Das Buch macht leider gar keinen guten Eindruck. Ein Großteil der
Anleitungen ist sehr gut auf Ein- und Umsteiger zugeschnitten, sodass diese schon
die Zielgruppe gut ansprechen. Nachteilig sind allerdings die zahlreichen optischen und
inhaltlichen Fehler, die schon ziemlich gravierend sind und womit der Autor seine
Unwissenheit zur Schau stellt. Für die meisten dürften sich daher das Buch
nicht sonderlich lohnen, da kann man die 15 Euro sinnvoller einsetzen. Der Autor selbst
verweist häufig auf das Wiki von ubuntuusers.de. Dort findet man nicht nur mehr Informationen,
diese enthalten in der Regel auch nicht solche gravierenden Fehler.

Die [Errata zum Buch](http://www.it-fachportal.de/media/vmi-buch/texte/download/7641_errata.pdf)
gibt einen kleinen Einblick in das Geschriebene, wobei in der Errata immer noch
zahlreiche Fehler enthalten sind, die in dieser Rezension aufgeführt werden.

#### Buchinfo:

**Titel:** Ubuntu 14.04 - Für Ein- und Umsteiger

**Autor:** Christoph Troche

**Verlag:** bhv, 2014

**Umfang:** 267 Seiten

**ISBN:** 978-3-8266-7641-3

**Preis:** 14,99€
