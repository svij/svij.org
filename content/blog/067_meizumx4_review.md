---
title: "Review Meizu MX4 - Ubuntu Edition"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2015-07-05 11:20:00+02:00"
toc: true
---

Since the end of June one can buy the Meizu MX4 in the EU, if you manage to get an invite. I'm in the group of the „Ubuntu Phone Insiders“ and got the device a few of weeks ago. It's time for a review!

A few months back I did a long review of the [bq Aquaris E4.5](https://svij.org/blog/2015/03/01/ubuntu-phone-a-deep-look-into-the-bq-aquaris-e45/). The Meizu MX4 is the third available Ubuntu Phone, next to the Aquaris E4.5 and the Aquaris E5, both from bq. The MX4 is the first device from the chinese manufacturer Meizu.

### The Software

I've already covered all the software features in my review of the Aquaris E4.5. The software on both phones is pretty much the same. The installed apps and scopes are the same. Over the last couple of months Canonical rolled out a few updates of the system, this included the update of the base system from 14.10 to 15.04 Vivid Vervet. Many bugs were fixed and many features were added. Therefore, this article doesn't mention many software features. 

All software features which are associated with the hardware of the MX4 itself, are mentioned in the upcoming sections.

### Hardware

#### The Screen

One of the best things about this device is the screen. The size of the screen is pretty big at 5,36 inches. It has a high resolution of 1920 x 1152 pixels which results in a pixel density of ~418 ppi. The Aquaris E4.5 on the other side has 240 ppi on a 4.5 inch screen.

<a href="/graphics/meizumx4_vorderseite.webp" data-lightbox="image" data-title="The front side">
    <img src="/graphics/meizumx4_vorderseite.webp" width="250" style="float: left; margin:20px;">
</a>

Besides the numbers, the screen is truly very great. The fonts, the photos and also app icons are pretty sharp on this screen. Also, the brightness and the colors are excellent. 

On the other hand, the user interface doesn't yet make the most out of the high resolution. App icons and fonts are pretty huge, the App Scope e.g. only shows three app icons next to each other. This will be hopefully fixed with the next OTA software update. So you can expect that the higher resolution will be better used in the near future.

#### The Camera

Next to the display, the camera is another thing which is great. It makes photos with 20,7 Megapixels and the photos are pretty good! The camera of the Aquaris E4.5 makes washy photos with a low color reproduction even on good lighting conditions. The MX4 on the other side even makes good photos in low light conditions.

There is one disadvantage though: The camera doesn't seem to focus properly in low light conditions. Therefore, pictures get blurred pretty easily.

<a href="/graphics/meizumx4_photo1.webp" data-lightbox="image" data-title="Flowing water">
    <img src="/graphics/meizumx4_photo1.webp" style="margin:20px;">
</a>

<a href="/graphics/meizumx4_photo2.webp" data-lightbox="image" data-title="The 'Dortmunder U'">
    <img src="/graphics/meizumx4_photo2.webp" style="margin:20px;">
</a>

<a href="/graphics/meizumx4_photo3.webp" data-lightbox="image" data-title="At a church in Dortmund">
    <img src="/graphics/meizumx4_photo3.webp" style="margin:20px;">
</a>

#### Mobile data and WiFi

Many people criticised the missing LTE support on the bq Aquaris E4.5. The MX4 does support the LTE frequencies used by european network providers. It also supports HSDPA. The phone has one Micro SIM slot on the backside, you have to remove the back cover to insert the sim card.

There is obviously also WiFi available. The automatic switch between mobile data and WiFi is really buggy, it often happens that the device doesn't connect automatically to the saved WiFi Access-Points, even if they're available. If you manually click on the WiFi Access-Point then it does connect normally. Similar things happen the other way around: sometimes it says that you're still connected to the WiFi, even if you moved out of coverage. I hope this will be fixed in the near future.

#### Battery life

The battery has a capacity of 3100 mAh. It powers the MediaTek Octa-Core CPU, 2GB of RAM and the big display. Sadly there is a bug in the system, which consumes too much energy even if you the phone is idling in standby. Therefore the battery often discharges very fast. A similar bug was also present on the Aquaris E4.5, but that one was fixed a few weeks before it got delivered to the first customers.

You can remove the back cover of the phone, but the battery cannot be removed.

#### Haptics and Quality

<a href="/graphics/meizumx4_hand.webp" data-lightbox="image" data-title="The MX4 is comfortable in the hand">
    <img src="/graphics/meizumx4_hand.webp" width="250" style="float: left; margin:20px;">
</a>

The overall finish is great. The frame and the backside are made of aluminium. The device isn't creaking on any point. It has a higher build quality than the Aquaris E4.5. The screen is protected by Gorilla Glass 3 which should be scratch-resistant. Anyway my phone got a few scratches after I carried it in my pocket for a few weeks. The glass on the camera on the other side is protected by a Saphire Glass. Also, the back cover got a small scratch. There are three hardware buttons on the device: two volume buttons and the power button. The volume buttons are on the left side and the power button at the top. The position of the latter is a disadvantage because you can't easily reach it with a finger. There also is a soft home button below the display.

Even if the device is pretty big, it feels comfortable in my hand. At the beginning you have to be carefully to get used to the slippery back. Otherwise you might drop it.

<a href="/graphics/meizumx4_rueckseite.webp" data-lightbox="image" data-title="The back side">
    <img src="/graphics/meizumx4_rueckseite.webp" width="250" style="float: right; margin:20px;">
</a>

#### Performance

The phone is powered by a [MediaTek Octa-Core CPU](http://www.mediatek.com/en/products/mobile-communications/smartphone1/mt6595/). It has four ARM Cortex-A17 cores and four ARM Cortex-A7 cores. It follows the big.LITTLE CPU architecture. The higher powered cores are only used when they are really needed, otherwise the lower powered cores are being used. This results in a lower overall power consumption.

Theoretically the CPU can be pretty fast. In practice the system isn't as fast as you might expect when compared to high end Android phones. There are still many stutters when you switch between scopes. The start-up time for apps is still relatively high at roughly three seconds.

The device has 2 GB of RAM, but it seems that apps are often killed and automatically restarts when you switch between them. This is another bug which is currently worked on, but it really is annoying for the end user.

### Conclusion

Actually the device has pretty good hardware, particularly the display is really good. The sad thing is, that the software isn't in a satisfying state. The Aquaris E4.5 definitely has much better software support than the MX4. For example there is the soft home button below the display which also includes the notification LED, which is actually not working right now. When you press the button, you switch to the first scope. This is actually a breach with the Ubuntu Phone UX design, which doesn't need any buttons besides the power and volume keys. One often accidentally presses the home button when one use a bottom edge gesture.

Another big problem is the overheat of the device. Even if you only use the phone for a couple of minutes it heats up extremely. It's not happening all the time, but if you play games or use the browser for example, then the temperature goes up to beyond 40°C.

Besides of all the negative points, the MX4 is still faster than the Aquaris E4.5 on a daily usage. The apps are starting faster, but many elements of the system are still laggy and stuttering, similar to the Aquaris E4.5. It seems for me, that the MX4 didn't get much love from the developers compared to the E4.5. The latter device has a far better hardware support. Many things will be hopefully fixed with the upcoming updates. The issues with the battery and the WiFi-Connection are one of the things which are annoying every day. Anyway the MX4 will be in my pocket next to my Nexus 4 with Android. The bq Aquaris E4.5 was mostly off and at home. I really love the screen and the camera and I hope that software will get better over time!
