---
title: "Git Merge-Konflikte lösen mit fac"
categories: ["Git"]
date: "2018-02-09T08:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/7e15f67f2eb04daea278bf8f66531ec6"
---

Wer häufiger mit Git arbeitet und dort auf Merge-Konflikte stößt, hat bei größeren
Konflikten keinen sonderlich großen Spaß diese aufzulösen. Häufig bietet es
sich dabei an, ein Mergetool zu verwenden, der einem die Arbeit erleichtern soll.
Einige Mergetools existieren für die Kommandozeile, andere auch als grafische
Tools.

Kürzlich bin ich auf das kleine Tool „[fac](https://github.com/mkchoi212/fac)“ gestoßen, was
kurz für „Fix All Commits“ steht. „fac“ ist ein in Go geschriebenes CUI, also
ein Commandline User Interface, um Merge-Konflikte aufzulösen.

Das Schöne daran ist die einfache Nutzung und die übersichtliche Darstellung
im Terminal, der zunächst erstmal nur die benötigten und relevanten Zeilen anzeigt.
Zusätzliche Zeilen lassen sich allerdings mit wenigen Tastendrücken anzeigen.

Aussehen tut dies etwa so:

![Git Merge fac](/graphics/git-merge-fac.webp)

„fac“ ist in Go geschrieben und kann über `go` auch heruntergeladen werden:

    $ go get github.com/mkchoi212/fac

Die Binärdatei liegt anschließend unter `~/go/bin/fac`. Den Pfad sollte man dann
entweder zu seiner `$PATH` Variable hinzufügen oder direkt aufrufen. Leider
gibt es noch keine Unterstützung als Git Mergetool. Dazu existiert aber ein
[Issue](https://github.com/mkchoi212/fac/issues/4).

Natürlich gibt es auch andere Mergetools, die man verwenden kann. Mir gefällt in
diesem Fall die einfache Benutzung und die geringe Einstiegshürde, die das Tool
mit sich bringt. Verbesserungspotenzial ist da natürlich auch da, da eine schönere
Diff-Ansicht des Konfliktes begrüßenswert wäre.
