---
title: "Mit Git Worktree mehrere Arbeitsverzeichnisse managen"
categories: ["Git"]
date: "2017-04-14T23:10:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg09.met.vgwort.de/na/ca9bc8e1baea40ee828a6f02277d23c5"
---

In der Regel klont man sich ein Git-Repository, in dem auch ein Arbeitsverzeichnis
eines Branches ausgecheckt ist. Je nach Projektart
kann es passieren, dass eine Arbeitskopie nicht reicht. Das ist etwa dann der Fall,
wenn man ein Projekt kompilieren muss, was eine Zeit in Anspruch nimmt, in derselben
Zeit aber auf einem anderen Branch weiterarbeiten möchte. Im Standard
geht das so mit einem geklonten Repository nicht, da sich beim Wechsel des Branches
das Arbeitsverzeichnis verändert und somit auch die Kompilierung nicht voll
durchläuft bzw. es zu Fehlern kommt.

Was an dieser Stelle einige machen ist folgendes: Ein weiteren Klon des Repositorys
anlegen und darin weiterarbeiten. Das ist aus gleich mehreren Gründen eine nicht
optimale Option. Zum einen verliert man die ganze Konfiguration eines Git-Repositorys,
darunter fallen insbesondere die Konfiguration von Remote-Repositorys und die
angelegten lokalen Branches und Commits. Bei dem zweiten Klon kann man zwar die
Remote-Repositorys konfigurieren, zwischen rein lokalen Branches zu wechseln
ist dann aber schwieriger. Ein weitere Grund ist der Plattenplatz, da
gleich zweimal das gleiche Repository Platz auf der Festplatte belegt. Bei kleinen Repositorys ist das
vielleicht weniger relevant und verschmerzbar, bei größeren Repositorys kann das aber durchaus
ungünstig sein.

Eine Lösung für das Problem bietet `git worktree`. Mit `worktree` lassen sich
mehrere Arbeitsverzeichnisse handhaben, die zu einem Repository gehören. Damit
kann man gleich mehrere Branches in verschiedenen Ordnern auschecken, mit diesen
Arbeiten. Im Endeffekt hat man weiterhin nur ein Repository, in dem alle Revisionen
und Objekte gespeichert sind.

### Mit Worktrees arbeiten

Ein zusätzlicher Worktree kann recht einfach angelegt werden. Der Befehl ist
allerdings vergleichsweise lang. Wenn man sich schon in einem Arbeitsverzeichnis
eines Git-Repositorys befindet, dann reicht folgender Befehl, um einen weiteren
Worktree anzulegen:

    $ git worktree add -b fix/0815 ../temp-worktree master

Der Subbefehl `worktree` ist wohl genauso klar, wie der Parameter `add` um einen
weiteren Worktree anzulegen. Mit `-b fix/0815` gibt man den Namen des neuen Branches
an, der für den Worktree genutzt werden soll – dies ist somit äquivalent zu
`git checkout -b fix/0815`. Danach folgt der Pfad, wo der Worktree
lokal hingesetzt werden soll. Zum Schluss noch der Name des Branches, von dem
der neue Branch angelegt werden soll, in diesem Fall einfach `master`.

Das ganze geht auch etwas einfacher, wenn man nicht einen neuen Branch anlegen
möchte:

    $ git worktree add ../fix-4711

Wenn man anschließend in den Worktree wechselt und schaut, auf welchem Branch
man sich befindet, dann ist es der neue Branch `fix-4711`. Als Basis-Branch wurde
dafür dann automatisch der aktuelle Branch genommen. Wie man sieht, legt Git-Worktree
dann automatisch den Worktree an dem angegebenen Verzeichnis an und legt den Branch
mit demselben Namen an. Mit `list` kann man sich alle Worktrees anzeigen lassen.

    $ git worktree list
    /home/sujee/Repositories/svijorg       bbc5c37 [master]
    /home/sujee/Repositories/fix-4711      bbc5c37 [fix-4711]

Wenn man die Arbeit an einem Worktree abgeschlossen hat und es nicht mehr braucht,
kann man den Worktree einfach entfernen, etwa mit `rm`. Anschließend sollte man
nur die Referenz im eigentlichen Repository dann löschen.

    $ rm -rf ../fix-4711
    $ git worktree prune

Die Referenz auf nicht erreichbare bzw. nicht existente Worktrees werden von
der Garbage Collection nach und nach selbst gelöscht. Wenn man allerdings Worktrees
auf potenziell nicht erreichbaren Verzeichnissen ablegt, also auf mobilen oder
Netzwerk-Datenträgern, dann kann man sie auch locken, um ein `prune` von der GC
zu verhindern.

Die übliche Handhabung von Worktress ist sonst wie in einem normalen Repository
auch: Man kann wie gehabt von allen Worktrees aus Branches anlegen, Commits
erzeugen und die Änderungen pushen und pullen.

Für viele dürfte `git worktree` ein Feature sein, dass sie nicht brauchen, für
einige wiederum ist dies doch ein sinnvolles Feature von Git, was man hin und
wieder gebrauchen kann und dabei etwas Arbeit und Zeit spart.
