---
title: "Homeserver Setup in 2020..."
categories: ["Server"]
date: 2020-06-12T07:40:00+02:00
tags: ["ubuntuusers-planet"]
toc: true
vgwort: "https://vg02.met.vgwort.de/na/adf6f9e8e80741b392ba21d562704762"
---

Dieses Blog nutze ich leider viel zu selten, um mein eigenes Hardware- und
Software-Setup zu dokumentieren. Manchmal tue ich das zum Glück schon, sodass
ich im Nachhinein dann doch hin und wieder mal nachlesen kann, wie ich das
ein oder andere früher mal gelöst habe.

Jetzt ist es jedenfalls wieder an der Zeit ein Update für mein Homeserver
zu liefern. Wie auch in den letzten Jahren diente das Frickeln an meinem Homeserver zwei Zielen:

1) Ich will was lernen.
2) Es sollen ein paar Dienste betrieben werden.

Prinzipiell sollten zwar die Dienste funktionieren, aber im Vordergrund steht
auch neue Technologien und Tools auszuprobieren, um was Neues zu lernen.

# Früher™

Vor knapp [sechs Jahren](/blog/2014/07/15/homeserver-setup/)
ersetzte ich meinen Raspberry Pi durch „richtige“ Hardware. Damals allerdings
auch nur 4GB RAM mit einem Board mit einer Atom CPU. Damit geriet ich schnell
an meine Grenzen, sodass das ganze wenig Spaß gemacht hat. Damals lief darauf
ein ArchLinux und diente hauptsächlich als NAS.

2016 [migrierte ich von ArchLinux auf Ubuntu 16.04](/blog/2016/04/24/homeserver-migration-auf-ubuntu-1604/).
Dann lief schon mit ownCloud ein bisschen mehr als nur ein Fileserver und mit
mehreren LXC/LXD Containern. Später im Jahr 2016 ersetzte ich die Hardware und
schaffte mir ein Dell PowerEdge T20 mit einer Quad-Code Xeon CPU und 32GB RAM
an. Die Hardware läuft auch noch heute und hat noch genügend Leistung.

In der Zwischenzeit experimentierte ich vor allem Software aus der „Cloud Native“ Welt herum.

# Heute

Heute läuft der Server mit Ubuntu 20.04. Darauf laufen keine LXC/LXD Container mehr.
Stattdessen läuft jetzt Kubernetes darauf. Zu Beginn nutzte ich für die Installation
von Kubernetes [Kubespray](https://github.com/kubernetes-sigs/kubespray) ein, das
Standard-Kubernetes war allerdings deutlich zu fett. Größtes Manko war, dass
das `etcd` verdammt viel auf die SSD geschrieben hat und ich regelmäßig bei
jedem Upgrade das Setup kaputt gespielt habe. Für den Privatgebrauch ergab das
so daher keinen Sinn.

Mittlerweile setze ich auf das vergleichsweise schlanke
[k3s.io](https://k3s.io/). Das braucht im Heimbetrieb deutlich weniger Leistung
und erledigt trotzdem das, was ich brauche. Statt etcd setzt es auf sqlite und
statt Docker auf containerd.

Die Kubernetes Manifeste verwalte ich ausschließlich mit „dem GitOps Operator“
[fluxcd](https://fluxcd.io/). Wesentlicher Vorteil ist, dass der Zustand des
Clusters in einem Git-Repository definiert ist und eine vollständige Neuinstallation
innerhalb weniger Minuten durchgeführt werden kann. Im Gegensatz
zu Ansible etwa, gebe ich hier explizit meinen Zielzustand an und eben keine
Abfolge von Anweisungen. Die meisten Anwendungen sind über Helm-Charts installiert.
Einige wenige sind ganz normal als Deployment YAML geschrieben. `kubectl`
rufe ich daher nur zum Testen manuell auf. In der Regel mache ich daher einfach
Commits in das Repository und pushe es auf mein GitLab-Repository. fluxcd kümmert
sich dann darum, es auf dem Cluster auszurollen. Ziemlich angenehm.

Nicht zu vernachlässigen ist bei diesem Setup allerdings, dass die Lernkurve
von Kubernetes verdammt steil ist. Wenn man Zeit und Lust hat, kann man das gut
und gerne so umsetzen. Wirklich sinnvoll im Heimgebrauch ist es für meinen
Anwendungsfall eigentlich nicht, da ich kaum die Skalierungsmechanismen von Kubernetes
brauche und die Komplexität von Kubernetes nicht zu unterschätzen ist. Die
Handhabung ist, sobald man sich mit Kubernetes angefreundet hat, für mich
jedenfalls deutlich angenehmer, als vorher, wo ich Ansible Rollen und Playbooks
geschrieben habe, die auch sehr viel Zeit in Anspruch genommen haben.

# Anwendungen

Folgende Anwendungen laufen mittlerweile auf meinem Kubernetes-Cluster:

 * [Nextcloud](https://nextcloud.com/) für Kalender, Kontakte und Dateisynchronisation
 * [Bitwarden](https://github.com/dani-garcia/bitwarden_rs) als Passwortmanager
 * [Prometheus](https://prometheus.io/) für das Monitoring
 * [Grafana](https://grafana.com) für die Visualisierung der Daten aus Prometheus, Influx und PostgreSQL
 * [GitLab](https://about.gitlab.com) für Git-Repositorys
 * [Unifi](https://ui.com) Controller für meine Unifi APs
 * [Syncthing](https://syncthing.net) für das Synchronisieren von Backups zu einer Offsite-Stelle
 * [rclone](https://rclone.org) als Cronjob für das Hochladen von Backups „in die Cloud“
 * [Dataexporter der Photovoltaik-Anlage](https://gitlab.com/svij/kostal-dataexporter), der die Daten in eine Influx und PostgreSQL Datenbank schreibt
 * [Influxdb](https://www.influxdata.com/) für die Daten der PV-Anlage, sowie Stromverbrauch und Temperatur-Sensoren
 * [PostgreSQL](https://www.postgresql.org/) für Nextcloud, Kostal Datenhaltung und GitLab
 * [Plex](https://www.plex.tv/) für Multimedia-Daten
 * [minio](https://min.io/) als S3-kompatibles Object-Storage-Backend für GitLab
 * [traefik](https://traefik.io) als Reverse Proxy
 * Bald: [Home-Assistant](https://www.home-assistant.io/) für Smart-Home

Fast alles installiere ich über fertige Helm-Charts. Einzig zwei Datensammler-Skripte
für smarte Stromsteckdosen und für die PV-Anlage erfolgen über eigene, selbst
gebaute, Container. Diese baue ich sehr einfach über die [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)
Funktionalität von GitLab auf GitLab.com.

Außerhalb von Kubernetes läuft sonst nur noch [borgbackup](/blog/2016/11/12/verteiltes-backup-konzept-mit-borg/)
und sonst eigentlich nichts mehr. Den Rest habe ich sonst weiterhin mit
Ansible durch automatisiert, allerdings durch den Einsatz von Kubernetes mit fluxcd
in einem deutlich geringerem Umfang.
