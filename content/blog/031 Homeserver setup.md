---
title: "Homeserver Setup"
categories: ["Server"]
date: "2014-07-15 18:20:00+02:00"
toc: true
---

Vor einigen Monaten überlegte ich mir einen NAS zuzulegen. Konkret dachte ich
dabei eben an einen zentralen Massenspeicher bei uns im Haus, auf dem die
diversen Rechner im Haushalt zugreifen können. Allerdings wollte ich mir auch
noch die Option offen lassen, den NAS für möglichst viele kommenden Einsatzzwecke
einzusetzen, sodass es eher ein Homeserver als ein NAS werden sollte.

Mir blieb also die Wahl zwischen Fertig-NAS-Systemen, wo man möglichst
wenig frickeln muss und einen NAS bzw. Homeserver im Eigenbau. Die Wahl
fiel mir aus mehreren Gründen leicht. Zum einen sind Fertig-NAS-Systeme
relativ teuer und zum anderen sind mir zu starr, auch wenn man diverse
Anwendungen auf den Geräten installieren kann. Ich habe nämlich durchaus Lust
zu frickeln um möglichst viele Dienste zu nutzen.
Fertige NAS-Systeme mit vier Einschubfächern für Festplatten sind mir bei Preisen
von teils weit über 300€ deutlich zu teuer.

## Die Hardware

Praktisch ist es ja, wenn man Hardware geschenkt bekommt. Durch das [Homeserver-Upgrade](http://blog.stefan-betz.net/2014/07/03/homeserver-upgrade/)
von Stefan Betz, hatte er noch alte Hardware übrig, die er mir überließ. Danke
an dieser Stelle dafür. So bekam ich ein [Gigabyte GA-D525TUD](http://www.gigabyte.de/products/product-page.aspx?pid=3549#ov)
Mainboard mit einem Intel Atom D525, 4 GB DDR2 RAM sowie einem Netzteil.

Für den Betrieb meines Homeservers brauchte ich dementsprechend noch ein
geeignetes Gehäuse sowie Festplatten. Als Gehäuse bestellte ich mir einen [Chieftec CH-09-B-B](http://www.chieftec.eu/de/gehaeuse/midi-tower-high-end/ch-09b-b.html).
Das Gehäuse ist schön groß und besitzt viel Platz für genügend Festplatten.
Für den Anfang bestellte ich mir zunächst zwei Western Digital Red 2TB Festplatten
die speziell für den 24/7-Betrieb entwickelt und getestet wurde.

## Die Software

Die beiden Festplatten betreibe ich nicht mit einem Hardware-, sondern in einem
Software-RAID-1. Effektiv kann ich somit 2 TB. Ich lasse mir dabei
ganz bewusst die Option offen, in Zukunft und bei Bedarf, auf ein RAID5 mit
mehreren Platten migrieren bzw. erweitern zu können.

Da ich faul bin, bekam der Homeserver ein Arch Linux als System spendiert.
Als Dienste laufen aktuell Plex als DLNA-Medienserver, ein Pacman Package
Cache für meine beiden Arch Linux Rechner im Haus und letztendlich noch ein
NFS-Share für alle meine Dateien. Weiterhin richtete ich mir einen dynamisches
DNS ein, dazu existiert ein [weiterer Blog-Artikel](/blog/2014/07/05/dynamisches-dns-mit-eigener-domain/).
Da ich Nutzer von Taskwarrior bin, liegen auf dem Homeserver ebenfalls zentral
dessen Daten. Einen Taskserver habe ich allerdings noch nicht eingerichtet. Ganz nebenbei
läuft noch der IRC-Terminal-Client weechat. Letztere beiden Anwendungen liefen
bei mir vorher auf einem Raspberry Pi.

Da die Festplatten-Kapazität meines Desktop-Rechners identisch mit dem des
Homeserver ist, werden meine Medien-Dateien (Fotos, Videos, etc.) sowie
mein komplettes Home-Directory auf dem Server gesichert. Dazu reichen mir
zwei simple rsync-Befehle, welche stündlich mittels systemd timer units
ausgeführt werden.

## Fazit

Die Entscheidung einen Eigenbau-Homeserver zu betreiben, bereue ich nach wenigen
Wochen Einsatz bislang noch nicht. Die Einrichtung ging abgesehen von der
Daten-Übertragung zügig von statten. In Zukunft werden wohl noch die ein oder
anderen Dienste hinzukommen.
