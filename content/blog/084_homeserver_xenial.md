---
title: "Homeserver-Migration auf Ubuntu 16.04"
categories: ["Server"]
date: "2016-04-24 18:00:00+02:00"
tags: ["ubuntuusers-planet"]
slug: "homeserver-migration-auf-ubuntu-1604"
toc: true
---

Vor fast zwei Jahren richtete ich mir meinen ersten [Homeserver ein](/blog/2014/07/15/homeserver-setup/).
Bis letztes Wochenende lief der Rechner noch mit ArchLinux. Generell war der
Betrieb von ArchLinux angenehm: Ich musste fast nichts machen und habe täglich
Updates installiert. Nichtsdestotrotz habe ich jetzt den Homeserver auf Ubuntu 16.04
migriert. Hauptsächlicher Grund war, dass ich durch die Umstellung deutlich mehr an System-Administration
lernen kann, was ich später und auch jetzt  an anderer Stelle gebrauchen kann, als es bei
einem ArchLinux-Server der Fall ist. Die Migration wäre prinzipiell recht einfach
gewesen, wenn ich den grundsätzlichen Aufbau nicht verändert hätte. Die Hardware
hingegen ist gleich geblieben.

## LXD

Eines der Gründe wieder auf Ubuntu zu setzen ist der Einsatz von Ubuntu-Tools die
nur eingeschränkt unter ArchLinux laufen oder anderweitig anstrengender sind
zum Laufen zu kriegen. Einige Dienste habe ich deshalb in LXD/LXC-Container verpackt,
was ich deutlich schöner und angenehmer in der Nutzung finde als Docker. Konkret
laufen in einzelnen Containern owncloud, transmission für das Seeden von Ubuntu-ISOs
und gogs.io für das Hosten von Git-Repositorys. Wer sich näher mit LXD beschäftigen will, der
schaut sich am Besten die [Blog-Serie](https://www.stgraber.org/2016/03/11/lxd-2-0-blog-post-series-012/)
vom LXD-Entwickler Stéphane Graber an. Für den Einstieg reichen auch schon die
ersten paar Teile. Gegebenenfalls schreibe ich zur Einrichtung und Nutzung von LXD
auch noch einen eigenständigen Artikel.

## DHCP- und DNS-Server

Der Homeserver dient jetzt auch als DHCP- und DNS-Server im LAN. Mein Hauptproblem
war, dass ich ownCloud innerhalb des LANs nicht über die dynamische DNS erreichen
konnte, Das ist zwar für stationäre Laptops und PCs recht einfach mit einem Eintrag
in `/etc/hosts` gelöst, doch geht das nicht auf Android-Geräten. Letztere hatten
nämlich ständig eine unschöne Fehlermeldung angezeigt, dass CalDav, CardDav und
ownCloud Sofort-Uploads nicht zu erreichen sind. Zum Einsatz kommt bei mir [dnsmasq](https://wiki.ubuntuusers.de/Dnsmasq/)
was es mit einfachen mitteln erlaubt einen DHCP- und DNS-Server zu betreiben. Im
gleichen Zug habe ich LXD auch an die auf dem Homeserver konfigurierte Bridge
gehangen, damit auch die Container eine IP-Adresse bekommen, damit man einfach
per SSH auf die Container zugreifen kann.

## Filesystem

Das Datengrab mit vormals 2x 2TB-Platten im Software-RAID und einem LVM habe ich
soweit aufgelöst. Dazu gekommen ist eine 120GB-SSD, wo das System und die Container
drauf liegen. Die 2x 2TB-Platten sind jetzt mit einem btrfs RAID-1 bestückt, somit
ohne Software-RAID und ohne LVM. Der kurze Gedanke ZFS einzusetzen war zwar auch
verhanden, habe es allerdings dann doch verworfen. Die Kenntnisse im Umgang mit
btrfs kann ich dann weiterhin gut auch auf anderen Distributionen nutzen.

## Configuration-Management mit Ansible

Der wohl letzte Punkt ist die Administration des Servers mit Ansible. Klingt am
Anfang zwar toll und einfach, erfordert aber dann doch recht viel Arbeit, wenn man
es ordentlich machen will. Da der Server doch eher früher als später wieder funktionsfähig
sein sollte, erfolgt die Übernahme der Konfigurationen in Ansible-Playbooks
eher nach und nach. Wiederholende Tasks lassen sich aber besonders einfach und
sinnvoll mit Ansible automatisieren. Besonders das Updaten von mehreren Containern und
den Ubuntu-PCs im Haushalt ist damit deutlich einfacher möglich.

## Fazit

Schon jetzt viel Neues gelernt und vieles noch in Planung. Ziel erreicht. ;-)
