---
title: "Neues Buch! Git Schnelleinstieg"
date: "2022-03-31T18:10:00+02:00"
categories: ["Git"]
---

Im August 2016 erschien die [erste Auflage meines Git-Buches](/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/),
welches in Zusammenarbeit mit dem [mitp-Verlag](https://mitp.de) erschienen ist.
Im Mai 2019 folgt anschließend die zweite Auflage. Und im März 2021 die dritte
Auflage.

Ab heute ist eine neue Variante des Buches da: Git Schnelleinstieg – Versionswaltung
lernen in 14 Tagen!

![Git-Schnelleinstieg](/graphics/git-schnelleinstieg.webp)

## Was ist anders?

Das Buch ist eine komprimierte Fassung meines bisherigen Git Buches: Jetzt ist
es noch erschwinglicher und fokussiert auf die wirklich wichtigen Grundlagen,
Funktionen und Workflows von Git.

## Was steht drin?

Das Buch geht in 14 kompakten Kapitel auf alle wichtige Funktionen von Git ein.
Insgesamt umfasst das Buch dabei 256 Seiten.

 * **Einführung in die Welt der Versionsverwaltung:**
    Im ersten Kapitel geht es zunächst um den Einstieg in das Thema von
    Versionsverwaltung. Konkret also was eine Versionsverwaltung ist, wofür sie
    gut ist und welche Arten es gibt.

 * **Die ersten Schritte mit Git:**
    Das zweite Kapitel befasst sich mit den Grundlagen, also wie man ein
    Repository anlegt, wie Commits man tätigt und wie Git intern grob arbeitet.

 * **Branches erstellen und mergen**
    Im Anschluss folgt auch schon das Kapitel zum Branching mit Git. Erklärt
    wird, wie man Branches erstellt, wofür sie gut sind und wie sie
    zusammengeführt werden können. 

 * **Branches per Rebase zusammenführen und das Aufräumen des Repositorys**
    Dieses Kapitel geht nun darauf ein, was Rebases sind und wie man diese ausführt.
    Weiterhin wird ebenfalls thematisiert, wie man das Repository aufräumen kann.

 * **Mit verteilten Repositorys arbeiten**
    Verteilte Repositorys kamen in den bisherigen Kapiteln noch nicht vor. In
    diesem Kapitel wird erläutert wie man mit Remote-Repositorys arbeitet und
    wie ein kleiner Workflow mit drei Repositorys aussieht.

 * **Git-Hosting mit GitHub und GitLab**
    Das sechste Kapitel befasst sich mit dem Einsatz von Git auf dem Server. Im
    Wesentlichen wird der Workflow GitHub und GitLab erläutert, sodass am Ende
    bekannt ist, wie man ein kleines Repositorys maintaint und wie man Änderungen
    mittels Pull-Requests bei anderen Projekten beiträgt und was dabei zu beachten
    ist.

* **CI/CD: Continuous Integration and Continuous Delivery**
    Hier wird thematisiert wie GitHub Actions und GitLab CI/CD funktionieren und
    wie beide Lösungen zu kontinuierlicher Integration und zum kontinuierlichen Deployment
    beitragen können.

 * **Workflows für Einzelpersonen**
    Wichtig beim täglichen Einsatz von Git in einem Projekt ist die Auswahl
    eines passenden Workflows. Hierbei werden verschiedene Workflows
    beispielhaft genannt die für Einzelpersonen relevant sind.

 * **Workflows im Team**
    Dieses Kapitel richtet sich vor allem an Teams: Es wird näher betrachtet,
    welche Workflows existieren und welche Vor- und Nachteile mit sich bringen.

 * **Umstieg von Subversion**
    Wenn der Leser nicht gerade völliger Neuling ist, dann ist der Umstieg von
    Subversion wahrscheinlich. Das Kapitel behandelt die wesentlichen Unterschiede
    in den Konzepten und in den Kommandos und bringt auch eine Anleitung wie man
    ein Subversion-Repository in ein Git-Repository überführt.

 * **Nachvollziehbare Git-Historien**
    Das erste der beiden neuen Kapiteln befasst sich mit meinem Lieblingsthema:
    Wie muss man vorgehen, um eine nachvollziehbare Git-Historie zu erstellen.
    Das Kapitel umfasst sowohl Tipps und Hinweise, wie die Commit-Messages
    auszusehen haben sollten, als auch wie groß bzw. klein die Commits sein sollten.

 * **Tipps und Tricks**
    Das Kapitel befasst sich mit einer Sammlung von Tipps und Tricks die
    man mehr und weniger regelmäßig gebrauchen kann. Thematisiert werden unter
    anderem die Fehlersuche mit Bisect und Möglichkeiten die Historie neu zu schreiben.

 * **Frequently Asked Questions**
    Dieses Kapitel stellt einige Problemstellungen vor, die bei Git-Nutzern
    regelmäßig zu Irritationen führen und häufiger nachgefragt werden. Für
    viele Probleme gibt es mehrere Lösungen und teilweise wurden diese Lösungen
    schon diverse Male in den vorherigen Kapiteln behandelt. Trotzdem soll es
    hier einen kurzen Überblick über allgemeine Probleme zum Nachschlagen
    geben, der auf die entsprechenden Stellen im Buch für nähere Erläuterungen
    verweist.

 * **Ausblick**
    Zum Schluss wird ein kleiner Ausblick gegeben, was man sich als nächstes
    Anschauen könnte.

## Leseprobe und Kauf

Eine Leseprobe gibt es natürlich auch. Diese findet sich [hier](https://www.mitp.de/out/media/9783747505267_Leseprobe.pdf)

Bücher lassen sich bekanntlicher weise an vielen Stellen kaufen zu einem festen
Preis. Das Buch ist so erschwinglich wie nie, denn sowohl in der Print, als auch
in der E-Book Variante kostet es 14,99 €. Wer das Printexemplar kauft, bekommt
die E-Book Variante direkt mit dabei.

Wer sich das Buch kaufen möchte, dem kann ich ein Kauf direkt auf der [mitp-Verlagsshop](https://www.mitp.de/IT-WEB/Software-Entwicklung/Git-Schnelleinstieg.html)
nahe legen, da dies für den Verlag und dem Autor (also mich) am besten ist.

Auf [Amazon findet man das Buch natürlich auch](https://amzn.to/3qSqlTT)
Es ist sowohl dort als auch bei anderen Händlern seit dem 31. März 2022 verfügbar.
