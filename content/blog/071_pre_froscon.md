---
title: "Ubuntu auf der FrOSCon"
categories: ["Ubuntu"]
date: "2015-07-18 12:00:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Am 22. und 23. August findet in St. Augustin bei Bonn die [FrOSCon](https://www.froscon.de)
statt. Für mich wird es der erste Besuch bei dieser Konferenz sein. Das Programm
wurde [kürzlich](http://programm.froscon.de/2015/) veröffentlicht.

Mit dabei ist auch ein Vortrag von mir: [„Snappy Ubuntu Core“](http://programm.froscon.de/2015/events/1523.html).
Dieser findet direkt zum Start am Samstag um 10 Uhr statt. Im Anschluss folgt
ein Ubuntu Phone Vortrag, der leider nicht von mir ist.
Unabhängig davon sind einige interessante Vorträge dabei, die ich mir anschauen möchte.

Mich findet man erneut am Ubuntu Stand, zusammen mit [hefeweiz3n](https://ubuntuusers.de/user/Hefeweiz3n/)
(also der Person hefeweiz3n, nicht mit Bier!). Voraussichtlich werden auch meine
beiden Ubuntu Phones (bq Aquaris E4.5 und das Meizu MX4) verfügbar sein, für die
Leute, die es ansehen und ausprobieren möchten.
