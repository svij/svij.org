---
title: "Ubuntu Phone: A deep look into the bq Aquaris E4.5"
categories: ["Ubuntu"]
tags: ["ubuntu-planet"]
date: "2015-03-01 17:30:00+02:00"
slug: "ubuntu-phone-a-deep-look-into-the-bq-aquaris-e45"
toc: true
---

<a href="/graphics/ubuntuphone_bq2.webp" data-lightbox="image" data-title="The first Ubuntu Phone.">
    <img src="/graphics/ubuntuphone_bq2.webp" width="250" style="float: left; margin:20px;">
</a>

Two years ago Canonical announced the project "Ubuntu Touch". A few weeks
back they released the first phone which is running Ubuntu pre-installed. This article
focuses on both: the hardware and the software.

The last time I have used an Ubuntu Phone is roughly 1.5 years ago on a Nexus 4. Since
then many things regarding the system has changed.

## The Software

Using Ubuntu on your phone is quite different from using any other mobile operating
system like Android or iOS. The biggest difference is the big use of gestures which
you can use from every edge of the display.

### Scopes

If you turn on the phone for the first time you get a quick introduction how to
use the Ubuntu Phone. Everyone who never used an Ubuntu Phone should not skip
this introduction, because it really helps you to use the phone. After that introduction
you can set a password or a PIN to lock your device.

If you press the power button, then you see the Welcome-Screen. There you see different
information of your usage of your phone. That includes the number of sent
and received short messages, number of calls and the number of taken photos
and videos. You can swipe the Welcome-Screen to both sides to unlock the phone. If
you have set up a lock mechanism, it will be shown there.

#### What are scopes?

<a href="/graphics/ubuntuphone_today-scope.webp" data-lightbox="image" data-title="Today-Scope.">
    <img src="/graphics/ubuntuphone_today-scope.webp" width="250" style="float: left; margin:20px;">
</a>

After you unlocked the device, you see one of the scopes. Scopes are one of the special
features of Ubuntu Phone. There are two types of scopes: aggregated and branded
scopes. Both type of scopes show different information to the users. Branded scopes have
an app-like experience. Aggregation scopes on the other side are bringing content from
different sources to the screen. It's like a good summary of different sources.

Ubuntu Phone uses a different approach compared to other mobile operating systems.
On Android or iOS you mainly have app icons and a few widgets in grids, while you have
different scopes on Ubuntu Phone. The benefit is that the user does not have to
go through all those apps if there are suitable scopes which are bringing the content
easily to the screen.

#### Pre-installed scopes

If you unlock the device the first time, you see the Today-Scope. This scope shows
you a couple of information of the current day. These are the current date, today's weather
divided into morning, evening and afternoon including the temperature. Additionally,
it shows today's appointments, the next holiday and a list of the last calls and messages.

<a href="/graphics/ubuntuphone_nearby-scope.webp" data-lightbox="image" data-title="NearBy-Scope.">
    <img src="/graphics/ubuntuphone_nearby-scope.webp" width="250" style="float: right; margin:20px;">
</a>

After a swipe to the left, you reach the NearBy-Scope. This scopes aggregates different
information from your current location. The scope shows the weather of your current
position and a couple of other interesting information. It mainly uses [yelp](http://yelp.com)
to gather the information. In the default setting it shows places of interest, restaurants
or schools. This all depends on your current location. Additionally, it shows a couple of
photos from Flickr and Wikipedia articles of your current place. A rather cool feature is
that you can set your current mood. Depending on that, the scope shows different content.
You can choose between "I'm Bored", "On The Move", "I'm Hungry", "I'm Thirsty" and "I'm Stressed".
If you are currently on the move, you can set it to "On The Move" and it will show the next
bus stop and railway station. If you click on one of those, it will launch HERE Maps and shows
the way to it. Funnily the option "I'm stressed" displays shoe and fashion stores. On the other side
"I'm Hungry" shows you a couple of restaurants right next to you.

The third scope is simple. It's just the app launcher where you find all apps in a grid. The apps
are divided into two groups: commonly used apps and all the other apps. The commonly used
apps are fixed: Telephone, Messaging, Contacts, Camera, Browser and the Clock. Using the
drop down menu you can filter the apps using categories like Games, Social Networking or Productivity.

<a href="/graphics/ubuntuphone_apps-scope.webp" data-lightbox="image" data-title="Apps-Scope.">
    <img src="/graphics/ubuntuphone_apps-scope.webp" width="250" style="float: left; margin:20px;">
</a>

The fourth scope is the news scope. It displays news from different news pages like
BBC, Engadget or Euronews. You can actuall configure the sources, but sadly you can only
disable the given sources.

There are a couple of other pre-installed and pre activated scopes like the music, video and
photo scope. All three scopes presents local content and content from the internet. For example,
if you shot a few photos and videos and copied some music songs to your device, they will be shown
on the scope. Furthermore, it shows different content from YouTube, Flickr, Facebook and Instagram. That
all depends on the specific scope and accounts.

All described scopes are pre-installed and are activated when you turn on the device
for the first time. There are still plenty of other scopes which you can activate when
you swipe up from the bottom edge. There you see the names of all scopes in a list. You can
activate or deactivate scopes by pressing the star on each scope.

There are many scopes which just cover different websites like BBC, cnet, Amazon, ebay, Reddit and many more.
All these scopes are aligned to those specific websites. Consequently, you can search
the website using the scope's searchbar. For example you get a few results if you search for items
on the ebay or Amazon scope. The only thing is that you can't actually buy anything in the scope itself. It will
just launch the browser.

### Gestures

<a href="/graphics/ubuntuphone_app-launcher.webp" data-lightbox="image" data-title="App-Launcher.">
    <img src="/graphics/ubuntuphone_app-launcher.webp" width="200" style="float: left; margin:20px;">
</a>
<a href="/graphics/ubuntuphone_app-switcher.webp" data-lightbox="image" data-title="App-Switcher..">
    <img src="/graphics/ubuntuphone_app-switcher.webp" width="200" style="float: left; margin:20px;">
</a>

Next to the scopes, another noticeable thing are the system-wide gestures. On the right and on the left
side of the screen you have two gestures which can be used globally in any app or scope. If you swipe from the left side,
you can access the quick-app-launcher similar to the Unity app-launcher on the desktop.
This can be opened when you do a short swipe. If you do a long swipe, you switch to your home scope.
On the other edge of the display you can switch between the apps. Either you can do a short swipe, then
you switch easily between two apps, or a long swipe brings you to a 3D overview of all opened apps. There
you can close an app with another swipe or you can switch to the app by clicking on it.

<a href="/graphics/ubuntuphone_anruf-verpasst.webp" data-lightbox="image" data-title="Notifications.">
    <img src="/graphics/ubuntuphone_anruf-verpasst.webp" width="200" style="float: right; margin:20px;">
</a>

On the top edge of the screen you can access the notifications and the quick-settings. In Ubuntu these
are called indicators. The cool thing here is, that you can start swiping from a specific indicator. That means
that you can swipe down from the battery indicator, which will show you the quick-settings of the battery. Same
thing when you swipe down from the notification indicator. If you misplaced your finger you can simply switch
to the other indicator by moving your finger to the right or left while swiping down.

The bottom edge of the screen does not have a global gesture. This gesture is based on the app you are currently
using. The app developer can implement this gesture in their app.

### Core- and System-Apps

There a several pre-installed apps which you need for your daily use of a smartphone. This includes
the telephone app to place calls, the messaging app to send and receive SMS and MMS messages
or the contacts app to organise your contacts. Additionally, there is obviously a camera app to take
photos and videos. The camera doesn't have any special features but it can take HDR photos, tag
your photos with your current GPS position or you can use the autotimer.

There is also a browser, which sadly doesn't have any settings. Therefore, you can only type
in the URL of a web page, bookmark a web page and open and switch between tabs.

<a href="/graphics/ubuntuphone_galerie.webp" data-lightbox="image" data-title="Gallery.">
    <img src="/graphics/ubuntuphone_galerie.webp" width="300" style="float: right; margin:20px;">
</a>

A pretty nice app is the gallery app to view your photos and videos on the device. The photos and videos
are grouped by date. So you can scroll vertically to see the photos of different days and you can
scroll horizontally on a specific date to see all thumbnails of the photos of that day. You also have
the possibility to edit photos. This functionality is rather basic so you can cut details of a photo or
you can automagically improve it.

The only available messaging app is Telegram. Other messengers like Google Hangouts, Threema, WhatsApp
or Facebook Messenger are not (yet) available. The Telegram app is working completely, you can send
messages to individual persons or to groups.

There are a few other apps which I don't mention in detail here, like the Clock, Music, Weather and Todo app.

All named apps are native Ubuntu Phone apps. There are quite many other pre-installed apps which are
mainly packaged WebApps. For example WebApps for Amazon, eBay, Facebook, GMail, Twitter or YouTube. Some
of them supports hardware interfaces or notifications. Twitter notifies you for every Twitter-mention or DM, GMail
notifies you for a new E-Mail.

In the system settings you find the same settings you can also change via the indicators, but here 
you have a few more options. For example you can add online accounts
for services like Evernote, Soundcloud, Flickr, Facebook, Ubuntu One, Twitter, Fitbit, Instagram, Vimeo
or Google. Additionally, you can set the Date and Time, Security and Privacy and you find the Update-Manager
for apps and the systems.

### Ubuntu Store

The Ubuntu Store is the place where you can find apps and scopes for your Ubuntu Phone. You can download
scopes and apps for free or there are also a few paid apps. To enter the store you need to log in into
your Ubuntu One account. Currently (March 1st) you find more than 800 apps. One guy developed an
[unofficial](https://appstore.bhdouglass.com/apps) website where you can search and find all items in the store.

Many apps are only WebApps. The quality varies strongly. Anyway you find a few good apps, like the Terminal
to use the command-line on the phone. You also find an E-Mail-Client named "Dekko" where you can set up
your IMAP account. This app is still in beta, so there are still a few basic features missing. You can't actually receive
push E-Mails and you can't automatically fetch your mails. You can only fetch the mails manually.
Another app is the calendar which isn't pre-installed. Like the contacts app you only have the possibility to
synchronise your appointments using Google Calendar. "Fahrplan" is a nice app, if you are a frequent user of public
transportation. You can plan and check your trip using this app.

## The hardware

<a href="/graphics/ubuntuphone_bq_notificationLED.webp" data-lightbox="image" data-title="Notification LED.">
    <img src="/graphics/ubuntuphone_bq_notificationLED.webp" width="300" style="float: right; margin:20px;">
</a>

The bq Aquaris E4.5 has a Quadcore-MediaTek-CPU with a clock rate of 1.3 GHz. The display has a size of 4.5"
with a resolution of 540x960 pixels. It has 1 GB of RAM and 8GB of internal storage which you can extend with extra 32GB
by using a microSD card. The operating system is already using 2.5 GB of the internal storage.

The smartphone is 6.7 cm wide and 13,7 cm high. On the front side you have obviously the display, the ear cap, a front
camera with 5 Megapixels and a notification LED. On the rear you find the 8 Megapixel Kamera with a Dual-LED-Flashlight.

<a href="/graphics/ubuntuphone_bq_rueckseite.webp" data-lightbox="image" data-title="Back cover of the phone.">
    <img src="/graphics/ubuntuphone_bq_rueckseite.webp" width="300" style="float: left; margin:20px;">
</a>

The device is only 9 mm thin and weighs 123 gram. It is relatively light. On the bottom edge the device has the
microUSB connection and the speakers. On the left side it has two microSIM slots and on the top you can insert the microSD
card and you can connect your headphones.

### The camera

The camera can take photos with a resolution of 8 Megapixel. If you take and view photos or videos on the device
itself, it does look good, but if you view it on your computer then you see that the quality isn't really good. The colors
are washy and even on pretty good lighting conditions the color reproduction isn't very well.

If you take photos on low light, you see many disadvantages of the camera. The photos do have a high image noise.

You should not forget that the phones price is only 170€, so you can't expect really good cameras in the phones
in this price area. The quality of the photos is enough for some simple photos.

<a href="/graphics/ubuntuphone_cam_food.webp" data-lightbox="image" data-title="Actual camera photo: Food.">
    <img src="/graphics/ubuntuphone_cam_food.webp" width="300" style=" margin:20px;">
</a>
<a href="/graphics/ubuntuphone_cam_london.webp" data-lightbox="image" data-title="Actual camera photo: London.">
    <img src="/graphics/ubuntuphone_cam_london.webp" width="300" style=" margin:20px;">
</a>

### Haptics and Quality

The haptics of the phone is pretty good. The back cover is made of flat hard plastics with the disadvantage that it is a
bit slippery. Also, the build quality is good, you can't hear any creaking when pressing the device on different spots.

I pretty much like the design. It looks like the phone is divided in two parts, because the front half is a bit smaller than the
back half of the device. I'm using the device for three weeks now and the back cover doesn't have any scratches. Sadly
the front does have some scratches. You can see and feel it on the border of the display. Even though the display itself
does only have a few scratches, which you can only see when you have good eyes. Anyway you should avoid carrying the
phone and your keys in the same pocket.

## Overall impression

Ubuntu Phone as a system and the device are in a rather good shape with a few restrictions. You do need to
learn to use the device which takes a while. But afterwards you might use the device easier and faster than other
devices with another operating systems. At the start it's hard to use the correct gestures. Personally I managed to
perform a gesture which I didn't want to do. Therefore, I often ended up in another app. For example this happened
while swiping between all the scopes.

Basically the system is quite fast and doesn't have too many "minutes of silence". But the system does stutter a few
times a day. It is extremely noticeable when swiping between different scopes and refreshing their content. Complete
crashes of the full system don't really happen. There are mainly crashes when using scopes or apps.

I pretty much like all the Core apps which are in a good shape. As a developer, I see how native Ubuntu Phone
apps should look and behave.

On a few spots you notice that the device or the system is rather slow. If you have taken a few photos and then you open
the gallery app, it takes some seconds to display the overview. The gallery app slowly builds up the thumbnails and the
overview. The same thing happens, when you are viewing the photos. Sometimes you have to wait up to five seconds
to view a single photo.

Besides these issues there are many small other issues which you notice every day. Most of them can be fixed
by upcoming software updates. The phone will get monthly updates. Even tough I already got two system updates
in three weeks. One really annoying bug is the battery bug. The phone consumes a high amount of energy
even if you don't use the phone. Therefore, the battery is empty quickly. The developers are currently working on a
fix. 

## Conclusion

Canonical released the first Ubuntu Phone with bq. The hardware isn't too bad, even if the display doesn't have a
high resolution. You'll get a device for 170€ from bq with a quite good build quality. The device and the system
does have a few bugs, hick-ups and lags.

Anyway, I personally wouldn't recommend this device for end users because there are still many apps and scopes
missing, to have a nice smarpthone operating system. This phone is mainly targeted for early-adopters,
Ubuntu enthusiasts and developers. I hope the ecosystem around Ubuntu Phone will expand quickly. And anyway
I'm really looking forward to the future improvements of Ubuntu Phone! Especially I'm waiting for the Meizu MX4
with Ubuntu pre-installed.
