---
title: WLAN-Karte Ralink 3062 (rt3062) unter Ubuntu zum Laufen bringen
categories: ["Ubuntu"]
date: "2011-06-25T23:00:00+02:00"
toc: true
---

Es gibt immer noch einige WLAN-Karten die nicht Out-of-the-Box funktionieren, so zum Beispiel die WLAN-Karte Ralink 3062 die in meinem PC steckt. Eine Anleitung wie ich diese Karte zum Laufe bringe, fand ich in einem Thread im englischsprachigen ubuntuforums.org Forum.

Zunächst muss man einige Zeilen zu einer Datei hinzufügen. Zuerst öffnet man die Datei mit einem beliebigen Editor mit Root Rechten.

    sudo nano /etc/modprobe.d/blacklist.conf

In dieser Datei muss man die folgenden Zeilen hinzufügen und abspeichern:

    blacklist rt2800pci
    blacklist rt2800lib
    blacklist rt2x00pci
    blacklist rt2x00lib

Für die folgenden Schritte müssen die Pakete <code>build-essential</code> und <code>linux-headers-generic</code> installiert sein.

Jetzt muss man folgendes Paket von der [Ralinktech Webseite](http://www.ralinktech.com/support.php?s=2) herunterladen:

    RT3062PCI/mPCI/CB/PCIe(RT3060/RT3062/RT3562/RT3592)

Nach dem entpacken des Archivs muss man nun noch zwei Werte innerhalb von <code>os/linux/config.mk</code> angepasst werden.

    HAS_WPA_SUPPLICANT=y
    HAS_NATIVE_WPA_SUPPLICANT_SUPPORT=y

Nach dem Anpassen dieser beiden Werte muss man im Terminal in den Unterordner DPO wechseln.
Dort müssen nun nacheinander folgende Befehle ausgeführt werden um die WLAN-Treiber zu kompilieren:

    sudo su
    make
    make install
    echo rt3562sta >> /etc/modules
    exit

Nach einem Neustart des Systems sollte nun die WLAN-Karte funktionsfähig sein.

Englischsprachige Quelle: [ubuntuforums.org](http://ubuntuforums.org/showpost.php?p=10391091&postcount=12)
