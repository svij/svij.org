---
title: "UbuCon Summit and SCALE 14x in Pasadena"
categories: ["Events"]
date: "2016-02-06 22:00:00+02:00"
tags: ["ubuntu-planet"]
toc: true
---

From the 21st to the 24th the 14th Southern California Linux Expo took place
in Pasadena. Since many years, there is also an UbuCon happening. This year
was the first time when the newly created "UbuCon Summit" took place.

<a href="/graphics/ubuconsummit2.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit2.webp" width="400" style="margin:10px;">
</a>

The people who are involved in the international Ubuntu-Community probably heard
about the UbuCon Summit. It is the new attempt by Ubuntus Community Team and
the community to bring the community back together, after the end of the Ubuntu
Developer Summits. The Ubuntu Developer Summits got abandoned in 2012 because it
was too expensive. Personally I didn't have a chance to visit a Ubuntu Developer
Summit, mainly because I wasn't much involved in the international community back
then. I couldn't decline to jump at the chance to attend the UbuCon Summit.

___At this point I want to thank everybody who donated to the Ubuntu Community Donations
program. Without that I wouldn't have the chance to go to the UbuCon Summit, Thanks everyone!___

### Day 0

After a long (and delayed) flight, I arrived in Pasadena. The day "0" was the day
before the UbuCon Summit officially started. It was the first meet and greet
at a wine bar. For me it was the first time to meet a couple of people which I
only knew online before, so I was really happy to finally meet Nathan Haines, Richard
Gaskin, Michael Hall, José Rey, Elizabeth K. Joseph and all the others which I forgot to
mention.

### Day 1

<a href="/graphics/ubuconsummit_opening.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_opening.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_shuttle_keynote1.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_shuttle_keynote1.webp" width="400" style="margin:10px;">
</a>

At 10 o'clock in the morning the UbuCon Summit officially started. Before the
keynote by Mark Shuttleworth started, Nathan Haines and Richard Gaskin as the
main organisers of the event, welcomed everybody in their welcoming speech. The
ballroom was already full at that time, there weren't that many free seats left.
Mark mainly focused Snappy, Containers and the Internet of Things on his keynote,
interestingly the Phone did only get a small reference at the end of the keynote,
when he slightly touched the topic of convergence. After his talk, a few lightning
talks followed. Sergio Schvezov talked about building snap packages with snapcraft,
followed by Jorge Castro about Gaming on Ubuntu. Didier Roche presented the tool
"Ubuntu Make" and was followed by the new Kubuntu lead Scarlett Clark. She was
really shy, so it was just a couple of minutes long talk about the current tasks
of the Kubuntu project. After that the group photo followed, which is not yet
public. (Where is it Nathan? ;)).

<a href="/graphics/ubuconsummit_shuttle_keynote2.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_shuttle_keynote2.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_snappy.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_snappy.webp" width="400" style="margin:10px;">
</a>

After lunch, the actual talks started. Nathan Haines started with "The Future of Ubuntu"
and on the same time Sergio Schvezov and Canonicals Product Manager Manik Taneja
talked about snappy. I've attended the snappy talk, where I didn't learn much new stuff,
because I was already familiar with the basics of snappy. After that, my own
talk followed about the project [Labdoo.org](http://labdoo.org). The project aims
to collect old and used laptops in industrial countries, which are not used anymore
or got replaced by new hardware. This was actually my very fist talk in English,
as a non-native English speaker this was a harder step for me. Anyhow only 15-20
people attended my talk and I was already done after 30 minutes, because I was
too nervous and also spoke too fast so I ended up forgetting half of the stuff…
Beside my not that great performance at my talk, Jono Bacons talk was on the same
time, where all the people went.

After these talks, three more talks followed in two tracks. I didn't follow the
next talks completely, so I had more time to talk to so many other cool people.

At the evening there was the Canonical-sponsored social-event. I had another
priority for that time, so I went to the talk "Floss reflections" by Jon 'maddog'
Hall, Jono Bacon and Keila Banks. It was followed by Bryan Lundukes amusing "Linux Sucks"
talk which you shouldn't miss [seeing on YouTube](http://www.youtube.com/watch?v=WipM3SAYqK4).
Finally, I joined the social event at the Brazilian bar afterwards. It was totally
worth! :)

### Day 2

<a href="/graphics/ubuconsummit_leadership.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_leadership.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_conference.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_conference.webp" width="400" style="margin:10px;">
</a>

The second day was mainly the Unconference-Day. But before that the second keynote
of SCALE took place. Cory Doctorow talked about the topic "No Matter Who's Winning
the War on General Purpose Computing, You're Losing". One interesting slot
followed after the keynote. The "Ubuntu Leadership Panel" discussed many
things, including positive and negative aspects in the last year. The panel
included Daniel Holbach as part of the Community Counil, David Planella as the
Community Team Manager, Olli Ries as the Director of Engineering, Mark Shuttleworth
as the Ubuntu Founder, Elizabeth Joseph as a former Community Council Member,
Nathan Haines as part of the LoCo Council and José Rey as a UbuCon LA organiser.

One of the sponsors of the UbuCon Summit was Dell. Barton George, founder of
"Project Sputnik" - the ubuntu powered Dell XPS 13 Developer Edition - also talked
about the project in a lightning talk. After that the raffle took place, where
I sadly didn't win, boo! A cool thing about Barton George was, that he was very
open and cool to talk to, he even started talking to people (like me) and was
interested what we do in the community. The second lightning talk was by Michael
Hall about Ubuntu Convergence Demo, he even connected his Nexus 4 to the projector
to make the demo. Alan Pope and Jorge Castro followed then by the instructions
about the unconference sessions. There were many sessions which were spontaneously
added to the schedule. The sessions covered topics like "Snappy for Sys-Admins",
"Snap Packaging", "In App Purchases" or "Attracting Non-Ubuntu App Developers".
Sadly the amount of people radically reduced. There were only like 30-40 people
in all unconference session altogether and the bigger part of these were Canonical employees.
The UbuCon Summit ended in the afternoon, after the Unconference-Session.

<a href="/graphics/ubuconsummit_unconference_session.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_unconference_session.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_badvoltage.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_badvoltage.webp" width="400" style="margin:10px;">
</a>

During the Unconference-Session the exhibit hall opened, where many companies
and project had their booth, of course there was also a booth from Canonical and
Ubuntu. They had different ubuntu devices, like laptops, phones and a drone
running snappy Ubuntu, even though it wasn't really snappy but rather slow…

At the afternoon it was time for Bad Voltage Live! It was really cool and not only
because I listen to their podcast regularly. The beginning was delayed because
Jono Bacons Macbook (running Mac OS and Keynote) had issues playing audio. That
was funny, but they somehow got managed to fix it partly. I recommend watching
the recording [on YouTube](https://www.youtube.com/watch?v=cCd2jbfPSPw), specially
the beginning with all the audio issues!

After the show there was another social-event, the after party of Bad Voltage,
which was sponsored by a sponsor, again at the wine bar.

### Day 3

The third day was the first SCALE-only day and it had a huge schedule with
up to ten simultaneously talks. Like the other days, also the third day started
with a keynote, this time again from Mark Shuttleworth. The topic of his talk
was "Free Software in the age of app stores". The talk was pretty similar to his
talk on the opening of the UbuCon, so I would say that there was like 20% new or
other stuff in it.

<a href="/graphics/ubuconsummit_scale.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_scale.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_gamenight.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_gamenight.webp" width="400" style="margin:10px;">
</a>

I've visited a few talks on Saturday, mainly "Continuous Delivery of Infrastructure
with Jenkins", "Building Awesome Communities with GitHub" from Jono Bacon and
"Docker, Kubernetes, and Mesos: Compared". Sadly my unterstanding of the container
technologies aren't that high currently, so I didn't understand most of the talk.
But beside that the other talks were pretty good.

On the other time I was mostly walking around in the exhibit hall, also sometimes
standing and talking with people at the Ubuntu booth. There were a lot of people
at the Ubuntu booth and many were interested in convergence. One guy was so much
excited about the Nexus 4 and the attached screen/keyboard/mouse that he said
"This is so awesome, I would kill for it!". It was kind of strange, but made a good
laugh. ;-)

At the end of the day the "Game Night" took place in a smaller part of the exhibit
hall. The idea and the realisation were great! There were a lot of people playing
different types of games in the hall, like table tennis, pinball, kicker table
or even with Lego. I didn't see many people with their phones in their hands (except
for making photos) and nearly no person who was using their laptop.

### Day 4

The last day started with the keynote of Sarah Sharp about "Improving Diversity
with Maslow's Hierarchy of Needs". This keynote was in fact very interesting and
she mentioned many points which I didn't think of. She started her talk with
the instruction to raise your hand if you're male and then proceed with the second
clause if you're white. So I had to put down my hand again. All these people had
to say "Improving diversity in open source communities is my responsibility." While
I generally agree with that, I was a little confused because I didn't have
to raise my hand because I'm not white. Personally I never had (and never directly heard about)
any issues in open source communities because I'm not white. Beside that point
her talk really impressed many people. She even got standing ovations afterwards.

<a href="/graphics/ubuconsummit_booth.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_booth.webp" width="400" style="margin:10px;">
</a>
<a href="/graphics/ubuconsummit_organisers.webp" data-lightbox="image">
  <img src="/graphics/ubuconsummit_organisers.webp" width="400" style="margin:10px;">
</a>

Next talk for me was "From Sys Admin to Netflix SRE“ from two Netflix guys. This
was a talk again, where I couldn't follow completely because I didn't know much
about the sys admin stuff. The third and last talk for me was from Dustin Kirkland
about "adapt" which was quite interesting.

## Conclusion

Looking back at the event, I've noticed that I skipped too many talk slots. Even though
there were many talks at the same time, so I had to decide which one I should
go to. Compared to German Linux or Open Source Conferences I've noticed that
the (felt) percentage of attending women were higher than in Germany. One cool
point of SCALE is also that they help young and new people to talk at conferences.
I've never seen kids under the age of 15 talking in front of many people yet. Sadly
the video and live-streaming recording weren't that great. The [YouTube](https://www.youtube.com/user/socallinuxexpo/videos)
channel gets all the talks slowly added. My talk is also [available](https://www.youtube.com/watch?v=iUvKom0TNU8)
with broken slides and bad audio quality.

I really enjoyed UbuCon and SCALE. It was the first time to go to a Linux and
Open Source Conference which was not in Germany and it was also the first time
at an international Ubuntu event. At the end I talked to so many people and also skipped
some talks because I was talking to different people. I hope to have the
chance to go to another UbuCon Summit in the next year and hope to see you all
there again! :-)

___Beside the UbuCon Summit the next bigger UbuCon is the UbuCon Europe which takes place in Essen,
Germany from the 18th to 20th November 2016! Me as the organiser of the event
hopes to see you there too!___
