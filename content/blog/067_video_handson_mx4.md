---
title: "Video Hands on Meizu MX4 Ubuntu Edition"
categories: ["Ubuntu"]
tags: ["ubuntuusers-planet"]
date: "2015-06-21 22:15:00+02:00"
toc: true
---

Ich bin nun seit wenigen Tagen im Besitz des „Meizu MX4 Ubuntu Edition“, also
nach dem „bq Aquaris E4.5“ das zweite Ubuntu Phone, welches allerdings zum jetzigen
Zeitpunkt noch nicht käuflich erwerbbar ist.

Heute habe ich ein kurzes Video aufgenommen, in dem ich ein wenig auf die Hardware
und die Besonderheiten des Telefons eingehe. Obwohl ich eigentlich nicht dachte,
dass das Video lang wird, ist es mit fast 7 Minuten doch länger als ich ursprünglich
geplant hatte.

Das Video habe ich auf YouTube hochgeladen und lässt sich [hier ansehen](https://www.youtube.com/watch?v=CRCGdWT952Q).

<iframe width="560" height="315" src="https://www.youtube.com/embed/CRCGdWT952Q" frameborder="0" allowfullscreen></iframe>

Ein ausführlicher Test-Bericht folgt im Laufe der nächsten Wochen.
