---
title: "Ubuntu Touch ausprobiert"
categories: ["Mobile"]
tags: ["Mobile"]
date: "2013-10-27 17:35:00+02:00"
toc: true
---

Die Liste der Betriebssysteme, die unter einer Open-Source-Lizenz stehen und auf
mobilen Endgeräten laufen, ist lang. Ein neues System auf diesem Feld ist 
Ubuntu Touch, welches zur Zeit von Canonical entwickelt wird. Am 17. Oktober
erschien die erste offiziell stabile Version.

<img src="/graphics/touch_lockscreen.webp" width="250">

Ubuntu Touch ist von der Bedienung her grundlegend anders aufgebaut als
Googles Android oder Apples iOS. Das System und dessen Apps lassen sich
hauptsächlich durch verschiedene Gesten bedienen. Zu Beginn ist diese Art
der Bedienung stark gewöhnungsbedürftig. Der Test erfolgte
auf zwei Nexus-4-Smartphones, die freundlicherweise von Canonical
für die diesjährige [Ubucon](http://ubucon.de) zur Verfügung gestellt wurden.

## Bedienung
-------------

<img src="/graphics/touch_change2home.webp" width="250" style="float:right; margin-right:20px;">

Beim allerersten Anschalten eines Ubuntu-Touch-Gerätes erhält man zunächst ein kurze Einführung in die
Bedienung des Systems. Diese kann man überspringen, wenn man möchte,
oder man folgt den Anweisungen auf dem Bildschirm. Dabei wird erklärt,
dass jede der vier Kanten 
unter Ubuntu Touch mit verschiedenen Gesten belegt sind. Beim Wischen von der 
linken Kante her erscheint der App-Launcher. Dieser ist vergleichbar mit dem 
Unity-Launcher des Ubuntu-Desktops, wo es sich ebenfalls an der linken Seite 
des Bildschirms befindet. Unter Ubuntu Touch sind allerdings nicht alle Apps aufgelistet, 
sondern nur eine bestimmte Auswahl. Beim Wisch über die rechte Kante 
können die laufenden Apps einfach gewechselt werden. Falls sich der 
Nutzer auf dem Welcome-Screen (siehe unten) befindet, wird eben dieser zur Seite geschoben. An 
der oberen Kante befindet sich die Indikator-Leiste, über die man einen schnellen 
Zugriff auf verschiedene Dinge bekommt, wie etwa die Konfigurationen von WLAN, 
GPS und Bluetooth. Weiterhin lassen sich dort auch noch Lautstärke, 
Bildschirmhelligkeit und Uhrzeit anzeigen und anpassen. Praktisch ist dabei 
besonders, dass immer die Einstellung dargestellt wird, die man mit dem Finger 
zuvor heruntergezogen hat. Wenn der Nutzer beispielsweise seine empfangenen 
Nachrichten lesen möchte, reicht es, die Wischgeste direkt am entsprechenden 
Indikator beginnen zu lassen. Dann wird auch direkt das ausgewählte Untermenü 
geöffnet. Durch seitliche Wischgesten kann man allerdings auch zwischen den 
einzelnen Indikatoren schnell und einfach wechseln. 

<img src="/graphics/touch_runningApps.webp" width="250" style="float:left; margin-right:20px;">

Die untere Kante besitzt 
entgegen der restlichen Kanten nur hin und wieder eine Funktion. Je nach geöffneter Anwendung befinden 
sich dort Bedienelemente, um einen Schritt zurückzugehen, Einstellungen 
anzupassen oder auch neue Elemente hinzuzufügen. Hierbei haben die App-Entwickler 
freie Hand. Wenn man die Wischgeste bis zur Mitte des Gerätes 
durchzieht, öffnet sich ein halb-transparentes Menü. Dieses beinhaltet ein 
Suchfeld sowie darüber einige Schaltflächen, die zur Zeit allerdings ohne 
weitere Funktionen sind.

Beim Betätigen der Power-Taste erscheint bei einem bereits eingeschalteten 
Ubuntu-Touch-Gerät die Bildschirmsperre. Streng genommen ist dies aber gar kein Sperre, da sie
das System nicht vor fremdem Zugriff schützt. 
Stattdessen nennt man es unter Ubuntu Touch den „Welcome-Screen“. Die Anzeige
besitzt hierbei andere Funktionen als die Bildschirmsperre, wie man sie 
von Android oder iOS kennt.

Es werden drei Dinge angezeigt: die 
aktuelle Uhrzeit, das aktuelle Datum und ein Kreis, der diverse nützliche 
Daten anzeigen soll. Im aktuellen Zustand zeigt dieser nur die Anzahl der am 
selben Tag geschossenen Fotos sowie die Anzahl der am selben Tag getätigten
Anrufe und erhaltene Nachrichten an. Wenn also an einem Tag mit dem Smartphone zehn 
Fotos geschossen wurden und insgesamt drei Anrufe getätigt wurden, dann zeigt Ubuntu
Touch diese Information auf dem 
Welcome-Screen in der Mitte des Kreises an. In Zukunft sollen dort mehr 
Informationen angezeigt werden, etwa die Anzahl erreichten E-Mails oder auch ungelesene Tweets.

Der Welcome-Screen ist allerdings nur wenig interaktiv. Auf dem ersten Blick scheint
es, als wenn die Information innerhalb des Kreises nur beim Anschalten des Displays
erscheint. Mit einem Doppelklick innerhalb des Kreises erscheint allerdings auch die
nächste Information.

## Scopes
-------------

Für Ubuntu Touch gibt es insgesamt vier Scopes, die man auch als „Home-Screens“ 
ansehen kann. Nach dem Wegwischen des Welcome-Screens erscheint der 
„Home-Scope“. In diesem werden zum aktuellen Zeitpunkt einige wenige 
Apps aufgelistet und einige gespeicherte Musik-Stücke im Raster 
dargestellt.

<img src="/graphics/touch_homescope.webp" width="300" style="float:left; margin-right:20px;">

Der Home-Scope befindet sich an der zweiten Position. Links davon 
befindet sich der Music-Scope. Dort werden aktuell zwei Dinge jeweils einzeln 
gruppiert: die gespeicherten Songs mitsamt eines Covers sowie populäre Songs 
aus dem Internet. Das gleiche gilt auch für den Video-Scope der sich an der 
vierten und somit ganz rechten Position befindet. An der dritten Position 
befinden sich der Application-Scope. Sofern Apps zuvor geöffnet worden sind, 
werden diese dort aufgelistet. Weiterhin werden dort im Raster die installierten 
Apps, sowie „weitere App-Vorschläge“ aufgelistet. Bei letzterem lassen sich neue 
Apps installieren. Ganz unten befindet sich noch eine Vielzahl an „Dash Plugins“, die 
man aktivieren bzw. deaktivieren kann.
Als Beispiel seien hier die Dash Plugins „Commands“, „Applications“, 
„Click Packages“ genannt. Das Commands-Plugin bezieht Suchergebnisse aus Binärdateien
von installierten Anwendungen in der Unity-Suche mit ein. Ebenfalls zusammenhängend 
mit der Unity-Suche ist das Applications-Plugin, welches installierte 
Anwendungen über die Suche finden lässt. Wenn man „Click Packages“ deaktiviert, 
lassen sich keine externen Apps installieren.

In einem der [Vorstellungsvideos](http://www.youtube.com/watch?v=cpWHJDLsqTU) von
Ubuntu Touch existiert übrigens eine „People-Scope“. In der aktuellen Fassung ist dieser
allerdings nicht enthalten.

## Core & System Apps
-------------

Auf dem Ubuntu Touch Image sind in der Standardversion bereits einige Apps 
vorinstalliert. Es folgt zunächst ein Überblick über diese Apps und deren Funktionen.

### Browser
-------------

<img src="/graphics/touch_browser.webp" width="200" style="float:left; margin-right:20px;">

Die Browser-App nutzt zum Start der App den vollen Bildschirm aus. Die 
Adress-Leiste ist dabei verborgen und wird erst sichtbar, wenn man von der 
unteren Kante aus wischt. Daraufhin erscheint am unteren Rand die Adressleiste, 
die noch einen zusätzlichen „Activity“- sowie einen „Back“-Button besitzt. Beim 
Klick auf „Activity“ wird die Browser-Historie angezeigt. Diese ist dabei in 
verschiedene Zeitspannen unterteilt. Zusätzlich werden die aktuell geöffneten 
Tabs angezeigt, zwischen denen gewechselt werden kann bzw. ein neuer Tab 
geöffnet werden kann. Die Browser-App unterstützt zusätzlich zu der kurzen 
Wischgeste von der unteren Kante aus noch eine weitere Möglichkeit: Wenn man die 
Wischgeste bis zur Mitte durchführt, erhält man einige weitere 
Bedienmöglichkeiten des Browsers. Darunter die „Vor“- und „Zurück“-Buttons sowie 
einen Reload- und einen Bookmark-Button. Beim Betätigen des Bookmark-Buttons 
wird ein Lesezeichen hinzugefügt, das dann über das oben aufgeführte Menü 
erreichbar ist.

### Kalender
-------------

Die Kalender-App unterstützt verschiedene Darstellungsarten. Standardmäßig 
öffnet sich beim Start der App die Monatsansicht. Um zwischen den Monaten zu 
wechseln, reichen Wischgesten nach rechts bzw. links aus. Über die Titelleiste 
kann man zwischen der Tages-, Wochen-, Monats- und Jahres-Ansicht wechseln. 
Beim Klick auf einen spezifischen Tag in der Monatsansicht, öffnet sich 
automatisch die Tagesansicht. In der Optionsleiste, die wiederum durch die 
Wischgeste von der unteren Kante her aufgerufen wird, gibt es zwei Buttons, die 
genutzt werden können. Zum einen kann ein neuer Termin angelegt werden. Zum 
anderen kann die Ansicht auf den aktuellen Tag gestellt werden.

<img src="/graphics/touch_kalender.webp" width="300" style="float:left; margin-right:20px;">

Wenn man einen neuen Kalender-Eintrag hinzufügen möchte, kann man dem Eintrag 
einen Titel geben, Start- und End-Uhrzeit festlegen sowie einen Ort definieren. 
Zusätzlich lassen sich Personen eintragen, die man trifft. Der Kalender-Eintrag
kann zusätzlich als wiederholenden Eintrag angelegt werden. Nach dem Anlegen 
eines Kalender-Eintrages wird dieser im Kalender dargestellt. In der 
Tagesansicht wird der Start- und Endzeitpunkt klar – mitsamt des 
Titels – dargestellt. Mit einem Klick auf einen Eintrag öffnet sich die 
ausführliche Beschreibung. Der Hintergrund der Termin-Details ist hier
wiederum in weiß gehalten. Die Termin-Beschreibung wird allerdings nicht
dargestellt, sondern nur alle anderen Informationen. Ein angelegter
Termin lässt sich im nachhinein nicht wieder anpassen. Die Optionsleiste 
besitzt in der Terminansicht nur einen Zurück-Button.
Wenn man mehrere zeitlich überschneidende Termine anlegt, dann 
lassen sie sich unter Umständen nicht anklicken, da die Elemente übereinander 
liegen. Hier ist also definitiv noch einiges an Arbeit zu erledigen, um den 
Kalender sinnvoll nutzen zu können. Ebenfalls fehlen Möglichkeiten
zur Synchronisation des Kalenders mit Online-Kalendern, die beispielsweise
[CalDAV](https://de.wikipedia.org/wiki/Caldav). Die oben genannten
Funktionen des Kalenders sind größtenteils erst am Release-Tag in
das Ubuntu Touch Image mit eingeflossen. Davor waren da noch etliche
Platzhalter enthalten.

Der Kalender ist in den verschiedenen Ansichten durchweg in Grün gehalten. Der 
Anblick ist dabei stark gewöhnungsbedürftig, da sich die Farbe optisch nicht ins 
Gesamtsystem einfügt. Einzig beim Dialog-Fenster zum Hinzufügen von Terminen 
begegnen einem die gewohnten Orange-Töne.

### Kamera
-------------

<img src="/graphics/touch_kamera.webp" width="300" style="float:left; margin-right:20px;">

Die Kamera-App besitzt im aktuellen Stand nur einige wenige, aber gängige 
Funktionen. Der Standard-Modus ist (selbstverständlich) die Möglichkeit zur 
Aufnahme von Fotos. Mit einem Klick auf die zu fokussierende Stelle in der 
Vorschau, kann die Fokussierung der Kamera manuell eingestellt werden. Am 
unteren Rand befinden sich einige Buttons sowie ein Regler zum Zoomen. Der große 
mittlere Knopf ist zum Schießen von Fotos gedacht. Im Button links daneben lässt 
sich zwischen dem Foto- und Videomodus umschalten. Am linken Rand befindet sich 
zusätzlich die Option, den Kamera-Blitz an- bzw. auszuschalten oder ihn 
automatisch auslösen zu lassen. Rechts neben dem Kamera-Auslöser ist der Button 
zum Wechseln zwischen der Front- und Rückkamera platziert, welches problemlos 
und schnell vonstatten geht. Der Button am rechten Rand führt zur Galerie-App.
In den Images vor dem Release enthielt das Menü, welches über die
ie Wischgeste zur Mitte des Bildschirms auslösbar ist, die gleichen 
Bedienelemente in Listenform. In der finalen Veröffentlichung ist dies
anscheinend wieder entfernt worden.

Im Moment funktioniert auch der Video-Modus nicht. Der Video-Kamera-Button
innerhalb der App ist schlicht ausgegraut, da diese Funktionen
nicht rechtzeitig fertig geworden ist.

### Uhr
-------------

Die Uhr-App zeigt – wer hätte es gedacht – die aktuelle Uhrzeit an. Sie wird in 
Form einer Mischung aus einer analogen und digitalen Uhr dargestellt. Unterhalb 
dieser Uhr wird der aktuelle Ort und dessen Uhrzeit zusätzlich in digitaler Form 
angezeigt. Unter „Welt“ werden zusätzlich andere Städte und ihre Uhrzeiten 
aufgelistet. Darüber hinaus kann man die Städte über die Optionsleiste 
hinzufügen.

Über die Titelleiste erreicht man weitere Funktionen der Uhr-App, 
etwa die Alarm-Funktion. Dort lassen sich entweder neu einzelne einstellen 
oder aber auch sich täglich oder wöchentlich wiederholende Alarme festlegen. Beim 
wöchentlichen Alarm lassen sich speziell einzelne Tage aktivieren und 
deaktivieren, sodass man dort beispielsweise nur die Arbeitstage auswählen 
kann. Die Alarme lassen sich allerdings nicht speichern und somit lässt sicht
kein Alarm auslösen.

Die Timer-Funktion kann individuell genutzt werden. Wenn man spontan nur 
einen Timer starten möchte, reicht es den Uhrzeiger des Kreises mit dem Finger 
zu drehen, um die Uhrzeit einzustellen. Das Verhalten des Timers ist am Anfang 
etwas verwirrend, da der Uhrzeiger sich nur bewegt, wenn man ihn direkt anfasst. 
Die Genauigkeit ist hierbei auf die Minute beschränkt. Eine sekundengenaue Definition 
der Zeit ist nicht möglich. Praktisch ist hingegen die Speicherung von 
Voreinstellungen, die im Anschluss unterhalb des Timers angezeigt werden. 
So kann man häufig genutzte Timer-Zeiten schon vorher erstellen, ohne sie jedes Mal neu konfigurieren zu müssen.

Das letzte Feature der Uhr-App ist eine Stoppuhr. Durch einen Klick in die Mitte 
des Kreises kann dort die Stoppuhr gestartet werden. Zusätzlich gibt es die 
„Runden“-Funktion, um beispielsweise die einzelnen Bahnzeiten über eine Laufstrecke zu stoppen.

### Kontakte
-------------

<img src="/graphics/touch_contacts.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_contacts_panel.webp" width="250" style="float:left; margin-right:20px;">

Die Kontakte-App zeigt die gespeicherten Kontakte in alphabetischer 
Reihenfolge an. Die Sortierung geschieht anhand der Vornamen. In 
der Liste werden nur die Anfangsbuchstaben als Überschrift angezeigt, für die auch 
Kontakte existieren. Mit dem Klick auf einen der Buchstaben 
lassen sich die Kontakte-Listen ausblenden, sodass man schnell den/die Gesuchte 
findet, auch wenn viele Einträge vorhanden sind. Zu einem Kontakt kann man 
dabei Informationen wie etwa Name, mehrere 
Telefonnummern und auch Adressen hinzufügen. Zudem lassen sich die Daten für 
diverse Instant-Messenger-Dienste hinzufügen, ebenso wie ein Bild des Kontakts. 
Bei zu langen Namen hat die App allerdings noch Probleme, sodass die Namen 
einfach abgeschnitten werden und so Teile unter Umständen fehlen
können.

### Telefon-App
-------------

Zu jedem Smartphone gehört eine Telefon-App. Für die einen ist es eine 
nützliche App, für die anderen eventuell eher weniger. Entweder kann man die
Telefonnummer einer Person eintippen oder man greift über die Titelleiste auf
die gespeicherten Kontakte zu. Beim Eintippen der Telefonnummer werden dabei
automatisch gespeicherte Kontakte vorgeschlagen. Ebenfalls enthalten ist die 
obligatorische Anrufliste.

-------------
<img src="/graphics/touch_incomingCall.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_callLog.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_anruf.webp" width="250" style="float:left; margin-right:20px;">

-----------
Das Telefonieren funktioniert problemlos. Während des Telefonats kann man auch
den Lautsprecher aktivieren und deaktivieren. Das gleiche gilt für
das Mikrofon. Bei einem eingehenden Anruf erscheint eine Benachrichtigung
als kleines Overlay am oberen Rand des Displays. Sofern der Anrufer als Kontakt
gespeichert ist, wird dort neben der Rufnummer auch dessen Name angezeigt.
Man kann dabei wie gewohnt den Anruf annehmen oder ablehnen. [USSD-Codes](https://de.wikipedia.org/wiki/USSD-Codes)
funktionieren aktuell allerdings noch nicht. Wenn man beispielsweise sein Guthaben – etwa mit
*100# (variiert je nach Netzbetreiber) – abrufen möchte, erkennt
die Telefon-App dies noch nicht und führt einen normalen Anruf durch. Beim 
Telefonieren wird der Annäherungs-Sensor des Smartphones genutzt, um den 
Bildschirm auszuschalten, während man das Gerät an das Ohr hält. Dies 
funktioniert soweit gut, allerdings flackert das Display, sobald man es während
des Telefonats nicht mehr an das Ohr hält.

Die Anrufliste sortiert die eingegangenen und ausgegangen Anrufe chronologisch
abwärts pro Tag. In dieser Übersicht werden sowohl die Uhrzeiten, als auch Name und Profilbild
des Kontakts aufgelistet. 

### Friends
-------------

<img src="/graphics/touch_friends.webp" width="250" style="float:left; margin-right:20px;">

Die Friends-App ist eine kombinierte Twitter- und Facebook-App. Sie dient also
zum Abrufen von Statusmeldungen auf den beiden Plattformen. Beim ersten Start 
der App wird zunächst gemeldet, dass man ein oder mehrere Online-Konten in den 
Systemeinstellungen hinzufügen soll. Dort lassen sich aktuell Facebook, Twitter, 
Google und Ubuntu One hinzufügen. Über die App ist es nicht nur möglich, die Tweets 
und Facebook-Statusmeldungen abzurufen, sondern selbstverständlich auch Posts bzw. 
Tweets zu tätigen. Dabei kann man auswählen, ob man auf beiden Plattformen posten 
möchte oder nur auf einer. Die Postings dürfen dabei nicht länger als 140 Zeichen sein,
weil die Texte sonst zu lang für Twitter wären. Zudem ist die App bei den
Konfigurationsmöglichkeiten ziemlich stark eingeschränkt – sie existieren 
schlicht nicht. Auch das Abrufen der Statusmeldungen funktioniert nur bedingt,
da zum Teil etliche Tweets fehlen. Twitter-Mentions, 
also wenn in Tweets der eigene Twitter-Name erwähnt wird, werden als Overlay auch 
außerhalb der App als Benachrichtigung angezeigt. Nach einigen Sekunden 
verschwindet diese Meldung allerdings wieder, sodass man nicht wirklich schnell
darauf zugreifen kann. Ferner werden diese Benachrichtigungen nirgends in der
App angezeigt.

### Galerie
-------------

Um geschossene Fotos anzuzeigen, gibt es die Galerie-App. Dabei existieren drei 
verschiedene Foto-Übersichts-Modi: „Events“, „Photos“ und „Albums“. 
Standardmäßig wird Events geöffnet, in dem die Fotos tageweise sortiert sind. Es 
werden die zuletzt aufgenommenen Fotos des Tages vorne angezeigt. Die Anzahl 
der dargestellten Fotos ist unterschiedlich, meist werden zwischen drei und 
fünf Fotos gezeigt. Mit einem seitlichen Wisch ist es möglich, die 
Vorschaubilder der einzelnen Fotos eines Tages zu überblicken. Mit einem Klick
auf ein Bild öffnet es sich dann in der vollständigen Ansicht. Die Fotos können
auch minimal bearbeitet werden, Funktionen wie das Rotieren und Ausschneiden 
sind bereits implementiert. Zusätzlich gibt es eine „Auto enhance“-Funktion,
die das Foto verbessert bzw. verschlimmbessert. Aktuell scheint diese Funktion
ein geschossenes Foto aufzuhellen oder auch abzudunkeln. Bei einigen Fotos ist
es eine Verbesserung, bei anderen wiederum nicht.

---------------
<img src="/graphics/touch_gallery.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_gallery_albums.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_gallery_edit.webp" width="250" style="float:left; margin-right:20px;">
---------------

Nach dem Ausführen der jeweiligen Aktion gibt 
es auch einen „Undo-Button“, um einige Schritte rückgängig zu machen. Alternativ 
kann man das bearbeitete Foto auch komplett in den Originalzustand zurückversetzen. In 
der Menüleiste gibt es zudem die Möglichkeit, das aktuelle Foto einem Foto-Album 
hinzuzufügen. Wenn man zuvor noch kein Album angelegt hat, kann man im gleichen 
Schritt einfach ein neues erzeugen – inklusive eines Titels und Untertitels. Da 
die Alben wie echte klassische Fotoalben aussehen, kann man deren Buchumschlägen verschiedene Farben zuordnen. 
Im Anschluss lassen sich dem Album außerdem weitere Fotos hinzufügen. Das Ganze 
funktioniert für eine Vorabversion bereits ziemlich gut und ist auch sehr nett anzusehen, weil hier zusätzlich 
einige Animationen hinzugefügt wurden. Die Alben kann man über die 
Titelleiste erreichen. Wenn man dann ein Album geöffnet hat, werden zwei Bilder 
gleichzeitig angezeigt und mit einem Wisch nach links oder rechts kann man 
wie in einem echten Buch blättern. Die dritte Ansicht ist die oben erwähnte 
Fotos-Ansicht, in dem die Fotos schlicht in einem Raster dargestellt werden.

### Messaging
-------------

Mit der Messaging-App lassen sich in erster Linie SMS schreiben. Eine MMS-Unterstützung
ist aktuell nicht enthalten. Die Kurznachrichten lassen sich auch problemlos empfangen.
Bei Eingang einer SMS erscheint am oberen Rand des Displays eine Benachrichtigung
mit dem Inhalt der Nachricht. Weiterhin lässt sich die Nachricht im
Messenger-Indicator anzeigen. Dort ist es ebenfalls möglich, direkt zu antworten.
Alternativ kann der Nutzer die Messaging-App starten und von dort aus antworten.

Jede einzelne Nachricht kann man mit einer Wischbewegung in beide Richtungen löschen.
Das gilt auch für alle Nachrichten mit einem Kontakt. Eine Nachfrage, ob
die Nachrichten wirklich gelöscht werden soll, erfolgt nicht. Wenn ein Nutzer
also zwischen den Apps wechseln will und dabei die Geste leicht versetzt durchführt,
kann man versehentlich Nachrichten löschen. Die gleiche Geste ist übrigens
auch bei den Kontakten enthalten, so dass auch dort versehentlich
Kontakte gelöscht werden können.

---------------
<img src="/graphics/touch_MessageNotification.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_MessageIndicator.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_MessageApp.webp" width="250" style="float:left; margin-right:20px;">
---------------

### Musik
-------------

Die Musik-App spielt die auf dem Gerät gespeicherten Musik-Dateien ab. Es gibt 
dazu verschiedene Modi, die genutzt werden können, um zwischen den einzelnen 
Musik-Stücken zu wechseln. Der Start-Modus heißt schlicht „Musik“, in dem sowohl 
die zuletzt abgespielten Stücke gelistet werden, als auch eine Kategorisierung 
in Genres stattfindet. Über die Titelleiste erreicht man zusätzlich die 
Modi „Künstler“, „Alben“, „Songs“ und „Playlists“. Die einzelnen Modi sollten dabei 
selbsterklärend sein. Beim Klick auf einen Song öffnet sich eine Liste der 
Songs, die nacheinander abgespielt werden.

### Notizen
-------------

Eine kleine aber feine App ist die Notiz-App. Die bisher einzige Funktion ist das 
Aufbewahren von kleinen Texten. Das Löschen einer Bemerkung ist nur möglich, 
wenn man jeden einzelnen Buchstaben der ganzen Notiz löscht. Sortiert werden sie 
absteigend vom letzten Bearbeitungszeitpunkt. Sinnvolle Funktionen wie das 
Gruppieren von Notizen oder ein Verschieben in den Papierkorb sind zur Zeit nicht 
implementiert.

### Systemeinstellungen
-------------

Die Systemeinstellungen sind in drei Kategorien unterteilt: Netzwerk, Persönlich 
und System. Unter Netzwerk sind drei Konfigurationen verfügbar: WLAN, Bluetooth 
und Mobilfunknetz. Das WLAN lässt sich einschalten, die Umgebung kann nach neuen Netzen durchsucht werden 
und man kann sich problemlos mit diesen verbinden. Zudem gibt es die Einstellung, dass sich mit bereits 
betretenen Netzwerken wieder automatisch verbunden werden soll. Unter den 
Mobilfunk-Einstellungen lassen sich viele Optionen konfigurieren, etwa die 
Auswahl des Mobilfunk-Providers, die mobile Datennutzung oder auch Daten-Roaming 
im Ausland. In den Bluetooth-Einstellungen lässt sich zur Zeit ein Headset 
verbinden.

---------------
<img src="/graphics/touch_systemsettings.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_systemsettings_accounts.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_systemsettings_background.webp" width="250" style="float:left; margin-right:20px;">
---------------

Unter den persönlichen Einstellungen gibt es vier Punkte: Hintergrund, Sound, 
Sprache & Text sowie Accounts. Unter Hintergrund lässt sich ein Hintergrundbild 
für die Scopes festlegen. Standardmäßig ist das Hintergrundbild ein 
orange-lilaner Farbverlauf in den aktuell typischen Ubuntu-Farben. Beim 
Festlegen eines Hintergrundbildes wird die Galerie geöffnet und man kann ein Foto auswählen.
Unter den Ton-Einstellungen befinden sich aktuell nur zwei verschiedene Punkte, nämlich die Töne für einen eingehenden 
Anruf sowie einer eingehenden SMS. Unter Sprache & Text ist es möglich die 
Systemsprache zu ändern. Der Standard ist US-Englisch, aber es sind weit mehr 
Sprachen verfügbar. Der letzte Punkt unter den persönlichen Einstellungen sind die
Konten, in dem sich Konten von Ubuntu One, Twitter, Facebook und Google 
konfigurieren lassen.

Unter System finden sich vier Punkte: Batterie, Zeit & Datum, Sicherheit & 
Datenschutz sowie Aktualisierungen. Unter Batterie wird die Ladekapazität des 
Akkus in einem Graphen dargestellt. Zudem kann die Bildschirmhelligkeit mit 
Hilfe eines Schiebereglers angepasst werden sowie Funknetzwerke an- und ausgeschaltet werden. Unter 
Zeit & Datum lässt sich die aktuelle Zeitzone sowie die aktuelle Zeit anpassen 
und das aktuelle Datum setzen. Alternativ kann man die Uhrzeit und das Datum 
auch automatisch über das Netzwerk synchronisieren lassen. Unter den 
Datenschutz-Einstellungen ist es möglich, einige Funktionen zu beschneiden. Die 
Statistiken, die auf dem Welcome-Screen erscheinen, können so beispielsweise deaktiviert 
werden, damit Fremde sie nicht lesen können. Die Dash-Suche schlägt in der Standardeinstellung 
sowohl lokal auf dem Gerät als auch im Internet das Eingabewort nach. Dies kann man, wenn gewünscht, auf das Telefon selbst begrenzen. 
Weiterhin kann man anpassen, ob Absturz-Berichte an Canonical gesendet werden sollen oder nicht. Unter den 
Aktualisierungen lassen sich relativ einfach die Systemaktualisierungen suchen 
und aktualisieren. Es kann auch ausgewählt werden, ob die Aktualisierungen 
automatisch installiert werden oder man diese eben manuell durchführt.
Hierbei handelt es sich allerdings ausschließlich um Systemaktualisierungen;
App-Aktualisierungen werden separat gehandhabt.

Der letzte Punkt in den Systemeinstellungen ist „Über das Telefon“. In diesem 
Punkt werden fast nur Informationen zum Gerät dargestellt, wie etwa die 
Seriennummer, die IMEI, die Betriebssystem-Version (hier: Ubuntu 13.10) oder 
auch der letzte Aktualisierungszeitpunkt. Zusätzlich gibt es den Storage-Punkt, 
in dem die Speichernutzung angezeigt wird. Dort wird außerdem der freie 
Speicher angezeigt sowie welche Arten von Dateien den Platz auf dem Gerät 
belegen.

### Terminal
-------------

<img src="/graphics/touch_terminal_htop.webp" width="250" style="float:left; margin-right:20px;">

Zu (fast) jedem Linux-System gehört ein Terminal-Emulator. Auch Ubuntu Touch 
besitzt eine Terminal-App, mit der man Kommandos ausführen kann. Für den 
täglichen Gebrauch von Ubuntu Touch braucht man eigentlich
keine Terminal-App. Ein Otto-Normalverbraucher sollte also 
eigentlich mit der Terminal-App nichts anfangen können. Shell-Liebhaber werden 
vermutlich Gefallen an der Terminal-App gewinnen, da man durchaus diverse 
Vorteile hat, wenn man einige Konsolen-Anwendungen nutzen möchte. Näheres dazu 
folgt im späteren Verlauf des Textes. In den Einstellungen zu der App lässt sich 
sowohl die Textgröße anpassen, als auch das Farbschema konfigurieren.

#### Weitere Apps

Einige der Core Apps werden in diesem Test-Bericht nicht näher behandelt. Es 
gibt nämlich zusätzlich zu den oben genannten, ausführlich
vorgestellten Apps weitere vorinstallierte Apps für das Wetter, für Aktien, 
einen Datei-Manager sowie einen RSS-Reader namens Shorts.

Weiterhin sind vier Apps für Twitter, Facebook, Amazon und Google Mail 
enthalten. Die Apps sind allerdings
nichts anderes als „Wrapper-Apps“, die jeweils die mobilen Seiten des 
jeweiligen Dienstes aufrufen. Problematisch ist dabei, dass einige Apps
eigene Gesten benötigen. In der Twitter-App kann man nämlich die Tweets 
abrufen bzw. aktualisieren, wenn man die Timeline nach unten zieht. Das
funktioniert allerdings unter Ubuntu Touch nicht, da systemweit ein sogenanntes „Bouncing“
aktiv ist. Das heißt, der Inhalt der App wird beim Scrollen nach oben oder
nach unten gezogen, auch wenn kein Inhalt mehr folgt. Beim Loslassen des
Fingers rutscht es wieder an die richtige Stelle. Dieses Feature ist beispielsweise
bei iOS und auf Samsungs Android-Geräten aktiv. Das Problem hierbei ist also,
dass das systemweite Feature die Geste innerhalb der App überschreibt. Hier
wäre es also notwendig, dass die Gesten auch von HTML5-Apps genutzt werden
können, sofern nicht das Ubuntu SDK verwendet wird.

---------------
<img src="/graphics/touch_file_manager.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_weatherapp.webp" width="250" style="float:left; margin-right:20px;">
<img src="/graphics/touch_rss.webp" width="250" style="float:left; margin-right:20px;">
---------------

Vorinstalliert sind zusätzlich zwei Spiele: Sudoku und Dropping Letters. Sudoku 
sollte für die meisten bekannt sein. Beim Spiel Dropping
Letters fallen Buchstaben von oben nach unten. Ziel des Spiels ist es, an 
Hand der Buchstaben Wörter zu bilden, damit die Kästchen verschwinden.
Ist eine der Spalten vollständig mit Buchstaben gefüllt, dann ist das Spiel zu 
Ende.

#### Click Packages

Das Ubuntu Software Center gibt es zur Zeit nicht für Ubuntu Touch. Um Apps zu 
installieren, existiert ein Bereich unterhalb der installierten Apps
im Application-Scope. Dieser wird einfach nur mit „weitere Vorschläge“ betitelt. Dort 
finden sich zusätzlich zu den installierten
Apps noch etwas mehr als 50 Apps. Einige der Apps sind einfache Wrapper über 
mobile Webseiten, andere sind wiederum „echte“ native
Apps, die speziell für Ubuntu Touch entwickelt wurden. Im Rahmen des 
[Ubuntu App Showdown Contests](http://ikhaya.ubuntuusers.de/2013/08/08/ubuntu-app-showdown-contest-startet/)
sind unter anderem drei Apps entstanden: ein Reddit-Client namens „Karma Machine“, eine 
Rezeptverwaltungs-App „Saucy Bacon“ sowie das Spiel „Snake“.

Weiterhin gibt es bislang nur wenige interessante Apps. Zu diesen gehört zum Beispiel [Fahrplan](http://notyetthere.org/?page_id=230),
mit der man Fahrten mit dem öffentlichen Nahverkehr in Europa und Australien
planen kann. Mit TouchWriter lassen sich Texte am Smartphone schreiben, die 
dann zu PDF, ODT oder auch HTML exportiert werden können. Die 
App „Geldliste“ reiht sich in die Reihe der produktiven Apps ein, mit denen man 
seine Ausgaben verwalten kann. Mit Ubuntu Tasks und Simple ToDo sind auch 
Todo-List Manager verfügbar.

#### Was fehlt?

Einige aufmerksame Leser haben vielleicht schon gemerkt, was für eine wichtige 
App fehlt: eine für E-Mails! In den Core Apps gibt es aktuell
keinen E-Mail-Client. Es ist zwar eine Gmail-Web-App vorinstalliert, allerdings 
ist diese weder nativ, noch möchte jeder Google Mail nutzen.
Für das erste Release von Ubuntu Touch wird es somit keine native E-Mail-App 
geben. Vermutlich wird frühestens für Ubuntu 14.04 eine entsprechende
Anwendung erscheinen.

Einige Nutzer dürften zusätzlich vermutlich einen WhatsApp-Client vermissen. 
Dieser Dienst wird  
von vielen Nutzern weltweit genutzt. Das Problem hierbei ist das proprietäre 
Protokoll. [Es gab](http://pmeyhoefer.de/blog/whatsapp-fur-ubuntu-und-ubuntu-phone-bereits-in-der-entwicklung/)
zwar [bereits einen Ansatz](https://launchpad.net/whosthere), 
einen WhatsApp-kompatiblen Client für Ubuntu Touch bereitzustellen, aber die 
Arbeiten sind bereits wieder zum Erliegen gekommen. Grund war, dass dem Autor 
die Zeit zum Entwickeln fehlte, nachdem WhatsApp das Protokoll verändert hatte. 
Hier wäre natürlich das Angebot einer App durch die Firma hinter WhatsApp ideal, 
was zur Zeit noch nicht in Sicht ist.

## Unter der Haube
-------------

Mit dem ersten stabilen Release von Ubuntu Touch 13.10 Saucy Salamander ist 
das System für offiziell zwei Geräte verfügbar. Canonical unterstützt neben dem 
[Google Nexus 4](http://www.google.de/nexus/4/) noch das [Galaxy Nexus](https://de.wikipedia.org/wiki/Galaxy_Nexus).
Weiterhin werden noch zwei weitere Builds für Geräte zur Verfügung 
gestellt, die aber explizit nicht zum Release von Saucy Salamander gehören 
und somit nicht zum Stable-Zweig gehören. Dies wäre zum einen das 
[Nexus 7 (2012)](https://de.wikipedia.org/wiki/Nexus_7) sowie das [Nexus 10](https://de.wikipedia.org/wiki/Nexus_10).
Für das erste Release von Ubuntu Touch wurde ein großer Wert auf die Smartphones 
gesetzt und somit weniger auf die Tablets, weshalb für die beiden Tablets kein 
stabiles Release erfolgte.

Neben den oben genannten Geräten gibt es noch weitere Images, die auch auf 
anderen Geräten laufen. Diese werden allerdings nicht offiziell von Canonical 
unterstützt, sondern sind Arbeiten aus der Community. Die Liste der 
funktionierenden findet sich im [Ubuntu Wiki](https://wiki.ubuntu.com/Touch/Devices).
Unter den Funktionsfähigen sind viele aktuelle High-End-Geräte, aber 
auch Geräte, die ein bis zwei Jahre alt sind. So gibt es zwar Builds für das 
Samsung Galaxy S2 und S3, allerdings nicht für das S4.

Für die Entwicklung auf den vier Nexus-Geräten pflegt das Ubuntu Touch Team eine 
Tabelle in [GoogleDocs](https://docs.google.com/spreadsheet/ccc?key=0ArLs7UPtu-hJdDZDNWliMmV1YUJ3Zk1pQlpDdGp4VFE#gid=0),
welches die aktuelle 
Unterstützung der Hardware auf den [verschiedenen Geräten](https://wiki.ubuntu.com/Touch/Porting)
übersichtlich darstellt. Aktuell wird das Nexus 4 am besten unterstützt; das Galaxy Nexus 
folgt als Nächstes. Bei einigen Sensoren wurde allerdings die Arbeit noch gar 
nicht begonnen, sodass diese nicht funktionieren. Als Beispiel sei hier das 
Gyroskop oder der Kompass genannt.

### „Flipped“ vs. „unflipped“
-----------------------------

Bei den Ubuntu Touch Images wird zwischen „flipped“ und „unflipped“ 
unterschieden. „Unflipped“ Images sind stark Android-basierend. Auf diesen 
Systemen läuft als Hauptbetriebssystem Android. Zunächst startet Android 
komplett und anschließend wird Ubuntu mittels chroot gestartet. Diese 
Variante wurde allerdings nur zu Beginn der Entwicklung angewandt. Im Laufe der 
Zeit wurde dann allerdings ein „flip“ durchgeführt. Das bedeutet, dass nun nicht 
mehr Android komplett bootet und dann Ubuntu gestartet wird, sondern zunächst 
Ubuntu gestartet wird und im Anschluss einige Android spezifischen Tools in 
einem Container gestartet werden. Da hier die Funktionsweise vertauscht bzw. 
„gedreht“ wurde, bekamen die entsprechenden Images den Namenszusatz „flipped“. 
In den Community-Builds sind viele Images weiterhin „unflipped“ wie man aus der 
entsprechenden Tabelle entnehmen kann. Die offiziell unterstützten Geräte von 
Canonical sind schon sehr früh in der Entwicklung von Saucy auf die neue Methode 
umgeschwenkt.

### Display-Server
------------------

Ubuntu Touch nutzt als Display-Server bereits jetzt standardmäßig Mir. Mir wurde 
allerdings erst knapp eine Woche vor dem Release von Saucy zum Standard erhoben. 
Vorher war stets SurfaceFlinger als Display-Server im Einsatz, welches von 
Google entwickelt wird und unter Android zum Einsatz kommt.

### Einfluss von Android
-------------------------

Android hat aktuell immer noch großen Einfluss auf Ubuntu Touch. Sowohl aus 
Kompatibilitätsgründen zu spezifischen Geräten, als auch an der einfacheren 
Umsetzung werden viele Android Services unter Ubuntu Touch ausgeführt. Die 
Services laufen dabei innerhalb eines [LXC-Container](wiki.ubuntuusers.de/LXC)
und sind somit vom 
restlichen System abgeschottet. Der Linux-Kernel wird weitestgehend von Android 
übernommen; dort werden nur wenige Ubuntu-spezifische Änderungen implementiert. 
Ein Beispiel ist die Nutzung von [AppArmor](wiki.ubuntuusers.de/AppArmor).
Zusätzlich wird OpenGL ES 2.0 [HAL](https://de.wikipedia.org/wiki/HAL_(Software))
sowie Treiber von Android übernommen. Weiterhin 
wird ebenfalls der Media HAL übernommen, womit im Wesentlichen die 
Hardware-Video-Dekodierung ermöglicht wird. Zuallerletzt sei auch noch RILD 
erwähnt. Dies ist ein Interface, um die Mobilfunkverbindung zu einem Provider 
aufzubauen und aufrecht zu erhalten. Die Kommunikation zwischen Ubuntu und dem 
Android Linux-Kernel wird mittels Binder, Sockets und [libhybris](https://en.wikipedia.org/wiki/Hybris_(software))
realisiert. Binder ist für die 
Interprozess-Kommunikation zuständig, während libhybris dafür zuständig ist, 
[Bionic-Code](http://en.wikipedia.org/wiki/Bionic_(software)) unter einer gängigen 
Linux-Distribution auszuführen.

## Fazit
----------

Die Veröffentlichung der ersten Version von Ubuntu Touch ist erst wenige Tage 
her. Viele Dinge funktionieren entweder gar nicht oder nur bedingt. Andere Dinge 
sind wiederum gut umgesetzt und funktionieren. In der Bedienführung wurden 
einige sehr interessante Ideen implementiert, womit sich ein Ubuntu Touch Gerät 
schnell und einfach bedienen lassen. Einige dieser Ideen sind allerdings auch 
etwas sehr umständlich; etwa das Bedienen des „Zurück“-Buttons innerhalb von 
Anwendungen, wofür sowohl eine umständlich zu erreichende Geste, als auch ein 
normaler Tastendruck notwendig sind. Je mehr Anwendungen man startet, desto 
häufiger passiert es, dass das System ziemlich träge und langsam wird. Es 
tummeln sich weiterhin noch viele Bugs, die hin und wieder auftreten, etwa ein 
plötzliches Verschwinden der Tastatur. Nach dem Beenden von Apps lässt sich das 
System dann aber wieder ordentlich schnell nutzen. Einige Core & System Apps 
sind noch stark beschnitten, sodass man Ubuntu Touch kaum auf produktiven 
Geräten nutzen kann, wenn man auf mobile Datenverbindung, 
Kalender-Synchronisation oder einer nativen E-Mail-Client angewiesen ist. Man 
darf also noch etwas abwarten, bis man Ubuntu Touch produktiv einsetzen kann. 
Möglicherweise gibt es dann auch schon Geräte, bei denen Ubuntu Touch als System 
standardmäßig ausgeliefert wird. Laut Canonical soll dies bereits Anfang 
nächsten Jahres der Fall sein.
