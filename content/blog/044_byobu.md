---
title: "tmux mit byobu nutzen"
categories: ["Shell"]
date: "2014-12-14 13:20:00+02:00"
tags: ["ubuntuusers-planet"]
toc: true
---

Mit dem Programm tmux ist es möglich laufende Terminal-Programme von der
aktuellen Sitzung zu trennen und wieder anzuhängen. Die darin laufenden
Programme laufen im Hintergrund weiter. Byobu erweitert einige Funktionen
von tmux und erleichtert den Einsatz und den Umgang mit tmux.

Das Programm tmux wurde in der [Mai-Ausgabe 2014](http://www.freiesmagazin.de/20140504-maiausgabe-erschienen)
bereits ausführlich von Wolfgang Hennerbichler beschrieben. Byobu selbst
ist mehr ein Erweiterungs-Script bzw. ein Hilfsprogramm, welches auf tmux aufbaut.
Byobu bietet unter anderem eine einfach zu konfigurierende Statusbar, wo man
nicht viel Zeit und Aufwand in der Erstellung von Konfigurationsdateien stecken
muss. 

Byobu lässt sich einfach über die Paketverwaltung installieren, sofern es in
den Paketquellen der verwendeten Distribution vorhanden ist. Ubuntu beispielsweise
bietet eine Version in den Paketquellen an.

## Der erste Start

Byobu kann im Terminal mit dem eigenen Namen gestartet werden.

    $ byobu

Nach dem Starten erscheint am unteren Fensterrand eine Status-Leiste, wie man
es von Screen oder tmux gewohnt ist. Im Gegensatz zu eben diesen Programmen
werden in byobu im Standard schon einige Informationen über die laufende Sitzung
und über das System angezeigt. Darunter fällt die etwa die aktuelle Uhrzeit, die
Anzahl der verfügbaren Aktualisierungen, die Uptime oder auch der aktuelle RAM-Verbrauch.

<a href="/graphics/byobu_statusbar.webp" data-lightbox="image" data-title="Die Standard-Statusbar von byobu unter Ubuntu 14.04.">
	<img src="/graphics/byobu_statusbar.webp" style="margin:20px;">
</a>

Byobu lässt sich über zwei Arten steuern. Wenn man etwa neue Sitzungen öffnen
oder zwischen den geöffneten Sitzungen wechseln möchte, dann kann man dies
über die Funktionstasten (F-Tasten) tun. Durch das Drücken der Taste F2 öffnet
sich eine etwa eine neue Sitzung, welches häufig Tab auch genannt. Wenn man hingegen
zwischen den Sitzungen wechseln möchte, kann man mit F3 zur vorherigen Sitzung
nach links oder mit F4 zur nächsten Sitzung nach rechts wechseln.

Die Steuerung von byobu über die Funktionstasten ist nicht die einzige Möglichkeit.
Nutzer von Screen oder tmux sind etwa die Steuerung über Tastenkombination STRG + A 
gewohnt. Sobald man diese Tastatenkombination auf einer frischen byobu Installation
durchführt, öffnet sich eine Abfrage in der Konsole, welches Verhalten man
für die Tastenkombination wünscht. Angeboten werden zwei Varianten, die erste
Variante ist die Nutzung des Screen-Modus, alternativ kann man den Emacs-Modus
verwenden. Der Emacs-Modus ist das Standard Konsolen-Verhalten. In einer normalen
Konsole verschiebt sich der Cursor auf den Anfang der Zeile, beim Drücken von
STRG + A.

## Konfiguration

Wie bereits erwähnt, lassen sich diverse Informationen in der Statusbar darstellen.
Hierfür muss man keine Konfigurationsdateien anpassen. Durch das Drücken der
F9-Taste öffnet sich zunächst das Konfigurationsmenü von byobu.

Der erste Punkt ist die Hilfe, in der alle nötigen und vorhanden Tasten-Kombinationen
aufgeführt und kurz erläutert werden. Darunter fallen sowohl das simple Wechseln
zwischen den Sitzungen, als auch etwas komplexere Dinge wie das gleichzeitige
Anzeigen zweier Sitzungen auf dem Bildschirm.

Im zweiten Menü-Punkt kann man die Statusmeldungen in der unteren Statusbar
bearbeiten. Dort lassen sich über 30 Informationen aktivieren oder deaktivieren.
Je nach Einsatzzweck und persönlichen Vorlieben kann man so die ein oder andere
Option aktivieren. So kann man etwa den Hostname, die IP oder auch Informationen
über die CPU, Festplatte, RAM oder Netzwerk darstellen lassen. 

<a href="/graphics/byobu_statusmeldungen_config.webp" data-lightbox="image" data-title="Im Konfigurationsmenü kann man diverse Informationen an- und ausschalten.">
	<img src="/graphics/byobu_statusmeldungen_config.webp" style="margin:20px;">
</a>

Die dritte Option ist die Änderung der Escape-Folge. Dies ist, wenn man sich
im Screen-Modus befindet, die Tastenkombination STRG + A. Sofern gewünscht, kann
man die Escape-Folge auf eine andere Tastenkombination mappen.

Die letzte Konfigurations-Option ist das automatische Starten von byobu beim Login.
Wenn man diese Funktion aktiviert, muss man hier allerdings beachten, dass dies
in der Regel nur auf das Einloggen in der Konsole ohne graphische Oberfläche bezieht.
Es wirkt sich allerdings auch auf das Einloggen auf einen Rechner per SSH aus.
In beiden Varianten startet beim Einloggen automatisch byobu. Wenn man beispielsweise
auf einem Server byobu installiert hat, bietet es sich ggf. an diese Option zu
aktivieren, da man beim erneuten Verbinden im selben Zustand landet, wie es beim
Schließen der vorherigen Verbindung der Fall war.

## Sonstiges

Byobu lässt sich nicht nur mit tmux nutzen. Wenn man möchte, kann man auch auf
Screen zurückgreifen. Dazu reicht es folgenden Befehl auszuführen:

    $ byobu-select-backend

Anschließend kann man zwischen tmux oder Screen wechseln. Der Standard ist
allerdings tmux.

Es ist weiterhin möglich auf einem System mehrere byobu-Sessions zu Starten.
Bei einem Ausführen von "byobu" im Terminal, öffnet sich, sofern keine Session
vorhanden ist, automatisch eine neue Session. Wenn man allerdings explizit
eine weitere Session starten will, dann ist dies mit dem folgenden Befehl möglich.

    $ byobu new-session

## Fazit

Byobu bietet eine einfache Möglichkeit tmux oder Screen zu nutzen, ohne viel
Aufwand betreiben zu müssen um praktische Informationen in der Statusbar
darstellen zu können. Weiterhin bietet es einen guten Einstieg in die Nutzung
von Terminal-Multiplexer.
