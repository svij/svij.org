---
title: "Warum man keine Fachbücher schreiben sollte …"
categories: ["Books"]
date: 2024-02-11T10:50:00+01:00
tags: ["ubuntuusers-planet"]
vgwort: https://vg07.met.vgwort.de/na/c1845fcd084c4f7ab9ddf20880438509
cover:
  image: "/graphics/meme-writing-books.webp"
---

… und warum und wie ich es trotzdem tat.

Vor wenigen Wochen habe ich mein zweites Fachbuch fertiggestellt. In diesem Blogpost gehe ich ein wenig tiefer ein, was meine Motivation war, wie sich der Fortschritt gestaltet hat und welche Hilfsmittel (nicht) geholfen haben.

Bücher schreiben klingt für viele total toll. Die Realität fühlte sich häufig allerdings das Meme oben an …

## Rückblick

Vor langer Zeit fing ich hier auf meinem Blog eine vierteilige Tutorial-Reihe zu Git an. [Der erste Blogpost](https://svij.org/blog/2014/10/25/git-fur-einsteiger-teil-1/) erschien im Oktober 2014. Das ist jetzt auch schon fast zehn Jahre her. Meine damalige Intention war recht einfach: Ich hatte Lust etwas über das Thema zu schreiben, denn ich bekam sowohl bei der Arbeit, als auch bei der Mitarbeit in Open-Source-Projekten ständig Fragen, wie dieses Git denn nun funktioniert. Da ich keine Lust hatte, es immer wieder zu erklären, schrieb ich es eben einmal ordentlich runter und verlinkte diese Blog-Posts und beantwortete erst später konkretere Fragen.

Durch eine Verkettung von Umständen kam es dann dazu, dass ich die Möglichkeit hatte, ein Buch über Git zu schreiben. Diese vier Artikel dienten als Basis für das Buch, wo ich natürlich dann sehr sehr viel erweitert habe. Zwei Jahre später im August 2016, erschien dann [die erste Auflage meines Git-Buches](https://svij.org/blog/2016/08/03/mein-git-buch-beim-mitp-verlag-ist-da/) beim mitp-Verlag. Im Mai 2019 folgte [die zweite Auflage](https://svij.org/blog/2019/05/10/zweite-auflage-meines-git-buches-erschienen/), im März 2021 [die dritte](https://svij.org/blog/2021/03/03/dritte-auflage-meines-git-buches-erschienen/) und im März 2022 folgte der [Git Schnelleinstieg](https://svij.org/blog/2022/03/31/neues-buch-git-schnelleinstieg/) als abgespeckte Variante. Über die vergangenen 7,5 Jahre verkauften sich knapp über 4000 Exemplare.
### Die Vor- und Nachteile des Autorendaseins

Ein Buch geschrieben zu haben, hat sowohl diverse Vor- als auch Nachteile. Jedes Mal, wenn mir jemand Anfängerfragen zu Git gestellt hat, folgte von mir ein stark ironisches „Also … ich kann dir da ein Buch empfehlen“. Das war immer wieder zur Aufheiterung lustig. Was ich meistens nicht so geil fand, waren Random Gespräche bei Feiern jeglicher Art. Da wurde man plötzlich von Freunden, Bekannten und Verwandten anderen Personen als Autor vorgestellt … was dazu geführt hat, dass ich ständig wildfremden Leuten, die *nicht* in der IT sind, erklären musste, was denn diese Versionsverwaltung mit Git ist. Meistens sah ich nach zwei oder drei Sätzen schon, wie sie bei meinem Versuch es zu erklären, ausgestiegen sind, aber natürlich empfanden sie es als super und toll! Spaß macht das nicht. Außerdem ist scheinbar „Buch schreiben“ ein Bucketlist-Item für viele Leute. Ich weiß bis heute nicht, warum, aber ich muss auch nicht alles verstehen.

Wer übrigens etwas näher hören will, wie so ein Buch entsteht, darüber habe ich in der [zweiten Folge unseres Podcasts TILpod](https://tilpod.net/episode/til002-ueber-das-fachbuecher-schreiben) von und mit Dirk Deimeke gesprochen.

### Ursprüngliche Motivation & Umsetzung

Zurück zur Motivation und der Umsetzung: Nachdem ich die ursprünglichen Blogposts ohne tieferen Hintergedanken geschrieben habe, war das bei einem Buch natürlich etwas anderes. Die Qualitätsansprüche sind hier natürlich deutlich höher, denn hier bezahlen die Kunden mit Geld. Von diesem Geld blieb allerdings nicht sehr viel in meinem Geldbeutel hängen. Dass man als Autor nicht reich wird, war mir bewusst.

Eine gewisse Reputation und ein „einfacherer“ Einstieg in idealerweise gut bezahlten Job mit guten Arbeitsbedingungen war mein Hauptmotivationstreiber. 2015, als ich mit der Arbeit am Buch begann, war ich gerade erst mit dem Bachelor fertig. Die darauffolgenden Jobwechsel waren beim Hinblick auf Gehalt, Arbeitsbedingungen und Interessantheit der Tätigkeiten alle nicht so das wahre (in verschiedenen Konstellationen).

Long story short: Im April 2020 fing ich meinen Job als [Solutions Architect](https://handbook.gitlab.com/job-families/sales/solutions-architect/) bei [GitLab](https://about.gitlab.com) an. Zu den ausgeschriebenen Jobs bei GitLab hatte ich ohnehin schon seit einigen Jahren einen Blick geworfen und dann hat es auch auf Anhieb geklappt. Ein deutlicher Gehaltssprung war dabei, deutlich bessere Arbeitsbedingungen und auch die Tätigkeit im Pre-Sales, was ich so vorher auch nie gemacht habe, hat super gepasst. Mein Plan ging also einige Jahre später auf: Durch das Git-Buch habe ich mich deutlich tiefer in Git und dem gesamten Software-Delivery-Lifecycle eingearbeitet, sodass ich mich für den Job qualifizieren konnte.

Soweit so gut. Ein Grund für ein komplett neues Buch brauchte ich eigentlich nicht … oder? **Oder?**

## Motivation für das zweite Buch

Tja, ich wäre nicht ich, wenn ich immer wieder ein größeres Projekt angehen würde.

### Fachliche Motivation

Zunächst zur fachlichen Motivation: In tagtäglichen Gesprächen mit diversen (potenziellen) Kunden von GitLab merkte ich primär eins: Viele fokussieren sich viel zu stark auf die eingesetzten Tools – etwa GitLab –, ohne jedoch zu schauen, ob und wie „kaputt“ die Arbeitskultur ist.

GitLab selbst positioniert sich als ganzheitliche [DevOps-Plattform](https://about.gitlab.com/solutions/devops-platform/), womit man einfacher und schneller Projekte mit DevOps-Prinzipien umsetzen kann, in dem besser in einem, statt in vielen, Tools kollaboriert wird. Das ist gut, richtig und wichtig.

Wichtige Voraussetzung ist allerdings, dass man erst die Arbeitskultur anpasst und dann die Prozesse anpasst, gefolgt vom Tooling. Im DevOps-Kontext spricht man nicht umsonst von „[People over Processes over Tools](http://dev2ops.org/2010/02/people-over-process-over-tools/)“. Die besten Tools helfen nicht, wenn die Prozesse beschissen sind. Die Prozesse helfen nicht, wenn die Kolleginnen und Kollegen mit den verschiedenen Rollen (Development, Operations, Security, QA, …) gegeneinander statt miteinander arbeiten. 

Mit diesem Buch möchte ich aufklären, wie es „richtig“ geht. Das beste, tollste, schnellste und effizienteste Auto bringt schließlich auch nicht, wenn man keine Straßen hat … und einen Führerschein auch nicht.

Fachlich war die Motivation also klar: Wenn es um DevOps geht, gibt es großen Nachholbedarf, das sehe ich schließlich bei den ganzen Gesprächen. Ein „echtes“ deutsches Buch zu DevOps gab es auch nicht, sondern nur zwei Übersetzungen aus dem Englischen. Und so reifte die Idee für das zweite Buch.

### Nicht fachliche Motivation

Auf der nicht fachlichen Ebene sah es noch ein wenig anders aus. Hier kommen gleich mehrere Faktoren ins Spiel. Zunächst einmal war das Git-Buch recht technisch. Obwohl ich die Arbeit an dem Buch schon ziemlich herausfordernd fand, gab es hier meist nur eine technische Lösung, was das Schreiben deutlich vereinfacht hat. Bei DevOps ist das Ganze anders. Hier geht es viel um „weiche“ Themen, wie Menschen miteinander arbeiten. Das ist um ein Vielfaches herausfordernder. Das hat mich auch angespornt.

Weiterhin wollte ich dieses Mal bei einem größeren Verlag veröffentlichen. Die Zusammenarbeit mit dem [mitp-Verlag](https://mitp.de) war zwar gut und ich soweit zufrieden, aber im Marketing und bei Verkaufszahlen merkt man dann schon einige Unterschiede. Der Einstieg beim größten deutschen IT-Verlag, dem [Rheinwerk-Verlag](https://www.rheinwerk-verlag.de/), hat mit diesem Buch dann auch funktioniert.

Die damalige Motivation für einen „besseren“ Job hatte ich beim DevOps-Buch hingegen nicht. Finanzielles spielte demnach weder primär noch sekundär eine Rolle. Ich bin trotzdem gespannt, welche neue Türe sich hierdurch gegebenenfalls öffnen.

Meine grundlegende Motivation liest sich bis hierhin eigentlich gut, aber die Motivation muss schließlich nicht nur beim Start vorhanden sein, sondern auch während man das Buch schreibt …

## Der Fortschritt …

Die Motivation kontinuierlich und mit (höchster) Disziplin am Buch zu arbeiten, klang zu Beginn viel einfacher als es am Ende dann war. Um es kurz zu sagen: Es war ein Kampf. Und das war überhaupt nicht geil.

### Der Unterschied zu damals™

Im Vergleich zum Git-Buch hatte ich ein Aspekt komplett vernachlässigt. Mein Job damals war deutlich weniger anstrengend. Damals war ich rein in der Technik und ohne Reisetätigkeiten. Das lässt sich viel einfacher steuern, als mein jetziger Job. Bei GitLab hatte und habe ich jeden Tag etliche Videocalls mit Kunden. Bei diesen Calls muss ich stets fokussiert und effizient sein, was für den Kopf *deutlich* anstrengender ist. Zudem machte mir der Job auch noch deutlich mehr Spaß, ich hatte auch noch (ab Mai 2022) etliche Dienstreisen, die einen regelten Tagesablauf nicht ermöglicht haben. Am Ende des Arbeitstages und auch am Wochenende war so nicht mehr so viel Gehirnkapazität übrig, um stetigen Fortschritt zu gewährleisten.

Das „weiche“ Thema führte auch dazu, dass ich immer wieder überlegen musste, wie ich diverse Themen angehen sollte, damit man daraus auch etwas Sinnvolles herausziehen kann. Das führte immer wieder zu Verzögerungen, da ich dann noch die Gedanken schweifen lassen oder mit der ein oder der anderen Person aus dem Arbeitsumfeld besprechen musste, um mir neue Eindrücke abzuholen. Gleichzeitig hing die Frist immer im Nacken …

### Produktivitätstechniken

Während des Schreibens probierte ich auch immer wieder ein paar Produktivitätstechniken aus. Je nach Phase des Buches war das auch relativ gut und hilfreich.

Zu Beginn, nachdem die Grobstruktur gestanden hatte, fokussierte ich mich zunächst um Stichpunkte. Ich tippte also, was mir in den Kopf kam zu den einzelnen Themen in Stichpunkten herunter, und kümmerte mich kaum um die Grammatik. Das war wichtig, um überhaupt mal die Gedanken strukturieren zu können. Dabei fielen mir noch etliche Themen ein, die ich vorher gar nicht betrachtet hatte … die dann zu neuen Kapiteln führten.

Ich setzte hierfür auf die [Pomodoro-Technik](https://de.wikipedia.org/wiki/Pomodoro-Technik): Ich versuchte jeden Tag mindestens 25min, ohne Ablenkungen, Stichpunkte herunterzuschreiben, gefolgt von einer 5min Pause. Danach habe ich weiter gemacht … oder häufig auch nicht. Das war auch völlig in Ordnung.

Das Ganze funktionierte so lange, bis es nicht mehr funktionierte. Irgendwann hatte ich dann alles notiert und es ging mehr Zeit drauf, zu überlegen, was man noch vergessen hat. Gerade für diese Phase benötigte ich viel Ruhe und Zeit, die durch Real-Life und Vollzeitjob nicht sonderlich gegeben war.

Es gab also immer wieder Phasen – teilweise einige Monate – wo ich wenig bis gar nicht am Buch gearbeitet habe. Dabei war das offene Großprojekt, das Wissen, noch etwas erledigen zu müssen, die Frist und was sonst noch dazu gehört, stets im Hinterkopf. Schön war das zugegeben nicht wirklich.

Später, beim Ausformulieren der Stichpunkte, habe ich mir hingegen ein Output-Ziel gesetzt. An Arbeitstagen wollte ich für zwei zusätzliche Seiten sorgen, an arbeitsfreien Tagen hingegen fünf. Da dann primär viel ausformuliert werden musste, klappte das für eine Weile auch einigermaßen gut.

### Öffentlicher Statusbericht

Gleichzeitig habe ich meinen eigenen Podcast [TILpod](https://tilpod.net/), den ich mit Dirk Deimeke betreibe, vorangetrieben. Dort erzählte ich in jeder Folge, wie der Fortschritt am Buch ist. Treue Hörer und Hörerinnen haben also stets mitbekommen, wie ich vorankomme … oder auch nicht.

Das öffentliche Dokumentieren des Fortschritts hat hingegen nicht zu mehr Druck geführt, zumindest nicht bewusst. Einige lustige Gegebenheiten haben sich hingegen schon dadurch ergeben. Vergangenes Jahr war ich dienstlich bei der KubeCon in Amsterdam, wo ich Standdienst bei GitLab gemacht habe. Es kamen erstaunlich viele mir unbekannte Personen auf mich zu und fragten, ob mein Buch denn nun fertig ist. Auch eine spannende Form, auf Hörer vom Podcast zu treffen.

Ursprünglich war mein Plan, bis Ende 2022 fertig zu werden. Dass das nicht zu halten war, war mir schon zu Beginn klar. Unterschrieben habe ich den Autorenvertrag übrigens im Januar 2022. Erst im Sommer 2023, nachdem ich mehrfach die Fertigstellung nach hinten schieben musste, war der erste Entwurf fertig. Und dann ging erst die Arbeit mit diversen Korrekturläufen, dann zusammen mit meinem Lektor, los.

Mein Ziel es dann noch im Jahr 2023 fertigzustellen hat dann nicht auch nicht mehr geklappt. Nachdem ich selbst für so viel Verspätung gesorgt hatte, lag es dann auch noch eine Weile bei meinem Lektor auf Halde: Mein Buch ist schließlich nicht das Einzige, meins musste entsprechend warten.

Der Spätsommer und Herbst war dann ebenfalls ziemlich frustrierend. Jeden Monat verschob sich die Fertigstellung um einen Monat. Jedes Mal, wenn ich dachte, dass ich jetzt wirklich mal fertig wurde, mussten wir es abermals um einen Monat verschieben. Insgesamt lagen zwischen der Unterzeichnung des Autorenvertrags und der Fertigstellung ziemlich genau zwei Jahre.

### (Keine) Hilfsmittel

Ganz unabhängig von der Motivation und der Produktivität finde ich es auch noch spannend auf mögliche Hilfsmittel zu werfen. Insbesondere ChatGPT, LanguageTool und Obsidian.

#### ChatGPT 

Während des Schreibens des Buches ploppte dann auch noch ChatGPT auf und der AI-Hype ging so richtig los. An vielen Stellen las ich dann, dass man ja jetzt „ganz einfach“ Texte schreiben lassen kann und bald nur noch AI-generierte Bücher gibt.

Ich dachte dann: Ja gut, ich habe hier ja ein Beispiel, schauen wir mal, ob ChatGPT hier überhaupt realistisch helfen kann. Tja, nun … tat es nicht wirklich.

Ich hatte natürlich nicht vor, mein Buch per ChatGPT schreiben zu lassen. Das funktioniert ohnehin nicht. Die Frage, die ich mir stattdessen stellte, ging mehr in die Richtung: Wie kann mir ggf. ChatGPT helfen?

Typische Prompts à la „Schreib mir etwas zu $THEMA im Rahmen von DevOps“ produzierten (natürlich) sehr, sehr oberflächliche Texte, die sich zu 95 % ohnehin immer nur wiederholten. Praktisch war hingegen, ChatGPT zu nutzen, um zu prüfen, ob man etwas vergessen hatte. So ergänzte ich zur „normalen“ Recherche auch ChatGPT, wobei der tatsächliche Nutzen eher gering war, schließlich stand der Großteil und ChatGPT kann man ja bekanntlich auch nicht alles glauben. Häufig kam da auch nur Mist raus.
#### LanguageTool

ChatGPT war also nur wenig hilfreich, anders sah es bei [LanguageTool](https://languagetool.org/) aus, was für sprachliche Korrekturen sehr hilfreich war. Dafür setzte ich auf die Premium-Variante für knapp 60 € pro Jahr.

LanguageTool gab mir *einige* Hinweise für die korrekte Nutzung der deutschen Sprache. Viel zu lange Sätze, Wortwiederholungen, Kommasetzung und noch etliche weitere Hinweise wurden mir dann regelmäßig angezeigt, sodass ich da noch einmal drauf geschaut habe.

Praktisch war zudem das Feature „Sentence rephrasing by A.I.“. Hier konnte man einzelne Sätze aus einigen Paragrafen neu formulieren lassen. Das half mir insbesondere dann, wenn ich an einigen komplexen Sätzen viel zu lange hing, weil diese kaum verständlich waren.
#### Obsidian

DIe meiste Zeit beim Schreiben verbrachte ich im Tool [Obsidian](https://obsidian.md/). Das ist eigentlich mehr als persönliches Knowledge-Base gedacht. Für mich half es hingegen das Buch primär in Markdown herunter zu schreiben, da es mich nicht zu sehr abgelenkt hat. Viele der Features von Obsidian habe ich allerdings nicht verwendet. Einige fehlerhafte Verlinkungen fielen mir dann nur regelmäßig auf. LanguageTool ließ sich zudem als Plug-in einbinden und nutzen.

Zum Ende hin musste ich allerdings sowieso auf Microsoft Word switchen, weil das der Workflow vom Verlag so vorgibt. Bis dahin konnte man es wenigstens gut in Git-Repositorys tracken, sodass ich Anpassungen von Reviewern und Lektor einfach nachvollziehen konnte.
## Fazit

Warum sollte man also eher kein Fachbuch schreiben?

Es ist verdammt anstrengend, es geht richtig viel Zeit drauf und ist häufig auch echt frustrierend. Reich wird man nicht. Ob es sich für mich gelohnt hat, wird sich zeigen. In den letzten zwei Jahren verbrachte ich also sehr viel Zeit mit meinem Vollzeitjob und dem Schreiben des Buches. Viel Zeit drum herum, blieb da nicht. Gesund war das ganze auch nicht sonderlich.

## Das fertige Buch

Wer bis hierhin gelesen hat, wird gemerkt haben, dass das Thema des Buches eine untergeordnete Rolle gespielt hat, dazu folgt noch ein weiterer Artikel. Das DevOps-Buch gibt es überall da, wo es Bücher gibt – insbesondere [direkt beim Verlag](https://www.rheinwerk-verlag.de/devops/). Es erscheint am 5. März 2024. Ich warte zurzeit noch auf meine Belegexemplare und kann es kaum erwarten.
