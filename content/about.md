---
title: "About"
layout: "about"
url: "/about"
summary: "about"
---

## That's me

* **Name:** Sujeevan Vijayakumaran
* **E-Mail:** mail at svij dot org

## Podcast

* **since 2020-11**: [TILpod (🇩🇪)](https://tilpod.net)

## Books

* **2024-12:** [DevOps: A cultural refresh for IT projects! (Rheinwerk Computing, 1st Edition, 🇺🇸)](https://www.sap-press.com/devops_6030/)
* **2024-03:** [DevOps: Wie IT-Projekte mit einem modernen Toolset und der richtigen Kultur gelingen (Rheinwerk Verlag, 1st Edition, 🇩🇪)](https://www.rheinwerk-verlag.de/devops/)
* **2022-03:** [Git Schnelleinstieg (mitp Verlag, 1st Edition, 🇩🇪)](https://www.mitp.de/IT-WEB/Software-Entwicklung/Git-Schnelleinstieg.html)
* **2021-02:** [Versionsverwaltung mit Git (mitp Verlag, 3rd Edition, 🇩🇪)](https://www.mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git.html)
* **2019-05:** Versionsverwaltung mit Git (mitp Verlag, 2nd Edition, 🇩🇪)
* **2016-07:** Versionsverwaltung mit Git (mitp Verlag, 1st Edition, 🇩🇪)

## Professional Experience

* **2024-12 - now:**  Grafana Labs (Solutions Engineer)
* **2020-04 - 2024-10:** GitLab Inc. (Solutions Architect)
* **2018-10 - 2020-04:** SEVEN PRINCIPLES AG (IT Consultant)
* **2017-11 - 2018-09:** credativ GmbH (IT Consultant)
* **2016-07 - 2017-10:** otris software AG (Software Developer, part time)
* **2016-01 - 2016-06:** LocaNet oHG (Software Developer, part time)
* **2013-10 - 2015-12:** otris software AG (Software Developer, full time)
* **2011-08 - 2013-09:** otris software AG (Dualer Student - Software Developer, part time)

## Education

* **2015-10 - 2018-12:** \
    M.Sc. Software and Network Engineering ([Universität Duisburg-Essen](https://www.uni-due.de/))
* **2011-10 - 2015-04:** \
    B.Sc. IT- und Software-Systeme ([IT-Center Dortmund](https://itc-dortmund.de) / [FH Dortmund](https://www.fh-dortmund.de/))

## Open Source

**since 2011: Author and speaker**:

 * speaker at several Open Source Events (see [svij.org/talks](https://svij.org/talks))
 * author of several articles at (now dead) [pro-linux.de](https://www.pro-linux.de/?=0.7.0.1.11182.0.0.0.0.00&q=sujeevan%20vijayakumaran)
 * author of several articles at (now dead) magazine [freiesMagazin.de](http://freiesmagazin.de)

**2010-2017: Ubuntu Community**:
 
 * article author at [ikhaya.ubuntuusers.de](https://ikaya.ubuntuusers.de) (7y)
 * team lead (1y) and project lead (2y) for [ubuntuusers.de](https://ubuntuusers.de)
 * head of organisation team for [ubucon.de 2015](http://ubucon.de)
 * head of organisation team for the very first [ubucon Europe](http://ubucon.eu) 2016
 * covering the Ubuntu booth at several Open Source Events

## Skills (small selection)

* **Languages:** German (native), English
* **Programming Languages:** Python, Shell-Scripting, Java, JavaScript
* **Operating Systems:** Ubuntu, Debian, ArchLinux
* **Tools:** Git, GitLab, Kubernetes, Jenkins, Ansible, Puppet
