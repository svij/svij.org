---
title: "Books"
layout: "books"
url: "/books"
---

I am the author of the books „DevOps“, „Versionsverwaltung mit Git“ and „Git Schnelleinsteig“
which are all books written in German.

The first edition of the Git book came out in 2016, the second edition
followed in year 2019 and the third edition came out in 2021. The
second book is basically the fourth edition, but a shortened and compressed
edition of the previous book with pretty similar content.

The first edition of DevOps book came out in March 2024.

## DevOps (1. Auflage, 2024)

 * Verlag: Rheinwerk
 * 460 Seiten
 * Preis: 39,90€ (Print/E-Book), 44,90€ (Buch & E-Book)
 * **HIER KAUFEN: [rheinwerk-verlag.de](https://www.rheinwerk-verlag.de/devops/), [Amazon](https://amzn.to/3HMlpYM)** (Affiliate-Link)

![Cover DevOps-Buch](/graphics/devops-buch.png)

DevOps bedeutet nicht, dass Entwickler und Admins nun die gleichen Jobs
erledigen.  DevOps bedeutet auch nicht, dass man beim Programmieren tägliche
neue Tools einsetzen muss, es keine geplanten Deployments mehr gibt und
Software nur noch in Containern läuft.  DevOps ist viel größer: Es verspricht
eine neue Kultur der Zusammenarbeit sowie bessere Prozesse und Workflows. So
liefern Sie Änderungen schneller aus und sorgen für kürzere Feedback-Schleifen
zwischen Development und Operations.

In zahlreichen Projekten hat Sujeevan Vijayakumaran gelernt, was in der
Entwicklung und im Betrieb moderner Software gut funktioniert. Mit vielen
Beispielen und Praxistipps zeigt er Ihnen, wie Sie eine moderne und zeitgemäße
Arbeitsumgebung für Ihre IT-Projekte schaffen und die DevOps-Transformation in
Ihrem Team gelingt.

 - Effizientes und effektives Teamwork mit schlanken Prozessen Moderne
 - IT-Infrastrukturen aufbauen und Projekte meistern Source Code Management,
 - Continuous Integration, Deployment & Delivery, Observability,
 - Day-2-Operations


## Git Schnelleinstieg (1. Auflage, 2022)

 * Verlag: mitp
 * 256 Seiten
 * Preis: 14,99 (Print + E-Book)
 * **HIER KAUFEN: [mitp.de](https://www.mitp.de/IT-WEB/Software-Entwicklung/Git-Schnelleinstieg.html), [Amazon](https://amzn.to/3NMoo5c)** (Affiliate-Link)

![Cover Git Schnelleinstieg](/graphics/git-schnelleinstieg.jpg)
    
 * Von grundlegenden Funktionen über die Handhabung von Branches und Remote-Repositories bis zu Tipps und Tricks für (nicht nur) alltägliche Funktionen
 * Auswahl sinnvoller Workflows und Einsatz in Teams
 * Git-Repositories hosten mit GitHub und GitLab

Git ist in der Softwareentwicklung bereits weit verbreitet – sowohl in Firmen
als auch in Open-Source-Projekten. Zum Einstieg lernen Anfänger häufig nur die
wichtigsten Befehle, die schnell nicht mehr ausreichen, vor allem wenn die
ersten Fehler auftreten.

Dieses Buch behandelt einerseits die gängigen Befehle, die Sie beim täglichen
Arbeiten mit Git brauchen. Andererseits geht es dem Autor auch darum, dass Sie
Git als Ganzes verstehen, um es effektiv im Entwicklungsprozess einsetzen zu
können.

Der Fokus des Buchs liegt auf dem praktischen Arbeiten mit Git. Sie lernen
anhand eines Projekts, welche Befehle es gibt, wie diese arbeiten und wie Sie
auftretende Probleme lösen können. Neben alltäglichen Funktionen finden Sie
auch seltener gebrauchte Kommandos, die aber ebenfalls wichtig sind. Dabei legt
der Autor großen Wert auf die Einbindung und Anpassung des
Entwicklungsprozesses.

Im zweiten Teil des Buchs werden fortgeschrittene Themen behandelt. Der
Schwerpunkt liegt auf dem Einsatz von Git in Teams, darunter etwa das Hosten
verteilter Repositories mit GitHub und GitLab. Ein weiteres Kapitel behandelt
die Workflows, die je nach Anzahl der beteiligten Personen, Branches und
Repositories eines Projekts variieren.

Darüber hinaus gibt Ihnen der Autor hilfreiche Informationen für den Umstieg
von Subversion sowie Hinweise für eine langfristig nachvollziehbare
Git-Historie. Ein Kapitel zu häufig gestellten Fragen rundet diesen
Praxiseinstieg ab.

Das Buch richtet sich an Einsteiger, aber auch Leser mit grundlegender
Erfahrung können hier noch viel lernen.

## Versionsverwaltung mit Git (3. Auflage, 2021)

 * Verlag: mitp
 * 328 Seiten
 * Preis: 29,99€ (Print), 25,99€ (E-Book), 34,99€ (Buch & E-Book)
 * **HIER KAUFEN: [mitp.de](https://www.mitp.de/IT-WEB/Software-Entwicklung/Versionsverwaltung-mit-Git.html), [Amazon](https://amzn.to/2O02L7k)** (Affiliate-Link)

![Cover der dritten Auflage des Git-Buches](/graphics/git-buch-v3-cover.jpg)

 * Von grundlegenden Funktionen über die Handhabung von Branches und Remote-Repositorys bis zu Tipps und Tricks für (nicht nur) alltägliche Funktionen
 * Auswahl sinnvoller Workflows und Einsatz für Teams
 * Git-Repositorys hosten mit GitHub und GitLab

Git ist in der Softwareentwicklung bereits weit verbreitet – sowohl in Firmen
als auch in Open-Source-Projekten. Zum Einstieg lernen Anfänger häufig nur die
wichtigsten Befehle, die schnell nicht mehr ausreichen, vor allem wenn die
ersten Fehler auftreten.

Dieses Buch behandelt einerseits die gängigen Befehle, die Sie beim täglichen
Arbeiten mit Git brauchen. Andererseits geht es dem Autor auch darum, dass Sie
Git als Ganzes verstehen, um es effektiv im Entwicklungsprozess einsetzen zu
können.

Der Fokus des Buchs liegt auf dem praktischen Arbeiten mit Git. Sie lernen
anhand eines Projekts, welche Befehle es gibt, wie diese arbeiten und wie Sie
auftretende Probleme lösen können. Neben alltäglichen Funktionen finden Sie
auch seltener gebrauchte Kommandos, die aber ebenfalls wichtig sind. Dabei legt
der Autor großen Wert auf die Einbindung und Anpassung des
Entwicklungsprozesses.

Im zweiten Teil des Buchs werden fortgeschrittene Themen behandelt. Der
Schwerpunkt liegt auf dem Einsatz von Git in Teams. Darunter etwa das Hosten
verteilter Repositories mit GitHub und GitLab. Ein weiteres Kapitel behandelt
die Workflows, die je nach Anzahl der beteiligten Personen, Branches und
Repositories eines Projekts variieren.

Darüber hinaus gibt Ihnen der Autor hilfreiche Informationen für den Umstieg
von Subversion, einen Überblick über grafische Git-Programme, Hinweise für eine
langfristig nachvollziehbare Git-Historie sowie eine Einführung in DevOps,
wofür Git die technische Grundlage bildet. Ein Kapitel zu häufig gestellten
Fragen rundet diesen Praxiseinstieg ab.

Das Buch richtet sich sowohl an blutige Einsteiger als auch an erfahrene Leser.
