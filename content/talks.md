---
title: "Talks"
layout: "talks"
url: "/talks"
summary: "talks"
---

In den letzten Jahren habe ich einige Vorträge gehalten, die sich vor allem
über Git und Ubuntu gedreht haben. Auf dieser dieser Seite finden sich die Talks
inklusive der „Folien“ und der jeweiligen Aufzeichnung des Vortrags.

## Übersicht der Vorträge

 * [Warum DevOps scheitert … (2023, Chemnitzer Linuxtage)](#warum-devops-scheitert-)
 * [Warum man nicht in der IT arbeiten sollte und warum wir es trotzdem tun (2023, Chemitzer Linuxtage)](#warum-man-nicht-in-der-it-arbeiten-sollte-und-warum-wir-es-trotzdem-tun)
 * [Mehr Automatisierung bei weniger Arbeit mit GitLab CI/CD (2020, GitLab Connect-Day)](#mehr-automatisierung-bei-weniger-arbeit-mit-gitlab-cicd)
 * [Feature Flags und CI/CD mit GitLab (2020, FrOSCon)](#feature-flags-und-cicd-mit-gitlab)
 * [Praxiseinstieg in Prometheus (2019, FrOSCon)](#praxiseinstieg-in-prometheus)
 * [Dinge, die man nicht mit Git tun sollte (2019, FrOSCon & GPN19)](#dinge-die-man-nicht-mit-git-tun-sollte)
 * [How To Commit: Nachvollziehbare Git Historien (2019, Chemnitzer Linuxtage)](#how-to-commit-nachvollziehbare-git-historien)
 * [Mitmachen bei Git(Hub)-Projekten! (2018, Chemnitzer Linuxtage)](#mitmachen-bei-git-hub-projekten)
 * [Nicht alltägliche Git-Funktionen (2017, FrOSCon)](#nicht-alltägliche-git-funktionen)
 * [Automation mit Jenkins 2.0 (2016, committerconf)](#automation-mit-jenkins-2-0)
 * [Snappy Ubuntu Core (2015, FrOSCon & OpenRheinRuhr)](#snappy-ubuntu-core)
 * [Ubuntu Phone (2015, FrOSCon & OpenRheinRuhr)](#ubuntu-phone)

## 2023

### Warum DevOps scheitert …

DevOps ist in aller Munde, so gut wie alle hier dürften schon was davon gehört
haben und jeder hat vermutlich auch eine Vorstellung davon, was es ist.

Tatsächlich ranken sich allerdings – leider! – sehr viele Mythen und
Fehlinformationen. Dieser Vortrag räumt damit ein wenig auf. Der Fokus liegt
vor allem auf die Gründe, warum DevOps-Initiativen in Firmen und Organisationen
scheitern und was man dagegen tun kann, damit gemeinsam besser
zusammengearbeitet wird. 

###### Aufzeichnung:
<iframe width="840" height="506" src="https://media.ccc.de/v/clt23-153-warum-devops-scheitert/oembed" frameborder="0" allowfullscreen></iframe>

### Warum man nicht in der IT arbeiten sollte und warum wir es trotzdem tun

Ying und Yang, die helle und die dunkle Seite der Macht, Glück und Unglück. Wie
so vieles im Leben hat auch die Arbeit in den vielfältigen Betätigungsfeldern
in der IT-Branche schlechte wie auch gute Seiten. Man muss nicht verrückt sein,
um hier zu arbeiten. Aber es hilft enorm.

Dirk und Jörg versuchen in ihrem Job zu verstehen, wie Systeme funktionieren
und diese am Laufen zu halten. Sujeevan war lange gar nicht klar, was er
überhaupt machen wollte. Lasst euch überraschen, was er heute so tut. Sie geben
in diesem Vortrag einen Einblick in unterschiedliche Tätigkeitsfelder innerhalb
der IT und teilen einige ihrer Erkenntnisse aus über einem halben Jahrhundert
Erfahrung in dieser Branche.

Gespickt mit ein wenig Humor wird der Weg von der Ausbildung in den Untergang
(also die IT) beschrieben. Es geht um die ständigen Herausforderungen mit
Nutzern, Druckern, Chefs und anderen Problemquellen. Es geht um eine
Berufswelt, in der es nie langweilig wird und in der man manchmal selbst nicht
mehr so genau weiß, ob man jetzt Admin, Entwickler oder Nutzer ist.

###### Aufzeichnung:
<iframe width="840" height="506" src="https://media.ccc.de/v/clt23-156-warum-man-nicht-in-der-it-arbeiten-sollte-und-warum-wir-es-trotzdem-tun/oembed" frameborder="0" allowfullscreen></iframe>

## 2020

### Mehr Automatisierung bei weniger Arbeit mit GitLab CI/CD

In das Schreiben von Pipelines kann man sehr viel Arbeit reinstecken. Wir
zeigen, wie Sie mit und ohne Auto DevOps projektübergreifend mehr aus Ihren
Pipelines herausholen können.

Der Vortrag zeigt auf, wie man mit GitLabs Auto DevOps Funktionen deutlich
an Arbeit sparen kann und was wir uns dabei gedacht haben.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://www.youtube-nocookie.com/embed/R9jqjQYO3o0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Feature Flags und CI/CD mit GitLab

GitLab unterstützt Entwickler nicht nur bei der Versionierung ihrer Projekte,
sondern u.a. auch beim Projekt-Management sowie beim Deployment der einzelnen
Anwendungen. Dieser Vortrag zeigt auf, wie CI/CD mit GitLab durchgeführt werden
kann. Insbesondere wird veranschaulicht, wie über GitLab einzelne Features ein-
und ausgeschaltet werden können, um verschiedene Implementierungen der
Anwendung im produktiven Umfeld einführen und testen zu können.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/froscon2020-2599-feature_flags_und_ci_cd_mit_gitlab/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="77a3e95165f9421f8fa2eda59c5ed172" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

## 2019

### Praxiseinstieg in Prometheus

Prometheus ist ein Monitoring-System, welches zur Realzeit Metriken von
Systemen und Software erfasst und in einer Time-Series Database abspeichert.
Dazu werden Key-Value Paare, bestehend aus einem Metrik-Namen sowie dem
dazugehörigen Wert zu dem Zeitstempel abgespeichert.

Basierend auf diesen Daten lassen sich so Systeme über einen Zeitraum
überwachen und Alarme generieren. Dazu existiert die Query-Language PromQL, die
eigens für Prometheus entwickelt wurde.

Dieser Vortrag gibt einen Praxiseinstieg in Prometheus. Zunächst erfolgt eine
grundsätzliche Erklärung wie Prometheus funktioniert und wie man erste Systeme
zum Erfassen der Metriken hinzufügen kann. Danach folgt auch direkt ein
Einstieg in das Schreiben von eigenen Queries in PromQL und die Einbindung von
Graphen in Grafana. Anschließend wird auch noch erläutert, wie man Skripte
schreibt, um eigene Metriken zu exportieren, die von Prometheus erfasst werden.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/froscon2019-2431-praxiseinstieg_in_prometheus/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="996aa82b23144164a9472c7d7097a39f" data-ratio="1.77966101694915" src="//speakerdeck.com/assets/embed.js"></script>

### Dinge, die man nicht mit Git tun sollte

Ständig versuchen Leute Git richtig zu erklären. So auch ich, schließlich hat
sich Git schon seit einigen Jahren als Standard festgesetzt. Viele nutzen zwar
Git, kennen aber nicht alle Funktionen oder verstehen die Arbeit damit richtig.
Nichtsdestotrotz ist Git nicht für alle Probleme eine Lösung.

Dieser Vortrag stellt einige Ideen vor, die Menschen schon mit Git Repositorys
angestellt haben, die vielleicht nicht ganz so sinnvoll gewesen sind. Es werden
einige Beispiele genannt, die man ausdrücklich nicht machen sollte. Darüber
hinaus wird natürlich auch noch darauf eingegangen, warum solche Aktionen nicht
sinnvoll mit Git umzusetzen sind und was für Alternativen es gibt.

Den Vortrag hielt ich auf der GPN19 unter dem Titel „Dämliche Dinge mit Git Repositorys anstellen“
und auf der FrOSCon unter dem Titel „Dinge, die man nicht mit Git tun sollte“.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/gpn19-4-dmliche-dinge-mit-git-repositorys-anstellen/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="7bdc9f02d28c47d09b4478ff8a1cb81e" data-ratio="1.77966101694915" src="//speakerdeck.com/assets/embed.js"></script>

### How To Commit: Nachvollziehbare Git Historien 

Eine der Hauptfunktionen von Versionsverwaltungsprogrammen wie Git ist das
Nachvollziehen von Änderungen, die man selbst oder die andere in dem Repository
getätigt haben. Diese Funktion ist allerdings nur so gut, wie sie die Committer
umsetzen. Commit-Messages wie »Aktueller Stand«, in denen tausende geänderte
Zeilen vorhanden sind, sind nicht wirklich nachvollziehbar.

Dieser Talk gibt Tipps, Tricks und Hinweise, wie und wozu man gute und
nachvollziehbare (Git-)Commits tätigt. Der Vortrag enthält sowohl schlechte,
mittelmäßige als auch gute Commits, um an praktischen Beispielen zu zeigen: Der
richtige Umgang beim Erstellen von Commits ist essenziell für die
Nachvollziehbarkeit von Git-Historien. 

###### Aufzeichnung:

<video widht="800" height="470" controls>
  <source src="https://chemnitzer.linux-tage.de/2019/media/vortraege/video/196-vijayakumaran.mp4" type="video/mp4">
</video>

###### Slides:

<script async class="speakerdeck-embed" data-id="962f7bede84142c0bff7f000e2e4fc26" data-ratio="1.77966101694915" src="//speakerdeck.com/assets/embed.js"></script>

## 2018

### Mitmachen bei Git(Hub)-Projekten!

Sehr viele Open-Source-Projekte hosten ihre Projekte auf GitHub. Einige
Projekte wie Debian und GNOME unterhalten wiederum ihre eigenen GitLab-Server.
Dieser Vortrag richtet sich an Einsteiger, die bei solchen Projekten mitmachen
wollen, aber noch nicht durch die Begriffe wie Fork, Pull-Requests und
Code-Review durchblicken. Der Fokus liegt dabei auf GitHub und GitLab mit einem
vergleichbaren Workflow. Der Vortrag ist dabei gespickt mit Tipps und Tricks
über den GitHub-Workflow sowie Kommunikations- und Organisationshinweisen für
eigene und fremde Projekte. 

###### Aufzeichnung:

<video widht="840" height="470" controls>
  <source src="https://chemnitzer.linux-tage.de/2018/media/vortraege/video/v1_sa_187_a.mp4" type="video/mp4">
</video>

###### Slides:

<script async class="speakerdeck-embed" data-id="b2a13365e9e54ce3bb5152aaca44841f" data-ratio="1.6" src="//speakerdeck.com/assets/embed.js"></script>

## 2017

### Nicht alltägliche Git-Funktionen

Commiten, Pushen, Mergen kann jeder – aber was gibts darüber hinaus?

Erfahrene Git-Nutzer nutzen hauptsächlich folgende Kommandos: git add, git
commit, git checkout, git merge, git push und pull.

Doch Git bietet noch viel mehr Funktionen die man beim alltäglichen Arbeiten
nicht immer braucht. Trotzdem kann es sehr hilfreich sein diese Funktionen zu
kennen, denn wenn man sie braucht, dann können sie wertvolle Zeit sparen.
Darunter fallen Funktionen wie das Neu-Schreiben der kompletten Historie um
etwa Binärdateien oder Passwörter zu entfernen, kaputte Branches und verloren
gegangene Commits wieder herzustellen oder Spezial-Fälle beim Rebasen.

Der Talk richtet sich an diejenigen, die Git schon kennen und neue nützliche
Kenntnisse in der Nutzung gewinnen wollen.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/froscon2017-1919-nicht_alltagliche_git-funktionen/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="87ea46c8bc1047739fa47f42212ddf33" data-ratio="1.78086956521739" src="//speakerdeck.com/assets/embed.js"></script>

## 2016

### Automation mit Jenkins 2.0

Der Open Source Automation Server Jenkins kann seit der Version 2.0 noch
vielfältiger eingesetzt werden, als dies vorher der Veröffentlichung der
Version 2.0 der Fall war. Zuvor diente es vor allem als Continuous Integration
Server. Quellcode konnte auf einen beliebigen VCS-Server hochgeladen werden,
was Jenkins dann ausgecheckt, gebaut und getestet hat. Dies geschah in der
Regel mit Hilfe von einfachen Bash oder Batch-Skripten mit einer
Befehlskette. Mit Version 2.0 wird das ganze einfacher, flexibler und an
einigen Stellen auch komplexer: Pipelines unterstützen die Entwickler bei der
Umsetzung von Continuous Delivery. Wozu "nur" ein Jenkinsfile in Groovy
geschrieben werden muss.

Der Talk richtet sich sowohl an Einsteiger in Jenkins als auch an möglichen
Umsteigern von anderen Automation Servern wie etwa Travis-CI. Es wird gezeigt,
wie man eine Pipeline definieren kann, wie sie geschrieben werden und wie weit
man das ganze treiben kann, um höchste Flexibilität zu erreichen.

###### Slides:

<script async class="speakerdeck-embed" data-id="d9d6d99cbe204e5891600a80507bffc7" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

## 2015

### Snappy Ubuntu Core

Im Januar 2015 wurde mit „Ubuntu Core“ eine andere Variante von Ubuntu
vorgestellt, die nicht mehr auf Deb-Pakete setzt und sich auf alle möglichen
Geräte richtet.

Snappy Ubuntu Core setzt auf sogenannte Snap-Packages, die nicht gleich den
Deb-Paketen sind. Es ist zudem eine Rolling Release Distribution und kommt mit
dem Paket-Management-Tool namens „Snappy“. Snappy setzt auf eine große Breite
an Geräten: Router, Switches, Kühlschränke, Drohnen und auch Smartphones. Der
Vortrag stellt Snappy Ubuntu vor. Es wird u.a. auf die System-Eigenschaften
eingegangen, sowie das neue Paket-Format und alle weiteren Dinge, die Snappy
Ubuntu Core ausmachen.

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/froscon2015-1523-snappy_ubuntu_core/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="aa6e9cb0d76c45b3bcc262f4543ac94b" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

### Ubuntu Phone

Zwei Jahre nach der Ankündigung erschien Anfang 2015 das erste Ubuntu Phone vom
spanischen Hersteller bq. Doch was verbirgt sich hinter dem System? Wie sieht
die Nutzung aus und wie viel von Ubuntu steckt wirklich in dem
Smartphone-Betriebssystem?

Mittlerweile sind die ersten Smartphones mit Ubuntu Phone als Betriebssystem
erschienen. Ubuntu reit sich hinter den zahlreichen mobilen Betriebssystemen
wie Android, iOS, Firefox OS oder auch Tizen ein. Es bringt eigene, neue Ideen
mit, die bisher noch nicht in dieser Form auf anderen Plattformen zu sehen war.

Der Vortrag beinhaltet die verschiedenen Schichten von Ubuntu auf dem
Smartphone und Tablet: die grundlegende System-Architektur, die Nutzung durch
den Endnutzer und die Besonder- und Eigenheiten. Auch wird ein Blick in die
vergangene, aktuelle und zukünftige Entwicklung geworfen. 

###### Aufzeichnung:

<iframe width="840" height="506" src="https://media.ccc.de/v/froscon2015-1503-ubuntu_phone/oembed" frameborder="0" allowfullscreen></iframe>

###### Slides:

<script async class="speakerdeck-embed" data-id="76cec84cceb5401aa850d5b1ab417987" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
